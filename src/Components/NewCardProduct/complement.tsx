import React from 'react';
import { View, FlatList } from 'react-native';
import { CustomText } from '../CustomTetx';
import { dimensions, colors, stylesText } from '../../theme';
import { formaterPrice } from '../../Utils/formaterPRice';

export default function Complement(props: any) {
  const { localeCode, currecy, data } = props;
  const renderSubItems = ({ item }) => {
    return (
      <View>
        <CustomText
          numberOfLines={2}
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.placeholderText,
            {
              width: dimensions.Width(50),
            },
          ]}>
          {item.quantity} × {item.name}{' '}
          {formaterPrice(item.price / 100, localeCode, currecy)}
        </CustomText>
      </View>
    );
  };

  const renderItems = ({ item }, quantity) => {
    const total = item.price * item.quantity * quantity;
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <CustomText
            light={colors.main}
            dark={colors.main}
            numberOfLines={1}
            style={[stylesText.secondaryTextBold, { marginRight: 7 }]}>
            {item.subItems && item.subItems.length > 0
              ? item.quantity
              : item.quantity > 1
              ? `${item.quantity} ×`
              : '-'}
          </CustomText>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.placeholderText,
              { width: dimensions.Width(50) },
            ]}>
            {item.name}{' '}
            {item.price
              ? `+ ${formaterPrice(total / 100, localeCode, currecy)}`
              : ''}
          </CustomText>
        </View>
        <FlatList
          data={item ? item.subItems : []}
          renderItem={(item: any) => renderSubItems(item)}
          keyExtractor={(item: any) => item._id}
          scrollEnabled={false}
        />
      </View>
    );
  };
  return (
    <FlatList
      data={data ? data.subItems : []}
      renderItem={(item: any) => renderItems(item, data.quantity)}
      keyExtractor={(item: any) => item._id}
      showsVerticalScrollIndicator={false}
      scrollEnabled={false}
    />
  );
}

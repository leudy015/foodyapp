import React, { useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  Image,
  Modal,
  Alert,
  ImageBackground,
} from 'react-native';
import { CustomText } from '../CustomTetx';
import { dimensions, colors, stylesText, image } from '../../theme';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useDynamicValue,
} from 'react-native-dynamic';
import DetailProducto from './DetailsProduct';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import LottieView from 'lottie-react-native';
import { Grayscale } from 'react-native-color-matrix-image-filters';
import { formaterPrice } from '../../Utils/formaterPRice';
import { AddItemsToCadt } from './AddAndRemove';
import * as Animatable from 'react-native-animatable';
import CardActive from './CardAnadida';

export interface Producto {
  datosProducto: any;
  search: string;
  storeID: string;
  localeCode: string;
  currecy: string;
  inCartItem: [];
  setinCartItem?: any;
}
export default function Card(props: Producto) {
  const [visibleModal, setModalVisible] = useState(false);
  const [dataProducto, setdataProducto] = useState(null);
  const [ids, setID] = useState(null);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {
    datosProducto,
    search,
    storeID,
    localeCode,
    currecy,
    inCartItem,
    setinCartItem,
  } = props;

  const Burguer = new DynamicValue(image.NoImagen, image.NoImagenBlack);

  const source = useDynamicValue(Burguer);

  const openModal = (datos: any) => {
    setdataProducto(datos);
    setModalVisible(true);
  };

  const getId = async () => {
    const id = await AsyncStorage.getItem('id');
    setID(id);
  };

  useEffect(() => {
    getId();
  }, [ids]);

  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  const addToCarstItems = async (
    items: any,
    openModals: boolean,
    quantity: number,
  ) => {
    Object.assign(items, {
      quantity: quantity,
      subItems: [],
    });
    const input = {
      id: Number.parseInt(getRandomArbitrary(100, 1000000)),
      userId: ids,
      storeId: storeID,
      productId: items._id,
      addToCart: true,
      items: items,
    };

    if (openModals) {
      openModal(items);
    } else {
      AddItemsToCadt(
        inCartItem,
        setinCartItem,
        items._id,
        input,
        setModalVisible,
        2,
      );
    }
  };

  const _renderItems = ({ item }) => {
    var i = inCartItem.findIndex((x: any) => x.productId === item._id);

    if (i !== -1) {
      Object.assign(item, {
        //@ts-ignore
        cartItems: inCartItem[i]['productId'] === item._id ? inCartItem[i] : {},
      });
    } else {
      Object.assign(item, {
        cartItems: {},
      });
    }

    inCartItem.length === 0
      ? (item.cartItems = {})
      : (item.cartItems = item.cartItems);

    const addToCart = item.cartItems ? item.cartItems.addToCart : false;

    const isOpen = item.isOpen;

    const product = addToCart ? item.cartItems.productId : null;

    const totaly = item.price / 100;

    return (
      <>
        <TouchableOpacity
          onPress={() => {
            if (item.snoozed) {
              Alert.alert('Producto fuera de stock');
            } else {
              openModal(item);
            }
          }}>
          <View style={styles.card}>
            <View>
              {item.imageUrl ? (
                <ImageBackground
                  style={[styles.proIMG]}
                  imageStyle={{ borderRadius: 10 }}
                  resizeMode="cover"
                  source={source}>
                  <Grayscale amount={item.snoozed ? 1 : 0}>
                    <Image
                      source={{ uri: item.imageUrl }}
                      style={styles.proIMG}
                    />
                  </Grayscale>
                </ImageBackground>
              ) : (
                <Image source={source} style={styles.proIMG} />
              )}
            </View>
            <View
              style={{
                marginLeft: 15,
                marginRight: 15,
              }}>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  {
                    width: dimensions.IsIphoneX()
                      ? dimensions.Width(63)
                      : dimensions.Width(60),
                  },
                ]}>
                {item.name}
              </CustomText>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginLeft: -5,
                  }}>
                  {item.new ? (
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#510090', '#4600DA']}
                      style={[styles.tagshipping]}>
                      <CustomText
                        light={colors.white}
                        dark={colors.white}
                        style={stylesText.placeholderText}>
                        Nueva
                      </CustomText>
                    </LinearGradient>
                  ) : null}

                  {item.popular ? (
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={[colors.orange, '#fcbe4a']}
                      style={styles.tagshipping}>
                      <CustomText
                        light={colors.white}
                        dark={colors.white}
                        style={stylesText.placeholderText}>
                        Popular
                      </CustomText>
                    </LinearGradient>
                  ) : null}

                  {item.offert ? (
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={[colors.red, colors.ERROR]}
                      style={[styles.tagshipping]}>
                      <CustomText
                        light={colors.white}
                        dark={colors.white}
                        style={stylesText.placeholderText}>
                        Oferta
                      </CustomText>
                    </LinearGradient>
                  ) : null}
                </View>
              </View>
              <CustomText
                numberOfLines={2}
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    marginTop: 5,
                    paddingBottom: 5,
                    lineHeight: 18,
                    width: dimensions.Width(60),
                  },
                ]}>
                {item.description}
              </CustomText>
              <View
                style={{
                  marginTop: 5,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                  }}>
                  <CustomText
                    numberOfLines={2}
                    light={colors.main}
                    dark={colors.main}
                    style={[stylesText.secondaryTextBold]}>
                    {formaterPrice(totaly, localeCode, currecy)}
                  </CustomText>
                  {item.previous_price ? (
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[
                        stylesText.secondaryText,
                        { textDecorationLine: 'line-through' },
                      ]}>
                      {formaterPrice(
                        item.previous_price / 100,
                        localeCode,
                        currecy,
                      )}
                    </CustomText>
                  ) : null}
                </View>

                <View>
                  <TouchableOpacity
                    style={styles.btn}
                    onPress={() => {
                      if (item.snoozed) {
                        Alert.alert('Producto fuera de stock');
                      } else {
                        addToCarstItems(item, isOpen, 1);
                      }
                    }}>
                    <CustomText
                      numberOfLines={1}
                      light={item.snoozed ? colors.rgb_153 : colors.main}
                      dark={item.snoozed ? colors.rgb_153 : colors.main}
                      style={[
                        stylesText.secondaryTextBold,
                        { paddingBottom: -5 },
                      ]}>
                      Añadir
                    </CustomText>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        {addToCart && product === item._id ? (
          <>
            {inCartItem &&
              inCartItem.map((p, i) => {
                return (
                  <View key={i}>
                    {
                      //@ts-ignore
                      p.items._id === item._id ? (
                        <CardActive
                          //@ts-ignore
                          item={p.items}
                          storeID={storeID}
                          localeCode={localeCode}
                          currecy={currecy}
                          inCartItem={inCartItem}
                          setinCartItem={setinCartItem}
                          //@ts-ignore
                          itemId={p.id}
                          //@ts-ignore
                          quantityProdt={p.items.quantity}
                          addToCart={addToCart}
                          quantity={2}
                        />
                      ) : null
                    }
                  </View>
                );
              })}
          </>
        ) : null}
      </>
    );
  };

  return (
    <View style={styles.container}>
      <>
        <FlatList
          data={datosProducto}
          renderItem={(item: any) => _renderItems(item)}
          keyExtractor={(item: any) => item._id}
          showsVerticalScrollIndicator={false}
          scrollEnabled={false}
          ListEmptyComponent={
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <LottieView
                source={require('../../Assets/Animate/no_order.json')}
                autoPlay
                loop
                style={{ width: 250 }}
              />
              <CustomText
                ligth={colors.rgb_153}
                dark={colors.rgb_153}
                style={[stylesText.secondaryText, { paddingBottom: 5 }]}>
                No hay resultado para {search}
              </CustomText>
            </View>
          }
        />
        {dataProducto ? (
          <Modal
            animationType="slide"
            visible={visibleModal}
            presentationStyle="formSheet"
            collapsable={true}
            statusBarTranslucent={true}
            onRequestClose={() => setModalVisible(false)}>
            <View style={styles.centeredView}>
              <DetailProducto
                datas={dataProducto}
                quantityTotal={1}
                option={[]}
                setModalVisible={setModalVisible}
                localeCode={localeCode}
                currecy={currecy}
                addToCarts={false}
                userId={ids}
                storeId={storeID}
                inCartItem={inCartItem}
                setinCartItem={setinCartItem}
              />
            </View>
          </Modal>
        ) : null}
      </>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.ScreenWidth,
  },
  centeredView: {
    backgroundColor: new DynamicValue(colors.colorInput, 'black'),
    flex: 1,
  },
  card: {
    padding: 0,
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  cardActive: {
    padding: 0,
    paddingTop: 20,
    paddingBottom: 20,
    paddingRight: 20,
    paddingLeft: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderLeftWidth: 7,
    borderLeftColor: colors.main,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  proIMG: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },

  btn: {
    height: 30,
    minWidth: 100,
    width: 'auto',
    paddingHorizontal: 10,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    borderRadius: 5,
  },

  tagshipping: {
    zIndex: 200,
    marginHorizontal: 5,
    marginTop: 10,
    padding: 3,
    width: 60,
    height: 22,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 0,
    borderRadius: 100,
    marginBottom: 5,
    backgroundColor: colors.green,
  },

  sumaResta: {
    width: 140,
    height: 45,
    borderRadius: 7,
    marginLeft: 15,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
  },

  iconos: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  plus: {
    padding: 7,
    borderRadius: 100,
  },
});

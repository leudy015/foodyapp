import React from 'react';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { dimensions, colors, stylesText } from '../theme';
import { CustomText } from './CustomTetx';
import { View } from 'react-native';
import Icon from 'react-native-dynamic-vector-icons';

export default function Safe() {
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.container}>
      <Icon name="Safety" type="AntDesign" size={34} color={colors.main} />
      <View style={{ width: dimensions.Width(70), marginLeft: 10 }}>
        <CustomText
          style={stylesText.secondaryTextBold}
          light={colors.main}
          dark={colors.main}>
          Pago 100% seguro
        </CustomText>
        <CustomText
          style={[stylesText.secondaryText, { lineHeight: 18 }]}
          light={colors.main}
          dark={colors.main}>
          Pagos 100% cifrado y seguro, todas las transacciones en Wilbby son con
          certificado SSL.
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    marginHorizontal: 15,
    marginVertical: 20,
    width: dimensions.Width(94),
    borderRadius: 15,
    padding: 20,
    height: 'auto',
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
    flexDirection: 'row',
  },

  user: {
    flexDirection: 'row',
  },
});

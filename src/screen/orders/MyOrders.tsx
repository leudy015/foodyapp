import React, { useEffect, useState } from 'react';
import {
  View,
  Image,
  FlatList,
  ScrollView,
  Alert,
  RefreshControl,
  TouchableOpacity,
  Platform,
  ImageBackground,
} from 'react-native';
import { mutations, newMutation } from '../../GraphQL';
import { useMutation } from 'react-apollo';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors, image } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Navigation from '../../services/Navigration';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';
import { scheduleTime } from '../../Utils/scheduleTime';
import { Grayscale } from 'react-native-color-matrix-image-filters';
import { formaterPrice } from '../../Utils/formaterPRice';
import moment from 'moment';
import Valoration from './Valoration';
import ProgressBar from 'react-native-animated-progress';

const MyOrders = (props) => {
  const {
    refetch,
    respuesta,
    lat,
    lgn,
    city,
    localeCode,
    currecy,
    riders,
  } = props;
  const [ids, setID] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [rest, setRest] = useState(null);
  const [starCount, setStarCount] = useState(0);
  const [riderVal, setriderVal] = useState(0);
  const [comment, setComment] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [crearValoracion] = useMutation(mutations.CREAR_VALORACIONES);
  const [NewOrdenProceed] = useMutation(newMutation.NEW_ORDER_PROCEED);
  const [actualizarRiders] = useMutation(mutations.ACTUALIZAR_RIDER);

  const OpenModal = (dat: any) => {
    setModalVisible(true);
    setRest(dat);
  };

  const onStarRatingPress = (rating: any) => {
    setStarCount(rating);
  };

  const styles = useDynamicValue(dynamicStyles);

  const onStarRatingPressRiders = (rating: any) => {
    setriderVal(rating);
  };

  const Burguer = new DynamicValue(image.NoImagen, image.NoImagenBlack);

  const source = useDynamicValue(Burguer);

  const valorateRIder = (id, rating) => {
    const input = {
      _id: id,
      rating: rating.concat(riderVal),
    };
    actualizarRiders({ variables: { input: input } });
  };

  useEffect(() => {
    const getId = async () => {
      const id = await AsyncStorage.getItem('id');
      setID(id);
    };
    getId();
  }, [ids]);

  const ordersProcess = (
    orden: string,
    status: string,
    value: number,
    refetch: any,
  ) => {
    NewOrdenProceed({
      variables: {
        ordenId: orden,
        status: status,
        IntegerValue: value,
        statusProcess: {
          status: status,
          date: moment(new Date()).format(),
        },
      },
    })
      .then((res) => {
        if (res.data.NewOrdenProceed.success) {
          refetch();
        } else {
          refetch();
        }
      })
      .catch((e) => console.log(e));
  };

  const isDone = (estado) => {
    switch (estado) {
      case 'Finalizada':
        return true;
      case 'Entregada':
        return false;

      case 'Rechazado':
        return true;

      case 'Rechazada por la tienda':
        return true;

      case 'Resolución':
        return true;

      case 'Devuelto':
        return true;

      default:
        return false;
    }
  };

  const textConfig = {
    light: colors.white,
    dark: colors.back_suave_dark,
  };

  const config = useDynamicValue(textConfig);

  const renderItems = ({ item }) => {
    return (
      <View>
        <View style={{ flexDirection: 'row', marginTop: 5 }}>
          <CustomText
            light={colors.main}
            dark={colors.main}
            numberOfLines={1}
            style={[stylesText.secondaryText, { marginRight: 5 }]}>
            {item.items.quantity} x
          </CustomText>
          <CustomText
            numberOfLines={1}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              { width: dimensions.Width(50), paddingBottom: 3 },
            ]}>
            {item.items.name}
          </CustomText>
        </View>
      </View>
    );
  };

  const _itemsRender = ({ item }, refetch: any) => {
    const schedule = item.storeData.schedule;
    const isOK = () => {
      if (scheduleTime(schedule) && item.storeData.open) {
        return true;
      }
      return false;
    };

    var totals = 0;

    item.items.forEach(function (items: any) {
      const t = items.items.quantity * items.items.price;
      totals += t * items.items.quantity;
      items.items.subItems.forEach((x) => {
        const s = x.quantity * x.price;
        totals += s * items.items.quantity;
        const t = x && x.subItems ? x.subItems : [];
        t.forEach((y) => {
          const u = y.quantity * y.price;
          totals += u * items.items.quantity;
        });
      });
    });

    const datosNavigation = {
      data: item.items,
      time: 'Lo antes posible',
      restaurants: item.storeData,
      user: item.customerData,
      city: city,
      lat: lat,
      lgn: lgn,
      isOK: isOK,
      localeCode: localeCode,
      currecy: currecy,
      takeaway: riders ? false : true,
      subtotal: totals / 100,
      delivery: riders,
    };

    const navegar = () => {
      switch (item.status) {
        case 'Finalizada':
          Navigation.navigate('DestailsOrden', {
            data: { item, refetch, lat, lgn, city, localeCode, currecy },
          });
          break;
        case 'Entregada':
          OpenModal(item);
          break;

        case 'Rechazado':
          Navigation.navigate('DestailsOrden', {
            data: { item, refetch, lat, lgn, city, localeCode, currecy },
          });
          break;

        case 'Rechazada por la tienda':
          Navigation.navigate('DestailsOrden', {
            data: { item, refetch, lat, lgn, city, localeCode, currecy },
          });
          break;

        case 'Resolución':
          Navigation.navigate('DestailsOrden', {
            data: { item, refetch, lat, lgn, city, localeCode, currecy },
          });
          break;

        case 'Devuelto':
          Navigation.navigate('DestailsOrden', {
            data: { item, refetch, lat, lgn, city, localeCode, currecy },
          });
          break;

        default:
          Navigation.navigate('Segumiento', {
            data: { item, refetch, localeCode, currecy },
          });
          break;
      }
    };

    return (
      <TouchableOpacity
        onPress={() => navegar()}
        style={!isDone(item.status) ? styles.itemssActive : styles.itemss}>
        <ImageBackground
          style={[styles.img]}
          imageStyle={{ borderRadius: 10 }}
          resizeMode="cover"
          source={source}>
          <Grayscale amount={isOK() ? 0 : 1}>
            <Image
              source={{
                uri: item.storeData.image,
              }}
              style={[styles.img, { marginLeft: 0 }]}
            />
          </Grayscale>
        </ImageBackground>
        <View>
          <CustomText
            light={colors.black}
            dark={colors.white}
            numberOfLines={1}
            style={[stylesText.titleText2, { width: dimensions.Width(60) }]}>
            {item.storeData.title}
          </CustomText>

          <FlatList
            data={item.items}
            renderItem={(item: any) => renderItems(item)}
            keyExtractor={(item: any) => item._id}
            scrollEnabled={false}
          />

          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {formaterPrice(item.payment / 100, localeCode, currecy)}
            </CustomText>

            <View
              style={{
                marginLeft: 'auto',
                marginRight: dimensions.IsIphoneX() ? 0 : 10,
              }}>
              {item.status !== 'Entregada' &&
              item.status !== 'Finalizada' &&
              item.status !== 'Devuelto' &&
              item.status !== 'Rechazada por la tienda' &&
              item.status !== 'Rechazada por el rider' &&
              item.status !== 'Resolución' ? (
                <View style={{ flex: 1, width: 130, paddingTop: 20 }}>
                  <ProgressBar
                    height={8}
                    indeterminate
                    backgroundColor={colors.main}
                    trackColor={config}
                  />
                </View>
              ) : null}
              {item.status === 'Entregada' ? (
                <TouchableOpacity
                  style={[
                    styles.btnSmal,
                    {
                      backgroundColor: colors.orange,
                    },
                  ]}
                  onPress={() => OpenModal(item)}>
                  <CustomText
                    light={colors.white}
                    dark={colors.white}
                    style={stylesText.placeholderText}>
                    Valorar pedido
                  </CustomText>
                </TouchableOpacity>
              ) : null}

              {isDone(item.status) ? (
                <TouchableOpacity
                  style={[
                    styles.btnSmal,
                    {
                      backgroundColor: colors.main,
                    },
                  ]}
                  onPress={() => {
                    if (isOK()) {
                      Navigation.navigate('Order', {
                        data: datosNavigation,
                      });
                    } else {
                      Alert.alert(
                        'Tienda cerrada',
                        'Este establecimiento está cerrado te avisaremos cuando esté disponible',
                        [
                          {
                            text: 'OK',
                            onPress: () => console.log('calcel'),
                          },
                        ],

                        { cancelable: false },
                      );
                    }
                  }}>
                  <CustomText
                    light={colors.white}
                    dark={colors.white}
                    style={stylesText.placeholderText}>
                    Repetir pedido
                  </CustomText>
                </TouchableOpacity>
              ) : null}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const valorar = async (restaurantes: any, id: any, order: any) => {
    const isOkVal = () => {
      if (order.orderType === 'delivery') {
        if (starCount > 0 && riderVal > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        if (starCount > 0) {
          return true;
        } else {
          return false;
        }
      }
    };
    if (isOkVal()) {
      const input = {
        user: ids,
        comment: comment,
        value: starCount,
        restaurant: restaurantes,
      };

      crearValoracion({ variables: { input: input } })
        .then(async () => {
          setModalVisible(false);
          setStarCount(0);
          setComment(null);
          ordersProcess(id, 'Finalizada', 140, refetch);
        })
        .catch(async (err) => console.log(err));
    } else {
      Alert.alert(
        'Debes elegir una puntuación',
        'Para continuar con la valoración del servicio debes elegir una puntuación',
        [
          {
            text: 'OK',
            onPress: () => console.log('OK'),
          },
        ],
        { cancelable: false },
      );
      return null;
    }
  };

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  return (
    <View style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }>
        <View
          style={{
            marginHorizontal: dimensions.Width(4),
            marginTop: dimensions.Height(1),
            marginBottom: dimensions.Height(2),
            width: dimensions.Width(70),
          }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={stylesText.titleText}>
            Mis pedidos
          </CustomText>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              { marginTop: 10, paddingBottom: 5, lineHeight: 17 },
            ]}>
            Encuentra aquí tus pedidos anteriores y en curso
          </CustomText>
        </View>

        <View
          style={{
            marginBottom: dimensions.Height(10),
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <FlatList
            data={respuesta}
            renderItem={(item: any) => _itemsRender(item, refetch)}
            keyExtractor={(item: any) => item._id}
            showsVerticalScrollIndicator={false}
            scrollEnabled={false}
            ListEmptyComponent={
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <LottieView
                  source={require('../../Assets/Animate/no_order.json')}
                  autoPlay
                  loop
                  style={{ width: 300 }}
                />
                <CustomText
                  ligth={colors.black}
                  dark={colors.white}
                  style={stylesText.secondaryTextBold}>
                  ¡Anímate a hacer tu primer pedido!
                </CustomText>
              </View>
            }
          />
        </View>
        <Valoration
          onStarRatingPressRiders={onStarRatingPressRiders}
          rest={rest}
          valorar={valorar}
          setComment={setComment}
          onStarRatingPress={onStarRatingPress}
          starCount={starCount}
          riderVal={riderVal}
          valorateRIder={valorateRIder}
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
        />
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    height: 'auto',
    minHeight: dimensions.Height(100),
  },

  content__modal: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 6 },
    shadowOpacity: 0.45,
    shadowRadius: 16,
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 15,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    paddingLeft: dimensions.Width(4),
    flexDirection: 'row',
  },

  itemss: {
    width: dimensions.IsIphoneX() ? dimensions.Width(96) : dimensions.Width(94),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    paddingVertical: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 15,
    flexDirection: 'row',
  },

  itemssActive: {
    width: dimensions.IsIphoneX() ? dimensions.Width(96) : dimensions.Width(94),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    paddingVertical: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 15,
    flexDirection: 'row',
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  img: {
    width: 100,
    height: 100,
    borderRadius: 10,
    marginLeft: 15,
    marginRight: dimensions.Height(2),
  },

  constd: {
    flexDirection: 'row',
    height: 130,
  },

  btnSmal: {
    borderRadius: 5,
    marginBottom: 10,
    paddingHorizontal: 15,
    paddingVertical: 10,
    width: 'auto',
  },

  signupButtonContainer: {
    alignSelf: 'center',
    marginRight: 15,
    marginLeft: 'auto',
    marginTop: 25,
  },
  buttonView: {
    width: dimensions.Width(50),
    borderRadius: 100,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  logo: {
    width: 60,
    height: 60,
    marginRight: 15,
    borderRadius: 50,
  },

  logos: {
    width: 80,
    height: 80,
    marginRight: 15,
    borderRadius: 50,
    marginTop: 12,
  },

  selected: {
    width: dimensions.Width(94),
    height: 60,
    borderRadius: 100,
    flexDirection: 'row',
    flex: 1,
    backgroundColor: colors.main,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    marginBottom: dimensions.Height(10),
  },

  input: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    padding: Platform.select({
      android: 10,
      ios: dimensions.IsIphoneX() ? 20 : 10,
    }),
    marginTop: 20,
    marginBottom: 30,
    marginHorizontal: 10,
    borderRadius: 5,
    color: colors.rgb_153,
    borderColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    borderWidth: 0.5,
    flexDirection: 'row',
  },
});

export default MyOrders;

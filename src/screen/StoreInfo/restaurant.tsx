import React, { useState } from 'react';
import {
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Linking,
  Alert,
  Platform,
  SafeAreaView,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { useNavigationParam } from 'react-navigation-hooks';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { image, dimensions, colors } from '../../theme';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../../services/Navigration';
import { customMaspStyles } from '../../Components/MapStyle';
import Horarios from './horario';

const Restaurant = () => {
  const styles = useDynamicValue(dynamicStyles);
  const datas = useNavigationParam('data');
  const stylos = useDynamicValue(customMaspStyles);
  const [line, setline] = useState(5);
  const adress = datas.datos;
  const res = datas.data;

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };
  const mode = useColorSchemeContext();
  const border = backgroundColors[mode];

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.containerIcon}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => Navigation.goBack()}
            style={styles.icon}>
            <Icon
              name="arrow-left"
              type="Feather"
              size={24}
              color={colors.main}
            />
          </TouchableOpacity>
          <View style={{ marginLeft: 10 }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.secondaryTextBold}>
              {res.title}
            </CustomText>
          </View>
        </View>
      </SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <MapView
          showsUserLocation={true}
          provider={PROVIDER_GOOGLE}
          customMapStyle={stylos}
          style={{
            height: dimensions.Height(35),
            width: dimensions.Width(100),
          }}
          region={{
            latitude: Number.parseFloat(adress.lat),
            longitude: Number.parseFloat(adress.lgn),
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
          <Marker
            title={datas.data.title}
            description={`${adress.calle} ${adress.numero}, ${adress.codigoPostal}, ${adress.ciudad} `}
            coordinate={{
              latitude: Number.parseFloat(adress.lat),
              longitude: Number.parseFloat(adress.lgn),
            }}>
            <Image source={image.Maping} style={{ width: 50, height: 50 }} />
          </Marker>
        </MapView>

        <View
          style={[
            styles.items,
            {
              width: dimensions.Width(90),
              marginTop: -100,
              marginBottom: 30,
              borderRadius: 15,
              padding: 10,
              justifyContent: 'space-between',
              alignItems: 'center',
            },
          ]}>
          <View>
            <Image source={{ uri: datas.data.logo }} style={styles.logo} />
          </View>
          <View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {datas.data.title}
            </CustomText>
            <View>
              <CustomText
                numberOfLines={1}
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    marginTop: 2,
                    width: dimensions.Width(55),
                    paddingBottom: 5,
                  },
                ]}>
                {`${adress.calle} ${adress.numero}`}
              </CustomText>
              <CustomText
                numberOfLines={1}
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                {adress.codigoPostal}, {adress.ciudad}
              </CustomText>
            </View>
          </View>
          <View
            style={{
              borderLeftWidth: 0.5,
              borderLeftColor: colors.rgb_235,
              width: 50,
              justifyContent: 'center',
              alignItems: 'center',
              position: 'relative',
            }}>
            <TouchableOpacity
              onPress={() =>
                OpenURLButton(
                  `https://www.google.com/maps/search/?api=1&query=${adress.lat},${adress.lgn}`,
                )
              }>
              <Icon
                name="location-arrow"
                type="FontAwesome"
                size={30}
                color={colors.main}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ marginHorizontal: 20, marginVertical: 30 }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryTextBold]}>
            QUIENES SOMOS
          </CustomText>
          <CustomText
            numberOfLines={line}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText, { marginTop: 15 }]}>
            {res.description}
          </CustomText>
          <TouchableOpacity
            onPress={() => {
              if (line > 5) {
                setline(5);
              } else {
                setline(20);
              }
            }}>
            <CustomText
              light={colors.main}
              dark={colors.main}
              style={[stylesText.secondaryText, { marginTop: 10 }]}>
              {line > 5 ? 'Ver menos' : 'Ver más'}{' '}
              <Icon
                name={line > 5 ? 'chevron-up' : 'chevron-down'}
                type="Feather"
                size={16}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
        </View>

        <View>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              { marginLeft: 15, marginBottom: 15 },
            ]}>
            CONTACTAR
          </CustomText>

          <View
            style={[
              styles.items,
              {
                backgroundColor: 'transparent',
                borderTopColor: border,
                borderTopWidth: 1,
                borderBottomColor: border,
                borderBottomWidth: 1,
                paddingVertical: 15,
                marginBottom: 20,
                flexDirection: 'row',
                width: dimensions.Width(90),
                alignItems: 'center',
                justifyContent: 'space-between',
              },
            ]}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name="smartphone"
                type="Feather"
                size={30}
                color={colors.rgb_153}
              />
              <TouchableOpacity
                onPress={() => OpenURLButton(`tel:+${datas.data.phone}`)}>
                <CustomText
                  light={colors.main}
                  dark={colors.main}
                  style={[stylesText.mainText, { marginLeft: 5 }]}>
                  {res.phone}
                </CustomText>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => OpenURLButton(`tel:+${datas.data.phone}`)}
              style={{
                borderWidth: 1,
                borderColor: border,
                padding: 7,
                width: 100,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[stylesText.secondaryText, { marginLeft: 5 }]}>
                LLAMAR
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>

        {datas.data.alegeno_url ? (
          <View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                { marginLeft: 15, marginBottom: 15 },
              ]}>
              ALÉRGENOS
            </CustomText>

            <View
              style={[
                styles.items,
                {
                  backgroundColor: 'transparent',
                  borderTopColor: border,
                  borderTopWidth: 1,
                  borderBottomColor: border,
                  borderBottomWidth: 1,
                  paddingVertical: 15,
                  marginBottom: 20,
                  flexDirection: 'row',
                  width: dimensions.Width(90),
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="sun"
                  type="Feather"
                  size={30}
                  color={colors.rgb_153}
                />
                <View>
                  <CustomText
                    light={colors.main}
                    dark={colors.main}
                    style={[stylesText.mainText, { marginLeft: 5 }]}>
                    Alérgenos
                  </CustomText>
                </View>
              </View>
              <TouchableOpacity
                onPress={() =>
                  Navigation.navigate('PdfVier', {
                    data: datas.data.alegeno_url,
                  })
                }
                style={{
                  borderWidth: 1,
                  borderColor: border,
                  padding: 7,
                  width: 130,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 10,
                }}>
                <CustomText
                  light={colors.main}
                  dark={colors.main}
                  style={[stylesText.secondaryText, { marginLeft: 5 }]}>
                  VER DETALLES
                </CustomText>
              </TouchableOpacity>
            </View>
          </View>
        ) : null}

        <View
          style={{
            marginBottom: res.socialLink ? 0 : dimensions.Height(15),
          }}>
          <Horarios res={res} />
        </View>

        {res.socialLink ? (
          <View style={{ marginBottom: dimensions.Height(15) }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                { marginLeft: 15, marginBottom: 15 },
              ]}>
              REDES SOCIALES
            </CustomText>

            <View
              style={[
                styles.items,
                {
                  backgroundColor: 'transparent',
                  borderTopColor: border,
                  borderTopWidth: 1,
                  borderBottomColor: border,
                  borderBottomWidth: 1,
                  paddingVertical: 15,
                  marginBottom: 20,
                  flexDirection: 'row',
                  width: dimensions.Width(90),
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  style={styles.btnSocialContet}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginHorizontal: 15,
                      paddingLeft: 15,
                    }}>
                    <TouchableOpacity
                      style={[styles.btnSocial]}
                      onPress={() => OpenURLButton(res.socialLink.instagram)}>
                      <Icon
                        name="instagram"
                        type="Feather"
                        size={24}
                        color={colors.rgb_153}
                      />
                    </TouchableOpacity>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryText, { marginTop: 5 }]}>
                      Instagram
                    </CustomText>
                  </View>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <TouchableOpacity
                      style={styles.btnSocial}
                      onPress={() => OpenURLButton(res.socialLink.facebook)}>
                      <Icon
                        name="facebook"
                        type="FontAwesome"
                        size={24}
                        color={colors.rgb_153}
                      />
                    </TouchableOpacity>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryText, { marginTop: 5 }]}>
                      Facebook
                    </CustomText>
                  </View>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <TouchableOpacity
                      style={styles.btnSocial}
                      onPress={() => OpenURLButton(res.socialLink.twitter)}>
                      <Icon
                        name="twitter"
                        type="AntDesign"
                        size={24}
                        color={colors.rgb_153}
                      />
                    </TouchableOpacity>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryText, { marginTop: 5 }]}>
                      Twitter
                    </CustomText>
                  </View>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <TouchableOpacity
                      style={styles.btnSocial}
                      onPress={() => OpenURLButton(res.socialLink.web)}>
                      <Icon
                        name="web"
                        type="MaterialCommunityIcons"
                        size={24}
                        color={colors.rgb_153}
                      />
                    </TouchableOpacity>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryText, { marginTop: 5 }]}>
                      Sitio web
                    </CustomText>
                  </View>
                </ScrollView>
              </View>
            </View>
          </View>
        ) : null}
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  containerIcon: {
    width: dimensions.Width(100),
    height: Platform.select({
      ios: dimensions.Height(12),
      android: dimensions.Height(13),
    }),
    position: 'absolute',
    backgroundColor: new DynamicValue('white', colors.back_dark),
    paddingTop: Platform.select({
      ios: dimensions.IsIphoneX() ? dimensions.Height(0) : dimensions.Height(2),
      android: dimensions.Height(7),
    }),
    zIndex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },

  icon: {
    width: 36,
    height: 36,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  logo: {
    width: 50,
    height: 50,
    marginRight: 10,
    borderRadius: 50,
  },

  btnSocial: {
    width: 40,
    height: 40,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
  },
  btnSocialContet: {},
});

export default Restaurant;

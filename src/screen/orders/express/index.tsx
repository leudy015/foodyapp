import React, { useEffect, useState } from 'react';
import {
  View,
  Image,
  FlatList,
  ScrollView,
  RefreshControl,
  ActionSheetIOS,
  Platform,
  Linking,
  Alert,
  TouchableOpacity,
} from 'react-native';
import { query } from '../../../GraphQL';
import { Query } from 'react-apollo';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors, image } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import { stylesText } from '../../../theme/TextStyle';
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../../../Components/PlaceHolder/OrderLoading';
import LottieView from 'lottie-react-native';
import Icon from 'react-native-dynamic-vector-icons';
import { NETWORK_INTERFACE_URL } from '../../../Config/config';
import { customMaspStyles } from '../../../Components/MapStyle';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { useMutation } from 'react-apollo';
import { mutations } from '../../../GraphQL';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { formaterPrice } from '../../../Utils/formaterPRice';
import Details from './details';
import Valoration from './Valoration';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const MyOrders = (props: any) => {
  const { localeCode, currecy } = props;
  const [ids, setID] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [visibleModal, setvisibleModal] = useState(false);
  const [dataModal, setdataModal] = useState(null);
  const [riderVal, setriderVal] = useState(0);

  const [actualizarRiders] = useMutation(mutations.ACTUALIZAR_RIDER);
  const [actualizarOrderProcess] = useMutation(mutations.CUSTOM_ORDER_PROCESS);

  const onStarRatingPressRiders = (rating: any) => {
    setriderVal(rating);
  };

  const SendPushNotificationNeworden = (OnesignalID, messageTexteNeworden) => {
    fetch(
      `${NETWORK_INTERFACE_URL}/send-push-notification-riders?IdOnesignal=${OnesignalID}&textmessage=${messageTexteNeworden}`,
    ).catch((err) => err);
  };

  const OrderProcesso = (datas, state, status, progress, refetch, message) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', options);
    const input = {
      id: datas.id,
      riders: datas.riders,
      estado: state,
      status: status,
      progreso: progress,
    };
    actualizarOrderProcess({ variables: { input: input } }).then((res) => {
      if (res.data.actualizarOrderProcess.success) {
        ReactNativeHapticFeedback.trigger('notificationSuccess', options);
        refetch();
        setModalVisible(false);
        if (state != 'Rechazada por el rider') {
          SendPushNotificationNeworden(datas.Riders.OnesignalID, message);
        }
      }
    });
  };

  const valorateRIder = (id, rating, dataRider, refetch) => {
    const input = {
      _id: id,
      rating: rating.concat(riderVal),
    };
    actualizarRiders({ variables: { input: input } }).then((res) => {
      if (res.data.actualizarRiders._id) {
        setvisibleModal(false);
        OrderProcesso(
          dataRider,
          'Valorada',
          'success',
          '100',
          refetch,
          `🌟 El cliente a valorado tu entrega con un ${riderVal} de 5`,
        );
      }
    });
  };

  const styles = useDynamicValue(dynamicStyles);
  const stylos = useDynamicValue(customMaspStyles);

  useEffect(() => {
    const getId = async () => {
      const id = await AsyncStorage.getItem('id');
      setID(id);
    };
    getId();
  }, [ids]);

  const OpenDetails = (datos: any) => {
    setdataModal(datos);
    setModalVisible(true);
  };

  const OpenValoracion = (datos: any) => {
    setdataModal(datos);
    setvisibleModal(true);
  };

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const onPress = () => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: 'Contactar con soporte',
          options: ['Cancelar', 'Asistencia teléfonica', 'Enviar un email'],
          cancelButtonIndex: 0,
        },
        (buttonIndex) => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            OpenURLButton(`tel:+34669124487`);
          } else if (buttonIndex === 2) {
            OpenURLButton('mailto:info@wilbby.com');
          }
        },
      );
    } else {
      OpenURLButton(`tel:+34669124487`);
    }
  };

  const _itemsRender = ({ item }, refetch: any) => {
    const origin = Number.parseFloat(item.origin.lat);
    const origin1 = Number.parseFloat(item.origin.lgn);

    const destino = Number.parseFloat(item.destination.lat);
    const destino1 = Number.parseFloat(item.destination.lgn);

    return (
      <TouchableOpacity
        style={styles.itemssActive}
        onPress={() =>
          item.estado === 'Entregado' ? OpenValoracion(item) : OpenDetails(item)
        }>
        <MapView
          provider={PROVIDER_GOOGLE}
          customMapStyle={stylos}
          showsUserLocation={true}
          zoomEnabled={true}
          style={{
            height: 110,
            width: 110,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            borderRadius: 20,
            overflow: 'hidden',
            marginRight: 10,
            marginLeft: 10,
          }}
          region={{
            latitude: origin,
            longitude: origin1,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
          <Marker
            coordinate={{
              latitude: origin ? origin : 42.3352296,
              longitude: origin1 ? origin1 : -3.7110048,
            }}>
            <Image source={image.Maping} style={{ width: 50, height: 50 }} />
          </Marker>

          <Marker
            coordinate={{
              latitude: destino ? destino : 42.3352296,
              longitude: destino1 ? destino1 : -3.7110048,
            }}>
            <Image
              source={image.Maping_Shop}
              style={{ width: 50, height: 50 }}
            />
          </Marker>
        </MapView>
        <View>
          <CustomText
            light={colors.black}
            dark={colors.white}
            numberOfLines={1}
            style={[
              stylesText.secondaryTextBold,
              { width: dimensions.Width(60) },
            ]}>
            {item.estado}
          </CustomText>
          <View style={{ marginTop: 10 }}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.placeholderText,
                { width: dimensions.Width(50) },
              ]}>
              <Icon
                name="flag"
                type="AntDesign"
                size={14}
                color={colors.main}
              />{' '}
              {item.origin.address_name}
            </CustomText>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.placeholderText,
                { width: dimensions.Width(50), marginTop: 10 },
              ]}>
              <Icon
                name="enviromento"
                type="AntDesign"
                size={14}
                color={colors.main}
              />{' '}
              {item.destination.address_name}
            </CustomText>
          </View>

          <View
            style={{
              flexDirection: 'row',
              paddingTop: 15,
              display: 'flex',
            }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              Total... {formaterPrice(item.total, localeCode, currecy)}
            </CustomText>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  return (
    <Query query={query.GET_CUSTOM_ORDER} variables={{ id: ids }}>
      {(response: any) => {
        if (response.loading) {
          return <Loading />;
        }
        if (response) {
          const respuesta =
            response && response.data && response.data.getCustomOrder
              ? response.data.getCustomOrder.data
              : [];
          response.refetch();
          return (
            <View style={styles.container}>
              <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={_onRefresh}
                  />
                }>
                <View
                  style={{
                    marginHorizontal: dimensions.Width(4),
                    marginTop: dimensions.Height(1),
                    marginBottom: dimensions.Height(2),
                    width: dimensions.Width(70),
                  }}>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={stylesText.titleText}>
                    Mis pedidos express
                  </CustomText>
                  <CustomText
                    light={colors.rgb_153}
                    dark={colors.rgb_153}
                    style={[
                      stylesText.secondaryText,
                      { marginTop: 10, paddingBottom: 5, lineHeight: 17 },
                    ]}>
                    Encuentra aquí tus pedidos anteriores y en curso
                  </CustomText>
                </View>
                <View
                  style={{
                    marginBottom: dimensions.Height(10),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <FlatList
                    data={respuesta}
                    renderItem={(item: any) =>
                      _itemsRender(item, response.refetch)
                    }
                    keyExtractor={(item: any) => item._id}
                    showsVerticalScrollIndicator={false}
                    scrollEnabled={false}
                    ListEmptyComponent={
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <LottieView
                          source={require('../../../Assets/Animate/no_order.json')}
                          autoPlay
                          loop
                          style={{ width: 250 }}
                        />
                        <CustomText
                          ligth={colors.black}
                          dark={colors.white}
                          style={stylesText.secondaryTextBold}>
                          ¡Anímate a hacer tu primer pedido express!
                        </CustomText>
                      </View>
                    }
                  />
                </View>
              </ScrollView>
              {dataModal ? (
                <Details
                  dataModal={dataModal}
                  localeCode={localeCode}
                  currecy={currecy}
                  modalVisible={modalVisible}
                  setModalVisible={setModalVisible}
                />
              ) : null}

              {dataModal ? (
                <Valoration
                  dataModal={dataModal}
                  localeCode={localeCode}
                  currecy={currecy}
                  refetch={response.refetch}
                  visibleModal={visibleModal}
                  setvisibleModal={setvisibleModal}
                  setModalVisible={setModalVisible}
                />
              ) : null}
            </View>
          );
        }
      }}
    </Query>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  const: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  container: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    height: 'auto',
    minHeight: dimensions.Height(100),
  },

  centeredView: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    flex: 1,
  },

  itemssActive: {
    width: dimensions.IsIphoneX() ? dimensions.Width(96) : dimensions.Width(94),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 15,
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  img: {
    width: 100,
    height: 100,
    borderRadius: 10,
    marginLeft: 15,
    marginRight: dimensions.Height(2),
    backgroundColor: colors.white,
  },

  headerCont: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 100,
    paddingHorizontal: dimensions.Width(4),
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  closet: {
    color: new DynamicValue(colors.main, colors.white),
    paddingRight: 15,
    paddingLeft: 15,
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  btnhelp: {
    backgroundColor: colors.main,
    paddingHorizontal: 30,
    padding: 7,
    borderRadius: 100,
  },

  textInputContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
  },

  cardscuston: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    marginTop: 30,
    borderRadius: 20,
    shadowColor: new DynamicValue(colors.black, colors.black),
    marginHorizontal: 10,
    shadowOffset: {
      width: 0,
      height: 15,
    },
    shadowOpacity: 0.34,
    shadowRadius: 8.27,
    elevation: 20,
    overflow: 'hidden',
  },

  cards: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    marginTop: 30,
    padding: 20,
    borderRadius: 20,
    shadowColor: new DynamicValue(colors.rgb_235, colors.black),
    marginHorizontal: 10,
    shadowOffset: {
      width: 0,
      height: 15,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  distance: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: -40,
    marginLeft: 'auto',
    marginRight: 15,
    marginBottom: 10,
    borderRadius: 15,
    flexDirection: 'row',
  },

  logos: {
    width: 60,
    height: 60,
    marginRight: 15,
    borderRadius: 50,
  },
});

export default MyOrders;

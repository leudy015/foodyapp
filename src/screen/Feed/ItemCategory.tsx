import React from 'react';
import { TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { useTranslation } from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ItemCategory(props) {
  const styles = useDynamicValue(dynamicStyles);
  const { t } = useTranslation();

  const {
    setTipo,
    setTipoTitle,
    item,
    category,
    catTitle,
    setcategory,
    setcatTitle,
    setpage,
    setdatos,
  } = props;

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };
  const BorderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];
  const borderColor = BorderColor[mode];

  return (
    <TouchableOpacity
      style={[
        styles.cat,
        {
          backgroundColor:
            category === item._id ? colors.main : backgroundColor,
          borderColor: borderColor,
        },
      ]}
      onPress={() => {
        ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
        if (catTitle === 'Comida') {
          setTipo(item._id);
          setTipoTitle(item.title);
          setpage(1);
          setdatos([]);
        } else {
          setpage(1);
          setdatos([]);
          setcategory(item._id);
          setcatTitle(item.title);
        }
      }}>
      <CustomText
        light={category === item._id ? colors.white : colors.back_suave_dark}
        dark={category === item._id ? colors.white : colors.rgb_235}
        style={[stylesText.secondaryText, { fontWeight: '500' }]}>
        {t(`category:${item.title}`)}
      </CustomText>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cat: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 30,
    borderRadius: 100,
  },
});

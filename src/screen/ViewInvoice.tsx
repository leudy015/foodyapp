import React from 'react';
import { Dimensions, View, ScrollView, Share } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Pdf from 'react-native-pdf';
import Header from '../Components/Header';
import { useNavigationParam } from 'react-navigation-hooks';
import { colors } from '../theme/colors';

export default function PdfView() {
  const data = useNavigationParam('data');
  const source = {
    uri: data.invoiceUrl,
    cache: true,
  };
  const styles = useDynamicValue(dynamicStyles);

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Factura de tu pedido en Wilbby #${data.channelOrderDisplayId} en ${data.storeData.title}`,
        url: `${data.invoiceUrl}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <View style={styles.container}>
      <Header
        title={`Pedido #${data.channelOrderDisplayId}`}
        fromPDF={true}
        onPresPDF={() => onShare()}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Pdf
          source={source}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(`number of pages: ${numberOfPages}`);
          }}
          onPageChanged={(page, numberOfPages) => {
            console.log(`current page: ${page}`);
          }}
          onError={(error) => {
            console.log(error);
          }}
          onPressLink={(uri) => {
            console.log(`Link presse: ${uri}`);
          }}
          style={styles.pdf}
        />
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    flex: 1,
  },

  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    marginTop: 0,
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
  },
});

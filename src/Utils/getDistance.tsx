import { GOOGLE_DISTANCE_API, GOOGLE_API_KEY } from '../Config/config';

export const getDistacia = async (
  coordetate: any,
  coordenateone: any,
  coordetateRest: any,
  coordenateoneRest: any,
) => {
  let apiUrlWithParams = `${GOOGLE_DISTANCE_API}?units=imperial&origins=${coordetate},${coordenateone}&destinations=${coordetateRest},${coordenateoneRest}&key=${GOOGLE_API_KEY}`;
  const response = await fetch(apiUrlWithParams);

  return response;
};

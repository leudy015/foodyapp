import React from 'react';
import { TouchableOpacity, Image, Dimensions } from 'react-native';
import { colors } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { Grayscale } from 'react-native-color-matrix-image-filters';
import { validateDates } from '../../../Utils/CalculateHour';
import { useTranslation } from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import * as Animatable from 'react-native-animatable';

const heigth = Dimensions.get('window').height;
const alto = heigth > 800 ? 120 : 110;

export default function MainCategory(props) {
  const styles = useDynamicValue(dynamicStyles);
  const { data, onPress, width, height } = props;
  const { t } = useTranslation();

  const onPressCategory = () => {
    switch (data.title) {
      case 'Comida':
        return onPress;

      case 'Supermercados':
        return onPress;

      case 'Parafarmacia':
        return onPress;

      case 'Tiendas':
        return onPress;

      case 'Algo Especial':
        return null;

      case 'Envío Express':
        return null;

      default:
        onPress;
        break;
    }
  };

  const isCatAvailable = (): boolean => {
    switch (data.title) {
      case 'Comida':
        return true;

      case 'Supermercados':
        return true;

      case 'Parafarmacia':
        return true;

      case 'Tiendas':
        return true;

      case 'Algo Especial':
        return validateDates(8, 0, 23, 59);

      case 'Envío Express':
        return validateDates(8, 0, 23, 59);
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={100}
      onPress={isCatAvailable() ? onPress : onPressCategory()}>
      <Animatable.View
        animation="flipInY"
        style={[
          styles.card,
          { width: width ? width : alto, height: height ? height : alto },
        ]}>
        {!isCatAvailable() ? (
          <Icon
            name="sleep"
            type="MaterialCommunityIcons"
            size={18}
            color={colors.rgb_153}
            style={{
              position: 'absolute',
              top: heigth > 700 ? 5 : 3,
              right: 50,
            }}
          />
        ) : null}
        <Grayscale amount={isCatAvailable() ? 0 : 1}>
          <Image
            source={{ uri: data.image }}
            style={{
              width: heigth > 800 ? 90 : 80,
              height: heigth > 800 ? 70 : 65,
              resizeMode: 'contain',
            }}
          />
        </Grayscale>
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            {
              fontWeight: 'bold',
              fontSize: heigth > 800 ? 10 : 8,
              paddingBottom: 5,
            },
          ]}>
          {t(`category:${data.title}`)}
        </CustomText>
      </Animatable.View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    height: 140,
    marginVertical: 20,
    borderRadius: 200,
    marginHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: new DynamicValue(colors.light_grey, colors.back_dark),
    shadowOpacity: 0.24,
    shadowRadius: 15,
    elevation: 10,
  },
});

import React from 'react';
import { View, TouchableOpacity, Alert } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Navigation from '../../services/Navigration';
import CustomAnimationProgress from '../../Components/ProgressBar';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { formaterPrice } from '../../Utils/formaterPRice';
import * as Animatable from 'react-native-animatable';
import { query } from '../../GraphQL';
import { useQuery } from 'react-apollo';
import { scheduleTime } from '../../Utils/scheduleTime';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ButtonBar(props: any) {
  const styles = useDynamicValue(dynamicStyles);
  const {
    res,
    date,
    selecte,
    takeaway,
    city,
    lat,
    lgn,
    setLoding,
    isOK,
    localeCode,
    currecy,
    respuesta,
    setinCartItem,
    token,
    user,
    delivery,
    onOpen,
  } = props;

  const { data, refetch } = useQuery(query.USER_DETAIL);
  const users =
    data && data.getUsuario && data.getUsuario.data ? data.getUsuario.data : {};

  const usuario = user ? user : users;

  //refetch();

  console.log(date);

  var totals = 0;
  respuesta.forEach(function (items: any) {
    const t = items.items.quantity * items.items.price;
    totals += t;
    items.items.subItems.forEach((x) => {
      const s = x.quantity * x.price;
      totals += s * items.items.quantity;
      const t = x && x.subItems ? x.subItems : [];
      t.forEach((y) => {
        const u = y.quantity * y.price;
        totals += u * items.items.quantity;
      });
    });
  });

  const datos = {
    data: respuesta,
    time: date ? date : selecte,
    restaurants: res,
    user: usuario,
    city: city,
    lat: lat,
    lgn: lgn,
    isOK: isOK,
    localeCode: localeCode,
    currecy: currecy,
    takeaway: delivery ? takeaway : res.llevar ? true : false,
    setinCartItem: setinCartItem,
    delivery: delivery,
  };
  let mini =
    res && res.salvingPack && res.salvingPack.isSavingPack
      ? res.salvingPack.item.price
      : res.minime;
  let cantd = respuesta ? respuesta.length : 0;
  let restante: number = mini - totals;
  let minimo: number = mini;

  const processOrder = () => {
    setLoding(true);
    if (totals < minimo) {
      setLoding(true);
      ReactNativeHapticFeedback.trigger('notificationError', options);
      Alert.alert(
        'Cantidad mínima no superada',
        `El pedido mínimo de este establecimiento es de ${formaterPrice(
          minimo / 100,
          localeCode,
          currecy,
        )}`,
      );
    } else {
      ReactNativeHapticFeedback.trigger('notificationSuccess', options);
      if (token) {
        if (usuario._id) {
          setLoding(false);
          if (delivery) {
            setTimeout(() => {
              Navigation.navigate('Order', { data: datos });
            }, 500);
          } else {
            if (res.llevar) {
              setTimeout(() => {
                Navigation.navigate('Order', { data: datos });
              }, 500);
            } else {
              Alert.alert(
                'No hay repartidores disponibles',
                'Lo sentimos ahora no tenemos repartidores disponibles intentalo más tarde',
                [
                  {
                    text: '¡Vale!',
                    onPress: () =>
                      ReactNativeHapticFeedback.trigger(
                        'notificationError',
                        options,
                      ),
                  },
                ],

                { cancelable: false },
              );
            }
          }
        }
      } else {
        Alert.alert(
          'Inicia sesión',
          'Para continuar con el pedido debes iniciar sesión',
          [
            {
              text: 'Iniciar sesión',
              onPress: () => Navigation.navigate('Login', 0),
            },

            {
              text: 'Regístrarme',
              onPress: () => Navigation.navigate('Login', 0),
            },
          ],

          { cancelable: false },
        );
      }
    }
  };

  const continuerTo = () => {
    if (delivery) {
      processOrder();
    } else if (res.llevar) {
      processOrder();
    } else if (res.autoshipping) {
      processOrder();
    } else {
      Alert.alert(
        'No hay repartidores disponible',
        'No tenemos repartidores disponible en este momento y el restaurante no acepta pedido para recoger, intentalo de nuevo más tarde',
        [
          {
            text: '¡Vale!',
            onPress: () => console.log('calcel'),
          },
        ],

        { cancelable: false },
      );
    }
  };

  return (
    <Animatable.View
      animation={respuesta.length > 0 ? 'fadeInUp' : 'fadeOutDown'}
      duration={500}
      iterationCount={1}
      style={styles.containercon}>
      {totals < minimo ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 10,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 5,
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              numberOfLines={1}
              style={stylesText.secondaryText}>
              Con
            </CustomText>

            <CustomText
              light={colors.black}
              dark={colors.white}
              numberOfLines={1}
              style={[
                stylesText.secondaryText,
                {
                  marginRight: 5,
                  marginLeft: 5,
                  fontWeight: 'bold',
                },
              ]}>
              {formaterPrice(restante / 100, localeCode, currecy)}
            </CustomText>
            <CustomText
              light={colors.black}
              dark={colors.white}
              numberOfLines={1}
              style={stylesText.secondaryText}>
              completas el mínimo de compra
            </CustomText>
          </View>
          <View style={{ marginTop: 10 }}>
            <CustomAnimationProgress
              color2={colors.green}
              color1={colors.green}
              color3={colors.green}
              maxpercent={minimo}
              percent={totals}
              width={330} //numeric only
              height={14}
            />
          </View>
        </View>
      ) : (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 5,
            }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              numberOfLines={1}
              style={[
                stylesText.secondaryText,
                { marginRight: 5, fontWeight: 'bold' },
              ]}>
              !Felicidades!
            </CustomText>
            <CustomText
              light={colors.black}
              dark={colors.white}
              numberOfLines={1}
              style={stylesText.secondaryText}>
              Completaste el mínimo de compra
            </CustomText>
          </View>
          <View style={{ marginTop: 10 }}>
            <CustomAnimationProgress
              color2={colors.green}
              color1={colors.green}
              color3={colors.green}
              maxpercent={minimo}
              percent={totals}
              width={330} //numeric only
              height={14}
            />
          </View>
        </View>
      )}
      <View
        style={[
          styles.selected,
          {
            backgroundColor: colors.green,
          },
        ]}>
        <TouchableOpacity
          onPress={() => {
            if (res.scheduleOnly.available) {
              if (scheduleTime(res.schedule)) {
                if (!date) {
                  Alert.alert(
                    'Debes seleccionar una fecha',
                    'Para continuar con el pedido debes seleccionar una fecha y hora para tu pedido',
                    [
                      {
                        text: 'Seleccionar fecha y hora',
                        onPress: () => onOpen(),
                      },
                    ],

                    { cancelable: false },
                  );
                } else {
                  continuerTo();
                }
              } else {
                if (res.noScheduled) {
                  Alert.alert(
                    'Establecimiento cerrada',
                    `${res.title} solo admite pedido al instante y ahora no esta disponible`,
                    [
                      {
                        text: '¡Vale!',
                        onPress: () => console.log('calcel'),
                      },
                    ],

                    { cancelable: false },
                  );
                } else {
                  if (!date) {
                    Alert.alert(
                      'Debes seleccionar una fecha',
                      'Para continuar con el pedido debes seleccionar una fecha y hora para tu pedido',
                      [
                        {
                          text: 'Seleccionar fecha y hora',
                          onPress: () => onOpen(),
                        },
                      ],

                      { cancelable: false },
                    );
                  } else {
                    continuerTo();
                  }
                }
              }
            } else {
              continuerTo();
            }
          }}
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
            width: dimensions.Width(94),
            height: 50,
            borderRadius: 5,
            paddingHorizontal: 20,
          }}>
          <CustomText
            light={colors.white}
            dark={colors.white}
            numberOfLines={1}
            style={[
              stylesText.mainText,
              {
                backgroundColor: '#3d8800',
                padding: 10,
                borderRadius: 0,
                paddingBottom: 10,
              },
            ]}>
            {cantd}
          </CustomText>
          <CustomText
            light={colors.white}
            dark={colors.white}
            numberOfLines={1}
            style={[
              stylesText.mainText,
              {
                justifyContent: 'center',
                alignSelf: 'center',
                marginLeft: 20,
                marginBottom: -5,
              },
            ]}>
            Ver cesta
          </CustomText>
          <CustomText
            light={colors.white}
            dark={colors.white}
            numberOfLines={1}
            style={[stylesText.mainText, { marginBottom: -5 }]}>
            {formaterPrice(totals / 100, localeCode, currecy)}
          </CustomText>
        </TouchableOpacity>
      </View>
    </Animatable.View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  selected: {
    width: dimensions.Width(94),
    height: 50,
    borderRadius: 5,
    bottom: 30,
    position: 'absolute',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  containercon: {
    height: 150,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});

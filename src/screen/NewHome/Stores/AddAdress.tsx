import React from 'react';
import { View, Platform } from 'react-native';
import { dimensions, colors, stylesText } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import LottieView from 'lottie-react-native';
import { Button } from '../../../Components/Button';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import Navigation from '../../../services/Navigration';
const source = require('../../../Assets/Animate/burguer.json');
const AddAdres = (props: any) => {
  const {
    getAdress,
    city,
    lat,
    lgn,
    setcity,
    getRidersAvailable,
    setNotification,
    id,
  } = props;
  const styles = useDynamicValue(dynamicStyles);

  const confiAdres = {
    fromHome: true,
    getAdreesID: getAdress,
    city: city,
    lat: lat,
    lgn: lgn,
    setcity: setcity,
    getRidersAvailable: getRidersAvailable,
  };

  return (
    <View
      style={{
        alignItems: 'center',
        width: dimensions.Width(96),
        marginBottom: dimensions.Height(5),
        height: dimensions.Height(80),
      }}>
      <LottieView source={source} autoPlay loop style={{ width: 200 }} />
      <CustomText
        ligth={colors.black}
        dark={colors.white}
        style={[
          stylesText.secondaryTextBold,
          { textAlign: 'center', paddingHorizontal: 30, paddingBottom: 5 },
        ]}>
        Puede que aún no hayamos llegado a esta zona, prueba añadiendo otra
        dirección de entrega.
      </CustomText>

      <View style={styles.signupButtonContainer}>
        <Button
          light={colors.white}
          dark={colors.white}
          containerStyle={[styles.buttonView, { backgroundColor: colors.main }]}
          onPress={() => {
            if (id) {
              Navigation.navigate('Direccion', { data: confiAdres });
              setNotification();
              getRidersAvailable(city);
            } else {
              Navigation.navigate('Login', 0);
            }
          }}
          title="Añadir dirección de entrega"
          titleStyle={styles.buttonTitle}
        />
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  signupButtonContainer: {
    marginTop: Platform.select({
      ios: dimensions.Height(5),
      android: dimensions.Height(2),
    }),
    alignSelf: 'center',
    width: dimensions.Width(70),
  },
  buttonView: {
    width: dimensions.Width(70),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

export default AddAdres;

import React from 'react';
import { View, ScrollView, Alert, Modal } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors } from '../../theme';
import SearchProducts from '../SearchPRoducto';

export default function SearcProducts(props) {
  const {
    data,
    category,
    title,
    localeCode,
    currecy,
    inCartItem,
    setinCartItem,
    modalVisible,
    setModalVisible,
  } = props;
  const styles = useDynamicValue(dynamicStyles);

  return (
    <Modal
      animationType="slide"
      visible={modalVisible}
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.centeredView}>
        <SearchProducts
          datos={data}
          categoria={category}
          title={title}
          localeCode={localeCode}
          currecy={currecy}
          inCartItem={inCartItem}
          setinCartItem={setinCartItem}
          setModalVisible={setModalVisible}
        />
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  centeredView: {
    backgroundColor: new DynamicValue(colors.white, 'black'),
    flex: 1,
  },
});

import React, { useState, useEffect } from 'react';
import {
  View,
  RefreshControl,
  TouchableOpacity,
  Image,
  Platform,
  FlatList,
  Animated,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors, dimensions, image, stylesText } from '../../theme';
import Navigation from '../../services/Navigration';
import Header from '../../Components/Header';
import { useQuery } from 'react-apollo';
import { query } from '../../GraphQL';
import { useNavigationParam } from 'react-navigation-hooks';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Scooter from '../../Components/scooter';
import { useTranslation } from 'react-i18next';
import HeightDemand from '../../Components/HeightDemand';
import { validateDates } from '../../Utils/CalculateHour';
import NoLocation from '../../Components/NoLocation';
import AsyncStorage from '@react-native-community/async-storage';
import Filters from './filters';
import Card from '../../Components/RestaurantCard/Card';
import LoadingAnimated from '../../Components/LoadingAnimated';
import Highkitchen from './highkitchen';
import NoData from '../../Components/NoData';
import { CustomText } from '../../Components/CustomTetx';
import * as Animatable from 'react-native-animatable';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Feed() {
  const styles = useDynamicValue(dynamicStyles);
  const { t } = useTranslation();
  const myData = useNavigationParam('data');
  const categorys = myData.data;
  const city = myData.city;
  const isLocation = myData.isLocation;
  const riders = myData.riders;
  const [refreshing, setRefreshing] = useState(false);
  const [category, setcategory] = useState(categorys._id);
  const [catTitle, setcatTitle] = useState(
    categorys.title ? categorys.title : 'Comida',
  );
  const [tipo, setTipo] = useState('');
  const [tipotienda, setTipotienda] = useState('');
  const [tipoTitle, setTipoTitle] = useState('');
  const [llevar, setLlevar] = useState(false);
  const [id, setid] = useState(null);
  const [page, setpage] = useState(1);
  const [limit, setlimit] = useState(5);
  const [datos, setdatos] = useState([]);
  const [scrollY] = useState(new Animated.Value(0));

  const { data, refetch, loading } = useQuery(query.RESTAURANT_FOR_CATEGORY, {
    variables: {
      city: city,
      category: category,
      tipo: tipo,
      llevar: llevar,
      page: page,
      limit: limit,
    },
  });

  const stores =
    data && data.getRestaurantForCategory
      ? data.getRestaurantForCategory.data
      : [];

  let stopFetchMore = true;

  useEffect(() => {
    if (stores.length > 0) {
      stores.forEach((store) => {
        var i = datos.findIndex((x: any) => x._id === store._id);
        if (i !== -1) {
          datos.splice(i, 1);
          setdatos(datos.concat(stores));
        } else {
          setdatos(datos.concat(stores));
        }
      });
    } else if (stores.length < 4) {
      stopFetchMore = false;
    } else {
      stopFetchMore = false;
    }
  }, [stores]);

  const loadMoreData = () => {
    if (stopFetchMore && !loading) {
      setpage(page + 1);
    }
  };

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const getID = async () => {
    const ids = await AsyncStorage.getItem('id');
    setid(ids);
  };

  useEffect(() => {
    getID();
  }, [id]);

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const _renderItem = ({ item }) => {
    return (
      <Card
        item={item}
        id={id}
        city={city}
        lat={myData.lat}
        lgn={myData.lgn}
        localeCode={myData.localeCode}
        currecy={myData.currecy}
        refetch={refetch}
        riders={riders}
      />
    );
  };

  const renderFooster = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: dimensions.Height(20),
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 0,
          }}>
          <TouchableOpacity
            style={{
              borderRadius: 15,
            }}
            activeOpacity={100}
            onPress={() => {
              Navigation.navigate('ComparteyGana', 0);
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(type, optiones);
            }}>
            <Image
              source={image.FreeCredit}
              style={{
                width: dimensions.Width(96),
                height: 140,
                resizeMode: 'cover',
                borderRadius: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        {loading && stopFetchMore && (
          <LoadingAnimated
            name={`Buscando más ${catTitle.toLowerCase()} para ti`}
          />
        )}
      </View>
    );
  };

  const renderHeader = () => {
    return (
      <>
        {city === 'Tomelloso' || city === 'Alcázar de San Juan' ? null : (
          <>
            {catTitle === 'Comida' ? (
              <View
                style={{
                  marginTop: dimensions.Height(1),
                  paddingBottom: dimensions.Height(1),
                }}>
                <Highkitchen
                  id={id}
                  city={city}
                  lat={myData.lat}
                  lgn={myData.lgn}
                  localeCode={myData.localeCode}
                  currecy={myData.currecy}
                  isLocation={isLocation}
                  riders={riders}
                />
              </View>
            ) : null}
          </>
        )}
      </>
    );
  };

  const nav = {
    city: city,
    lat: myData.lat,
    lgn: myData.lgn,
    localeCode: myData.localeCode,
    currecy: myData.currecy,
    isLocation: isLocation,
    onsearch: '',
    riders: riders,
  };

  const getItemLayout = (data, index) => ({
    length: 50,
    offset: 50 * index,
    index,
  });

  let flatListRef;

  const scrollToIndex = () => {
    let randomIndex = 0;
    flatListRef.scrollToIndex({ animated: true, index: randomIndex });
  };

  const opacityText = scrollY.interpolate({
    inputRange: [300, 700],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  return (
    <View style={styles.container}>
      <Header
        title="Buscar"
        left={true}
        search={true}
        autoFocus={false}
        onFocus={() => {
          Navigation.navigate('Search', { data: nav });
        }}
        placeholder={`${t(`category:${catTitle}`)} en ${city}`}
        Loading={false}
        filter={true}
        setdatos={setdatos}
      />
      <View style={{ marginTop: dimensions.Height(1) }}>
        <View
          style={{
            flexDirection: 'row',
            paddingRight: 100,
            marginBottom: validateDates(23, 0, 23, 59) ? 0 : 20,
          }}>
          <View style={styles.filters}>
            <TouchableOpacity
              onPress={() => {
                setpage(1);
                setdatos([]);
                setLlevar(false);
                ReactNativeHapticFeedback.trigger(
                  'notificationSuccess',
                  optiones,
                );
              }}
              style={llevar ? styles.itemsInac : styles.items}>
              <Scooter />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setLlevar(true);
                setpage(1);
                setdatos([]);
                ReactNativeHapticFeedback.trigger(
                  'notificationSuccess',
                  optiones,
                );
              }}
              style={llevar ? styles.items : styles.itemsInac}>
              <Icon
                name="run"
                type="MaterialCommunityIcons"
                size={20}
                style={styles.icons}
              />
            </TouchableOpacity>
          </View>
          <Filters
            setTipo={setTipo}
            tipo={tipo}
            category={category}
            catTitle={catTitle}
            setcategory={setcategory}
            setcatTitle={setcatTitle}
            setpage={setpage}
            setdatos={setdatos}
          />
        </View>
        {/* {validateDates(23, 0, 23, 59) ? <HeightDemand /> : null} */}
        <View
          style={{
            height: dimensions.ScreenHeight,
          }}>
          {isLocation ? (
            <View>
              {isLocation ? (
                <>
                  {loading && datos.length < 1 ? (
                    <LoadingAnimated
                      name={`Buscando más ${catTitle.toLowerCase()} para ti`}
                    />
                  ) : (
                    <FlatList
                      style={{ marginBottom: dimensions.Height(20) }}
                      data={datos}
                      ref={(ref) => {
                        flatListRef = ref;
                      }}
                      onScroll={Animated.event([
                        { nativeEvent: { contentOffset: { y: scrollY } } },
                      ])}
                      renderItem={(item: any) => _renderItem(item)}
                      keyExtractor={(item: any) => item._id}
                      showsVerticalScrollIndicator={false}
                      ListEmptyComponent={
                        <NoData name="Aún no tenemos establecimientos con estas preferencias" />
                      }
                      getItemLayout={getItemLayout}
                      onEndReached={loadMoreData}
                      onEndReachedThreshold={0.1}
                      ListFooterComponent={renderFooster}
                      ListHeaderComponent={renderHeader}
                      refreshControl={
                        <RefreshControl
                          refreshing={refreshing}
                          enabled={true}
                          title={city ? city : 'Cargando'}
                          onRefresh={_onRefresh}
                        />
                      }
                    />
                  )}
                </>
              ) : (
                <NoLocation />
              )}
            </View>
          ) : (
            <NoLocation />
          )}
        </View>
      </View>
      <Animated.View style={[styles.top, { opacity: opacityText }]}>
        <Animatable.View animation="slideInUp" duration={100}>
          <TouchableOpacity
            onPress={scrollToIndex}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <CustomText
              style={[stylesText.secondaryTextBold, { fontSize: 14 }]}>
              Arriba
            </CustomText>
            <Icon
              name="chevron-up"
              type="Feather"
              size={20}
              color={colors.black}
            />
          </TouchableOpacity>
        </Animatable.View>
      </Animated.View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    flex: 1,
  },

  cat: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 30,
    borderRadius: 100,
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  filters: {
    width: 80,
    height: 40,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 100,
    paddingHorizontal: 5,
  },

  items: {
    width: 35,
    height: 35,
    backgroundColor: colors.main,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  itemsInac: {
    width: 35,
    height: 35,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  kitchen: {
    width: dimensions.Width(94),
    height: 140,
    marginBottom: 30,
    marginHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  cont: {
    marginTop: 0,
    marginBottom: 40,
    width: dimensions.ScreenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },

  top: {
    position: 'absolute',
    zIndex: 100,
    width: dimensions.Width(20),
    borderRadius: 100,
    backgroundColor: colors.white,
    padding: 10,
    bottom: 30,
    left: '40%',
    shadowColor: new DynamicValue(colors.light_grey, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});

import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  ImageBackground,
} from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { dimensions, colors, image } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import { stylesText } from '../../../theme/TextStyle';
import { Query } from 'react-apollo';
import { query } from '../../../GraphQL';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import LoadingAnimated from '../../../Components/LoadingAnimated';
import SubCollection from './ModalCollection';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Collection(props: any) {
  const [modalCollection, setmodalCollection] = useState(false);
  const [dataDetails, setdataDetails] = useState(null);
  const styles = useDynamicValue(dynamicStyles);

  const { store, localeCode, currecy, inCartItem, setinCartItem } = props;

  const setDetails = (items) => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    setdataDetails(items);
    setmodalCollection(true);
  };

  const renderItems = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.collection}
        onPress={() => setDetails(item)}>
        <ImageBackground
          imageStyle={{ borderRadius: 15 }}
          source={image.PlaceSuper}
          style={styles.imagenCollectionPlace}>
          <ImageBackground
            imageStyle={{ borderRadius: 15 }}
            source={{ uri: item.image }}
            style={styles.imagenCollection}>
            <CustomText
              numberOfLines={1}
              style={stylesText.titleText}
              light={colors.white}
              dark={colors.white}>
              {item.title}
            </CustomText>
          </ImageBackground>
        </ImageBackground>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <Query query={query.COLLECTIONS} variables={{ store: store._id }}>
        {(response: any) => {
          if (response.loading) {
            return <LoadingAnimated name="Estamos organizando los pasillos" />;
          }
          if (response) {
            const data =
              response && response.data && response.data.getCollection
                ? response.data.getCollection.data
                : [];
            response.refetch();
            return (
              <FlatList
                data={data}
                renderItem={(item: any) => renderItems(item)}
                keyExtractor={(item: any) => item._id}
                showsVerticalScrollIndicator={false}
              />
            );
          }
        }}
      </Query>
      {dataDetails ? (
        <SubCollection
          datos={dataDetails}
          store={store}
          localeCode={localeCode}
          currecy={currecy}
          inCartItem={inCartItem}
          setinCartItem={setinCartItem}
          modalVisible={modalCollection}
          setModalVisible={setmodalCollection}
        />
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },

  collection: {
    width: dimensions.ScreenWidth,
    height: 160,
    alignItems: 'center',
    marginVertical: 15,
  },

  imagenCollection: {
    width: dimensions.Width(90),
    height: 160,
    padding: 15,
  },

  imagenCollectionPlace: {
    width: dimensions.Width(90),
    height: 160,
  },
});

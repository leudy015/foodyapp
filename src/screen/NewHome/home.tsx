import React, { useState, useEffect, useRef } from 'react';
import { View, Animated, Platform, DeviceEventEmitter } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors } from '../../theme';
import ButtonBar from './ButtomBar';
import Header from './Header/header';
import Category from './MainCategory/Category';
import Geolocation from '@react-native-community/geolocation';
import RNLocation from 'react-native-location';
import AsyncStorage from '@react-native-community/async-storage';
import ModalCategory from './MainCategory/Modal';
import { Modalize } from 'react-native-modalize';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Navigation from '../../services/Navigration';
import NoLocation from '../../Components/NoLocation';
import Stores from './Stores';
import { SetCity } from '../../Utils/getCiti';
import Order from '../orders/index';
import MyAccount from '../Profile';
import Favourite from '../Favourites';
import ModalAllCategory from './MainCategory/Allcategory';
import {
  getTrackingStatus,
  requestTrackingPermission,
} from 'react-native-tracking-transparency';
import Notification from '../../Components/ModalPermision/Notification';
import NoInternet from '../../Components/ModalPermision/NoInternet';
import { checkNotifications } from 'react-native-permissions';
import OneSignal from 'react-native-onesignal';
import Location from '../../Components/ModalPermision/Location';
import NetInfo from '@react-native-community/netinfo';
import { NETWORK_INTERFACE_URL } from '../../Config/config';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function HomeScreen(props: any) {
  const { cities, id, refetch, ruteNames } = props;
  const styles = useDynamicValue(dynamicStyles);
  const [ruteName, setruteName] = useState(ruteNames ? ruteNames : 'Home');
  const [lat, setLat] = useState(null);
  const [lgn, setLgn] = useState(null);
  const [ids, setids] = useState(id);
  const [localeCode, setlocaleCode] = useState('');
  const [currecy, setcurrecy] = useState('');
  const [countryCode, setcountryCode] = useState('');
  const [category, setCategory] = useState(null);
  const [city, setcity] = useState(cities);
  const [isLocation, setisLocation] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [titleCat, settitleCat] = useState('');
  const [ModalNotification, setModalNotification] = useState(false);
  const [ModalLocation, setModalLocation] = useState(false);
  const [opacity, setopacity] = useState(new Animated.Value(100));
  const modalizeRef = useRef<Modalize>(null);
  const modalizeRefCategory = useRef<Modalize>(null);
  const [activate, setactivate] = useState('activate');
  const [conectedInterner, setconectedInterner] = useState(true);
  const [nodalInternet, setnodalInternet] = useState(false);

  const [riders, setriders] = useState(true);

  const getRidersAvailable = (ciudad: string) => {
    fetch(`${NETWORK_INTERFACE_URL}/riders-available?city=${ciudad}`).then(
      async (respuesta) => {
        const ridersData = await respuesta.json();
        if (ridersData.success) {
          setriders(ridersData.data.length > 0 ? true : false);
        } else {
          setriders(false);
        }
      },
    );
  };

  useEffect(() => {
    getRidersAvailable(city);
  }, [riders]);

  const getPermision = () => {
    Geolocation.getCurrentPosition((info) => {
      setLat(info.coords.latitude);
      setLgn(info.coords.longitude);
      if (!city) {
        SetCity(info.coords.latitude, info.coords.longitude, setcity);
      }
    });
  };

  const isConectedToNet = () => {
    NetInfo.fetch().then((state) => {
      setconectedInterner(state.isConnected);

      if (!state.isConnected) {
        setnodalInternet(true);
      }
    });
  };

  useEffect(() => {
    isConectedToNet();
  }, [nodalInternet]);

  const getUser = async () => {
    getRidersAvailable(city);
    const ids = await AsyncStorage.getItem('id');
    const noti = await AsyncStorage.getItem('notification');
    setactivate(noti ? noti : 'activate');
    setids(ids);
  };

  useEffect(() => {
    getUser();
  }, [ids]);

  const setNotification = async () => {
    const deviceState = await OneSignal.getDeviceState();
    checkNotifications().then(({ status }) => {
      if (status === 'denied') {
        if (activate === 'activate') {
          setModalNotification(true);
        }
      } else if (status === 'blocked') {
        if (activate === 'activate') {
          setModalNotification(true);
        }
      } else if (status === 'unavailable') {
        if (activate === 'activate') {
          setModalNotification(true);
        }
      } else {
        if (deviceState.isSubscribed === false) {
          OneSignal.addTrigger('prompt_ios', 'true');
        }
        if (activate === 'activate') {
          setModalNotification(false);
        }
      }
    });
  };

  const getTransparency = async () => {
    const trackingStatus = await getTrackingStatus();
    if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
    } else {
    }
  };

  const requetPermision = async () => {
    const trackingStatus = await requestTrackingPermission();
    if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
      getTransparency();
    } else {
      getTransparency();
    }
  };

  requetPermision();

  const getId = async () => {
    const localeCode = await AsyncStorage.getItem('localeCode');
    const currecy = await AsyncStorage.getItem('currecy');
    const countrycode = await AsyncStorage.getItem('countryCode');
    const citi = await AsyncStorage.getItem('city');
    setcity(citi);
    setcountryCode(countrycode);
    setlocaleCode(localeCode);
    setcurrecy(currecy);
  };

  useEffect(() => {
    getPermision();
    getId();
    RNLocation.checkPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
      },
    }).then((per) => {
      setisLocation(per);
      if (!per) {
        setModalLocation(true);
      }
    });
  }, []);

  const onClose = (nav: any) => {
    getRidersAvailable(city);
    isConectedToNet();
    refetch();
    getUser();
    ReactNativeHapticFeedback.trigger('selection', optiones);
    modalizeRef.current?.close();
    Navigation.navigate('Feed', {
      data: {
        data: nav,
        city: city,
        lat: lat,
        lgn: lgn,
        localeCode: localeCode,
        currecy: currecy,
        isLocation: isLocation,
        riders: riders,
      },
    });
  };

  const dato = {
    description: 'Restaurante',
    image: 'https://categoryimg.s3.eu-west-3.amazonaws.com/food.png',
    title: 'Comida',
    _id: '5fb7a32cb234a46c09297804',
  };

  const onOpen = () => {
    getRidersAvailable(city);
    isConectedToNet();
    getUser();
    setNotification();
    modalizeRef.current?.open();
    ReactNativeHapticFeedback.trigger('selection', optiones);
    modalizeRefCategory.current.close();
    refetch();
  };

  const onOpenStore = () => {
    getRidersAvailable(city);
    isConectedToNet();
    getUser();
    setNotification();
    modalizeRef.current?.open();
    ReactNativeHapticFeedback.trigger('selection', optiones);
    setCategory(dato);
    refetch();
  };

  const onPenAllCategory = () => {
    getRidersAvailable(city);
    setNotification();
    ReactNativeHapticFeedback.trigger('selection', optiones);
    modalizeRefCategory.current.open();
    refetch();
  };

  const openModal = (items: any) => {
    getRidersAvailable(city);
    getUser();
    setNotification();
    settitleCat(items.title);
    setModalVisible(true);
    refetch();
  };

  const renderContent = () => {
    switch (ruteName) {
      case 'Home':
        return (
          <View>
            <Category
              city={city}
              lat={lat}
              lgn={lgn}
              localeCode={localeCode}
              currecy={currecy}
              id={ids}
              countryCode={countryCode}
              setCategory={setCategory}
              modalizeRef={modalizeRef}
              onPenAllCategory={onPenAllCategory}
              setModalVisible={setModalVisible}
              modalVisible={modalVisible}
              titleCat={titleCat}
              settitleCat={settitleCat}
              setNotification={setNotification}
              getUser={getUser}
              riders={riders}
            />
            <Stores
              city={city}
              localeCode={localeCode}
              currecy={currecy}
              onOpen={onOpenStore}
              id={ids}
              lat={lat}
              lgn={lgn}
              isLocation={isLocation}
              setNotification={setNotification}
              riders={riders}
              getRidersAvailable={getRidersAvailable}
              getAdress={null}
              setcity={setcity}
            />
          </View>
        );
      case 'Order':
        return (
          <Order
            city={city}
            id={ids}
            lat={lat}
            lgn={lgn}
            localeCode={localeCode}
            currecy={currecy}
            riders={riders}
          />
        );

      case 'Account':
        return (
          <MyAccount
            city={city}
            id={ids}
            lat={lat}
            lgn={lgn}
            localeCode={localeCode}
            currecy={currecy}
            setcity={setcity}
            riders={riders}
            getRidersAvailable={getRidersAvailable}
          />
        );
      case 'Favourites':
        return (
          <Favourite
            city={city}
            id={ids}
            lat={lat}
            lgn={lgn}
            localeCode={localeCode}
            currecy={currecy}
            riders={riders}
          />
        );
    }
  };

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const changeTap = (rut: string) => {
    getRidersAvailable(city);
    setNotification();
    isConectedToNet();
    getUser();
    if (ids) {
      //@ts-ignore
      ReactNativeHapticFeedback.trigger(type, optiones);
      setruteName(rut);
    } else {
      setNotification();
      Navigation.navigate('Login', 0);
    }
  };

  return (
    <View style={styles.container}>
      <Header
        setopacity={setopacity}
        city={city}
        setcity={setcity}
        id={ids}
        lat={lat}
        lgn={lgn}
        localeCode={localeCode}
        currecy={currecy}
        getPermision={getPermision}
        setNotification={setNotification}
        isLocation={isLocation}
        riders={riders}
        getRidersAvailable={getRidersAvailable}
        content={() => {
          return (
            <View>
              {!isLocation && ruteName === 'Home' ? (
                <NoLocation
                  setcity={setcity}
                  setisLocation={setisLocation}
                  setModalLocation={setModalLocation}
                />
              ) : (
                <View>{renderContent()}</View>
              )}
            </View>
          );
        }}
      />
      <ButtonBar
        onPressHome={() => changeTap('Home')}
        onPressOrder={() => changeTap('Order')}
        onPressFavourites={() => changeTap('Favourites')}
        onPressAccount={() => changeTap('Account')}
        ruteName={ruteName}
        id={ids}
        opacity={opacity}
        city={city}
      />
      <ModalCategory
        city={city}
        onClose={onClose}
        data={category}
        modalizeRef={modalizeRef}
        riders={riders}
      />
      <ModalAllCategory
        modalizeRefCategory={modalizeRefCategory}
        onOpen={onOpen}
        id={ids}
        setCategory={setCategory}
        openModal={openModal}
        riders={riders}
      />
      <Notification
        visibleModal={ModalNotification}
        setModalVisible={setModalNotification}
      />
      <Location
        visibleModal={ModalLocation}
        setModalVisible={setModalLocation}
        setisLocation={setisLocation}
        setcity={setcity}
        getRidersAvailable={getRidersAvailable}
      />
      <NoInternet
        visibleModal={nodalInternet}
        setModalVisible={setnodalInternet}
        isINternet={conectedInterner}
        setconectedInterner={setconectedInterner}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});

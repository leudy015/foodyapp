import React, { useState } from 'react';
import { View, Platform, Alert } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import { Modalize } from 'react-native-modalize';
import { Button } from '../../Components/Button';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import DatePicker from 'react-native-date-picker';
import CheckBox from '@react-native-community/checkbox';
import { selectHour } from '../../Utils/selectHour';
import { scheduleTime } from '../../Utils/scheduleTime';

const jsCoreDateCreator = (dateString: string) => {
  let dateParam = dateString.split(/[\s-:]/);
  dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString();
  //@ts-ignore
  return new Date(...dateParam);
};

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const opciones = [
  {
    id: 1,
    title: 'Lo antes posible',
    icon: 'enviromento',
    type: 'AntDesign',
  },
  {
    id: 2,
    title: 'Programar entrega',
    icon: 'clockcircleo',
    type: 'AntDesign',
  },
];

export default function ModalSelecDate(props: any) {
  const { res, setDate, setSelected, date, selecte, modalizeRef } = props;

  const colorsText = {
    light: colors.black,
    dark: colors.white,
  };

  const data = res.schedule;

  const mode = useColorSchemeContext();
  const backgroundColor = colorsText[mode];

  const scheduleOnly = res.scheduleOnly
    ? res.scheduleOnly
    : { available: false, hour: 0 };

  const [handle, setHandle] = useState(false);
  const styles = useDynamicValue(dynamicStyles);
  const [toggle] = useState(true);

  const HEADER_HEIGHT = 300;

  const onClose = () => {
    ReactNativeHapticFeedback.trigger('selection', options);
    if (selecte === 'Lo antes posible') {
      modalizeRef.current?.close();
    } else {
      if (!date) {
        ReactNativeHapticFeedback.trigger('selection', options);
        Alert.alert('Debes seleccionar una fecha y hora para continuar');
      } else {
        modalizeRef.current?.close();
      }
    }
  };

  const onSeleAhora = (items: any) => {
    ReactNativeHapticFeedback.trigger('selection', options);
    if (items === 'Programar entrega') {
      setSelected(items);
    } else {
      setSelected(items);
      setDate(null);
    }
  };

  const handlePosition = (position) => {
    setHandle(position === 'top');
  };

  //@ts-ignore
  const gettheTime = selectHour(data).t;

  //@ts-ignore
  const gettheTimeClose = selectHour(data).c;

  const finis = selectHour(data).finis;

  const moreDay: number = finis || scheduleOnly.hour > 0 ? 1 : 0;

  const moremonth =
    scheduleTime(data) && res.open && scheduleOnly.available
      ? 2
      : scheduleTime(data)
      ? 2
      : 1;

  const d = new Date();
  const day = d.getDate() + moreDay;
  const year = d.getFullYear();
  const month = d.getMonth() + 1;
  const month2 = d.getMonth() + moremonth;

  const o = month > 9 ? '' : '0';
  const o2 = month2 > 9 ? '' : '0';

  //@ts-ignore
  const opentime = `${gettheTime.getHours()}-${gettheTime.getMinutes()}`;
  const closetime = `${gettheTimeClose.getHours()}-${gettheTimeClose.getMinutes()}`;

  const min = jsCoreDateCreator(`${year}-${o + month}-${day}-${opentime}`);
  const min1 = jsCoreDateCreator(
    `${year}-${o + month}-${day}-${d.getHours() + 1}-${d.getMinutes()}`,
  );

  const max = jsCoreDateCreator(`${year}-${o2 + month2}-${day}-${closetime}`);
  const max1 = jsCoreDateCreator(
    `${year}-${o2 + month2}-${day}-${d.getHours()}-${d.getMinutes()}`,
  );

  return (
    <Modalize
      ref={modalizeRef}
      adjustToContentHeight={toggle}
      snapPoint={HEADER_HEIGHT}
      scrollViewProps={{ showsVerticalScrollIndicator: false }}
      modalStyle={styles.mo}
      withHandle={handle}
      modalTopOffset={100}
      handlePosition="inside"
      handleStyle={{
        top: 25,
        width: 40,
        height: handle ? 6 : 6,
        backgroundColor: colors.main,
      }}
      onPositionChange={handlePosition}>
      <View style={styles.modalConten}>
        <View
          style={{
            alignSelf: 'center',
            marginTop: 50,
          }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={stylesText.mainText}>
            Programar entrega
          </CustomText>
        </View>
        <View
          style={{
            marginTop: 30,
          }}>
          {opciones.map((d, e) => (
            <View key={e} style={styles.items}>
              <View style={{ flexDirection: 'row', padding: 15 }}>
                <Icon
                  name={d.icon}
                  type="AntDesign"
                  size={22}
                  color={colors.rgb_153}
                  style={{ marginRight: 15 }}
                />
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={stylesText.titleText200}>
                  {d.title}
                </CustomText>
              </View>
              <CheckBox
                key={e}
                value={d.title === selecte ? true : false}
                disabled={
                  scheduleOnly.available && d.title === 'Lo antes posible'
                }
                onValueChange={() => onSeleAhora(d.title)}
                onTintColor={colors.main}
                tintColors={{ true: colors.main, false: colors.rgb_153 }}
                onCheckColor={colors.main}
                onAnimationType="fill"
                offAnimationType="fill"
              />
            </View>
          ))}
        </View>
        {selecte === 'Programar entrega' ? (
          <View
            style={{
              width: dimensions.ScreenWidth,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <DatePicker
              date={date ? date : new Date()}
              onDateChange={setDate}
              mode="datetime"
              androidVariant={
                Platform.OS === 'android' ? 'nativeAndroid' : 'iosClone'
              }
              minimumDate={scheduleTime(data) ? min1 : min}
              maximumDate={scheduleTime(data) ? max1 : max}
              locale="es"
              textColor={backgroundColor}
            />
          </View>
        ) : null}
        <View style={styles.signupButtonContainer}>
          <Button
            light={colors.white}
            dark={colors.white}
            containerStyle={[
              styles.buttonView,
              { backgroundColor: colors.main },
            ]}
            onPress={() => onClose()}
            title="Confirmar"
            titleStyle={styles.buttonTitle}
          />
        </View>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    alignSelf: 'flex-start',
  },

  modalConten: {
    flex: 1,
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 12,
    borderTopStartRadius: 12,
  },

  mo: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  signupButtonContainer: {
    zIndex: 1000,
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
    marginBottom: dimensions.Height(5),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  items: {
    width: dimensions.Width(96),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

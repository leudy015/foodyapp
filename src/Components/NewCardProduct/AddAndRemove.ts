import { Platform } from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Navigation from '../../services/Navigration';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export const revoveItemInCart = (
  inCartItem,
  setinCartItem,
  product,
  setModalVisible?,
  quantity?,
) => {
  //@ts-ignore
  ReactNativeHapticFeedback.trigger(type, optiones);
  var i = inCartItem.findIndex((x: any) => x.id === product);
  if (i !== -1) {
    inCartItem.splice(i, 1);
    setinCartItem(inCartItem.concat());
    if (setModalVisible) {
      setModalVisible(false);
    }

    if (quantity === 1) {
      Navigation.goBack();
    }
  }
};

export const AddItemsToCadt = (
  inCartItem,
  setinCartItem,
  product,
  input: any,
  setModalVisible?,
  quantity?,
) => {
  //@ts-ignore
  ReactNativeHapticFeedback.trigger(type, optiones);
  if (inCartItem.filter((e) => e.id === input.id).length > 0) {
    revoveItemInCart(
      inCartItem,
      setinCartItem,
      input.id,
      setModalVisible,
      quantity,
    );
    const inp = [...inCartItem, input];
    const cart = JSON.stringify(inp);
    setinCartItem(JSON.parse(cart));
    if (setModalVisible) {
      setModalVisible(false);
    }
  } else {
    const inp = [...inCartItem, input];
    const cart = JSON.stringify(inp);
    setinCartItem(JSON.parse(cart));
    if (setModalVisible) {
      setModalVisible(false);
    }
  }
};

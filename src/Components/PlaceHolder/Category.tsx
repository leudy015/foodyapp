import React from 'react';
import { View, Dimensions } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions } from '../../theme';

const height = Dimensions.get('window').height;

console.log(height);

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <View style={{ alignSelf: 'center', backgroundColor: 'transparent' }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(95),
            marginHorizontal: 20,
            marginBottom: 20,
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View>
            <View
              style={{
                width: dimensions.Width(28),
                height: dimensions.Width(28),
                borderRadius: 200,
              }}
            />
          </View>

          <View>
            <View
              style={{
                width: dimensions.Width(28),
                height: dimensions.Width(28),
                borderRadius: 200,
              }}
            />
          </View>

          <View>
            <View
              style={{
                width: dimensions.Width(28),
                height: dimensions.Width(28),
                borderRadius: 200,
              }}
            />
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

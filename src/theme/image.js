import Landing from '../Assets/Image/landing.png';
import Landing1 from '../Assets/Image/landing1.png';
import Landing2 from '../Assets/Image/landing2.png';
import Landing3 from '../Assets/Image/landing3.png';
import Google from '../Assets/Image/google.png';
import FaceBook from '../Assets/Image/facebook.png';
import Check from '../Assets/Image/check.png';
import Card from '../Assets/Image/Card.png';
import Card2 from '../Assets/Image/Card2.png';
import Card3 from '../Assets/Image/Card3.png';
import Card4 from '../Assets/Image/Card4.png';
import Wallet from '../Assets/Image/wallet.png';
import Cupones from '../Assets/Image/cupon.png';
import Visa from '../Assets/Image/visa.jpg';
import MasterCard from '../Assets/Image/master.png';
import Paypal from '../Assets/Image/paypal.png';
import AmericanExpress from '../Assets/Image/americam.png';
import NoComent from '../Assets/Image/nocoment.png';
import Notification from '../Assets/Image/notification.png';
import Heart from '../Assets/Image/heart.png';
import Share from '../Assets/Image/share.png';
import Order from '../Assets/Image/order.png';
import Nodata from '../Assets/Image/nodata.png';
import NoIternet from '../Assets/Image/nointernet.png';
import NoResult from '../Assets/Image/noresult.png';
import Camera from '../Assets/Image/camara.png';
import Hola from '../Assets/Image/hola.png';
import FARM from '../Assets/Image/FARM.png';
import Hecho from '../Assets/Image/hecho.png';
import Error from '../Assets/Image/error.png';
import Riders from '../Assets/Image/riders.png';
import LogoBlack from '../Assets/Image/lo.png';
import LogoWhite from '../Assets/Image/logowhite.png';
import Super from '../Assets/Image/super.png';
import ParaFarm from '../Assets/Image/parafarm.png';
import Shop from '../Assets/Image/shop.png';
import NoImagen from '../Assets/Image/noimagen.png';
import NoImagenBlack from '../Assets/Image/noimagenB.png';
import PlaceHolder from '../Assets/Image/placeholder.png';
import Icons from '../Assets/Image/icons.png';
import Location from '../Assets/Image/location.png';
import Main from '../Assets/Image/main.jpg';
import Curve from '../Assets/Image/curve.png';
import CurveDark from '../Assets/Image/curvedark.png';
import Loquesea from '../Assets/Image/loquesea.png';
import Scooter from '../Assets/Image/scooter.png';
import ScooterWhite from '../Assets/Image/scooterwithe.png';
import Line from '../Assets/Image/line.png';
import Mapa from '../Assets/Image/mapa.png';
import PaymentAccept from '../Assets/Image/cards.png';
import Original from '../Assets/Image/icons/original.png';
import Black from '../Assets/Image/icons/black.png';
import White from '../Assets/Image/icons/white.png';
import Mora from '../Assets/Image/icons/mora.png';
import Lgtbi from '../Assets/Image/icons/lgtbi.png';
import Trans from '../Assets/Image/icons/trans.png';
import Icono from '../Assets/Image/icono.png';
import Twitter from '../Assets/Image/Twitter-Logo.png';
import Maping from '../Assets/Image/maping.png';
import Maping_Shop from '../Assets/Image/maping-shop.png';
import FreeCredit from '../Assets/Image/free.png';
import RestaurantIcon from '../Assets/Image/restaurantes.png';
import StoreIcon from '../Assets/Image/storeicon.png';
import SaveIcon from '../Assets/Image/saveIcon.png';
import FarmacyIcon from '../Assets/Image/farmacyIcon.png';
import Logo from '../Assets/Image/logoblack.png';
import PlaceSuper from '../Assets/Image/placesuper.png';

export const image = {
  Landing,
  Landing1,
  Landing2,
  Google,
  FaceBook,
  Check,
  Card,
  Wallet,
  Cupones,
  Visa,
  MasterCard,
  Paypal,
  AmericanExpress,
  Card2,
  Card3,
  Card4,
  NoComent,
  Notification,
  Heart,
  Share,
  Order,
  Nodata,
  NoIternet,
  NoResult,
  Camera,
  Hola,
  Landing3,
  FARM,
  Hecho,
  Error,
  Riders,
  LogoBlack,
  LogoWhite,
  Super,
  ParaFarm,
  Shop,
  NoImagen,
  NoImagenBlack,
  PlaceHolder,
  Icons,
  Location,
  Main,
  Curve,
  CurveDark,
  Loquesea,
  Scooter,
  ScooterWhite,
  Line,
  Mapa,
  PaymentAccept,
  Original,
  Black,
  White,
  Mora,
  Lgtbi,
  Trans,
  Icono,
  Twitter,
  Maping,
  Maping_Shop,
  FreeCredit,
  RestaurantIcon,
  StoreIcon,
  SaveIcon,
  FarmacyIcon,
  Logo,
  PlaceSuper,
};

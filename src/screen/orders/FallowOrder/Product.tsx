import React from 'react';
import {
  View,
  FlatList,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import { dimensions, colors, stylesText, image } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { formaterPrice } from '../../../Utils/formaterPRice';
import Complement from '../../../Components/NewCardProduct/complement';
import Icon from 'react-native-dynamic-vector-icons';

export default function Products(props: any) {
  const { item, localeCode, currecy, OpenURLButton } = props;
  const styles = useDynamicValue(dynamicStyles);

  const barcolor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const barColor = barcolor[mode];

  const renderItems = ({ items }) => {
    return (
      <View
        style={[
          styles.cont,
          {
            borderBottomColor: barColor,
            borderTopColor: barColor,
            alignItems: 'flex-start',
          },
        ]}>
        <View>
          {items.imageUrl ? (
            <ImageBackground
              source={image.PlaceHolder}
              imageStyle={{ borderRadius: 100 }}
              resizeMode="cover"
              style={{
                width: 50,
                height: 50,
              }}>
              <Image
                source={{ uri: items.imageUrl }}
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 100,
                  resizeMode: 'cover',
                }}
              />
            </ImageBackground>
          ) : null}
        </View>
        <View style={{ marginLeft: 10 }}>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              { width: dimensions.Width(65) },
            ]}>
            <CustomText
              numberOfLines={1}
              light={colors.main}
              dark={colors.main}
              style={stylesText.secondaryTextBold}>
              {items.quantity} ×{' '}
            </CustomText>
            {items.name}
          </CustomText>
          <CustomText
            numberOfLines={3}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              { marginTop: 5, width: dimensions.Width(65), marginBottom: 10 },
            ]}>
            {items.description}
          </CustomText>
          <Complement localeCode={localeCode} currecy={currecy} data={items} />
          <CustomText
            numberOfLines={1}
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryTextBold, { marginTop: 15 }]}>
            {formaterPrice(items.price / 100, localeCode, currecy)}
          </CustomText>
        </View>
      </View>
    );
  };

  return (
    <View>
      <View
        style={[
          styles.cont,
          {
            borderBottomColor: barColor,
            borderTopColor: barColor,
            marginTop: 20,
            marginBottom: 20,
          },
        ]}>
        <View>
          <ImageBackground
            source={image.PlaceHolder}
            imageStyle={{ borderRadius: 100 }}
            resizeMode="cover"
            style={{
              width: 50,
              height: 50,
            }}>
            <Image
              source={{ uri: item.storeData.image }}
              style={{
                width: 50,
                height: 50,
                borderRadius: 100,
              }}
            />
          </ImageBackground>
        </View>
        <View style={{ marginLeft: 10 }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: dimensions.Width(80),
            }}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                { width: dimensions.Width(50) },
              ]}>
              {item.storeData.title}
            </CustomText>
            <TouchableOpacity
              style={styles.btns}
              onPress={() => OpenURLButton(`tel:${item.storeData.phone}`)}>
              <CustomText
                numberOfLines={1}
                style={[
                  stylesText.secondaryText,
                  {
                    color: colors.main,
                  },
                ]}>
                <Icon
                  name="phone"
                  type="Feather"
                  size={12}
                  color={colors.main}
                />{' '}
                Llamar
              </CustomText>
            </TouchableOpacity>
          </View>

          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              { marginTop: 10, width: dimensions.Width(70) },
            ]}>
            {item.storeData.adress.calle}, {item.storeData.adress.numero},
            {item.storeData.adress.codigoPostal}, {item.storeData.adress.ciudad}
            ,
          </CustomText>
        </View>
      </View>
      <View>
        <FlatList
          data={item.items}
          renderItem={(items: any) => renderItems(items.item)}
          keyExtractor={(items: any) => items._id}
          scrollEnabled={false}
        />
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: dimensions.ScreenWidth,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
  },

  btns: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
    paddingVertical: 5,
    borderRadius: 5,
    marginRight: 15,
    width: 100,
  },
});

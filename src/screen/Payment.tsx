import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  ScrollView,
  FlatList,
  Image,
  Platform,
  Alert,
  ImageBackground,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import Header from '../Components/Header';
import { useNavigationParam } from 'react-navigation-hooks';
import { stylesText } from '../theme/TextStyle';
import { NETWORK_INTERFACE_URL, STRIPE_CLIENT } from '../Config/config';
import { dimensions, colors, image } from '../theme';
import FieldCard from './Card';
import { Avatar } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Navigation from '../services/Navigration';
import Loader from '../Components/ModalLoading';
import Icon from 'react-native-dynamic-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import CardPayment from '../Components/CardPaiment';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import ApplePay from '../Components/applePayBotton';
import { formaterPrice } from '../Utils/formaterPRice';
import stripes from 'tipsi-stripe';
import { ModalWebView } from '../Components/WebViewMOdal/webViewModal';
import PaymentsSafe from '../Components/paymentssafe';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const Payment = () => {
  const modals = Array.from({ length: 1 }).map((_) => useRef(null).current);
  const styles = useDynamicValue(dynamicStyles);
  const datas = useNavigationParam('data');
  const [cards, setcards] = useState([]);
  const [Loding, setLoading] = useState(false);
  const [customer, setCustomer] = useState('');
  const [ids, setID] = useState(null);
  const [fromPaypal, setFromPaypal] = useState(false);
  const [selecCard, setselecCard] = useState('');
  const [countryCode, setcountryCode] = useState('');

  useEffect(() => {
    const getId = async () => {
      const countrycode = await AsyncStorage.getItem('countryCode');
      setcountryCode(countrycode);
      const id = await AsyncStorage.getItem('id');
      setID(id);
    };
    getId();
  }, [ids]);

  const todo = datas.total;

  const totalStripe = Number(todo);

  const total = todo / 100;

  const localeCode = datas.localeCode;
  const currecy = datas.currecy;
  const user = datas.user;
  const stripe = datas.stripe;
  const restaurant = datas.data.restaurants;
  const result = datas.results;

  const getCard = async () => {
    setLoading(true);
    let res = await fetch(
      `${NETWORK_INTERFACE_URL}/get-card?customers=${stripe}`,
    );
    const card = await res.json();
    setcards(card);
    if (res) {
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    getCard();
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  const METHOD_DATA = [
    {
      supportedMethods: ['apple-pay'],
      data: {
        merchantIdentifier: 'merchant.com.wilbby',
        supportedNetworks: ['visa', 'mastercard', 'amex'],
        countryCode: countryCode,
        currencyCode: currecy,
        paymentMethodTokenizationParameters: {
          parameters: {
            gateway: 'stripe',
            'stripe:publishableKey': STRIPE_CLIENT,
          },
        },
      },
    },
  ];

  const DETAILS = {
    id: 'wilbby',
    displayItems: [
      {
        label: restaurant.title,
        amount: {
          currency: currecy,
          value: String(Number(total)),
        },
      },
    ],
    total: {
      label: 'Wilbby',
      amount: {
        currency: currecy,
        value: String(Number(total)),
      },
    },
  };

  const _renderItem = ({ item }) => {
    let images = '';
    let brand = '';
    switch (item.card.brand) {
      case 'visa':
        images = image.Visa;
        brand = 'Visa';
        break;
      case 'mastercard':
        images = image.MasterCard;
        brand = 'Master Card';
        break;
      case 'amex':
        images = image.AmericanExpress;
        brand = 'American Express';
        break;
    }

    return (
      <CardPayment
        item={item}
        images={images}
        brand={brand}
        selecCard={selecCard}
        onPress={() => {
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          if (selecCard) {
            setselecCard('');
            setCustomer('');
            if (selecCard != item.id) {
              setselecCard(item.id);
              setCustomer(item.customer);
            }
          } else {
            setselecCard(item.id);
            setCustomer(item.customer);
          }
        }}
      />
    );
  };

  const paymentApple = async () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    if (Platform.OS === 'ios') {
      //@ts-ignore
      const paymentRequest = new PaymentRequest(METHOD_DATA, DETAILS);
      paymentRequest
        //@ts-ignore
        .show()
        .then(async (paymentResponse) => {
          const { paymentToken } = paymentResponse.details;
          const data = {
            stripeToken: paymentToken,
            //@ts-ignore
            amount: totalStripe,
            orders: result._id,
            userID: user._id,
            currency: currecy,
          };
          await fetch(`${NETWORK_INTERFACE_URL}/stripe/chargeToken/order`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
          }).then(async (res) => {
            const response = await res.json();
            if (response.status === 'succeeded') {
              ReactNativeHapticFeedback.trigger(
                'notificationSuccess',
                optiones,
              );
              paymentResponse.complete('success');
              setLoading(false);
              Navigation.navigate('Thank', { data: user._id });
            } else {
              setLoading(false);
              ReactNativeHapticFeedback.trigger(
                'notificationSuccess',
                optiones,
              );
              Navigation.navigate('Error', 0);
            }
          });
        })
        .catch((e) => console.log(e));
    }
  };

  const proccessPayment = async (paypa: boolean) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    setLoading(true);
    const input = {
      card: selecCard,
      customers: customer,
      amount: totalStripe,
      currency: currecy,
      order: result._id,
    };

    const input1 = {
      order: result._id,
      userID: user._id,
    };

    if (paypa) {
      setLoading(false);
      modals[0].open();
    } else {
      let res = await fetch(`${NETWORK_INTERFACE_URL}/payment-auth-new`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(input),
      });
      const payments = await res.json();
      const { status, client_secret } = payments;
      if (status === 'requires_action') {
        setLoading(false);
        let authResponse: any;
        try {
          authResponse = await stripes.authenticatePaymentIntent({
            clientSecret: client_secret,
          });

          if (authResponse.status === 'succeeded') {
            setLoading(false);
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            let resp = await fetch(`${NETWORK_INTERFACE_URL}/clean-cart`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(input1),
            });
            const clear = await resp.json();
            if (clear.success) {
              setLoading(false);
              Navigation.navigate('Thank', { data: user._id });
            } else {
              setLoading(false);
              ReactNativeHapticFeedback.trigger(
                'notificationSuccess',
                optiones,
              );
              Navigation.navigate('Error', 0);
            }
          }
        } catch (error) {
          Alert.alert('Algo salió mal intentalo de nuevo por favor');
        }
      } else if (status === 'succeeded') {
        setLoading(false);
        let resp = await fetch(`${NETWORK_INTERFACE_URL}/clean-cart`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(input1),
        });
        const clear = await resp.json();
        if (clear.success) {
          setLoading(false);
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          Navigation.navigate('Thank', { data: user._id });
        } else {
          setLoading(false);
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          Navigation.navigate('Error', 0);
        }
      } else {
        setLoading(false);
        ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
        Navigation.navigate('Error', 0);
      }
    }
  };

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const modes = useColorSchemeContext();
  const borderColot = backgroundColors[modes];

  const primary = {
    light: colors.white,
    dark: colors.black,
  };
  const modos = useColorSchemeContext();
  const primario = primary[modos];

  const secundary = {
    light: colors.colorInput,
    dark: colors.back_dark,
  };
  const modo = useColorSchemeContext();
  const secundario = secundary[modo];

  return (
    <View style={styles.container}>
      <Loader loading={Loding} color={colors.main} />
      <Header title="Completar el pago" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            marginBottom: fromPaypal
              ? dimensions.Height(8)
              : dimensions.Height(15),
          }}>
          <View
            style={[
              styles.cardinfo,
              {
                borderColor: borderColot,
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: 20,
              },
            ]}>
            <View>
              <ImageBackground
                source={image.PlaceHolder}
                style={styles.logo}
                imageStyle={{ borderRadius: 100 }}
                resizeMode="cover">
                <Image source={{ uri: restaurant.logo }} style={styles.logo} />
              </ImageBackground>
            </View>
            <View>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryTextBold]}>
                {restaurant.title}
              </CustomText>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[stylesText.terciaryText, { paddingBottom: 5 }]}>
                {restaurant.city}
              </CustomText>
            </View>
          </View>

          {!fromPaypal ? (
            <>
              <CustomText
                numberOfLines={1}
                ligth={colors.black}
                dark={colors.white}
                style={[
                  stylesText.titleText,
                  { marginBottom: 20, marginTop: 30, marginLeft: 15 },
                ]}>
                Tarjetas guardada
              </CustomText>
              <FlatList
                // @ts-ignore
                data={cards ? cards.data : []}
                renderItem={(item: any) => _renderItem(item)}
                keyExtractor={(item: any) => item.id}
                showsVerticalScrollIndicator={false}
                ListEmptyComponent={
                  <FieldCard
                    clientID={stripe}
                    getCard={getCard}
                    fromPaypal={fromPaypal}
                    setFromPaypal={setFromPaypal}
                    setLoading={setLoading}
                    primarycolor={primario}
                    secudandyColor={secundario}
                  />
                }
              />
            </>
          ) : null}

          {Platform.OS === 'ios' ? (
            <View>
              <CustomText
                numberOfLines={1}
                ligth={colors.black}
                dark={colors.white}
                style={[
                  stylesText.titleText,
                  { marginBottom: 20, marginTop: 30, marginLeft: 15 },
                ]}>
                Apple pay
              </CustomText>
              <ApplePay
                onPress={() => paymentApple()}
                border={10}
                padding={20}
                noTop={true}
              />
            </View>
          ) : null}

          {!selecCard ? (
            <>
              <CustomText
                numberOfLines={1}
                ligth={colors.black}
                dark={colors.white}
                style={[
                  stylesText.titleText,
                  { marginBottom: 20, marginTop: 30, marginLeft: 15 },
                ]}>
                Paypal
              </CustomText>
              <TouchableOpacity
                style={[styles.cardinfo, { borderColor: borderColot }]}
                onPress={() => {
                  setFromPaypal(!fromPaypal);
                  ReactNativeHapticFeedback.trigger(
                    'notificationSuccess',
                    optiones,
                  );
                }}>
                <View
                  style={{
                    alignSelf: 'center',
                    marginLeft: dimensions.Width(4),
                  }}>
                  <Avatar rounded size={50} source={image.Paypal} />
                </View>
                <View
                  style={{
                    alignSelf: 'center',
                    marginLeft: dimensions.Width(4),
                  }}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.white}
                    style={[stylesText.mainText, { fontWeight: 'bold' }]}>
                    Selecciona Paypal
                  </CustomText>
                  <CustomText light={colors.rgb_153} dark={colors.rgb_153}>
                    Seleccionar paypal para pagar
                  </CustomText>
                </View>
                <View
                  style={{
                    alignSelf: 'center',
                    marginLeft: 'auto',
                    marginRight: dimensions.Width(4),
                  }}>
                  {fromPaypal ? (
                    <Icon
                      name="checkcircle"
                      type="AntDesign"
                      size={20}
                      color={colors.green}
                    />
                  ) : null}
                </View>
              </TouchableOpacity>
            </>
          ) : null}

          <CustomText
            numberOfLines={1}
            ligth={colors.black}
            dark={colors.white}
            style={[
              stylesText.titleText,
              { marginBottom: 20, marginTop: 30, marginLeft: 15 },
            ]}>
            Añadir tarjeta
          </CustomText>

          <FieldCard
            clientID={stripe}
            getCard={getCard}
            fromPaypal={fromPaypal}
            setFromPaypal={setFromPaypal}
            setLoading={setLoading}
            primarycolor={primario}
            secudandyColor={secundario}
          />
          <PaymentsSafe />
        </View>
      </ScrollView>
      <View
        style={[
          styles.selected,
          { backgroundColor: !selecCard ? colors.rgb_153 : colors.main },
        ]}>
        {!selecCard && !fromPaypal ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              width: dimensions.Width(94),
              height: 60,
              borderRadius: 100,
            }}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              numberOfLines={1}
              style={stylesText.secondaryTextBold}>
              Pagar pedido total {formaterPrice(total, localeCode, currecy)}
            </CustomText>
          </View>
        ) : (
          <TouchableOpacity
            onPress={() => proccessPayment(fromPaypal)}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              width: dimensions.Width(94),
              height: 60,
              borderRadius: 15,
              backgroundColor: colors.main,
            }}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              numberOfLines={1}
              style={stylesText.secondaryTextBold}>
              Pagar pedido total {formaterPrice(total, localeCode, currecy)}
            </CustomText>
          </TouchableOpacity>
        )}
      </View>
      <ModalWebView
        //@ts-ignore
        fromPaymet={true}
        id={user._id}
        ref={(el) => (modals[0] = el)}
        //@ts-ignore
        urlPay={`${NETWORK_INTERFACE_URL}/paypal?price=${total}&orderid=${result._id}&userId=${ids}&currency=${currecy}`}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.colorInput, 'black'),
    flex: 1,
  },

  crecadr: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: 'transparent',
    padding: dimensions.Width(3),
    marginHorizontal: dimensions.Width(2),
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  noty: {
    marginLeft: 'auto',
    marginRight: dimensions.Width(0),
    backgroundColor: colors.white,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  centeredView: {
    flex: 1,
    backgroundColor: new DynamicValue('white', 'black'),
  },

  logo: {
    width: 60,
    height: 60,
    marginRight: 15,
    borderRadius: 50,
  },

  selected: {
    width: dimensions.Width(94),
    height: 60,
    borderRadius: 15,
    bottom: 20,
    position: 'absolute',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  checkBox: {
    width: 25,
    height: 25,
    marginLeft: 'auto',
    borderRadius: 50,
    borderWidth: 2,
    borderColor: colors.rgb_235,
    marginTop: 15,
  },

  cardinfo: {
    width: dimensions.Width(92),
    height: 'auto',
    paddingVertical: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(1),
    marginBottom: dimensions.Height(1),
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 0.5,
  },
});

export default Payment;

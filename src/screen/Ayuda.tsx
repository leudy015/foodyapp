import React from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Linking,
  Alert,
  Platform,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import Header from '../Components/Header';
import { colors, dimensions } from '../theme';
import { stylesText } from '../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import { useTranslation } from 'react-i18next';
import SafariView from 'react-native-safari-view';
import Navigation from '../services/Navigration';

const Ayuda = () => {
  const { t } = useTranslation();
  const styles = useDynamicValue(dynamicStyles);

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const openWeb = (url) => {
    if (Platform.OS === 'ios') {
      SafariView.show({
        url: url,
      });
    } else {
      Navigation.navigate('Web', { data: url });
    }
  };

  return (
    <View style={styles.container}>
      <Header title="Ayuda y contacto" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.nav}>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => OpenURLButton('mailto:info@wilbby.com')}>
            <Icon name="mail" type="AntDesign" size={18} style={styles.icons} />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              Contactar por email
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separator} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => OpenURLButton(`tel:+34669124487`)}>
            <Icon
              name="phone"
              type="AntDesign"
              size={18}
              style={styles.icons}
            />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              Ayuda teléfonica
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separator} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => OpenURLButton('mailto:info@wilbby.com')}>
            <Icon
              name="plussquareo"
              type="AntDesign"
              size={18}
              style={styles.icons}
            />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              Que quieres ver nuevo en Wilbby
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separator} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => OpenURLButton('mailto:info@wilbby.com')}>
            <Icon name="tool" type="AntDesign" size={18} style={styles.icons} />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              Comentar error técnico
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
        </View>

        <View style={styles.nav}>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => openWeb('https://wilbby.com')}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              {t('profile:about')}
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => openWeb('https://wilbby.com/preguntas-frecuentes')}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              {t('profile:q&a')}
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => openWeb('https://wilbby.com/riders')}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              {t('profile:riders')}
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => openWeb('https://stores.wilbby.com')}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              {t('profile:parnert')}
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => openWeb('https://blog.wilbby.com')}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              {t('profile:blog')}
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => openWeb('https://wilbby.com/condiciones-de-uso')}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              {t('profile:condition')}
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() =>
              openWeb('https://wilbby.com/politica-de-privacidad')
            }>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              {t('profile:privacity')}
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
        </View>

        <View style={[styles.nav, { marginBottom: dimensions.Height(10) }]}>
          <View style={styles.separators} />
          <TouchableOpacity
            style={styles.item}
            onPress={() =>
              OpenURLButton('https://www.facebook.com/wilbbyapp.es')
            }>
            <Icon
              name="facebook-square"
              type="AntDesign"
              size={18}
              style={styles.icons}
            />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              Seguir en Facebook
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separator} />
          <TouchableOpacity
            style={styles.item}
            onPress={() =>
              OpenURLButton(`https://www.instagram.com/wilbby_es`)
            }>
            <Icon
              name="instagram"
              type="AntDesign"
              size={18}
              style={styles.icons}
            />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              Seguir en Instagram
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separator} />
          <TouchableOpacity
            style={styles.item}
            onPress={() => OpenURLButton('https://twitter.com/wilbby_ES')}>
            <Icon
              name="twitter"
              type="AntDesign"
              size={18}
              style={styles.icons}
            />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              Seguir en Twitter
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separator} />
          <TouchableOpacity
            style={styles.item}
            onPress={() =>
              OpenURLButton('https://www.linkedin.com/company/wilbbyapp')
            }>
            <Icon
              name="linkedin-square"
              type="AntDesign"
              size={18}
              style={styles.icons}
            />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText200}>
              Seguir en Linkedin
            </CustomText>
            <CustomText
              style={{
                marginLeft: 'auto',
                marginRight: 15,
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={18}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
          <View style={styles.separators} />
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    flex: 1,
  },

  infoContact: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: dimensions.Height(5),
  },

  nav: {
    marginTop: dimensions.Height(6),
  },
  item: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 15,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: dimensions.Width(4),
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
    marginLeft: 'auto',
  },

  separators: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  icons: {
    color: new DynamicValue(colors.black, colors.white),
    marginRight: 10,
  },
});

export default Ayuda;

import React from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  Image,
  ImageBackground,
} from 'react-native';
import { Query } from 'react-apollo';
import { query } from '../../GraphQL';
import { dimensions, colors, image, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { useTranslation } from 'react-i18next';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import Loading from '../../Components/PlaceHolder/CategoryLoading';
import * as Animatable from 'react-native-animatable';

const PlaceHolder = require('./imges/placeholder.png');

export default function Category(props: any) {
  const { active, onPress } = props;
  const styles = useDynamicValue(dynamicStyles);
  const { t } = useTranslation();
  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity activeOpacity={100} onPress={() => onPress(item)}>
        <Animatable.View
          animation="flipInY"
          style={[styles.card]}
          duration={800}
          iterationCount={1}>
          {active === item._id ? (
            <Icon
              name="check"
              type="AntDesign"
              size={30}
              color={colors.main}
              style={styles.icons}
            />
          ) : null}
          <ImageBackground
            style={styles.image}
            imageStyle={{ borderRadius: 100 }}
            resizeMode="cover"
            source={PlaceHolder}>
            <Image
              source={{ uri: item.image }}
              style={[styles.image]}
              blurRadius={active === item._id ? 7 : 0}
            />
          </ImageBackground>
          <CustomText
            numberOfLines={1}
            light={active === item._id ? colors.main : colors.white}
            dark={active === item._id ? colors.main : colors.white}
            style={[
              stylesText.secondaryText,
              {
                marginTop: 10,
                letterSpacing: 3,
                fontWeight: active === item._id ? '900' : '300',
              },
            ]}>
            {item.title}
          </CustomText>
        </Animatable.View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{ marginTop: dimensions.Height(2) }}>
      <Query query={query.HighkitchenCategory}>
        {(response) => {
          if (response.loading) {
            return <Loading />;
          }
          if (response.error) {
            return <Loading />;
          }
          if (response) {
            response.refetch();
            const data =
              response && response.data && response.data.getHighkitchenCategory
                ? response.data.getHighkitchenCategory.data
                : [];
            return (
              <FlatList
                data={data}
                renderItem={(item: any) => renderItem(item)}
                keyExtractor={(item: any) => item._id}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              />
            );
          }
        }}
      </Query>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },

  image: {
    width: 75,
    height: 75,
    borderRadius: 100,
    zIndex: 1,
    position: 'relative',
  },

  icons: {
    position: 'absolute',
    zIndex: 100,
    bottom: 45,
  },
});

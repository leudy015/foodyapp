import React from 'react';
import { View, Platform, TouchableOpacity, Linking } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions } from '../../theme';
import { stylesText } from '../../theme/TextStyle';
import SafariView from 'react-native-safari-view';
import Navigation from '../../services/Navigration';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const openWeb = () => {
  if (Platform.OS === 'ios') {
    SafariView.show({
      url: 'https://wilbby.com/condiciones-de-uso',
    });
  } else {
    Navigation.navigate('Web', {
      data: 'https://wilbby.com/condiciones-de-uso',
    });
  }
};

export default function TermAndCond(props) {
  const { setIsTermino, isTermino } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={[styles.items, { marginTop: 20, flexDirection: 'row' }]}>
      <View
        style={{
          width: dimensions.Width(70),
          paddingTop: 15,
          marginBottom: 15,
        }}>
        <CustomText
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold, { marginBottom: 5 }]}>
          Términos y condiciones
        </CustomText>
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            { paddingBottom: 5, lineHeight: 18 },
          ]}>
          Confirmo que he leído y acepto el aviso legal sobre protección de
          datos personales y los{' '}
          <TouchableOpacity
            onPress={() => openWeb()}
            style={{ paddingTop: Platform.select({ ios: 40, android: 60 }) }}>
            <CustomText
              numberOfLines={3}
              light={colors.main}
              dark={colors.main}
              style={stylesText.secondaryText}>
              términos y condiciones
            </CustomText>
          </TouchableOpacity>{' '}
          de compra
        </CustomText>
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          marginLeft: 'auto',
          marginRight: 15,
        }}>
        <TouchableOpacity
          onPress={() => {
            setIsTermino(!isTermino);
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          }}>
          <View
            style={[
              styles.checkBox,
              {
                backgroundColor: isTermino ? colors.main : colors.white,
              },
            ]}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },

  checkBox: {
    width: 25,
    height: 25,
    marginLeft: 'auto',
    borderRadius: 50,
    borderWidth: 2,
    borderColor: colors.rgb_235,
  },
});

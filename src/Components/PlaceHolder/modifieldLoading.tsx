import React from 'react';
import { View, ScrollView } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions } from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  return (
    <View
      style={{
        alignSelf: 'center',
        marginTop: dimensions.Height(0),
      }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 20,
          }}>
          <View
            style={{
              width: dimensions.Width(96),
              height: 50,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(96),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(84),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(60),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(75),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />
        </View>

        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 20,
          }}>
          <View
            style={{
              width: dimensions.Width(96),
              height: 50,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(96),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(84),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(60),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(75),
              height: 20,
              borderRadius: 5,
              marginTop: dimensions.Height(2),
            }}
          />
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

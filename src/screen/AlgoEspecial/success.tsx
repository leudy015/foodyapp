import React from 'react';
import { View } from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { Button } from '../../Components/Button';
import source from '../../Assets/Animate/success.json';
import LottieView from 'lottie-react-native';
import Navigation from '../../services/Navigration';

export const SuccessPayment = (prosp) => {
  const { setModalVisible } = prosp;
  const styles = useDynamicValue(dynamicStyles);

  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: dimensions.Height(15),
      }}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          width: dimensions.Width(96),
        }}>
        <LottieView source={source} autoPlay loop style={{ width: 200 }} />
        <CustomText
          ligth={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { textAlign: 'center', paddingHorizontal: 30 },
          ]}>
          Pedido realizado con éxito en unos minutos tendras noticia del rider.
        </CustomText>
        <View style={styles.signupButtonContainer}>
          <Button
            light={colors.white}
            dark={colors.white}
            containerStyle={[
              styles.buttonView,
              {
                backgroundColor: colors.main,
              },
            ]}
            onPress={() => {
              setModalVisible(false);
              Navigation.navigate('Inicio', { data: 'Order' });
            }}
            title="Ver pedido"
            titleStyle={styles.buttonTitle}
          />
        </View>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(92),
  },
  buttonView: {
    width: dimensions.Width(92),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

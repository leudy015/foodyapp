import React, { useState } from 'react';
import { View, Platform, Modal, Alert, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors, dimensions } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import { stylesText } from '../../../theme/TextStyle';
import { useTranslation } from 'react-i18next';
import ExpressShipping from '../../AlgoEspecial';

export default function Modals(props: any) {
  const {
    city,
    titleCat,
    lat,
    lgn,
    id,
    localeCode,
    currecy,
    countryCode,
    modalVisible,
    setModalVisible,
  } = props;
  const styles = useDynamicValue(dynamicStyles);
  const { t } = useTranslation();

  const renderContent = () => {
    switch (titleCat) {
      case 'Algo Especial':
        return (
          <ExpressShipping
            lat={lat}
            lgn={lgn}
            user={id}
            city={city}
            title="Algo Especial"
            setModalVisible={setModalVisible}
            quequieres="¿Qué quieres que te compremos?"
            donde="¿Dónde lo compramos?"
            vamos="¿Dónde te lo llevamos?"
            especial={true}
            localeCode={localeCode}
            currecy={currecy}
            countryCode={countryCode}
          />
        );
      case 'Envío Express':
        return (
          <ExpressShipping
            lat={lat}
            lgn={lgn}
            user={id}
            city={city}
            title="Envío Express"
            setModalVisible={setModalVisible}
            quequieres="¿Qué quieres que te llevemos?"
            donde="¿Dónde lo recogemos?"
            vamos="¿Dónde te lo llevamos?"
            especial={false}
            localeCode={localeCode}
            currecy={currecy}
            countryCode={countryCode}
          />
        );
      default:
        return <CustomText>{titleCat}</CustomText>;
    }
  };

  return (
    <Modal
      animationType="slide"
      visible={modalVisible}
      presentationStyle="formSheet"
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.centeredView}>
        <View style={styles.HeaderModal}>
          <TouchableOpacity onPress={() => {}}></TouchableOpacity>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              width: dimensions.Width(50),
            }}>
            <CustomText
              numberOfLines={1}
              ligth={colors.black}
              dark={colors.white}
              style={[
                stylesText.mainText,
                { textAlign: 'center', marginLeft: 15 },
              ]}>
              {t(`category:${titleCat}`)}
            </CustomText>
          </View>
          <TouchableOpacity onPress={() => setModalVisible(false)}>
            <Icon
              name="closecircleo"
              type="AntDesign"
              size={24}
              style={styles.icons}
            />
          </TouchableOpacity>
        </View>
        {renderContent()}
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  HeaderModal: {
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    height: 90,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  centeredView: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    flex: 1,
  },
  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});

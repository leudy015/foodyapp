import React from 'react';
import { View, Platform, Linking } from 'react-native';
import { dimensions, colors, stylesText } from '../theme';
import { CustomText } from './CustomTetx';
import LottieView from 'lottie-react-native';
import { Button } from './Button';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import Mailer from 'react-native-mail';
const source = require('../Assets/Animate/no_order.json');

const NoData = (props: any) => {
  const { name } = props;
  const styles = useDynamicValue(dynamicStyles);

  const handleEmail = () => {
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: `Hola avísame cuendo llegue wilbby a esta zona`,
          recipients: ['info@wilbby.com'],
          body: `<b>Hola avísame cuendo llegue wilbby a esta zona</b>`,
          // Android only (defaults to "Send Mail")
          isHTML: true,
        },
        (error, event) => {},
      );
    } else {
      Linking.openURL('mailto:info@wilbby.com');
    }
  };

  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        width: dimensions.Width(96),
        marginBottom: dimensions.Height(5),
      }}>
      <LottieView source={source} autoPlay loop style={{ width: 200 }} />
      <CustomText
        ligth={colors.black}
        dark={colors.white}
        style={[
          stylesText.secondaryTextBold,
          { textAlign: 'center', paddingHorizontal: 30, paddingBottom: 5 },
        ]}>
        {name
          ? name
          : 'Aún no hemos llegado a esta zona te avisaremos cuando tengamos restaurantes disponibles.'}
      </CustomText>
      {!name ? (
        <View style={styles.signupButtonContainer}>
          <Button
            light={colors.white}
            dark={colors.white}
            containerStyle={[
              styles.buttonView,
              { backgroundColor: colors.main },
            ]}
            onPress={() => handleEmail()}
            title="Avisame por email"
            titleStyle={styles.buttonTitle}
          />
        </View>
      ) : null}
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  signupButtonContainer: {
    marginTop: Platform.select({
      ios: dimensions.Height(5),
      android: dimensions.Height(2),
    }),
    alignSelf: 'center',
    width: dimensions.Width(70),
  },
  buttonView: {
    width: dimensions.Width(70),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

export default NoData;

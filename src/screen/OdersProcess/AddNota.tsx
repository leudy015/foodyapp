import React from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Modal,
  TextInput,
  Platform,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions } from '../../theme';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import HerderModal from '../../Components/HeaderforNote';

export default function AddNota(props) {
  const styles = useDynamicValue(dynamicStyles);
  const { setModalVisible, modalVisible, nota, setNota } = props;

  const textConfig = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const config = useDynamicValue(textConfig);

  return (
    <>
      <TouchableOpacity
        style={[styles.items, { marginTop: 20 }]}
        onPress={() => setModalVisible(!modalVisible)}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              { paddingVertical: 20, width: dimensions.Width(70) },
            ]}>
            Añadir nota, alergias o intolerancias
          </CustomText>
          <View
            style={{
              marginLeft: 'auto',
              marginRight: 15,
              marginTop: 10,
            }}>
            <Icon
              name="pluscircle"
              type="AntDesign"
              size={30}
              color={colors.main}
            />
          </View>
        </View>
        {nota ? (
          <View style={{ flexDirection: 'row' }}>
            <CustomText
              numberOfLines={3}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  marginTop: 5,
                  marginBottom: 15,
                  width: dimensions.Width(80),
                  paddingBottom: 5,
                  lineHeight: 18,
                },
              ]}>
              {nota}
            </CustomText>
            <TouchableOpacity
              style={{ marginLeft: 'auto', marginRight: 15, marginTop: 10 }}
              onPress={() => setNota('')}>
              <Icon
                name="close"
                type="AntDesign"
                size={24}
                color={colors.ERROR}
              />
            </TouchableOpacity>
          </View>
        ) : null}
      </TouchableOpacity>
      <Modal
        animationType="slide"
        visible={modalVisible}
        presentationStyle="formSheet"
        statusBarTranslucent={true}
        onRequestClose={() => setModalVisible(false)}>
        <View style={styles.centeredView}>
          <HerderModal
            title="Añadir nota"
            icon="check"
            OnClosetModal={() => setModalVisible(false)}
          />
          <ScrollView showsVerticalScrollIndicator={false}>
            <View
              style={{
                marginTop: dimensions.Height(2),
                width: dimensions.Width(100),
              }}>
              <TextInput
                multiline={true}
                numberOfLines={4}
                selectionColor={colors.main}
                style={styles.input}
                clearButtonMode="always"
                placeholderTextColor={config}
                onChangeText={(value) => setNota(value)}
                placeholder="Nota del pedido"
              />
            </View>
          </ScrollView>
        </View>
      </Modal>
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
    minHeight: 60,
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  input: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    padding: Platform.select({
      android: 10,
      ios: dimensions.IsIphoneX() ? 20 : 10,
    }),
    height: dimensions.Height(30),
    marginTop: 10,
    marginHorizontal: 10,
    borderRadius: 5,
    color: colors.rgb_153,
    borderWidth: 0,
    borderColor: 'transparent',
    flexDirection: 'row',
    fontSize: 18,
  },
});

import React from 'react';
import { Animated } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';

export const CustomText = (props: any) => {
  const dynamicStyles = new DynamicStyleSheet({
    TextView: {
      color: new DynamicValue(props.light, props.dark),
    },
  });
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <Animated.Text {...props} style={[styles.TextView, props.style]}>
      {props.children}
    </Animated.Text>
  );
};

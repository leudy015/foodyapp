import React from 'react';
import { View, TouchableOpacity, Platform, Image } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { dimensions, colors, image } from '../theme';
import { stylesText } from '../theme/TextStyle';
import RNRestart from 'react-native-restart';
import LottieView from 'lottie-react-native';
import source from '../Assets/Animate/noInter.json';

const Ayuda = () => {
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.container}>
      <View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <LottieView
            source={source}
            autoPlay
            loop
            style={{
              width: 400,
              height: 300,
              marginBottom: 20,
            }}
          />

          <CustomText
            ligth={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              { marginHorizontal: 30, textAlign: 'center' },
            ]}>
            Hay un problema con tu conexión a internet
          </CustomText>
        </View>
        <TouchableOpacity
          onPress={() => RNRestart.Restart()}
          style={styles.selecteds}>
          <CustomText
            light={colors.white}
            dark={colors.white}
            numberOfLines={1}
            style={stylesText.mainText}>
            Volver a intentarlo
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
    width: dimensions.ScreenWidth,
  },

  selecteds: {
    width: dimensions.Width(90),
    height: 60,
    borderRadius: 10,
    flexDirection: 'row',
    backgroundColor: colors.main,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
});

export default Ayuda;

import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import {
  createStackNavigator,
  TransitionPresets,
} from 'react-navigation-stack';
import '../Language';

/////////Screens//////////////
import Login from '../screen/Login';
import VerifyPhone from '../screen/VerifyPhone';
import SendCode from '../screen/SendCode';
import UpdateProfile from '../screen/UpdateProfile';
import Wallet from '../screen/Wallet';
import Cupones from '../screen/Cupones';
import Ayuda from '../screen/Ayuda';
import Search from '../screen/Search';
import DestailsStore from '../screen/DetailsStore/index';
import Restaurant from '../screen/StoreInfo/restaurant';
import Order from '../screen/OdersProcess/OrderScreen';
import Payment from '../screen/Payment';
import Thank from '../screen/thank';
import Error from '../screen/errors';
import Web from '../screen/WebViwe';
import ComparteyGana from '../screen/ComparteyGana';
import Scaner from '../screen/Scaner';
import Direccion from '../screen/Adress/direccion';
import ProductSearch from '../screen/SearchPRoducto';
import Inicio from '../screen/NewHome';
import Feed from '../screen/Feed/feed';
import PdfVier from '../screen/pdfView';
import Apariencia from '../screen/apariencia';
import DestailsOrden from '../screen/orders/DestailsOrden';
import Segumiento from '../screen/orders/FallowOrder/Seguimiento';
import Highkitchen from '../screen/highkitchen';
import Vegankitchen from '../screen/VeganKitchen';
import ViewCollection from '../screen/DetailsStore/Collection/subCollection';
import CollectionDetails from '../screen/DetailsStore/Collection/CollectionDetails';
import PFDInvoice from '../screen/ViewInvoice';
import Forgot from '../screen/Forgot';
///END///
///////////end//////////////

const AuthAppNavigator = createStackNavigator(
  {
    Inicio: {
      screen: Inicio,
    },
    ViewCollection: {
      screen: ViewCollection,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    PFDInvoice,
    CollectionDetails: {
      screen: CollectionDetails,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    Highkitchen: {
      screen: Highkitchen,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    Vegankitchen: {
      screen: Vegankitchen,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    Thank,
    Order,
    UpdateProfile,
    ProductSearch,
    Wallet,
    Scaner,
    DestailsOrden,
    Error,
    Apariencia,
    Segumiento,
    ComparteyGana,
    Web,
    Cupones,
    Ayuda,
    ViewCollection: {
      screen: ViewCollection,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    Search: {
      screen: Search,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    Restaurant,
    Payment,
    PdfVier,
    Direccion: {
      screen: Direccion,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    DestailsStore: {
      screen: DestailsStore,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    Feed: {
      screen: Feed,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      ...TransitionPresets.DefaultTransition,
    },
  },
  {
    initialRouteName: Inicio,
  },
);

const UnAuthAppNavigator = createStackNavigator(
  {
    Login,
    Forgot,
    VerifyPhone,
    SendCode,
    PdfVier,
    Feed,
    Search,
    DestailsStore,
    Restaurant,
    Highkitchen: {
      screen: Highkitchen,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
    Vegankitchen: {
      screen: Vegankitchen,
      navigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid,
      },
    },
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      ...TransitionPresets.DefaultTransition,
    },
  },
  {
    initialRouteName: Login,
  },
);

export const UnAuthSwitch = createSwitchNavigator({
  UnAuth: UnAuthAppNavigator,
  Auth: AuthAppNavigator,
});

export const AuthSwitch = createSwitchNavigator({
  Auth: AuthAppNavigator,
  UnAuth: UnAuthAppNavigator,
});

export const UnAuthContainer = createAppContainer(UnAuthSwitch);

export const AuthContainer = createAppContainer(AuthSwitch);

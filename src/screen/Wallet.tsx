import React, { useState, useEffect } from 'react';
import { View, ScrollView, FlatList, Image, Platform } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import Header from '../Components/Header';
import { dimensions, colors, image } from '../theme';
import { stylesText } from '../theme/TextStyle';
import { NETWORK_INTERFACE_URL } from '../Config/config';
import { useNavigationParam } from 'react-navigation-hooks';
import Loader from '../Components/ModalLoading';
import Toast from 'react-native-toast-message';
import Icon from 'react-native-dynamic-vector-icons';
import { SwipeAction } from '@ant-design/react-native';
import { useTranslation } from 'react-i18next';
import Paypal from '../Components/Paypal/paypal';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import FieldCard from './Card';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const Wallet = () => {
  const { t } = useTranslation();
  const styles = useDynamicValue(dynamicStyles);
  const datas = useNavigationParam('data');
  const [cards, setcards] = useState([]);
  const [loading, setLoading] = useState(false);

  const getCard = async () => {
    setLoading(true);
    let res = await fetch(
      `${NETWORK_INTERFACE_URL}/get-card?customers=${datas.StripeID}`,
    );
    const card = await res.json();
    setcards(card);
    setLoading(false);
  };

  useEffect(() => {
    getCard();
    setTimeout(() => {
      setLoading(false);
    }, 1500);
  }, []);

  const deleteCard = async (StripeID: any, id: any) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    let res = await fetch(
      `${NETWORK_INTERFACE_URL}/delete-card-web?customers=${StripeID}&cardID=${id}`,
    );
    const deletec = await res.json();
    if (deletec ? deletec : false) {
      getCard();
      Toast.show({
        text1: 'Tarjeta eliminada',
        text2: 'Tarjeta eliminada con éxito',
        position: 'top',
        type: 'success',
        topOffset: 50,
        visibilityTime: 4000,
      });
    }
  };

  const _renderItem = ({ item }) => {
    let images = '';
    let brand = '';
    switch (item.card.brand) {
      case 'visa':
        images = image.Visa;
        brand = 'Visa';
        break;
      case 'mastercard':
        images = image.MasterCard;
        brand = 'Master Card';
        break;
      case 'amex':
        images = image.AmericanExpress;
        brand = 'American Express';
        break;
    }

    const right = [
      {
        text: 'Eliminar',
        onPress: () => deleteCard(item.customer, item.id),
        style: {
          backgroundColor: colors.ERROR,
          color: 'white',
        },
      },
    ];

    return (
      <View>
        <View
          style={[
            styles.cardinfo,
            { backgroundColor: bg, borderColor: borderColot },
          ]}>
          <SwipeAction
            autoClose
            style={{
              backgroundColor: 'transparent',
            }}
            right={right}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                source={images || image.Card3}
                style={{
                  width: 50,
                  height: 50,
                  marginRight: 15,
                  borderRadius: 50,
                }}
              />
              <View>
                <View>
                  <CustomText
                    numberOfLines={1}
                    light={colors.black}
                    dark={colors.white}
                    style={[stylesText.mainText, { fontWeight: 'bold' }]}>
                    {brand || 'Mi tarjeta'}
                  </CustomText>
                  <CustomText
                    numberOfLines={2}
                    light={colors.rgb_153}
                    dark={colors.rgb_153}
                    style={[
                      stylesText.secondaryText,
                      {
                        width: dimensions.Width(65),
                      },
                    ]}>
                    Terminada en {item.card.last4} - Caduca{' '}
                    {item.card.exp_month}/{item.card.exp_year}
                  </CustomText>
                </View>
              </View>
            </View>
          </SwipeAction>
        </View>
      </View>
    );
  };

  const backgroundColor = {
    light: colors.white,
    dark: colors.back_dark,
  };
  const mode = useColorSchemeContext();
  const bg = backgroundColor[mode];

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const modes = useColorSchemeContext();
  const borderColot = backgroundColors[modes];

  const primary = {
    light: colors.white,
    dark: colors.black,
  };
  const modos = useColorSchemeContext();
  const primario = primary[modos];

  const secundary = {
    light: colors.colorInput,
    dark: colors.back_dark,
  };
  const modo = useColorSchemeContext();
  const secundario = secundary[modo];

  return (
    <View style={styles.container}>
      <Toast ref={(ref) => Toast.setRef(ref)} style={{ zIndex: 100 }} />
      <Loader loading={loading} color={colors.main} />
      <Header title="Metódo de pago" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ marginBottom: dimensions.Height(20) }}>
          <CustomText
            numberOfLines={1}
            ligth={colors.black}
            dark={colors.white}
            style={[
              stylesText.titleText,
              { marginBottom: 30, marginTop: 30, marginLeft: 15 },
            ]}>
            Añadidos
          </CustomText>

          <FlatList
            // @ts-ignore
            data={cards ? cards.data : []}
            renderItem={(item) => _renderItem(item)}
            keyExtractor={(item) => item.id}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={
              <FieldCard
                clientID={datas.StripeID}
                getCard={getCard}
                setLoading={setLoading}
                primarycolor={primario}
                secudandyColor={secundario}
              />
            }
          />

          <CustomText
            numberOfLines={1}
            ligth={colors.black}
            dark={colors.white}
            style={[
              stylesText.titleText,
              { marginBottom: 30, marginTop: 30, marginLeft: 15 },
            ]}>
            Métodos de pago
          </CustomText>

          {Platform.OS === 'ios' ? (
            <View
              style={[
                styles.cardinfo,
                { backgroundColor: bg, borderColor: borderColot },
              ]}>
              <Icon
                name="apple1"
                type="AntDesign"
                size={32}
                style={styles.icon}
              />
              <View>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.mainText, { fontWeight: 'bold' }]}>
                  {t('applepay:title')}
                </CustomText>
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[stylesText.secondaryText, { paddingBottom: 3 }]}>
                  Paga con tu móvil
                </CustomText>
              </View>
            </View>
          ) : null}
          <Paypal user={datas} />
          <FieldCard
            clientID={datas.StripeID}
            getCard={getCard}
            setLoading={setLoading}
            primarycolor={primario}
            secudandyColor={secundario}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  crecadr: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: 'transparent',
    padding: dimensions.Width(3),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(4),
    marginHorizontal: dimensions.Width(2),
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  noty: {
    marginLeft: 'auto',
    marginRight: dimensions.Width(0),
    backgroundColor: colors.white,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  cardinfo: {
    width: dimensions.Width(92),
    padding: 20,
    height: 'auto',
    flexDirection: 'row',
    marginTop: dimensions.Height(1),
    marginBottom: dimensions.Height(1),
    borderRadius: 10,
    alignSelf: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
  },
  icon: {
    color: new DynamicValue(colors.black, colors.white),
    marginRight: 10,
  },
});

export default Wallet;

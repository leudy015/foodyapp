import React from 'react';
import { View, ScrollView, Alert, Modal } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors } from '../../theme';
import HerderModal from '../../Components/HeaderModal';
import RatingStore from '../../Components/RatingStore/ratingView';

export default function ModalRating(props) {
  const { modalVisible, setModalVisible, res } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <Modal
      animationType="slide"
      visible={modalVisible}
      presentationStyle="formSheet"
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.centeredView}>
        <HerderModal
          title="Opiniones de los clientes"
          icon="close"
          OnClosetModal={() => setModalVisible(false)}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <RatingStore data={res} />
        </ScrollView>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  centeredView: {
    backgroundColor: new DynamicValue(colors.white, 'black'),
    flex: 1,
  },
});

import React, { useState, useEffect } from 'react';
import { StatusBar, Platform, View } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { ApolloProvider } from 'react-apollo';
import NavigationService from './src/services/Navigration';
import { UnAuthContainer, AuthContainer } from './src/Navigation/index';
import Geolocation from '@react-native-community/geolocation';
import NetInfo from '@react-native-community/netinfo';
import { store, persistor } from './src/redux/store';
import NoInternet from './src/screen/NoInternet';
import OneSignal from 'react-native-onesignal';
import * as RNLocalize from 'react-native-localize';
import { LogBox } from 'react-native';
import { SetCity } from './src/Utils/getCiti';
import Loading from './src/screen/Loading';
import client from './src/Utils/apollo-client';
import { ColorSchemeProvider } from 'react-native-dynamic';

LogBox.ignoreAllLogs();

global.PaymentRequest = require('react-native-payments').PaymentRequest;

export default function Apps() {
  const [token, settoken] = useState(null);
  const [Loadings, setLoadings] = useState(false);
  const [conectedInterner, setconectedInterner] = useState(true);

  OneSignal.setLogLevel(6, 0);
  OneSignal.setAppId('db79b975-e551-4741-ae43-8968ceab5f09');

  OneSignal.setNotificationWillShowInForegroundHandler(
    (notificationReceivedEvent) => {
      let notification = notificationReceivedEvent.getNotification();
      notificationReceivedEvent.complete(notification);
    },
  );

  NetInfo.fetch().then((state) => {
    setconectedInterner(state.isConnected);
  });

  const country = RNLocalize.getLocales();
  const currecy = RNLocalize.getCurrencies();

  AsyncStorage.setItem('countryCode', country[0].countryCode);
  AsyncStorage.setItem('localeCode', country[0].languageTag);
  AsyncStorage.setItem('languageCode', country[0].languageCode);
  AsyncStorage.setItem('currecy', currecy[0]);

  Geolocation.getCurrentPosition((info) => {
    SetCity(info.coords.latitude, info.coords.longitude);
  });

  const getToken = () => {
    setLoadings(true);
    AsyncStorage.getItem('token')
      .then((value) => {
        //@ts-ignore
        global.token = value;
        settoken(value);
      })
      .catch(() => {
        settoken(null);
      })
      .finally(() => {
        setLoadings(false);
      });
  };

  useEffect(() => {
    getToken();
  }, [token]);

  const renderContent = () => {
    if (Loadings && !token) {
      return (
        <>
          <StatusBar translucent backgroundColor="transparent" />
          <Loading />
        </>
      );
    }
    if (!conectedInterner) {
      return (
        <>
          <StatusBar translucent backgroundColor="transparent" />
          <NoInternet />
        </>
      );
    } else
      return (
        <ColorSchemeProvider>
          <Provider store={store}>
            <PersistGate loading={true} persistor={persistor}>
              <StatusBar translucent backgroundColor="transparent" />
              <ApolloProvider client={client}>
                {!token ? (
                  <UnAuthContainer
                    ref={(navigatorRef) => {
                      NavigationService.setContainer(navigatorRef);
                    }}
                  />
                ) : (
                  <AuthContainer
                    ref={(navigatorRef) => {
                      NavigationService.setContainer(navigatorRef);
                    }}
                  />
                )}
              </ApolloProvider>
            </PersistGate>
          </Provider>
        </ColorSchemeProvider>
      );
  };

  return renderContent();
}

//cd android && ./gradlew clean && ./gradlew bundleRelease

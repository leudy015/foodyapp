import React from 'react';
import { View, FlatList, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions } from '../../theme';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import Navigration from '../../services/Navigration';
import CardActive from '../../Components/NewCardProduct/CardAnadida';

export default function ItemsCart(props) {
  const styles = useDynamicValue(dynamicStyles);
  const {
    localeCode,
    currecy,
    respuesta,
    inCartItem,
    setinCartItem,
    setinCartItems,
    storeID,
  } = props;

  const _itemsRender = ({ item }, quantity) => {
    return (
      <CardActive
        item={item.items}
        storeID={storeID}
        localeCode={localeCode}
        currecy={currecy}
        inCartItem={inCartItem}
        setinCartItem={setinCartItem}
        setinCartItems={setinCartItems}
        itemId={item.id}
        quantityProdt={item.items.quantity}
        addToCart={true}
        quantity={quantity}
      />
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={respuesta}
        renderItem={(item: any) => _itemsRender(item, respuesta.length)}
        keyExtractor={(item: any) => item.id}
        showsVerticalScrollIndicator={false}
      />
      <TouchableOpacity style={styles.btn} onPress={() => Navigration.goBack()}>
        <Icon
          name="plus-circle"
          type="Feather"
          size={24}
          color={colors.main}
          style={{ marginRight: 10 }}
        />
        <CustomText
          light={colors.main}
          dark={colors.main}
          style={stylesText.secondaryTextBold}>
          Añadir más productos
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.colorInput, 'black'),
    flex: 1,
  },
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    alignSelf: 'flex-start',
  },

  btn: {
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: 15,
    marginHorizontal: 30,
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  checkBox: {
    width: 25,
    height: 25,
    marginLeft: 'auto',
    borderRadius: 50,
    borderWidth: 2,
    borderColor: colors.rgb_235,
  },

  img: {
    width: 70,
    height: 70,
    borderRadius: 5,
    marginLeft: 'auto',
    marginRight: dimensions.Height(2),
  },

  itemss: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 15,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
    flexDirection: 'row',
    borderLeftColor: colors.main,
    borderLeftWidth: 7,
  },
});

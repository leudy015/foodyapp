import React from 'react';
import { View } from 'react-native';
import { colors, stylesText } from '../theme';
import { CustomText } from './CustomTetx';
import LottieView from 'lottie-react-native';

export default function LoadingAnimated(props: any) {
  const { name } = props;

  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <LottieView
        source={require('../Assets/Animate/loadinganimated.json')}
        autoPlay
        loop
        style={{ width: 100 }}
      />
      <CustomText
        ligth={colors.black}
        dark={colors.white}
        style={[stylesText.secondaryText, { textAlign: 'center' }]}>
        {name}
      </CustomText>
    </View>
  );
}

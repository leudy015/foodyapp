import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { colors, stylesText } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import UserAvatar from 'react-native-user-avatar';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../../../Config/config';
import Icon from 'react-native-dynamic-vector-icons';
import { dimensions } from '../../../theme/dimension';

export default function Riders(props) {
  const styles = useDynamicValue(dynamicStyles);
  const { item, heigth, OpenURLButton } = props;

  return (
    <View>
      {item.orderType === 'delivery' ? (
        <>
          {item.courierData ? (
            <>
              <View style={styles.infoContact}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <UserAvatar
                    size={50}
                    bgColors={[colors.secundary, colors.orange1, colors.main]}
                    name={`${item.courierData.name} ${item.courierData.lastName}`}
                    src={
                      NETWORK_INTERFACE_LINK_AVATAR + item.courierData.avatar
                    }
                  />
                  <View style={{ marginLeft: 10 }}>
                    <CustomText
                      numberOfLines={1}
                      light={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.secondaryTextBold,
                        { width: dimensions.Width(50) },
                      ]}>
                      {item.courierData.name} {item.courierData.lastName}
                    </CustomText>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[stylesText.terciaryText]}>
                      Repartidor
                    </CustomText>
                  </View>
                </View>

                <TouchableOpacity
                  style={styles.btns}
                  onPress={() =>
                    OpenURLButton(`tel:${item.courierData.telefono}`)
                  }>
                  <CustomText
                    numberOfLines={1}
                    style={[
                      stylesText.secondaryText,
                      {
                        color: colors.main,
                      },
                    ]}>
                    <Icon
                      name="phone"
                      type="Feather"
                      size={12}
                      color={colors.main}
                    />{' '}
                    Llamar
                  </CustomText>
                </TouchableOpacity>
              </View>
            </>
          ) : (
            <View style={styles.infoContact}>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={stylesText.secondaryTextBold}>
                Repartidor sin asignar
              </CustomText>
            </View>
          )}
        </>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  infoContact: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: dimensions.Width(88),
    marginTop: 20,
  },

  btns: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 5,
  },
});

function getStarFromScore(score) {
  switch (true) {
    case score < 2:
      return 1;
    case score < 3:
      return 2;
    case score < 4:
      return 3;
    case score < 5:
      return 4;
    default:
      return 5;
  }
}

export const DesgloseRating = (ValoracionList: []) => {
  var stars = {
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
  };
  // add the ratings to its respective star array
  ValoracionList.forEach(function (rate) {
    //@ts-ignore
    var star = getStarFromScore(rate.value);
    //@ts-ignore
    stars[star].push(rate.value);
  });

  return stars;
};

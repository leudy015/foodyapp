import React, { useState, useRef, useEffect } from 'react';
import {
  Image,
  ScrollView,
  SafeAreaView,
  View,
  Animated,
  ImageBackground,
  Platform,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { useNavigationParam } from 'react-navigation-hooks';
import { dimensions, colors, image } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../../services/Navigration';
import { Modalize } from 'react-native-modalize';
import Loader from '../../Components/ModalLoading';
import { RatingCalculator } from '../../Utils/rating';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { scheduleTime } from '../../Utils/scheduleTime';
import ButtomBar from './ButtonBar';
import ModalDate from './ModalSelecDate';
import ModaAutoShipping from './autoShippingModal';
import SwitchFilter from './Swich';
import SwitchFilterTwo from './SwichTow';
import Express from './Express';
import StoreRating from './ModalRating';
import Menu from './ProducsNew';
import InfoDistance from './InfoDistance';
import Scheduled from './Scheduled';
import Tabs from './Tabs/Tabs';
import TabsCollection from './TabsCollection/Tabscollection';
import Collection from './Collection';
import SavingPack from './SalvingPack';
import SearcProducts from './SearchProducts';
import ExistModal from './ModalExist';
import AsyncStorage from '@react-native-community/async-storage';

const height = Dimensions.get('window').height;

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const DestailStore = () => {
  const styles = useDynamicValue(dynamicStyles);
  const datas = useNavigationParam('data');
  const res = datas.store;

  const scheduled = res.scheduleOnly.available
    ? res.noScheduled
      ? 'Lo antes posible'
      : 'Programar entrega'
    : 'Lo antes posible';
  const [scrollY] = useState(new Animated.Value(0));
  const modalizeRef = useRef<Modalize>(null);
  const modalizeRefInfo = useRef<Modalize>(null);
  const [date, setDate] = useState(null);
  const [anadidotoFavorite, setanadidotoFavorite] = useState(
    res.anadidoFavorito,
  );
  const [takeaway, setTakeaway] = useState(false);
  const [ModalVisibleEsp, setModalVisibleEsp] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [selecte, setSelected] = useState(scheduled);
  const [Loding, setLoding] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [inCartItem, setinCartItem] = useState([]);
  const [modalVisibleSearch, setmodalVisibleSearch] = useState(false);
  const [exitmodalVisible, setexitmodalVisible] = useState(false);
  const [token, settoken] = useState(null);
  const [user, setuser] = useState(null);
  const [selected, setSelectede] = useState(0);

  const {
    id,
    yo,
    autoshipping,
    city,
    lat,
    lgn,
    localeCode,
    currecy,
    refetch,
    delivery,
  } = datas;

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      setinCartItem(inCartItem);
    }, 2000);
  };

  const getToken = async () => {
    settoken(await AsyncStorage.getItem('token'));
    const usuario = await AsyncStorage.getItem('user');
    setuser(JSON.parse(usuario));
  };

  useEffect(() => {
    setinCartItem(inCartItem);
  }, [inCartItem]);

  useEffect(() => {
    getToken();
  }, [token]);

  const alto = height > 720 ? 280 : 290;
  const altoLlevar = height > 720 ? 370 : 370;

  const top = height > 720 ? 400 : 400;
  const tops = height > 720 ? 480 : 480;

  const hei =
    takeaway || (!delivery && res.llevar)
      ? res.autoshipping
        ? takeaway
          ? altoLlevar
          : alto
        : altoLlevar
      : alto;

  const margintop = takeaway || (!delivery && res.llevar) ? tops : top;

  const hei1 = Platform.select({
    ios: dimensions.IsIphoneX() ? 165 : 140,
    android: 155,
  });

  const headerContainerWidth = scrollY.interpolate({
    inputRange: [0, 125],
    outputRange: [dimensions.Width(90), dimensions.Width(100)],
    extrapolate: 'clamp',
  });

  const headerContainerHeight = scrollY.interpolate({
    inputRange: [0, 200],
    outputRange: [hei, hei1],
    extrapolate: 'clamp',
  });

  const heightsHiden = scrollY.interpolate({
    inputRange: [0, 200],
    outputRange: ['100%', '0%'],
    extrapolate: 'clamp',
  });

  const headerContainerBorder = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [20, 0],
    extrapolate: 'clamp',
  });

  const opacity = scrollY.interpolate({
    inputRange: [0, 50, 90],
    outputRange: [1, 0.5, 0],
    extrapolate: 'clamp',
  });

  const opacityTitle = scrollY.interpolate({
    inputRange: [90, 100, 100],
    outputRange: [0, 50, 100],
    extrapolate: 'clamp',
  });

  const opacityText = scrollY.interpolate({
    inputRange: [140, 180, 200],
    outputRange: [0, 30, 100],
    extrapolate: 'clamp',
  });

  const imageContainerHeight = scrollY.interpolate({
    inputRange: [-200, 0],
    outputRange: [450, 250],
    extrapolate: 'extend',
  });

  const handleOpen = () => {
    if (modalizeRefInfo.current) {
      modalizeRefInfo.current.open();
    }
  };

  useEffect(() => {
    if (autoshipping) {
      handleOpen();
    }
  }, []);

  const onOpen = () => {
    ReactNativeHapticFeedback.trigger('selection', options);
    modalizeRef.current?.open();
  };

  const changeLlevar = () => {
    ReactNativeHapticFeedback.trigger('selection', options);
    setTakeaway(!takeaway);
  };

  const border = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const bordercolor = border[mode];
  //SaveDataWidget(res);
  const isOK = () => {
    if (scheduleTime(res.schedule) && res.open) {
      return true;
    }
    return false;
  };
  const averageRating = RatingCalculator(res.Valoracion);

  const backToStore = async () => {
    if (inCartItem.length > 0) {
      setexitmodalVisible(true);
    } else {
      Navigation.goBack();
    }
  };

  return (
    <View style={styles.container}>
      <Loader loading={Loding} color={colors.main} />
      <SafeAreaView style={styles.containerIcon}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity onPress={() => backToStore()} style={styles.icon}>
            <Icon
              name="arrow-left"
              type="Feather"
              size={24}
              color={colors.main}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.icon}
            onPress={() => setmodalVisibleSearch(true)}>
            <Icon type="Feather" name="search" size={24} color={colors.main} />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <Animated.View
        style={[styles.imageContainer, { height: imageContainerHeight }]}>
        <ImageBackground
          style={styles.image}
          resizeMode="cover"
          source={image.PlaceHolder}>
          <Image
            style={styles.image}
            source={{
              uri: res.image,
            }}
          />
        </ImageBackground>
      </Animated.View>
      <ScrollView
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: scrollY } } },
        ])}
        scrollEventThrottle={16}
        stickyHeaderIndices={[1]}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={_onRefresh}
            enabled={true}
            title={res.title}
          />
        }
        style={styles.scrollViewContainer}>
        <View
          style={{
            marginTop: margintop,
          }}>
          {res && res.salvingPack && res.salvingPack.isSavingPack ? (
            <SavingPack
              data={res}
              refetch={refetch}
              user={yo}
              inCartItem={inCartItem}
              setinCartItem={setinCartItem}
            />
          ) : null}
          {res.collections ? (
            <Collection
              store={res}
              localeCode={localeCode}
              currecy={currecy}
              inCartItem={inCartItem}
              setinCartItem={setinCartItem}
              modalVisible={modalVisibleSearch}
              setModalVisible={setmodalVisibleSearch}
            />
          ) : (
            <Menu
              res={res}
              localeCode={localeCode}
              currecy={currecy}
              inCartItem={inCartItem}
              setinCartItem={setinCartItem}
            />
          )}

          <View
            style={{
              width: dimensions.ScreenWidth,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: dimensions.Height(20),
            }}>
            <Express
              lat={lat}
              lgn={lgn}
              ModalVisibleEsp={ModalVisibleEsp}
              setModalVisibleEsp={setModalVisibleEsp}
              id={id}
              city={city}
            />
          </View>
        </View>
        <View style={styles.stickyHeaderContainer}>
          <Animated.View
            style={[
              styles.headerContainer,
              {
                width: headerContainerWidth,
                height: headerContainerHeight,
                borderRadius: headerContainerBorder,
              },
            ]}>
            <Animated.View
              style={[
                styles.HeaderTitle,
                {
                  opacity: opacityTitle,
                  zIndex: 1,
                },
              ]}>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.titleText,
                  {
                    opacity: opacityText,
                    width: dimensions.Width(70),
                  },
                ]}>
                {res.title}
              </CustomText>
              <Animated.View
                style={[
                  styles.tabs,
                  {
                    opacity: opacityText,
                    borderTopWidth: 0.5,
                    borderColor: bordercolor,
                    left: -10,
                    width: dimensions.ScreenWidth,
                    paddingTop: 15,
                  },
                ]}>
                {res.collections ? (
                  <TabsCollection res={res} />
                ) : (
                  <Tabs
                    res={res}
                    selected={selected}
                    setSelected={setSelectede}
                  />
                )}
              </Animated.View>
            </Animated.View>
            <Animated.View
              style={{
                opacity: opacity,
                zIndex: 2,
                height: heightsHiden,
              }}>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[stylesText.titleText]}>
                {res.title}
              </CustomText>
              <InfoDistance
                takeaway={takeaway}
                res={res}
                refetch={refetch}
                anadido={anadidotoFavorite}
                setanadidotoFavorite={setanadidotoFavorite}
                id={yo}
                localeCode={localeCode}
                currecy={currecy}
                handleOpen={handleOpen}
                averageRating={averageRating}
                setModalVisible={setModalVisible}
              />
              {res.autoshipping ? (
                <SwitchFilterTwo
                  changeLlevar={changeLlevar}
                  res={res}
                  takeaway={takeaway}
                  address={res.adress}
                  delivery={delivery}
                />
              ) : (
                <SwitchFilter
                  changeLlevar={changeLlevar}
                  res={res}
                  takeaway={takeaway}
                  address={res.adress}
                  delivery={delivery}
                />
              )}

              <Scheduled
                takeaway={takeaway}
                isOK={isOK}
                res={res}
                date={date}
                onOpen={onOpen}
                selecte={selecte}
                details={false}
                delivery={delivery}
              />
            </Animated.View>
          </Animated.View>
        </View>
      </ScrollView>
      {inCartItem.length > 0 ? (
        <ButtomBar
          res={res}
          id={yo}
          token={token}
          date={date}
          selecte={selecte}
          takeaway={takeaway}
          city={city}
          lat={lat}
          lgn={lgn}
          isOK={isOK}
          user={user}
          setinCartItem={setinCartItem}
          setLoding={(loading: boolean) => setLoding(loading)}
          localeCode={localeCode}
          currecy={currecy}
          respuesta={inCartItem}
          delivery={delivery}
          onOpen={onOpen}
        />
      ) : null}
      <ModalDate
        res={res}
        setDate={setDate}
        setSelected={(select) => setSelected(select)}
        date={date}
        selecte={selecte}
        modalizeRef={modalizeRef}
      />
      <ModaAutoShipping modalizeRefInfo={modalizeRefInfo} />
      <StoreRating
        res={res}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
      />
      <SearcProducts
        data={res}
        category=""
        title="Buscar productos"
        localeCode={localeCode}
        currecy={currecy}
        inCartItem={inCartItem}
        setinCartItem={setinCartItem}
        modalVisible={modalVisibleSearch}
        setModalVisible={setmodalVisibleSearch}
      />
      <ExistModal
        modalVisible={exitmodalVisible}
        setModalVisible={setexitmodalVisible}
        setinCartItem={setinCartItem}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
  },

  HeaderTitle: {
    width: dimensions.Width(90),
    marginTop: Platform.select({
      ios: dimensions.IsIphoneX()
        ? dimensions.Height(5.7)
        : dimensions.Height(5),
      android: dimensions.Height(5.3),
    }),
    position: 'absolute',
    height: 'auto',
    marginHorizontal: 50,
    zIndex: 1,
  },

  containerIcon: {
    width: dimensions.Width(96),
    position: 'absolute',
    marginTop: Platform.select({
      ios: dimensions.IsIphoneX() ? dimensions.Height(0) : dimensions.Height(4),
      android: dimensions.Height(5),
    }),
    marginHorizontal: 10,
    zIndex: 1,
  },
  scrollViewContainer: {
    flex: 1,
    height: dimensions.ScreenHeight,
    marginBottom: -150,
  },
  imageContainer: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: 200,
  },
  stickyHeaderContainer: {
    position: 'absolute',
    top: 100,
    left: 0,
    right: 0,
  },
  headerContainer: {
    width: dimensions.Width(90),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
    padding: 20,
  },
  image: {
    flex: 1,
  },
  icon: {
    width: 36,
    height: 36,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  tabs: {
    marginTop: dimensions.Height(2),
    marginLeft: -40,
  },
});

export default DestailStore;

import React, { useState } from 'react';
import {
  View,
  Image,
  Dimensions,
  Alert,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors, stylesText, image } from '../../../theme';
import { CustomText } from '../../CustomTetx';
import LinearGradient from 'react-native-linear-gradient';
// @ts-ignore
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { formaterPrice } from '../../../Utils/formaterPRice';
import Bundled from './Bundled';
import ModifieldGroup from './ModifieldGroup';
import { revoveItemInCart, AddItemsToCadt } from '../AddAndRemove';
import { newQuery } from '../../../GraphQL/newQuery';
import { Query, useQuery } from 'react-apollo';
import ModifieldLoading from '../../../Components/PlaceHolder/modifieldLoading';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const window = Dimensions.get('window');

export interface DetailProduct {
  datas: any;
  quantityTotal?: number;
  setModalVisible: any;
  localeCode: string;
  currecy: string;
  option: [];
  addToCarts: boolean;
  userId: string;
  storeId: string;
  inCartItem: [];
  setinCartItem?: any;
  itemId?: number;
}

const DetailsProduct = (props: DetailProduct) => {
  const {
    datas,
    quantityTotal,
    setModalVisible,
    localeCode,
    currecy,
    option,
    addToCarts,
    userId,
    storeId,
    inCartItem,
    setinCartItem,
    itemId,
  } = props;

  const opt = option.length > 0 ? option : [];

  const styles = useDynamicValue(dynamicStyles);
  const [quantity, setQuantity] = useState(quantityTotal);
  const [animated, setanimated] = useState(null);
  const [options, setOptions] = useState(opt);

  const { data, loading } = useQuery(newQuery.GET_BUNDLED, {
    variables: { products: datas.subProducts, storeID: datas.location },
  });

  const bundles = data && data.getBundled ? data.getBundled.data : [];

  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  const eliminarCart = () => {
    revoveItemInCart(inCartItem, setinCartItem, itemId, setModalVisible, 2);
  };

  const rescantidad = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    if (quantity < 2) {
      eliminarCart();
    } else {
      setQuantity(quantity - 1);
    }
  };

  const sumcantidad = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    if (quantity > 999) {
      Alert.alert('El máximo que puede seleccionar es 999');
    } else {
      setQuantity(quantity + 1);
    }
  };

  Object.assign(datas, {
    quantity: quantity,
    subItems: options,
  });

  const input = {
    id: itemId ? itemId : Number.parseInt(getRandomArbitrary(100, 1000000)),
    userId: userId,
    storeId: storeId,
    productId: datas._id,
    addToCart: true,
    items: datas,
  };

  const addTocardItems = () => {
    AddItemsToCadt(
      inCartItem,
      setinCartItem,
      datas._id,
      input,
      setModalVisible,
      2,
    );
    setModalVisible(false);
  };

  let noRequire = bundles.length === 0;
  let totalItems = 0;
  let totalProdut = datas.price;
  let totalSelected = 0;

  options.forEach(function (items: any) {
    const t = items.quantity * items.price;
    totalProdut += t;
    items &&
      items.subItems &&
      items.subItems.forEach((x) => {
        const s = x.quantity * x.price;
        totalProdut += s;
      });
  });

  bundles.forEach(function (element) {
    totalItems += element.min;
  });

  options.forEach(function (elements) {
    bundles.forEach((element) => {
      const bundledselected = element.Products.filter(
        (e) => e._id === elements._id,
      );
      bundledselected.forEach((y) => {
        totalSelected += y.quantity;
      });
    });
  });

  const verifyAdd = () => {
    if (noRequire) {
      return true;
    } else if (totalSelected >= totalItems) {
      return true;
    } else {
      return false;
    }
  };

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const selected = () => {
    if (verifyAdd()) {
      addTocardItems();
    } else {
      Alert.alert('Debes seleccionar todas las opciones');
      if (animated) {
        setanimated('');
      } else {
        setanimated('wobble');
      }
    }
  };
  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor="transparent"
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background" style={{ backgroundColor: colors.white }}>
            {datas.imageUrl ? (
              <>
                <ImageBackground
                  style={styles.imagenbg}
                  source={image.PlaceHolder}>
                  <Image
                    style={styles.imagenbg}
                    source={{ uri: datas.imageUrl }}
                    resizeMode="cover"
                  />
                  <View
                    style={{
                      position: 'absolute',
                      top: 0,
                      width: window.width,
                      height: PARALLAX_HEADER_HEIGHT,
                    }}
                  />
                </ImageBackground>
              </>
            ) : (
              <>
                <Image
                  style={styles.imagenbg}
                  source={image.PlaceHolder}
                  resizeMode="cover"
                />
                <View
                  style={{
                    position: 'absolute',
                    top: 0,
                    width: window.width,
                    height: PARALLAX_HEADER_HEIGHT,
                  }}
                />
              </>
            )}
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <TouchableOpacity
              style={styles.left}
              onPress={() => setModalVisible(false)}>
              <CustomText>
                <Icon
                  name="arrow-left"
                  type="Feather"
                  size={24}
                  style={styles.close}
                />
              </CustomText>
            </TouchableOpacity>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                { marginLeft: 65, width: dimensions.Width(75) },
              ]}>
              {datas.name}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View style={styles.items}>
            <CustomText
              numberOfLines={4}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.titleText,
                { marginTop: 0, width: dimensions.Width(80) },
              ]}>
              {datas.name}
            </CustomText>
            <View>
              <CustomText
                numberOfLines={5}
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    marginTop: 5,
                    width: dimensions.Width(80),
                    paddingBottom: 5,
                    lineHeight: 18,
                  },
                ]}>
                {datas.description}
              </CustomText>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingTop: 10,
                  width: dimensions.ScreenWidth,
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                  }}>
                  <CustomText
                    light={colors.main}
                    dark={colors.main}
                    style={[stylesText.secondaryTextBold, { marginTop: 15 }]}>
                    {formaterPrice(datas.price / 100, localeCode, currecy)}
                  </CustomText>
                  {datas.previous_price > 0 ? (
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[
                        stylesText.secondaryText,
                        {
                          textDecorationLine: 'line-through',
                        },
                      ]}>
                      {formaterPrice(
                        datas.previous_price / 100,
                        localeCode,
                        currecy,
                      )}
                    </CustomText>
                  ) : null}
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginLeft: 15,
                  }}>
                  {datas.new ? (
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#510090', '#4600DA']}
                      style={[styles.tagshipping]}>
                      <CustomText
                        light={colors.white}
                        dark={colors.white}
                        style={stylesText.placeholderText}>
                        Nueva
                      </CustomText>
                    </LinearGradient>
                  ) : null}

                  {datas.popular ? (
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={[colors.orange, '#fcbe4a']}
                      style={styles.tagshipping}>
                      <CustomText
                        light={colors.white}
                        dark={colors.white}
                        style={stylesText.placeholderText}>
                        Popular
                      </CustomText>
                    </LinearGradient>
                  ) : null}

                  {datas.offert ? (
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={[colors.red, colors.ERROR]}
                      style={[styles.tagshipping]}>
                      <CustomText
                        light={colors.white}
                        dark={colors.white}
                        style={stylesText.placeholderText}>
                        Oferta
                      </CustomText>
                    </LinearGradient>
                  ) : null}
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              marginBottom: dimensions.Height(25),
            }}>
            {loading ? (
              <ModifieldLoading />
            ) : (
              <Bundled
                data={bundles}
                animated={animated}
                options={options}
                setOptions={setOptions}
              />
            )}

            <Query
              query={newQuery.GET_MODIFIELD}
              variables={{
                products: datas.subProducts,
                storeID: datas.location,
              }}>
              {(response) => {
                if (response.loading) {
                  return <ModifieldLoading />;
                }
                if (response) {
                  const data =
                    response && response.data && response.data.getModifieldGroup
                      ? response.data.getModifieldGroup.data
                      : [];
                  response.refetch();
                  return (
                    <ModifieldGroup
                      data={data}
                      options={options}
                      setOptions={setOptions}
                    />
                  );
                }
              }}
            </Query>
          </View>
        </View>
      </ParallaxScrollView>
      {!loading ? (
        <View style={styles.selected}>
          <View style={styles.sumaResta}>
            <TouchableOpacity
              onPress={() => rescantidad()}
              style={[
                styles.plus,
                {
                  backgroundColor:
                    addToCarts && quantity === 1
                      ? '#f5365c5a'
                      : 'rgba(197, 248, 116, 0.3)',
                },
              ]}>
              {addToCarts && quantity === 1 ? (
                <Icon
                  name="delete"
                  type="AntDesign"
                  size={20}
                  color={colors.ERROR}
                />
              ) : (
                <Icon
                  name="minus"
                  type="AntDesign"
                  size={24}
                  color={colors.main}
                />
              )}
            </TouchableOpacity>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={stylesText.secondaryTextBold}>
              {quantity}
            </CustomText>
            <TouchableOpacity
              onPress={() => sumcantidad()}
              style={[
                styles.plus,
                {
                  backgroundColor: 'rgba(197, 248, 116, 0.3)',
                },
              ]}>
              <Icon
                name="plus"
                type="AntDesign"
                size={20}
                color={colors.main}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => selected()}
            style={[
              styles.btnAdd,
              {
                backgroundColor: verifyAdd() ? colors.main : backgroundColor,
              },
            ]}>
            <CustomText
              light={verifyAdd() ? colors.white : colors.mainBlue}
              dark={verifyAdd() ? colors.white : colors.rgb_235}
              style={[stylesText.secondaryText, { paddingBottom: -5 }]}>
              Añadir por{' '}
              <CustomText
                light={verifyAdd() ? colors.white : colors.mainBlue}
                dark={verifyAdd() ? colors.white : colors.rgb_235}
                style={[stylesText.secondaryTextBold, { paddingBottom: -5 }]}>
                {formaterPrice(
                  (totalProdut * quantity) / 100,
                  localeCode,
                  currecy,
                )}
              </CustomText>
            </CustomText>
          </TouchableOpacity>
        </View>
      ) : null}
    </View>
  );
};

const color1 = 'rgba(255, 255, 255, 0.5)';
const color2 = 'rgba(0, 0, 0, 0.5)';

const PARALLAX_HEADER_HEIGHT = 300;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contenedor: {
    flex: 1,
    width: dimensions.ScreenWidth,
    height: 'auto',
    minHeight: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  imagenbg: {
    width: dimensions.Width(100),
    height: PARALLAX_HEADER_HEIGHT,
  },

  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: '100%',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 0,
  },

  fixedSection: {
    position: 'absolute',
    bottom: 20,
    left: 15,
    flexDirection: 'row',
    paddingBottom: 5,
  },

  left: {
    marginRight: dimensions.Width(4),
    backgroundColor: new DynamicValue(color1, color2),
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },

  close: {
    color: colors.main,
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 25,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    paddingLeft: dimensions.Width(4),
  },

  selected: {
    width: dimensions.ScreenWidth,
    height: dimensions.IsIphoneX() ? 120 : 80,
    borderRadius: 5,
    bottom: 0,
    position: 'absolute',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    flexDirection: 'row',
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  sumaResta: {
    width: 135,
    height: 50,
    borderRadius: 7,
    marginLeft: 15,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
  },

  iconos: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  btnAdd: {
    height: 50,
    paddingHorizontal: dimensions.Width(3),
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
    borderRadius: 10,
    marginRight: 15,
  },

  tagshipping: {
    zIndex: 200,
    marginHorizontal: 5,
    marginTop: 10,
    padding: 3,
    width: 60,
    height: 22,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 0,
    borderRadius: 100,
    marginBottom: 5,
    backgroundColor: colors.green,
  },

  plus: {
    padding: 7,
    borderRadius: 100,
  },
});

export default DetailsProduct;

import React from 'react';
import { View, Modal } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors } from '../../../theme';
import Collection from './subCollection';

export default function ModalCollection(props) {
  const {
    store,
    datos,
    localeCode,
    currecy,
    inCartItem,
    setinCartItem,
    modalVisible,
    setModalVisible,
  } = props;

  const styles = useDynamicValue(dynamicStyles);
  return (
    <Modal
      animationType="slide"
      visible={modalVisible}
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.centeredView}>
        <Collection
          datos={datos}
          store={store}
          localeCode={localeCode}
          currecy={currecy}
          inCartItem={inCartItem}
          setinCartItem={setinCartItem}
          setModalVisible={setModalVisible}
          modalVisible={modalVisible}
        />
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  centeredView: {
    backgroundColor: new DynamicValue(colors.white, 'black'),
    flex: 1,
  },
});

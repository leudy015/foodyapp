import React, { useState } from 'react';
import {
  View,
  ScrollView,
  RefreshControl,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import Header from '../Components/Header';
import { dimensions, stylesText } from '../theme';
import { colors } from '../theme';
import { newQuery } from '../GraphQL';
import { Query } from 'react-apollo';
import Loadings from '../Components/PlaceHolder/OrderLoading';
import Paginacion from '../Components/Pagination';
import ProductCard from '../Components/NewCardProduct';
import { CustomText } from '../Components/CustomTetx';

const SeracrProducto = (props: any) => {
  const {
    categoria,
    inCartItem,
    setinCartItem,
    datos,
    setModalVisible,
  } = props;

  const [seach, setSeach] = useState('');
  const [page, setpage] = useState(1);
  const [limit] = useState(10);
  const [category, setcategory] = useState(categoria);
  const [refreshing, setRefreshing] = useState(false);
  const [products, setproducts] = useState([]);
  const styles = useDynamicValue(dynamicStyles);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };
  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };
  const BorderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];
  const borderColor = BorderColor[mode];

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={[
          styles.cat,
          {
            backgroundColor:
              category === item._id ? colors.main : backgroundColor,
            borderColor: borderColor,
          },
        ]}
        onPress={() => {
          setcategory(item._id);
          setproducts(item.products);
        }}>
        <CustomText
          light={category === item._id ? colors.white : colors.back_suave_dark}
          dark={category === item._id ? colors.white : colors.rgb_235}
          style={[stylesText.secondaryText, { fontWeight: '500' }]}>
          {item.name}
        </CustomText>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Header
        title={datos.title}
        search
        placeholder={`Buscar en ${datos.title}`}
        value={seach}
        onChangeText={(value: any) => {
          setSeach(value);
          setpage(1);
        }}
        fromSearch={true}
        setModalVisible={setModalVisible}
      />

      <View style={{ marginBottom: 15 }}>
        <FlatList
          data={datos.CategoryNew}
          renderItem={(item: any) => renderItem(item)}
          keyExtractor={(item: any) => item._id}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        scrollsToTop={true}
        snapToEnd={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={_onRefresh}
            enabled={true}
            title={'Cargando'}
          />
        }>
        <Query
          query={newQuery.GET_NEW_PRODUCT_SEARCH}
          variables={{
            store: datos._id,
            search: seach,
            page: page,
            limit: limit,
            products: products,
          }}>
          {(response) => {
            if (response.loading) {
              return <Loadings />;
            }
            if (response) {
              response.refetch();
              const products =
                response && response.data && response.data.getNewProductoSearch
                  ? response.data.getNewProductoSearch.data
                  : [];
              return (
                <View
                  style={{
                    marginBottom: dimensions.Height(10),
                    justifyContent: 'center',
                    alignSelf: 'center',
                    alignItems: 'center',
                  }}>
                  <ProductCard
                    datosProducto={products}
                    storeID={datos._id}
                    search={seach}
                    localeCode={datos.localeCode}
                    currecy={datos.currecy}
                    inCartItem={inCartItem}
                    setinCartItem={setinCartItem}
                  />

                  <View style={styles.cont}>
                    <Paginacion
                      page={page}
                      onPressMas={() => {
                        if (products.length >= limit) {
                          setpage(page + 1);
                        } else {
                          null;
                        }
                      }}
                      onPressMenos={() => {
                        if (page === 1) {
                          null;
                        } else {
                          setpage(page - 1);
                        }
                      }}
                    />
                  </View>
                </View>
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
    marginBottom: dimensions.Height(3),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(100),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(20),
  },

  cat: {
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 30,
    borderRadius: 100,
  },
  cont: {
    marginTop: 30,
    marginBottom: 40,
    width: dimensions.ScreenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});

export default SeracrProducto;

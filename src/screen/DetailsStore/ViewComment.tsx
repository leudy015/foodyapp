import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';

export default function ViewComment(props) {
  const { averageRating, setModalVisible, res } = props;
  const styles = useDynamicValue(dynamicStyles);

  let consideracion = '';
  let color = '';

  switch (averageRating) {
    case 0:
      consideracion = 'Sin valoraciones';
      color = colors.orange;
      break;

    case 1:
    case 1.1:
    case 1.2:
    case 1.3:
    case 1.4:
    case 1.5:
    case 1.5:
    case 1.6:
    case 1.7:
    case 1.8:
    case 1.9:
      consideracion = 'Mala';
      color = colors.ERROR;
      break;

    case 2:
    case 2.1:
    case 2.2:
    case 2.3:
    case 2.4:
    case 2.5:
    case 2.5:
    case 2.6:
    case 2.7:
    case 2.8:
    case 2.9:
      consideracion = 'Regular';
      color = colors.yellow;
      break;

    case 3:
    case 3.1:
    case 3.2:
    case 3.3:
    case 3.4:
    case 3.5:
    case 3.5:
    case 3.6:
    case 3.7:
    case 3.8:
    case 3.9:
      consideracion = 'Buena';
      color = colors.blue1;
      break;

    case 4:
    case 4.1:
    case 4.2:
    case 4.3:
    case 4.4:
    case 4.5:
    case 4.5:
    case 4.6:
    case 4.7:
    case 4.8:
    case 4.9:
      consideracion = 'Excelente';
      color = colors.green;
      break;

    case 5:
      consideracion = 'Excelente';
      color = colors.green;
      break;
  }
  return (
    <View style={[styles.items]}>
      <View style={{ flexDirection: 'row', padding: 15 }}>
        <Icon
          name="star"
          type="AntDesign"
          size={36}
          color={color}
          style={{
            marginRight: 10,
            alignSelf: 'center',
            marginLeft: -20,
          }}
        />
        <View>
          <CustomText
            numberOfLines={1}
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              {
                paddingBottom: dimensions.Height(0.5),
                width: dimensions.Width(85),
              },
            ]}>
            Comentarios
          </CustomText>
          <CustomText
            light={color}
            dark={color}
            style={[stylesText.secondaryText]}>
            {averageRating} {consideracion} ({res.Valoracion.length})
          </CustomText>
        </View>
        <TouchableOpacity
          onPress={() => setModalVisible(true)}
          style={{
            marginRight: 0,
            alignSelf: 'center',
            marginLeft: 'auto',
          }}>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText]}>
            Ver todos
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    paddingLeft: dimensions.Width(4),
  },
});

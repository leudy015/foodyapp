import React from 'react';
import { View, FlatList } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../theme';
import { CustomText } from '../Components/CustomTetx';
import { stylesText } from '../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import { query } from '../GraphQL';
import { Query } from 'react-apollo';
import moment from 'moment';
import { Avatar } from 'react-native-elements';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../Config/config';
import Loading from '../Components/PlaceHolder/OrderLoading';
import { RatingCalculator } from '../Utils/rating';
import Star from './star';
import LottieView from 'lottie-react-native';

export default function OpinionCard(props: any) {
  const styles = useDynamicValue(dynamicStyles);
  const { res } = props;

  const averageRating = RatingCalculator(res.Valoracion);

  const _renderItem = ({ item }) => {
    switch (item.value) {
      case 0:
        consideracion = 'Sin valoración';
        break;

      case 1:
        consideracion = 'Mala';

        break;

      case 2:
        consideracion = 'Regular';

        break;

      case 3:
        consideracion = 'Buena';

        break;

      case 4:
        consideracion = 'Excelente';

        break;

      case 5:
        consideracion = 'Excelente';

        break;
    }

    return (
      <View style={styles.items}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Avatar
            size={50}
            rounded
            source={{
              uri: NETWORK_INTERFACE_LINK_AVATAR + item.Usuario.avatar,
            }}
          />
          <View style={{ marginLeft: 10 }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {item.Usuario.name}
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginTop: 2 }]}>
              {moment(item.created_at).format('ll')}
            </CustomText>
          </View>
          <View style={{ marginLeft: 'auto' }}>
            <View style={{ flexDirection: 'row' }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignSelf: 'center',
                  marginRight: 10,
                }}>
                <Star star={item.value} styles={{ marginLeft: 'auto' }} />
              </View>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[stylesText.mainText, { textAlign: 'right' }]}>
                {item.value} /{' '}
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryText, { textAlign: 'right' }]}>
                  5
                </CustomText>
              </CustomText>
            </View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryText,
                { textAlign: 'right', paddingTop: 5 },
              ]}>
              {consideracion}
            </CustomText>
          </View>
        </View>
        <View
          style={{
            marginTop: 10,
            marginLeft: 60,
            marginRight: 10,
            marginBottom: 15,
          }}>
          <CustomText
            numberOfLines={3}
            light={colors.back_dark}
            dark={colors.rgb_235}
            style={[
              stylesText.secondaryText,
              { paddingBottom: 5, lineHeight: 18 },
            ]}>
            {item.comment ? item.comment : 'No ha dejado comentario'}
          </CustomText>
        </View>
      </View>
    );
  };

  let consideracion = '';
  let color = '';

  switch (averageRating) {
    case 0:
      consideracion = 'Sin valoraciones';
      color = colors.orange;
      break;

    case 1:
    case 1.1:
    case 1.2:
    case 1.3:
    case 1.4:
    case 1.5:
    case 1.5:
    case 1.6:
    case 1.7:
    case 1.8:
    case 1.9:
      consideracion = 'Mala';
      color = colors.ERROR;
      break;

    case 2:
    case 2.1:
    case 2.2:
    case 2.3:
    case 2.4:
    case 2.5:
    case 2.5:
    case 2.6:
    case 2.7:
    case 2.8:
    case 2.9:
      consideracion = 'Regular';
      color = colors.yellow;
      break;

    case 3:
    case 3.1:
    case 3.2:
    case 3.3:
    case 3.4:
    case 3.5:
    case 3.5:
    case 3.6:
    case 3.7:
    case 3.8:
    case 3.9:
      consideracion = 'Buena';
      color = colors.blue1;
      break;

    case 4:
    case 4.1:
    case 4.2:
    case 4.3:
    case 4.4:
    case 4.5:
    case 4.5:
    case 4.6:
    case 4.7:
    case 4.8:
    case 4.9:
      consideracion = 'Excelente';
      color = colors.green;
      break;

    case 5:
      consideracion = 'Excelente';
      color = colors.green;
      break;
  }

  return (
    <Query query={query.GET_VALORACION} variables={{ restaurant: res._id }}>
      {(response: any) => {
        if (response.loading) {
          return <Loading />;
        }
        if (response) {
          response.refetch();
          const valoracion =
            response && response.data && response.data.getValoraciones
              ? response.data.getValoraciones.data
              : [];
          console.log(valoracion, response);
          return (
            <View
              style={{
                marginTop: dimensions.Height(2),
                marginBottom: dimensions.Height(10),
              }}>
              <View style={styles.restaurante}>
                <View style={{ flexDirection: 'row', padding: 15 }}>
                  <Icon
                    name="star"
                    type="AntDesign"
                    size={36}
                    color={color}
                    style={{
                      marginRight: 10,
                      alignSelf: 'center',
                      marginLeft: -20,
                    }}
                  />
                  <View>
                    <CustomText
                      numberOfLines={1}
                      light={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.secondaryTextBold,
                        {
                          paddingBottom: dimensions.Height(0.5),
                          width: dimensions.Width(85),
                        },
                      ]}>
                      {res.title}
                    </CustomText>
                    <CustomText
                      light={color}
                      dark={color}
                      style={[stylesText.secondaryText]}>
                      {averageRating}/
                      <CustomText
                        light={color}
                        dark={color}
                        style={stylesText.terciaryText}>
                        5
                      </CustomText>{' '}
                      {consideracion} ({res.Valoracion.length}) opiniones
                    </CustomText>
                  </View>
                </View>
              </View>
              <FlatList
                // @ts-ignore
                data={valoracion}
                renderItem={(item) => _renderItem(item)}
                keyExtractor={(item) => item.id}
                showsVerticalScrollIndicator={false}
                ListEmptyComponent={
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      paddingHorizontal: 30,
                    }}>
                    <LottieView
                      source={require('../Assets/Animate/star.json')}
                      autoPlay
                      loop
                      style={{ width: 250, height: 200 }}
                    />
                    <CustomText
                      ligth={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.secondaryText,
                        { textAlign: 'center' },
                      ]}>
                      ¡Aún no hay valoraciones para este establecimiento, sé el
                      primero en opinar haciendo un pedido!
                    </CustomText>
                  </View>
                }
              />
            </View>
          );
        }
      }}
    </Query>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  items: {
    width: 'auto',
    height: 'auto',
    minHeight: 150,
    borderRadius: 15,
    margin: 10,
    padding: 15,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  restaurante: {
    marginLeft: 20,
  },

  rating: {
    borderWidth: 0,
  },
});

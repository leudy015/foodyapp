import React from 'react';
import { View, TouchableOpacity, Share } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import { mutations } from '../../GraphQL';
import { useMutation } from 'react-apollo';
import {
  AddStoreToFavorite,
  DeleteStoreToFavorite,
} from '../../Utils/AddFavourite';
import Scooter from '../../Components/scooter';
import { formaterPrice } from '../../Utils/formaterPRice';
import Navigation from '../../services/Navigration';

export default function InfoDistance(props) {
  const {
    takeaway,
    res,
    refetch,
    id,
    localeCode,
    currecy,
    handleOpen,
    averageRating,
    setModalVisible,
    anadido,
    setanadidotoFavorite,
  } = props;

  const onShare = async (restaurant: any, id: any) => {
    try {
      const result = await Share.share({
        message: `Mira el ofertón que hay en ${restaurant}`,
        url: `https://wilbby.com/store/${res.city}/${res.slug}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const styles = useDynamicValue(dynamicStyles);
  const [crearFavorito] = useMutation(mutations.ANADIR_RESTAURANT_FAVORITE);
  const [eliminarFavorito] = useMutation(
    mutations.ELIMINAR_RESTAURANT_FAVORITE,
  );

  const backgroundColors = {
    light: colors.white,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const adress = res.adress;

  function getRandomNumberBetween(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  const dist = getRandomNumberBetween(1, 2);

  const naviData = {
    data: res,
    km: dist,
    time: '5 min',
    datos: adress,
    localeCode: localeCode,
    currecy: currecy,
  };

  return (
    <View style={[styles.items]}>
      <View style={{ marginBottom: 5 }}>
        <View style={{ flexDirection: 'row', marginTop: 10 }}>
          {takeaway ? (
            <Icon
              name="run"
              type="MaterialCommunityIcons"
              size={20}
              style={styles.miniIcons}
            />
          ) : (
            <Scooter />
          )}
          <CustomText
            ligth={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              {
                marginRight: 10,
                marginTop: 5,
              },
            ]}>
            {takeaway
              ? 'Gratis'
              : `${
                  Number(res.shipping) == 0
                    ? 'Gratis'
                    : `${formaterPrice(
                        res.shipping / 100,
                        localeCode,
                        currecy,
                      )}`
                }`}
          </CustomText>
          <CustomText
            ligth={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryText, { marginTop: 5 }]}>
            {' '}
            <Icon
              name="clockcircleo"
              type="AntDesign"
              size={14}
              style={styles.miniIcons}
            />{' '}
            {res.stimateTime}
          </CustomText>
          <CustomText
            ligth={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              { marginLeft: 10, marginTop: 5 },
            ]}>
            {' '}
            <Icon
              name="shopping-outline"
              type="MaterialCommunityIcons"
              size={14}
              style={styles.miniIcons}
            />{' '}
            {formaterPrice(res.minime / 100, localeCode, currecy)} Mín
          </CustomText>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 10 }}>
          <TouchableOpacity
            style={[
              styles.back,
              {
                backgroundColor: anadido ? colors.ERROR : backgroundColor,
              },
            ]}
            onPress={() => {
              if (anadido) {
                setanadidotoFavorite(false);
                DeleteStoreToFavorite(res._id, refetch, eliminarFavorito);
              } else {
                setanadidotoFavorite(true);
                AddStoreToFavorite(id, res._id, refetch, crearFavorito);
              }
            }}>
            <CustomText>
              <Icon
                name="heart"
                type="AntDesign"
                size={24}
                color={anadido ? colors.white : colors.rgb_153}
              />
            </CustomText>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.back, { width: 70, flexDirection: 'row' }]}
            onPress={() => setModalVisible(true)}>
            <Icon
              name="star"
              type="AntDesign"
              size={16}
              color={colors.orange}
              style={{ marginRight: 5 }}
            />
            <CustomText
              ligth={colors.orange}
              dark={colors.orange}
              style={[stylesText.placeholderText, { color: colors.orange }]}>
              {averageRating.toFixed(1)}
            </CustomText>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.back]}
            onPress={() =>
              Navigation.navigate('Restaurant', { data: naviData })
            }>
            <Icon
              name="more-horiz"
              type="MaterialIcons"
              size={24}
              color={colors.main}
            />
          </TouchableOpacity>

          {res.autoshipping ? (
            <TouchableOpacity style={[styles.back]} onPress={handleOpen}>
              <Icon
                name="location-off"
                type="MaterialIcons"
                size={24}
                color={colors.main}
              />
            </TouchableOpacity>
          ) : null}

          <TouchableOpacity
            style={[styles.back]}
            onPress={() => onShare(res.title, res._id)}>
            <Icon
              name="share-outline"
              type="Ionicons"
              size={22}
              color={colors.main}
              style={{ marginLeft: 2 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
const dynamicStyles = new DynamicStyleSheet({
  back: {
    marginRight: dimensions.Width(2),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    shadowColor: new DynamicValue(colors.light_grey, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  miniIcons: {
    color: new DynamicValue(colors.black, colors.white),
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
  },
});

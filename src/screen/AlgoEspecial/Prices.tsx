import React from 'react';
import { ScrollView, TouchableOpacity } from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, dimensions, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { formaterPrice } from '../../Utils/formaterPRice';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const estimate = [
  {
    id: 1,
    title: 'Menos de',
    price: '10',
  },
  {
    id: 2,
    title: 'Menos de',
    price: '20',
  },
  {
    id: 3,
    title: 'Más de',
    price: '35',
  },
  {
    id: 4,
    title: 'Más de',
    price: '100',
  },
];

export default function Prices(props: any) {
  const { stimatePrice, setstimatePrice, localeCode, currecy } = props;
  const styles = useDynamicValue(dynamicStyles);

  const borderColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const border = borderColors[mode];

  return (
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
      {estimate.map((e: any, i: number) => {
        return (
          <TouchableOpacity
            key={i}
            style={[
              styles.estimate,
              {
                borderColor: stimatePrice === e.price ? colors.main : border,
              },
            ]}
            onPress={() => {
              ReactNativeHapticFeedback.trigger(
                'notificationSuccess',
                optiones,
              );
              if (stimatePrice) {
                setstimatePrice(e.price);
                if (stimatePrice != e.price) {
                  setstimatePrice(e.price);
                }
              } else {
                setstimatePrice(e.price);
              }
            }}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { fontSize: 18 }]}>
              {e.title}
            </CustomText>

            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold, { marginTop: 10 }]}>
              {formaterPrice(e.price, localeCode, currecy)}
            </CustomText>
          </TouchableOpacity>
        );
      })}
    </ScrollView>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  estimate: {
    width: 130,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 20,
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
  },
});

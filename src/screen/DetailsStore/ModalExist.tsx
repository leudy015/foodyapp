import React from 'react';
import { View, Modal, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Navigration from '../../services/Navigration';
import LottieView from 'lottie-react-native';
import { Button } from '../../Components/Button';

export default function ModalExist(props) {
  const { modalVisible, setModalVisible, setinCartItem } = props;
  const styles = useDynamicValue(dynamicStyles);

  const exitStore = () => {
    setinCartItem([]);
    Navigration.goBack();
  };
  return (
    <Modal
      animationType="fade"
      visible={modalVisible}
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.centeredView}>
        <View style={styles.content}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.titleText, { textAlign: 'center' }]}>
              ¿Quieres salir de este establecimiento?
            </CustomText>
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <LottieView
              source={require('../../Assets/Animate/burguer.json')}
              autoPlay
              loop
              style={{ width: 180 }}
            />
            <CustomText
              ligth={colors.black}
              dark={colors.white}
              style={[stylesText.mainText, { textAlign: 'center' }]}>
              Si sales ahora, perderás los productos que has seleccionado.
              ¿Seguro que quieres salir?
            </CustomText>
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={() => exitStore()}
                title="Si, quiero salir"
                titleStyle={styles.buttonTitle}
              />
            </View>
            <TouchableOpacity
              style={{ marginTop: 30 }}
              onPress={() => setModalVisible(false)}>
              <CustomText
                ligth={colors.main}
                dark={colors.main}
                style={[
                  stylesText.secondaryTextBold,
                  { textAlign: 'center', color: colors.main },
                ]}>
                No, me quedo
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  centeredView: {
    backgroundColor: new DynamicValue(colors.white, 'black'),
    flex: 1,
  },

  content: {
    justifyContent: 'space-between',
    flexDirection: 'column',
    height: dimensions.Height(70),
    marginTop: dimensions.Height(15),
    marginBottom: dimensions.Height(10),
    marginHorizontal: dimensions.Width(5),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

import { GOOGLE_API_KEY } from '../Config/config';
import AsyncStorage from '@react-native-community/async-storage';
import Geocoder from 'react-native-geocoding';

Geocoder.init(GOOGLE_API_KEY, { language: 'es' });

export const SetCity = async (lat: number, lgn: number, setcity?: any) => {
  const city = await AsyncStorage.getItem('city');
  Geocoder.from(lat, lgn)
    .then((json) => {
      var addressComponent = json.results[0].address_components[2].long_name;
      var formatted_address = json.results[0].formatted_address;
      AsyncStorage.setItem('adressNameHome', formatted_address);

      if (!city) {
        AsyncStorage.setItem('city', addressComponent);
      }
      if (setcity) {
        setcity(addressComponent);
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

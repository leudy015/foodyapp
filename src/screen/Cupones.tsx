import React from 'react';
import { View, ScrollView, FlatList } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import Header from '../Components/Header';
import { dimensions, colors } from '../theme';
import LottieView from 'lottie-react-native';
import source from '../Assets/Animate/8606-gift-box-effect.json';
import { stylesText } from '../theme/TextStyle';
import { Query } from 'react-apollo';
import { newQuery } from '../GraphQL';
import CuponCard from '../Components/Cupones';
import { useNavigationParam } from 'react-navigation-hooks';
import LoadingAnimated from '../Components/LoadingAnimated';
import Toast from 'react-native-toast-message';

const Cupones = () => {
  const styles = useDynamicValue(dynamicStyles);

  const data = useNavigationParam('data');

  const _itemsRender = ({ item }, refetch) => {
    return (
      <CuponCard
        data={item}
        refetch={refetch}
        localeCode={data.localeCode}
        currecy={data.currecy}
      />
    );
  };

  const _itemsRenderAll = ({ item }, refetch) => {
    return (
      <CuponCard
        data={item}
        refetch={refetch}
        localeCode={data.localeCode}
        currecy={data.currecy}
      />
    );
  };
  return (
    <View style={styles.container}>
      <Toast ref={(ref) => Toast.setRef(ref)} style={{ zIndex: 100 }} />
      <Header title="Códigos promocionales" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            paddingHorizontal: 20,
            paddingTop: 30,
            paddingBottom: dimensions.Height(10),
          }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.h1, { marginVertical: 10 }]}>
            Mis cupones
          </CustomText>
          <Query
            query={newQuery.GET_PRIVATE_PROMO_CODE}
            variables={{
              id: data.user._id,
            }}>
            {(response: any) => {
              if (response.loading) {
                return (
                  <View
                    style={{
                      height: dimensions.Height(30),
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <LoadingAnimated name="Estamos buscando los mejores descuentos" />
                  </View>
                );
              }

              if (response) {
                const respuesta =
                  response && response.data && response.data.getPrivatePromocode
                    ? response.data.getPrivatePromocode.data
                    : [];
                response.refetch();

                return (
                  <FlatList
                    data={respuesta}
                    renderItem={(item: any) =>
                      _itemsRender(item, response.refetch)
                    }
                    keyExtractor={(item: any) => item._id}
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={
                      <View
                        style={{
                          alignSelf: 'center',
                          marginVertical: 30,
                          alignItems: 'center',
                        }}>
                        <LottieView
                          source={source}
                          autoPlay
                          loop
                          style={{ width: 150, height: 150 }}
                        />
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={{
                            textAlign: 'center',
                            fontSize: dimensions.FontSize(20),
                            fontWeight: '200',
                            marginTop: 20,
                            paddingHorizontal: 30,
                          }}>
                          No tienes cupones disponibles, estamos buscando el
                          mejor descuento para ti.
                        </CustomText>
                      </View>
                    }
                  />
                );
              }
            }}
          </Query>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.h1, { marginVertical: 10 }]}>
            Elegidos para mi
          </CustomText>
          <Query
            query={newQuery.GET_PROMO_CODE}
            variables={{ city: data.city }}>
            {(response: any) => {
              if (response.loading) {
                return (
                  <View
                    style={{
                      height: dimensions.Height(30),
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <LoadingAnimated name="Estamos buscando los mejores descuentos" />
                  </View>
                );
              }

              if (response) {
                const respuesta =
                  response && response.data && response.data.getPromocode
                    ? response.data.getPromocode.data
                    : [];
                response.refetch();
                return (
                  <FlatList
                    data={respuesta}
                    renderItem={(item: any) =>
                      _itemsRenderAll(item, response.refetch)
                    }
                    keyExtractor={(item: any) => item._id}
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={
                      <View
                        style={{
                          alignSelf: 'center',
                          marginVertical: 30,
                          alignItems: 'center',
                        }}>
                        <LottieView
                          source={source}
                          autoPlay
                          loop
                          style={{ width: 150, height: 150 }}
                        />
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={{
                            textAlign: 'center',
                            fontSize: dimensions.FontSize(20),
                            fontWeight: '200',
                            marginTop: 20,
                            paddingHorizontal: 30,
                          }}>
                          No tienes cupones disponibles, estamos buscando el
                          mejor descuento para ti.
                        </CustomText>
                      </View>
                    }
                  />
                );
              }
            }}
          </Query>
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.colorInput, 'black'),
    flex: 1,
  },
});

export default Cupones;

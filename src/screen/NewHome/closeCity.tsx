import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import RNRestart from 'react-native-restart';

export default function closeCity(props) {
  const styles = useDynamicValue(dynamicStyles);
  const { data } = props;
  return (
    <View style={[styles.content, { backgroundColor: colors.main }]}>
      <Image source={{ uri: data.imagen }} style={styles.img} />
      <View style={{ marginHorizontal: 20 }}>
        <CustomText
          numberOfLines={2}
          light={colors.white}
          dark={colors.white}
          style={[
            stylesText.h1,
            { textAlign: 'center', marginTop: dimensions.Height(2) },
          ]}>
          {data.title}
        </CustomText>
        <CustomText
          numberOfLines={5}
          light={colors.white}
          dark={colors.white}
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              fontWeight: '200',
              marginTop: dimensions.Height(2),
            },
          ]}>
          {data.subtitle}
        </CustomText>
      </View>
      <TouchableOpacity
        onPress={() => RNRestart.Restart()}
        style={styles.selecteds}>
        <CustomText
          light={colors.white}
          dark={colors.white}
          numberOfLines={1}
          style={stylesText.mainText}>
          Volver a intentarlo
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  img: {
    height: 150,
    width: 150,
    resizeMode: 'cover',
  },

  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
  },

  selecteds: {
    width: dimensions.Width(90),
    height: 60,
    borderRadius: 10,
    flexDirection: 'row',
    backgroundColor: colors.secundary,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
});

import React from 'react';
import { View, ScrollView } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useDynamicValue } from 'react-native-dynamic';
import { colors } from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <ScrollView
      style={{ alignSelf: 'center', backgroundColor: back, height: 50 }}
      horizontal={true}
      showsHorizontalScrollIndicator={false}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: 200,
              height: 40,
              borderRadius: 100,
              marginBottom: 20,
              marginRight: 20,
            }}
          />
          <View
            style={{
              width: 200,
              height: 40,
              borderRadius: 100,
              marginBottom: 20,
              marginRight: 20,
            }}
          />
          <View
            style={{
              width: 200,
              height: 40,
              borderRadius: 100,
              marginRight: 20,
            }}
          />
          <View
            style={{
              width: 200,
              height: 40,
              borderRadius: 100,
              marginRight: 20,
            }}
          />
          <View
            style={{
              width: 200,
              height: 40,
              borderRadius: 100,
              marginRight: 20,
            }}
          />
        </View>
      </SkeletonPlaceholder>
    </ScrollView>
  );
}

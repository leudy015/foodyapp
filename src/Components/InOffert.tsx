import React from 'react';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { dimensions, colors, stylesText } from '../theme';
import { CustomText } from './CustomTetx';
import { View } from 'react-native';
import Icon from 'react-native-dynamic-vector-icons';

export default function Safe(props) {
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.container}>
      <Icon name="tag" type="Feather" size={32} color={colors.orange} />
      <View style={{ width: dimensions.Width(70), marginLeft: 10 }}>
        <CustomText
          style={stylesText.secondaryTextBold}
          light={colors.orange}
          dark={colors.orange}>
          OFERTAS EXCLUSIVAS
        </CustomText>
        <CustomText
          style={[stylesText.secondaryText, { lineHeight: 18 }]}
          light={colors.orange}
          dark={colors.orange}>
          Disfruta de las ofertas exclusivas que tiene para ti {props.title}.
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    marginHorizontal: 15,
    marginVertical: 20,
    width: dimensions.Width(94),
    borderRadius: 7,
    padding: 15,
    height: 'auto',
    backgroundColor: '#ffcb4846',
    flexDirection: 'row',
  },

  user: {
    flexDirection: 'row',
  },
});

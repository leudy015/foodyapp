import React, { useState, useRef } from 'react';
import {
  View,
  TouchableOpacity,
  Alert,
  Modal,
  Platform,
  Keyboard,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors, dimensions, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button } from '../../Components/Button';
import Card from './selecPaymentMethod';
import { mutations } from '../../GraphQL';
import { useMutation } from 'react-apollo';
import Toast from 'react-native-toast-message';
import Loader from '../../Components/ModalLoading';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { ModalWebView } from '../../Components/WebViewMOdal/webViewModal';
import { NETWORK_INTERFACE_URL, STRIPE_CLIENT } from '../../Config/config';
import ApplePay from '../../Components/applePayBotton';
import { formaterPrice } from '../../Utils/formaterPRice';
import stripes from 'tipsi-stripe';

import { Maps } from './Map';
import Prices from './Prices';
import { Comments } from './Comments';
import { TimeSelect } from './time';
import { SelectPayment } from './selectPayment';
import { ErrorPayment } from './ErrorPayments';
import { SuccessPayment } from './success';
import SelectOrigin from './selectOrigin';
import SelectDetination from './selectDetination';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function AlgoEspecial(props: any) {
  const modals = Array.from({ length: 1 }).map((_) => useRef(null).current);
  const {
    lat,
    lgn,
    city,
    user,
    setModalVisible,
    donde,
    vamos,
    quequieres,
    especial,
    title,
    localeCode,
    currecy,
    countryCode,
  } = props;
  const [date, setDate] = useState(null);
  const [selecte, setselecte] = useState('Lo antes posible');
  const [Loading, setLoading] = useState(false);
  const [success, setsuccess] = useState('');
  const [adress1, setadress1] = useState(donde);
  const [puertaPiso1, setpuertaPiso1] = useState('Añadir piso, puerta, etc...');
  const [puertaPiso2, setpuertaPiso2] = useState('Añadir piso, puerta, etc...');
  const [lat1, setlat1] = useState();
  const [lgn1, setlgn1] = useState();
  const [type1, settype1] = useState('');
  const [adress2, setadress2] = useState(vamos);
  const [stimatePrice, setstimatePrice] = useState('');
  const [lat2, setlat2] = useState();
  const [lgn2, setlgn2] = useState();
  const [type2, settype2] = useState('');
  const [nota, setnota] = useState('');
  const [distance, setdistance] = useState(0);
  const [userData, setuserData] = useState(null);
  const [orderID, setorderID] = useState(null);
  const [direccionModal, setDireccionModal] = useState(false);
  const [direccionModalDetination, setDireccionModalDetination] = useState(
    false,
  );

  const [visibleModal, setvisibleModal] = useState(false);
  const [fromPaypal, setfromPaypal] = useState(false);
  const [apple, setapple] = useState(true);
  const [card, setcard] = useState({
    brand: '',
    card: '',
    customer: '',
    fromPaypal: false,
    last4: '',
  });
  const styles = useDynamicValue(dynamicStyles);

  const [createCustonOrder] = useMutation(mutations.CREATE_CUSTOM_ORDER);

  const dist = distance / 1609;

  const total = dist > 0 ? dist * 0.9 : 4.5;

  const total_finaly = total < 4.5 ? 4.6 : total;

  const isOk = () => {
    if (
      (adress1 && adress2 && nota && card.card) ||
      (fromPaypal && adress1 && adress2 && nota) ||
      (apple && adress1 && adress2 && nota)
    ) {
      return true;
    } else {
      return false;
    }
  };

  const SetPayment = (user, refrecado) => {
    refrecado();
    setuserData(user);
    setvisibleModal(true);
  };

  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  const METHOD_DATA = [
    {
      supportedMethods: ['apple-pay'],
      data: {
        merchantIdentifier: 'merchant.com.wilbby',
        supportedNetworks: ['visa', 'mastercard', 'amex'],
        countryCode: countryCode,
        currencyCode: currecy,
        paymentMethodTokenizationParameters: {
          parameters: {
            gateway: 'stripe',
            'stripe:publishableKey': STRIPE_CLIENT,
          },
        },
      },
    },
  ];

  const DETAILS = {
    id: 'wilbby',
    displayItems: [
      {
        label: title,
        amount: {
          currency: currecy,
          value: String(Number(total_finaly).toFixed(2)),
        },
      },
    ],
    total: {
      label: 'Wilbby',
      amount: {
        currency: currecy,
        value: String(Number(total_finaly).toFixed(2)),
      },
    },
  };

  const createOrder = (apple) => {
    Keyboard.dismiss();
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    setLoading(true);
    const input = {
      display_id: Number.parseInt(getRandomArbitrary(100, 1000000)),
      origin: {
        address_name: adress1,
        address_number: puertaPiso1,
        postcode: '',
        type: type1,
        lat: String(lat1),
        lgn: String(lgn1),
      },
      destination: {
        address_name: adress2,
        address_number: puertaPiso2,
        postcode: '',
        type: type2,
        lat: String(lat2),
        lgn: String(lgn2),
      },
      schedule: selecte === 'Lo antes posible' ? false : true,
      distance: String(dist),
      nota: nota,
      date: selecte === 'Lo antes posible' ? 'Lo antes posible' : date,
      city: city,
      userID: user,
      total: String(total_finaly),
      product_stimate_price: String(stimatePrice),
    };

    createCustonOrder({ variables: { input: input } })
      .then(async (res) => {
        if (res.data.createCustonOrder.success) {
          setorderID(res.data.createCustonOrder.data.id);
          setLoading(false);
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);

          if (fromPaypal) {
            modals[0].open();
          } else if (apple) {
            if (Platform.OS === 'ios') {
              //@ts-ignore
              const paymentRequest = new PaymentRequest(METHOD_DATA, DETAILS);

              paymentRequest
                //@ts-ignore
                .show()
                .then(async (paymentResponse) => {
                  const { paymentToken } = paymentResponse.details;
                  const data = {
                    stripeToken: paymentToken,
                    //@ts-ignore
                    amount: total_finaly.toFixed(2) * 100,
                    orders: res.data.createCustonOrder.data.id,
                    currency: currecy,
                  };
                  await fetch(`${NETWORK_INTERFACE_URL}/stripe/chargeToken`, {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                  }).then(async (res) => {
                    const response = await res.json();
                    if (response.status === 'succeeded') {
                      ReactNativeHapticFeedback.trigger(
                        'notificationSuccess',
                        optiones,
                      );
                      paymentResponse.complete('success');
                      setsuccess('success');
                      setLoading(false);
                    } else {
                      setLoading(false);
                      setsuccess('failed');
                    }
                  });
                })
                .catch((e) => console.log(e));
            }
          } else {
            const input = {
              card: card.card,
              customers: card.customer,
              //@ts-ignore
              amount: total_finaly.toFixed(2) * 100,
              currency: currecy,
              orders: res.data.createCustonOrder.data.id,
            };

            let resp = await fetch(
              `${NETWORK_INTERFACE_URL}/payment-existing-card-custom-order`,
              {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify(input),
              },
            );
            const payments = await resp.json();
            const { status, client_secret } = payments; // from server */
            if (status === 'requires_action') {
              let authResponse: any;
              try {
                authResponse = await stripes.authenticatePaymentIntent({
                  clientSecret: client_secret,
                });

                if (authResponse.status === 'succeeded') {
                  ReactNativeHapticFeedback.trigger(
                    'notificationSuccess',
                    optiones,
                  );
                  setLoading(false);
                  setsuccess('success');
                }
              } catch (error) {
                setLoading(false);
                setsuccess('failed');
                ReactNativeHapticFeedback.trigger(
                  'notificationSuccess',
                  optiones,
                );
              }
            } else if (status === 'succeeded') {
              ReactNativeHapticFeedback.trigger(
                'notificationSuccess',
                optiones,
              );
              setLoading(false);
              setsuccess('success');
            } else {
              setLoading(false);
              setsuccess('failed');
              ReactNativeHapticFeedback.trigger(
                'notificationSuccess',
                optiones,
              );
            }
          }
        } else {
          setLoading(false);
          setsuccess('failed');
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          Toast.show({
            text1: 'Algo va mal',
            text2: 'Algo no va bien intentalo de nuevo',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 4000,
          });
        }
      })
      .catch(() => {
        ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
        setLoading(false);
        setsuccess('failed');
      });
  };

  return (
    <View style={styles.content}>
      <Loader loading={Loading} color={colors.main} />
      {success ? (
        <View style={{ marginHorizontal: 15 }}>
          {success === 'success' ? (
            <SuccessPayment setModalVisible={setModalVisible} />
          ) : (
            <ErrorPayment setsuccess={setsuccess} />
          )}
        </View>
      ) : (
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.h1, { marginLeft: 15 }]}>
            ¿Qué quieres que hagamos hoy por ti?
          </CustomText>

          <View style={styles.Adress}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold, { marginLeft: 15 }]}>
              Dirección de recogida y entrega
            </CustomText>

            <Maps
              originLat={lat1}
              originLng={lgn1}
              detinationLat={lat2}
              detitationLgn={lgn2}
              setDireccionModal={setDireccionModal}
              setDireccionModalDetination={setDireccionModalDetination}
              adress1={adress1}
              adress2={adress2}
              puertaPiso1={puertaPiso1}
              puertaPiso2={puertaPiso2}
              localeCode={localeCode}
              lat={lat}
              lgn={lgn}
              dist={dist}
              setdistance={setdistance}
            />

            {especial ? (
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  { marginTop: 30, marginBottom: 10, marginLeft: 15 },
                ]}>
                Precio estimado del producto
              </CustomText>
            ) : null}

            {especial ? (
              <Prices
                stimatePrice={stimatePrice}
                setstimatePrice={setstimatePrice}
                localeCode={localeCode}
                currecy={currecy}
              />
            ) : null}

            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                { marginLeft: 15, marginTop: 30 },
              ]}>
              Tu Wilbby
            </CustomText>

            <Comments quequieres={quequieres} nota={nota} setnota={setnota} />

            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                { marginLeft: 15, marginTop: 30 },
              ]}>
              ¿Cuádo te lo llevamos?
            </CustomText>

            <TimeSelect
              selecte={selecte}
              date={date}
              setselecte={setselecte}
              setDate={setDate}
            />

            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                { marginLeft: 15, marginTop: 30 },
              ]}>
              Método de pago
            </CustomText>

            <SelectPayment
              fromPaypal={fromPaypal}
              SetPayment={SetPayment}
              card={card}
            />

            <View
              style={{
                marginTop: 30,
                flexDirection: 'row',
                marginHorizontal: 15,
              }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryTextBold, { fontSize: 22 }]}>
                {dist > 0 ? 'Total envío' : 'Precio estimado.....'}
              </CustomText>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  { fontSize: 22, marginLeft: 'auto' },
                ]}>
                {formaterPrice(total_finaly, localeCode, currecy)}
              </CustomText>
            </View>

            <View style={{ marginTop: 30, marginHorizontal: 15 }}>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[stylesText.secondaryText]}>
                El precio del envío puede variar por la distancia recorrida, el
                coste se estima en unos{' '}
                {formaterPrice(0.99, localeCode, currecy)} / km.
              </CustomText>

              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[stylesText.secondaryText, { marginTop: 20 }]}>
                Entregas sin contacto, cuando sea posible los repartidores
                dejarán el pedido en tu puerta.
              </CustomText>
            </View>

            {Platform.OS === 'ios' ? (
              <ApplePay
                border={10}
                onPress={() => {
                  if (isOk()) {
                    setapple(true);
                    createOrder(true);
                  } else {
                    Alert.alert('Debes completar todos los campos para pagar');
                  }
                }}
              />
            ) : null}

            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={[
                  styles.buttonView,
                  { backgroundColor: isOk() ? colors.main : colors.rgb_153 },
                ]}
                onPress={() => (isOk() ? createOrder(false) : null)}
                title="Confirmar envío"
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
      )}
      <SelectOrigin
        lats={lat}
        lgns={lgn}
        direccionModal={direccionModal}
        setDireccionModal={setDireccionModal}
        valueAdress={(value: any) => {
          setadress1(value.adress);
          setlat1(value.lat);
          setlgn1(value.lgn);
          settype1(value.type);
          setpuertaPiso1(value.puertaPiso);
        }}
      />
      <SelectDetination
        lats={lat}
        lgns={lgn}
        direccionModal={direccionModalDetination}
        setDireccionModal={setDireccionModalDetination}
        valueAdress={(value: any) => {
          setadress2(value.adress);
          setlat2(value.lat);
          setlgn2(value.lgn);
          settype2(value.type);
          setpuertaPiso2(value.puertaPiso);
        }}
      />
      {userData ? (
        <Modal
          animationType="slide"
          visible={visibleModal}
          presentationStyle="formSheet"
          statusBarTranslucent={true}
          onRequestClose={() => setvisibleModal(false)}>
          <View style={styles.centeredView}>
            <View style={styles.HeaderModal}>
              <TouchableOpacity onPress={() => setvisibleModal(false)}>
                <Icon
                  name="closecircleo"
                  type="AntDesign"
                  size={24}
                  style={styles.icons}
                />
              </TouchableOpacity>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  width: dimensions.Width(50),
                }}>
                <CustomText
                  numberOfLines={1}
                  ligth={colors.black}
                  dark={colors.white}
                  style={[stylesText.mainText, { textAlign: 'center' }]}>
                  Selecciona un métodod de pago
                </CustomText>
              </View>
              <TouchableOpacity onPress={() => {}}></TouchableOpacity>
            </View>
            <Card
              datas={userData}
              setvisibleModal={setvisibleModal}
              selectedPayment={(da) => {
                setfromPaypal(da.fromPaypal);
                setcard(da);
              }}
            />
          </View>
        </Modal>
      ) : null}
      <ModalWebView
        //@ts-ignore
        fromPaymet={false}
        ref={(el) => (modals[0] = el)}
        //@ts-ignore
        urlPay={`${NETWORK_INTERFACE_URL}/paypal-custom-order?price=${total_finaly.toFixed(
          2,
        )}&order=${orderID}&currency=${currecy}`}
        setsuccess={setsuccess}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    paddingTop: 30,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  Adress: {
    marginTop: 30,
    marginBottom: dimensions.Height(20),
  },

  HeaderModal: {
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    height: 90,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  centeredView: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    flex: 1,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(92),
  },
  buttonView: {
    width: dimensions.Width(92),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});

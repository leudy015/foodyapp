import React from 'react';
import {
  View,
  Image,
  Modal,
  TouchableOpacity,
  Platform,
  TextInput,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import StarRating from 'react-native-star-rating';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../../Config/config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default function Valoration(props) {
  const {
    onStarRatingPressRiders,
    rest,
    valorar,
    setComment,
    onStarRatingPress,
    starCount,
    riderVal,
    valorateRIder,
    modalVisible,
  } = props;

  const styles = useDynamicValue(dynamicStyles);
  const textConfig = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const config = useDynamicValue(textConfig);

  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={modalVisible}
      statusBarTranslucent={true}>
      <View style={styles.centeredView}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}>
          {rest ? (
            <View style={{ marginBottom: dimensions.Height(10) }}>
              <View
                style={[
                  styles.items,
                  {
                    marginTop: dimensions.Height(10),
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                ]}>
                <View
                  style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Image
                    source={{ uri: rest.storeData.image }}
                    style={styles.logos}
                  />
                  <View>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.titleText,
                        { marginTop: 10, textAlign: 'center' },
                      ]}>
                      {rest.storeData.title}
                    </CustomText>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.secondaryText,
                        { textAlign: 'center' },
                      ]}>
                      Tienda
                    </CustomText>
                    <View
                      style={{
                        justifyContent: 'center',
                        marginTop: dimensions.Height(2),
                        alignSelf: 'center',
                      }}>
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={starCount}
                        selectedStar={(rating) => onStarRatingPress(rating)}
                        fullStarColor={colors.orange}
                      />
                    </View>
                  </View>
                </View>
              </View>

              {rest.orderType === 'delivery' ? (
                <View
                  style={{
                    marginTop: 30,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {rest.courier ? (
                    <View
                      style={[
                        styles.items,
                        { justifyContent: 'center', alignItems: 'center' },
                      ]}>
                      <View>
                        <Image
                          source={{
                            uri:
                              NETWORK_INTERFACE_LINK_AVATAR +
                              rest.courierData.avatar,
                          }}
                          style={styles.logos}
                        />
                      </View>
                      <View>
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={[
                            stylesText.titleText,
                            { marginTop: 10, textAlign: 'center' },
                          ]}>
                          {rest.courierData.name} {rest.courierData.lastName}
                        </CustomText>
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={[
                            stylesText.secondaryText,
                            { textAlign: 'center' },
                          ]}>
                          Repartidor
                        </CustomText>
                        <View
                          style={{
                            justifyContent: 'center',
                            marginTop: dimensions.Height(1),
                            alignSelf: 'center',
                          }}>
                          <StarRating
                            disabled={false}
                            maxStars={5}
                            rating={riderVal}
                            selectedStar={(rating) =>
                              onStarRatingPressRiders(rating)
                            }
                            fullStarColor={colors.orange}
                          />
                        </View>
                      </View>
                    </View>
                  ) : null}
                </View>
              ) : null}

              <View style={[styles.items, { padding: 20 }]}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.secondaryTextBold,
                    { marginTop: 15, marginBottom: 15, textAlign: 'center' },
                  ]}>
                  ¿Cuál fue tu experiencia?
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryText, { textAlign: 'center' }]}>
                  Valora el servicio ofresido por el establecimiento para ayudar
                  a otros clientes como tú.
                </CustomText>
              </View>

              <View
                style={{
                  marginTop: dimensions.Height(1),
                }}>
                <TextInput
                  multiline={true}
                  numberOfLines={4}
                  selectionColor={colors.main}
                  style={styles.input}
                  placeholderTextColor={config}
                  onChangeText={(value) => setComment(value)}
                  placeholder={`Dejar comentario a ${rest.storeData.title}. (Opcional)`}
                />
              </View>
              <View style={styles.selected}>
                <TouchableOpacity
                  onPress={() => {
                    valorar(rest.storeData._id, rest._id, rest);
                    if (rest.orderType === 'delivery') {
                      valorateRIder(
                        rest.courierData._id,
                        rest.courierData.rating,
                      );
                    }
                  }}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                    width: dimensions.Width(90),
                    height: 60,
                  }}>
                  <CustomText
                    light={colors.white}
                    dark={colors.white}
                    numberOfLines={1}
                    style={stylesText.mainText}>
                    Enviar valoración
                  </CustomText>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </KeyboardAwareScrollView>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    height: 'auto',
    minHeight: dimensions.Height(100),
  },

  content__modal: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 6 },
    shadowOpacity: 0.45,
    shadowRadius: 16,
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  itemss: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  itemssActive: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  img: {
    width: 100,
    height: 100,
    borderRadius: 10,
    marginLeft: 15,
    marginRight: dimensions.Height(2),
  },

  constd: {
    flexDirection: 'row',
    height: 130,
  },

  btnSmal: {
    borderRadius: 5,
    marginBottom: 10,
    paddingHorizontal: 15,
    paddingVertical: 10,
    width: 'auto',
  },

  signupButtonContainer: {
    alignSelf: 'center',
    marginRight: 15,
    marginLeft: 'auto',
    marginTop: 25,
  },
  buttonView: {
    width: dimensions.Width(50),
    borderRadius: 100,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  logos: {
    width: 100,
    height: 100,
    borderRadius: 100,
  },

  selected: {
    width: dimensions.Width(94),
    height: 60,
    borderRadius: 10,
    flexDirection: 'row',
    flex: 1,
    backgroundColor: colors.main,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    marginBottom: dimensions.Height(10),
  },

  input: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    padding: Platform.select({
      android: 10,
      ios: dimensions.IsIphoneX() ? 20 : 10,
    }),
    height: 80,
    marginTop: 10,
    marginHorizontal: 10,
    borderRadius: 5,
    color: colors.rgb_153,
    borderWidth: 0,
    borderColor: 'transparent',
    flexDirection: 'row',
    fontSize: 18,
  },
});

import React from 'react';
import { View, Alert, Linking } from 'react-native';
import { dimensions, colors, stylesText } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import LottieView from 'lottie-react-native';
import moment from 'moment';
import CustomAnimationProgress from '../../../Components/ProgressBar';
import Rider from './riders';

const salten = require('../../../Assets/Animate/salten.json');
const chica = require('../../../Assets/Animate/chica.json');

export default function Info(props: any) {
  const { item, heigth } = props;
  const styles = useDynamicValue(dynamicStyles);

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const minute = item.storeData.categoryName === 'Supermercados' ? 105 : 30;

  const minute1 = item.storeData.categoryName === 'Supermercados' ? 120 : 45;

  return (
    <View style={[styles.contentInfo]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText, { marginBottom: 5 }]}>
            {item.scheduled
              ? `Programado para ${moment(item.deliveryTime).format('l')}`
              : 'Hora estimada de entrega'}
          </CustomText>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={stylesText.secondaryTextBold}>
            {item.scheduled
              ? moment(item.deliveryTime).format('hh:mm A')
              : moment(item.created_at)
                  .add(minute, 'minute')
                  .format('hh:mm A')}{' '}
            -{' '}
            {item.scheduled
              ? moment(item.deliveryTime).add(30, 'minute').format('hh:mm A')
              : moment(item.created_at)
                  .add(minute1, 'minute')
                  .format('hh:mm A')}
          </CustomText>
        </View>
        <View>
          <LottieView
            source={item.status === 'En camino' ? chica : salten}
            autoPlay
            loop
            style={{
              width: item.status === 'En camino' ? 40 : 70,
            }}
          />
        </View>
      </View>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 15,
        }}>
        <CustomAnimationProgress
          color1={colors.main}
          color2={colors.main}
          color3={colors.main}
          maxpercent={100}
          percent={item.IntegerValue}
          width={dimensions.Width(90)} //numeric only
          height={14}
          style={{ marginHorizontal: 10, marginBottom: 5 }}
        />
      </View>
      <Rider item={item} heigth={heigth} OpenURLButton={OpenURLButton} />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  contentInfo: {
    width: dimensions.Width(96),
    alignSelf: 'center',
    padding: 20,
    borderRadius: 10,
  },
});

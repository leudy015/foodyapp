import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';

export default function AutoshippingInfo(props) {
  const { handleOpen } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={[styles.items, { marginTop: 15, marginBottom: 15 }]}>
      <View style={{ flexDirection: 'row' }}>
        <View>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText, { width: dimensions.Width(50) }]}>
            Este establecimiento entrega sus pedidos
          </CustomText>
        </View>
        <TouchableOpacity
          onPress={handleOpen}
          style={{
            marginRight: 15,
            alignSelf: 'center',
            marginLeft: 'auto',
          }}>
          <CustomText
            light={colors.main}
            dark={colors.main}
            style={[stylesText.secondaryText]}>
            Más info
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    paddingLeft: dimensions.Width(4),
  },
});

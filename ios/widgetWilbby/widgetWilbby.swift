//
//  widgetwilbby.swift
//  widgetwilbby
//
//  Created by Leudy Martes on 17/2/21.
//

import WidgetKit
import SwiftUI

struct Shared:Decodable {
  let title: String,
      image: String,
      categoryName: String,
      shipping: String,
      stimateTime: String,
      type: String
  
}

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
      SimpleEntry(date: Date(), title: "", image: "", categoryName: "", shipping: "", stimateTime: "", type: "")
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
      let entry = SimpleEntry(date: Date(), title: "", image: "", categoryName: "", shipping: "", stimateTime: "", type: "")
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []
         var title = ""
         var image = ""
         var categoryName = ""
         var shipping = ""
         var stimateTime = ""
         var type = ""
        let sharedDefaults = UserDefaults.init(suiteName: "group.com.wilbby")
          if sharedDefaults != nil {
                do{
                  let shared = sharedDefaults?.string(forKey: "myAppData")
                  if(shared != nil){
                  let data = try JSONDecoder().decode(Shared.self, from: shared!.data(using: .utf8)!)
                    title = data.title
                    image = data.image
                    categoryName = data.categoryName
                    shipping = data.shipping
                    stimateTime = data.stimateTime
                    type = data.type
                  }
                }catch{
                  print("hola")
                  print(error)
                }
          }

      // Generate a timeline consisting of five entries an hour apart, starting from the current date.
             let currentDate = Date()
             for hourOffset in 0 ..< 5 {
                 let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
                 let entry = SimpleEntry(date: entryDate, title: title, image: image, categoryName: categoryName, shipping: shipping, stimateTime: stimateTime, type: type )
                 entries.append(entry)
             }
        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
  let date: Date
  let title: String
  let image: String
  let categoryName: String
  let shipping: String
  let stimateTime: String
  let type: String
}


struct Wilbby_widgetEntryView : View {
    var entry: Provider.Entry
  @Environment(\.widgetFamily) var family
  let hei = UIScreen.main.bounds.height
  
    var body: some View {
      ZStack{
        Color("bg")
       
        VStack {
          HStack {
            Text("¿Qué quieres hoy?").font(family == .systemSmall ? .body : (family == .systemMedium ? .body : .body)).bold().padding(.all, 15)
            
            Spacer(minLength: 0)
            
            ZStack (alignment: .center){
              VStack {
                Image("icon")
                  .resizable()
                  .aspectRatio(contentMode: .fill)
                  .frame(width: 40, height: 40, alignment: .center)
                  .cornerRadius(20)
              }
              
            }
            
            
          }.padding(.bottom, 0)
          
          HStack(alignment: .center) {
            ZStack (alignment: .center){
          
              Circle()
                .fill(Color("bg_card"))
                .frame(width: hei > 700 ? 75 : 65, height: hei > 700 ? 80 : 70)
                .shadow(color: Color("shadow"), radius: 3)
          
              Image("category1")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: hei > 700 ? 60 : 40, height: hei > 700 ? 60 : 40, alignment: .center)
              
              Text("Comida").padding(.top, hei > 700 ? 90 : 80).font(.system(size: 8)).lineLimit(1)
            }
            
            ZStack (alignment: .center){
              
              Circle()
                .fill(Color("bg_card"))
                .frame(width: hei > 700 ? 75 : 65, height: hei > 700 ? 80 : 70)
                .shadow(color: Color("shadow"), radius: 3)
              
              Image("category2")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: hei > 700 ? 60 : 40, height: hei > 700 ? 60 : 40, alignment: .center)
              Text("Supermercados").padding(.top, hei > 700 ? 90 : 80).font(.system(size: 8)).lineLimit(1)
            }
            
            ZStack (alignment: .center){
              Circle()
                .fill(Color("bg_card"))
                .frame(width: hei > 700 ? 75 : 65, height: hei > 700 ? 80 : 70)
                .shadow(color: Color("shadow"), radius: 3)
              
              Image("category3")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: hei > 700 ? 60 : 40, height: hei > 700 ? 60 : 40, alignment: .center)
              Text("Parafarmacia").padding(.top, hei > 700 ? 90 : 80).font(.system(size: 8)).lineLimit(1)
            }
            
            ZStack (alignment: .center){
              Circle()
                .fill(Color("bg_card"))
                .frame(width: hei > 700 ? 75 : 65, height: hei > 700 ? 80 : 70)
                .shadow(color: Color("shadow"), radius: 3)
              
              Image("category4")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: hei > 700 ? 60 : 40, height: hei > 700 ? 60 : 40, alignment: .center)
              
              Text("Tiendas").padding(.top, hei > 700 ? 90 : 80).font(.system(size: 8)).lineLimit(1)
            }
          }.padding(.bottom, 10)
        }.padding(.all, 10)
        
      }
      
    }
}

@main
struct Wilbby_widget: Widget {
    let kind: String = "Wilbby"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            Wilbby_widgetEntryView(entry: entry)
        }
        .supportedFamilies([.systemMedium])
        .configurationDisplayName("Tus secciones favoritas")
        .description("Te facilitamos la vida, ahorrado tiempo con nuestro nuevo acceso rápido a tus secciones más usadas de Wilbby.")
    }
}

struct Wilbby_widget_Previews: PreviewProvider {
    static var previews: some View {
      Wilbby_widgetEntryView(entry: SimpleEntry(date: Date(), title: "", image: "", categoryName: "", shipping: "", stimateTime: "", type: ""))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}

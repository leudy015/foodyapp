import React from 'react';
import { View } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { colors, dimensions } from '../../theme';

export default function PlaceHolder() {
  return (
    <View style={{ alignSelf: 'center', backgroundColor: colors.black }}>
      <SkeletonPlaceholder
        backgroundColor={colors.back_dark}
        highlightColor={colors.back_suave_dark}>
        <View
          style={{
            width: dimensions.Width(100),
            height: dimensions.ScreenHeight,
            alignSelf: 'center',
            alignItems: 'center',
            marginHorizontal: 20,
          }}>
          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

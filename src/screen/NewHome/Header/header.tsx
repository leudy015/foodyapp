import React, { useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  RefreshControl,
  Platform,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../../theme';
import Adress from './Adress';
import Search from './Search';

export default function Header(props: any) {
  const {
    content,
    setopacity,
    city,
    id,
    lat,
    lgn,
    localeCode,
    currecy,
    getPermision,
    isLocation,
    setNotification,
    setcity,
    riders,
    getRidersAvailable,
  } = props;
  const styles = useDynamicValue(dynamicStyles);
  const [scrollY] = useState(new Animated.Value(0));
  const [refreshing, setRefreshing] = useState(false);

  const headerContainerWidth = scrollY.interpolate({
    inputRange: [0, 125],
    outputRange: [dimensions.Width(50), dimensions.Width(33)],
    extrapolate: 'clamp',
  });

  const borderRadius = scrollY.interpolate({
    inputRange: [0, 125],
    outputRange: [0, 30],
    extrapolate: 'clamp',
  });

  const opacity = scrollY.interpolate({
    inputRange: [0, 50],
    outputRange: [100, 0],
    extrapolate: 'clamp',
  });

  const top = scrollY.interpolate({
    inputRange: [0, 125],
    outputRange: [10, dimensions.Height(-6)],
    extrapolate: 'clamp',
  });

  setopacity(scrollY);

  const _onRefresh = () => {
    getRidersAvailable(city);
    setRefreshing(true);
    getPermision();
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  return (
    <Animated.View style={styles.cont}>
      <Animated.View
        style={[
          styles.headerContent,
          {
            height: headerContainerWidth,
            borderBottomLeftRadius: borderRadius,
            borderBottomRightRadius: borderRadius,
          },
        ]}>
        <View style={styles.content}>
          <Adress
            opacity={opacity}
            city={city}
            id={id}
            lat={lat}
            lgn={lgn}
            localeCode={localeCode}
            currecy={currecy}
            setNotification={setNotification}
            setcity={setcity}
            getRidersAvailable={getRidersAvailable}
          />
          <Search
            top={top}
            city={city}
            id={id}
            lat={lat}
            lgn={lgn}
            localeCode={localeCode}
            currecy={currecy}
            isLocation={isLocation}
            setNotification={setNotification}
            riders={riders}
            getRidersAvailable={getRidersAvailable}
          />
        </View>
      </Animated.View>

      <ScrollView
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: scrollY } } },
        ])}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={_onRefresh}
            enabled={true}
            title="Wilbby by EnCaminoo"
          />
        }
        scrollEventThrottle={16}
        stickyHeaderIndices={[1]}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        style={styles.scrollViewContainer}>
        <Animated.View style={styles.containerHome}>{content()}</Animated.View>
      </ScrollView>
    </Animated.View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    flex: 1,
  },
  absoluto: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  containerHome: {
    width: dimensions.Width(100),
    paddingBottom: Platform.select({
      ios: dimensions.Height(5),
      android: dimensions.Height(10),
    }),
  },

  scrollViewContainer: {
    width: dimensions.Width(100),
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  headerContent: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    zIndex: 100,
  },

  content: {
    marginHorizontal: 15,
    paddingBottom: 20,
    marginTop: dimensions.IsIphoneX()
      ? dimensions.Height(6)
      : dimensions.Height(5),
  },

  adrees: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  adreesCont: {
    width: dimensions.Width(92),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  btns: {
    width: 45,
    height: 45,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 10.27,
    elevation: 15,
  },
});

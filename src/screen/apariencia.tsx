import React from 'react';
import { View, Image, FlatList, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import AppIcon from 'react-native-dynamic-app-icon';
import Header from '../Components/Header';
import { dimensions, colors, stylesText, image } from '../theme';
import { CustomText } from '../Components/CustomTetx';

const data = [
  {
    id: 100,
    icon: 'original',
    imagen: image.Original,
  },
  {
    id: 0,
    icon: 'alternate',
    imagen: image.Black,
  },
  {
    id: 1,
    icon: 'alternate1',
    imagen: image.White,
  },
  {
    id: 2,
    icon: 'alternate2',
    imagen: image.Mora,
  },
  {
    id: 3,
    icon: 'alternate3',
    imagen: image.Lgtbi,
  },
  {
    id: 4,
    icon: 'alternate4',
    imagen: image.Trans,
  },
];

export default function Apariencia() {
  const styles = useDynamicValue(dynamicStyles);
  /* ;

  AppIcon.getIconName((result) => {
    Alert.alert('Icon name: ' + result.iconName);
  }); */

  const changeIcon = (icons) => {
    AppIcon.setAppIcon(icons);
  };

  const _renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.icono}
        onPress={() => changeIcon(item.icon)}>
        <Image
          source={item.imagen}
          style={{ width: 100, height: 100, borderRadius: 20 }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Header title="Apariencia" />
      <View style={[styles.icons]}>
        <Image
          source={image.Original}
          style={{
            width: 130,
            height: 130,
            borderRadius: 25,
            justifyContent: 'center',
            alignSelf: 'center',
          }}
        />
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            { marginTop: 10, textAlign: 'center' },
          ]}>
          Versión 1.3.6 Build (105)
        </CustomText>
      </View>

      <CustomText
        light={colors.rgb_153}
        dark={colors.rgb_153}
        style={[
          stylesText.secondaryText,
          { marginTop: dimensions.Height(5), marginLeft: 15 },
        ]}>
        Selecciona un icono para la app
      </CustomText>

      <View style={styles.content}>
        <FlatList
          data={data}
          renderItem={(item: any) => _renderItem(item)}
          keyExtractor={(item: any) => item.id}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
      </View>

      <CustomText
        light={colors.rgb_153}
        dark={colors.rgb_153}
        style={[
          stylesText.secondaryText,
          { marginTop: dimensions.Height(3), textAlign: 'center' },
        ]}>
        Copyright: {new Date().getFullYear()} Wilbby
      </CustomText>
      <CustomText
        light={colors.rgb_153}
        dark={colors.rgb_153}
        style={[
          stylesText.secondaryText,
          { marginTop: dimensions.Height(1), textAlign: 'center' },
        ]}>
        Hecho con 💚 en por el equipo de Wilbby by EnCaminoo
      </CustomText>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  icons: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: dimensions.Height(5),
    width: dimensions.ScreenWidth,
    textAlign: 'center',
  },

  content: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    marginTop: dimensions.Height(2),
    paddingVertical: 20,
  },

  icono: {
    margin: 10,
    borderRadius: 25,
  },
});

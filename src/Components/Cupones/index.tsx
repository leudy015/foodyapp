import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../CustomTetx';
import { stylesText, dimensions, colors } from '../../theme';
import moment from 'moment';
import Icon from 'react-native-dynamic-vector-icons';
import { formaterPrice } from '../../Utils/formaterPRice';
import Clipboard from '@react-native-clipboard/clipboard';
import Toast from 'react-native-toast-message';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function PromoCode(props: any) {
  const { data, localeCode, currecy } = props;
  const styles = useDynamicValue(dynamicStyles);

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const borderColor = backgroundColors[mode];

  const copyToClipboard = () => {
    Clipboard.setString(data.clave);
    Toast.show({
      text1: 'Código copiado',
      text2: 'Código copiado con éxito',
      position: 'top',
      type: 'success',
      topOffset: 50,
      visibilityTime: 4000,
    });
    ReactNativeHapticFeedback.trigger('selection', options);
  };

  return (
    <TouchableOpacity
      style={[styles.container, { borderColor: borderColor }]}
      onPress={() => copyToClipboard()}>
      <View>
        <Icon
          name="ticket-percent-outline"
          type="MaterialCommunityIcons"
          size={24}
          color={colors.main}
        />
      </View>
      <View style={{ marginLeft: 10, width: dimensions.Width(50) }}>
        <CustomText
          style={stylesText.secondaryTextBold}
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}>
          {data.clave}
        </CustomText>
        {data.description ? (
          <CustomText
            numberOfLines={2}
            style={stylesText.secondaryText}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {data.description}
          </CustomText>
        ) : null}

        {data.exprirable ? (
          <CustomText
            style={stylesText.secondaryText}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            Expira:{' '}
            {data.vencido ? 'Expirado' : moment(data.expire).format('LL')}
          </CustomText>
        ) : null}
        <CustomText
          style={stylesText.secondaryText}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          Para usar {data.usage} {data.usage > 1 ? 'veces' : 'vez'}
        </CustomText>
        {data.uniqueStore ? (
          <CustomText
            style={stylesText.secondaryText}
            light={colors.main}
            dark={colors.main}>
            Tienda: {data.Store.title}
          </CustomText>
        ) : null}
        {data.canjeado ? (
          <CustomText
            style={[stylesText.secondaryText, { marginTop: 5 }]}
            light={colors.ERROR}
            dark={colors.ERROR}>
            Utilizado
          </CustomText>
        ) : null}
      </View>
      <View
        style={{
          marginLeft: 'auto',
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}>
        {data.tipo === 'porcentaje' ? (
          <CustomText
            style={stylesText.secondaryTextBold}
            light={colors.black}
            dark={colors.white}>
            {data.descuento}%
          </CustomText>
        ) : (
          <CustomText
            style={stylesText.secondaryTextBold}
            light={colors.black}
            dark={colors.white}>
            {formaterPrice(data.descuento / 100, localeCode, currecy)}
          </CustomText>
        )}
        <CustomText
          style={stylesText.secondaryText}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          En productos
        </CustomText>

        {data.isShipping ? (
          <View
            style={{
              marginTop: 15,
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}>
            <CustomText
              style={stylesText.secondaryTextBold}
              light={colors.black}
              dark={colors.white}>
              GRATIS
            </CustomText>
            <CustomText
              style={stylesText.secondaryText}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              Envío
            </CustomText>
          </View>
        ) : null}
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    borderWidth: 0.5,
    padding: 20,
    marginVertical: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
});

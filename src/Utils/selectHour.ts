export const selectHour = (data: any) => {
  const now = new Date();
  const time1 = new Date(now);

  var days = [
    data.Sunday.day,
    data.Monday.day,
    data.Tuesday.day,
    data.Wednesday.day,
    data.Thursday.day,
    data.Friday.day,
    data.Saturday.day,
  ];

  var d = new Date();
  var dayName = days[d.getDay()];

  const open1 = new Date(now);
  const close1 = new Date(now);

  const open2 = new Date(now);
  const close2 = new Date(now);

  if (dayName === data.Sunday.day) {
    open1.setHours(data.Sunday.openHour1);
    open1.setMinutes(data.Sunday.openMinute1);
    close1.setHours(data.Sunday.closetHour1);
    close1.setMinutes(data.Sunday.closetMinute1);

    open2.setHours(data.Sunday.openHour2);
    open2.setMinutes(data.Sunday.openMinute2);
    close2.setHours(data.Sunday.closetHour2);
    close2.setMinutes(data.Sunday.closetMinute2);

    let finis = false;

    if (data.Sunday.isOpen) {
      if (data.Sunday.openHour2) {
        finis = time1 > close2;
      } else {
        finis = time1 >= close1;
      }
    } else {
      finis = true;
    }

    const t =
      time1 >= open1
        ? data.Sunday.openHour2
          ? finis
            ? open1
            : open2
          : open1
        : open1;
    const c =
      time1 >= open1
        ? data.Sunday.openHour2
          ? finis
            ? close1
            : close2
          : close1
        : close1;

    return { t, c, finis };
  } else if (dayName === data.Monday.day) {
    open1.setHours(data.Monday.openHour1);
    open1.setMinutes(data.Monday.openMinute1);
    close1.setHours(data.Monday.closetHour1);
    close1.setMinutes(data.Monday.closetMinute1);

    open2.setHours(data.Monday.openHour2);
    open2.setMinutes(data.Monday.openMinute2);
    close2.setHours(data.Monday.closetHour2);
    close2.setMinutes(data.Monday.closetMinute2);

    let finis = false;

    if (data.Monday.isOpen) {
      if (data.Monday.openHour2) {
        finis = time1 > close2;
      } else {
        finis = time1 >= close1;
      }
    } else {
      finis = true;
    }

    const t =
      time1 >= open1
        ? data.Monday.openHour2
          ? finis
            ? open1
            : open2
          : open1
        : open1;
    const c =
      time1 >= open1
        ? data.Monday.openHour2
          ? finis
            ? close1
            : close2
          : close1
        : close1;

    return { t, c, finis };
  } else if (dayName === data.Tuesday.day) {
    open1.setHours(data.Tuesday.openHour1);
    open1.setMinutes(data.Tuesday.openMinute1);
    close1.setHours(data.Tuesday.closetHour1);
    close1.setMinutes(data.Tuesday.closetMinute1);

    open2.setHours(data.Tuesday.openHour2);
    open2.setMinutes(data.Tuesday.openMinute2);
    close2.setHours(data.Tuesday.closetHour2);
    close2.setMinutes(data.Tuesday.closetMinute2);

    let finis = false;

    if (data.Tuesday.isOpen) {
      if (data.Tuesday.openHour2) {
        finis = time1 > close2;
      } else {
        finis = time1 >= close1;
      }
    } else {
      finis = true;
    }

    const t =
      time1 >= open1
        ? data.Tuesday.openHour2
          ? finis
            ? open1
            : open2
          : open1
        : open1;
    const c =
      time1 >= open1
        ? data.Tuesday.openHour2
          ? finis
            ? close1
            : close2
          : close1
        : close1;

    return { t, c, finis };
  } else if (dayName === data.Wednesday.day) {
    open1.setHours(data.Wednesday.openHour1);
    open1.setMinutes(data.Wednesday.openMinute1);
    close1.setHours(data.Wednesday.closetHour1);
    close1.setMinutes(data.Wednesday.closetMinute1);

    open2.setHours(data.Wednesday.openHour2);
    open2.setMinutes(data.Wednesday.openMinute2);
    close2.setHours(data.Wednesday.closetHour2);
    close2.setMinutes(data.Wednesday.closetMinute2);

    let finis = false;

    if (data.Wednesday.isOpen) {
      if (data.Wednesday.openHour2) {
        finis = time1 > close2;
      } else {
        finis = time1 >= close1;
      }
    } else {
      finis = true;
    }

    const t =
      time1 >= open1
        ? data.Wednesday.openHour2
          ? finis
            ? open1
            : open2
          : open1
        : open1;
    const c =
      time1 >= open1
        ? data.Wednesday.openHour2
          ? finis
            ? close1
            : close2
          : close1
        : close1;

    return { t, c, finis };
  } else if (dayName === data.Thursday.day) {
    open1.setHours(data.Thursday.openHour1);
    open1.setMinutes(data.Thursday.openMinute1);
    close1.setHours(data.Thursday.closetHour1);
    close1.setMinutes(data.Thursday.closetMinute1);

    open2.setHours(data.Thursday.openHour2);
    open2.setMinutes(data.Thursday.openMinute2);
    close2.setHours(data.Thursday.closetHour2);
    close2.setMinutes(data.Thursday.closetMinute2);

    let finis = false;

    if (data.Thursday.isOpen) {
      if (data.Thursday.openHour2) {
        finis = time1 > close2;
      } else {
        finis = time1 >= close1;
      }
    } else {
      finis = true;
    }

    const t =
      time1 >= open1
        ? data.Thursday.openHour2
          ? finis
            ? open1
            : open2
          : open1
        : open1;
    const c =
      time1 >= open1
        ? data.Thursday.openHour2
          ? finis
            ? close1
            : close2
          : close1
        : close1;

    return { t, c, finis };
  } else if (dayName === data.Friday.day) {
    const a = time1 >= close2;
    open1.setHours(data.Friday.openHour1);
    open1.setMinutes(data.Friday.openMinute1);
    close1.setHours(data.Friday.closetHour1);
    close1.setMinutes(data.Friday.closetMinute1);

    open2.setHours(data.Friday.openHour2);
    open2.setMinutes(data.Friday.openMinute2);
    close2.setHours(data.Friday.closetHour2);
    close2.setMinutes(data.Friday.closetMinute2);

    let finis = false;

    if (data.Friday.isOpen) {
      if (data.Friday.openHour2) {
        finis = time1 > close2;
      } else {
        finis = time1 >= close1;
      }
    } else {
      finis = true;
    }

    const t =
      time1 >= open1
        ? data.Friday.openHour2
          ? finis
            ? open1
            : open2
          : open1
        : open1;
    const c =
      time1 >= open1
        ? data.Friday.openHour2
          ? finis
            ? close1
            : close2
          : close1
        : close1;

    return { t, c, finis };
  } else if (dayName === data.Saturday.day) {
    open1.setHours(data.Saturday.openHour1);
    open1.setMinutes(data.Saturday.openMinute1);
    close1.setHours(data.Saturday.closetHour1);
    close1.setMinutes(data.Saturday.closetMinute1);

    open2.setHours(data.Saturday.openHour2);
    open2.setMinutes(data.Saturday.openMinute2);
    close2.setHours(data.Saturday.closetHour2);
    close2.setMinutes(data.Saturday.closetMinute2);

    let finis = false;

    if (data.Saturday.isOpen) {
      if (data.Saturday.openHour2) {
        finis = time1 > close2;
      } else {
        finis = time1 >= close1;
      }
    } else {
      finis = true;
    }

    const t =
      time1 >= open1
        ? data.Saturday.openHour2
          ? finis
            ? open1
            : open2
          : open1
        : open1;
    const c =
      time1 >= open1
        ? data.Saturday.openHour2
          ? finis
            ? close1
            : close2
          : close1
        : close1;

    return { t, c, finis };
  }
};

import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import source from '../../Assets/Animate/no_order.json';

export default function Scheduled(props) {
  const {
    takeaway,
    isOK,
    res,
    date,
    onOpen,
    selecte,
    details,
    delivery,
  } = props;
  const styles = useDynamicValue(dynamicStyles);

  return (
    <View
      style={[
        styles.items,
        {
          alignItems: 'center',
          width: details ? dimensions.Width(100) : dimensions.Width(85),
        },
      ]}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingTop: 10,
          paddingBottom: 15,
        }}>
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'row',
            width: details ? dimensions.Width(80) : dimensions.Width(70),
            paddingRight: details ? dimensions.Width(20) : dimensions.Width(30),
          }}>
          <LottieView
            source={source}
            autoPlay
            loop
            style={{ width: 50, paddingTop: 3 }}
          />
          <View>
            {takeaway ? (
              <View>
                <CustomText
                  numberOfLines={1}
                  light={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.secondaryTextBold,
                    {
                      paddingTop: 10,
                    },
                  ]}>
                  Para recoger en el establecimiento
                </CustomText>
                <CustomText
                  numberOfLines={1}
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.secondaryText,
                    {
                      width: dimensions.Width(40),
                      paddinBottom: 5,
                      paddingTop: 5,
                    },
                  ]}>
                  {selecte === 'Lo antes posible'
                    ? selecte
                    : moment(date ? date : new Date()).format(
                        'DD-MM-YYYY HH:mm',
                      )}
                </CustomText>
              </View>
            ) : (
              <View>
                {!isOK() ? (
                  <CustomText
                    numberOfLines={1}
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryTextBold,
                      {
                        width: dimensions.Width(40),
                        paddingTop: 10,
                        paddinBottom: 5,
                      },
                    ]}>
                    {selecte === 'Lo antes posible'
                      ? 'Cerrado ahora'
                      : moment(date ? date : new Date()).format(
                          'DD-MM-YYYY HH:mm',
                        )}
                  </CustomText>
                ) : (
                  <CustomText
                    numberOfLines={1}
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryTextBold,
                      {
                        width: dimensions.Width(40),
                        paddingTop: 10,
                        paddinBottom: 5,
                      },
                    ]}>
                    {selecte === 'Lo antes posible'
                      ? selecte
                      : moment(date ? date : new Date()).format(
                          'DD-MM-YYYY HH:mm',
                        )}
                  </CustomText>
                )}

                <CustomText
                  numberOfLines={1}
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.secondaryText,
                    { width: dimensions.Width(40) },
                  ]}>
                  {delivery
                    ? res.llevar
                      ? 'Entrega'
                      : 'Solo entrega a domicilio'
                    : !delivery && res.llevar
                    ? 'Recoger en el establecimiento'
                    : 'Entrega no disponible'}{' '}
                </CustomText>
              </View>
            )}
          </View>
        </View>
        {res.noScheduled ? (
          <View style={[styles.btn, { backgroundColor: 'transparent' }]}></View>
        ) : (
          <TouchableOpacity onPress={onOpen} style={styles.btn}>
            <CustomText
              numberOfLines={1}
              light={colors.main}
              dark={colors.main}
              style={stylesText.secondaryText}>
              Cambiar
            </CustomText>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    height: 'auto',
  },

  btn: {
    marginLeft: 'auto',
    marginTop: 20,
    height: 35,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
    paddingHorizontal: 20,
    borderRadius: 10,
    marginRight: 15,
  },
});

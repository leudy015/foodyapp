import React from 'react';
import {
  Image,
  View,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors, image } from '../../theme';
import { CustomText } from '../CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import { mutations } from '../../GraphQL';
import { useMutation } from 'react-apollo';
import Navigation from '../../services/Navigration';
import { RatingCalculator } from '../../Utils/rating';
import LinearGradient from 'react-native-linear-gradient';
import {
  AddStoreToFavorite,
  DeleteStoreToFavorite,
} from '../../Utils/AddFavourite';
import { Grayscale } from 'react-native-color-matrix-image-filters';
import Scooter from '../scooter';
import { formaterPrice } from '../../Utils/formaterPRice';
import * as Animatable from 'react-native-animatable';
import { selectHour } from '../../Utils/selectHour';
import moment from 'moment';

const height = Dimensions.get('window').height;

const jsCoreDateCreator = (dateString: string) => {
  let dateParam = dateString.split(/[\s-:]/);
  dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString();
  //@ts-ignore
  return new Date(...dateParam);
};

const RestaurantCard = (props: any) => {
  const {
    item,
    id,
    city,
    lat,
    lgn,
    localeCode,
    currecy,
    refetch,
    riders,
  } = props;

  const styles = useDynamicValue(dynamicStyles);
  const [crearFavorito] = useMutation(mutations.ANADIR_RESTAURANT_FAVORITE);
  const [eliminarFavorito] = useMutation(
    mutations.ELIMINAR_RESTAURANT_FAVORITE,
  );

  const backgroundColors = {
    light: colors.white,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const averageRating = RatingCalculator(item.Valoracion);
  const dat = {
    id: item._id,
    yo: id,
    autoshipping: item.autoshipping,
    city: city,
    lat: lat,
    lgn: lgn,
    localeCode: localeCode,
    currecy: currecy,
    store: item,
    refetch: refetch,
    delivery: riders,
  };

  const isOK = () => {
    if (item.open) {
      return true;
    }
    return false;
  };

  const NavToDetails = () => {
    Navigation.navigate('DestailsStore', { data: dat });
  };

  const data = item.schedule;

  const scheduleOnly = item.scheduleOnly;

  //@ts-ignore
  const gettheTime = selectHour(data).t;

  const finis = selectHour(data).finis;

  const moreDay: number = finis || scheduleOnly.hour > 0 ? 1 : 0;

  const d = new Date();
  const day = d.getDate() + moreDay;
  const year = d.getFullYear();
  const month = d.getMonth() + 1;

  const o = month > 9 ? '' : '0';

  //@ts-ignore
  const opentime = `${gettheTime.getHours()}-${gettheTime.getMinutes()}`;

  const min = jsCoreDateCreator(`${year}-${o + month}-${day}-${opentime}`);

  return (
    <TouchableOpacity activeOpacity={100} onPress={() => NavToDetails()}>
      <Animatable.View
        animation="fadeInUp"
        style={styles.container}
        duration={500}
        iterationCount={1}>
        {item.inOffert ? (
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={[colors.orange, '#ffac11']}
            style={[
              styles.tag,
              {
                position: 'absolute',
                marginLeft: 20,
                marginTop: 10,
                width: 90,
              },
            ]}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              style={stylesText.secondaryText}>
              Destacado
            </CustomText>
          </LinearGradient>
        ) : null}
        {item.image ? (
          <ImageBackground
            style={[
              styles.mainImage,
              { height: props.heigth ? props.heigth : 180 },
            ]}
            imageStyle={{ borderRadius: 10 }}
            resizeMode="cover"
            source={image.PlaceHolder}>
            <Grayscale amount={isOK() ? 0 : 1}>
              <Image
                style={[
                  styles.mainImage,
                  { height: props.heigth ? props.heigth : 180 },
                ]}
                source={{ uri: item.image }}
              />
            </Grayscale>
          </ImageBackground>
        ) : (
          <Image
            style={[
              styles.mainImage,
              { height: props.heigth ? props.heigth : 180 },
            ]}
            source={image.PlaceHolder}
          />
        )}
        <View style={[styles.stimate, { zIndex: 1000 }]}>
          <CustomText
            light={colors.black}
            dark={colors.black}
            style={[stylesText.secondaryTextBold, { fontSize: 14 }]}>
            <Icon
              name="clockcircleo"
              type="AntDesign"
              size={12}
              color={colors.black}
            />{' '}
            {item.scheduleOnly.available
              ? `${item.noScheduled ? 'Abre a las:' : 'Programar para:'} ${
                  finis
                    ? 'mañana'
                    : item.scheduleOnly.hour > 23
                    ? 'mañana'
                    : moment(min).format('HH:mm')
                }`
              : item.stimateTime}
          </CustomText>
        </View>
        <View
          style={[styles.types, { height: props.heigth ? props.heigth : 180 }]}>
          <CustomText
            numberOfLines={1}
            light={colors.white}
            dark={colors.white}
            style={[
              stylesText.h1,
              {
                alignItems: 'center',
                alignSelf: 'center',
                textAlign: 'center',
                paddingHorizontal: 20,
              },
            ]}>
            {item.title}
          </CustomText>
          <View style={[styles.type]}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              style={[
                stylesText.terciaryText,
                {
                  alignItems: 'center',
                  alignSelf: 'center',
                  paddingBottom: 3,
                },
              ]}>
              {item.type}
            </CustomText>
          </View>
        </View>

        <View style={{ marginTop: dimensions.Height(1), flexDirection: 'row' }}>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={[
                styles.hearto,
                {
                  backgroundColor: item.anadidoFavorito
                    ? colors.ERROR
                    : backgroundColor,
                },
              ]}
              onPress={() => {
                if (item.anadidoFavorito) {
                  DeleteStoreToFavorite(
                    item._id,
                    props.refetch,
                    eliminarFavorito,
                  );
                } else {
                  AddStoreToFavorite(
                    id,
                    item._id,
                    props.refetch,
                    crearFavorito,
                  );
                }
              }}>
              <Icon
                type="AntDesign"
                name="heart"
                size={18}
                color={item.anadidoFavorito ? colors.white : colors.rgb_153}
              />
            </TouchableOpacity>
            {item.isnew ? (
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#510090', '#4600DA']}
                style={[styles.tag, { marginTop: 8 }]}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={stylesText.secondaryText}>
                  Nuevo
                </CustomText>
              </LinearGradient>
            ) : null}
          </View>

          <View
            style={{
              marginLeft: 'auto',
              flexDirection: 'row',
              marginTop: 5,
            }}>
            <View style={styles.rating}>
              <CustomText
                ligth={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryText]}>
                <Icon
                  name="star"
                  type="AntDesign"
                  size={16}
                  color={colors.orange}
                />{' '}
                {averageRating.toFixed(1)}
              </CustomText>
            </View>
            <View
              style={[
                styles.rating,
                { flexDirection: 'row', alignItems: 'center' },
              ]}>
              {props.riders || item.autoshipping ? <Scooter /> : null}

              {item.llevar ? (
                <>
                  {props.riders ? (
                    <CustomText ligth={colors.black} dark={colors.white}>
                      /
                    </CustomText>
                  ) : null}
                  <Icon
                    name="run"
                    type="MaterialCommunityIcons"
                    size={20}
                    style={styles.icons}
                  />
                </>
              ) : null}

              {item.shipping === 0 ? (
                <View
                  style={{
                    backgroundColor: colors.orange,
                    padding: 5,
                    borderRadius: 100,
                  }}>
                  <CustomText
                    light={colors.white}
                    dark={colors.white}
                    style={stylesText.secondaryText}>
                    Gratis
                  </CustomText>
                </View>
              ) : (
                <View
                  style={{
                    padding: 5,
                    borderRadius: 100,
                    backgroundColor: item.previous_shipping
                      ? colors.orange
                      : '',
                  }}>
                  {props.riders ? (
                    <CustomText
                      ligth={colors.white}
                      dark={colors.white}
                      style={stylesText.secondaryText}>
                      {`${formaterPrice(
                        item.shipping / 100,
                        props.localeCode,
                        props.currecy,
                      )}`}
                    </CustomText>
                  ) : (
                    <>
                      {item.llevar ? (
                        <CustomText
                          ligth={colors.white}
                          dark={colors.white}
                          style={stylesText.secondaryText}>
                          Gratis
                        </CustomText>
                      ) : (
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          {item.autoshipping ? (
                            <CustomText
                              ligth={colors.white}
                              dark={colors.white}
                              style={stylesText.secondaryText}>
                              {`${formaterPrice(
                                item.shipping / 100,
                                props.localeCode,
                                props.currecy,
                              )}`}
                            </CustomText>
                          ) : (
                            <CustomText
                              ligth={colors.white}
                              dark={colors.white}
                              style={stylesText.secondaryText}>
                              No disponible
                            </CustomText>
                          )}
                        </View>
                      )}
                    </>
                  )}
                </View>
              )}
              {item.previous_shipping > 0 ? (
                <View
                  style={{
                    padding: 5,
                    borderRadius: 100,
                  }}>
                  <CustomText
                    ligth={colors.white}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryText,
                      { marginLeft: 5, textDecorationLine: 'line-through' },
                    ]}>
                    {`${formaterPrice(
                      item.previous_shipping / 100,
                      props.localeCode,
                      props.currecy,
                    )}`}
                  </CustomText>
                </View>
              ) : null}
            </View>
          </View>
        </View>
      </Animatable.View>
    </TouchableOpacity>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    paddingHorizontal: dimensions.Width(3),
    marginBottom: dimensions.Height(4),
    borderRadius: 10,
  },

  mainImage: {
    width: dimensions.Width(94),
    height: height < 600 ? 180 : 180,
    borderRadius: 10,
  },

  hearto: {
    width: 40,
    height: 40,
    marginTop: 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginRight: 10,
    shadowColor: new DynamicValue(colors.light_grey, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  type: {
    backgroundColor: 'rgba(0,0,0,.5)',
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 5,
  },

  tag: {
    zIndex: 200,
    marginLeft: 0,
    marginTop: 2,
    padding: 3,
    width: 60,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 0,
    borderRadius: 5,
    marginBottom: 5,
    backgroundColor: colors.green,
  },

  rating: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginLeft: 5,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: new DynamicValue(colors.light_grey, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  volvemos: {
    position: 'absolute',
    bottom: 65,
    left: 30,
    backgroundColor: 'rgba(0,0,0,.7)',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 100,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },

  types: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: dimensions.Width(3),
    width: dimensions.Width(94),
    borderRadius: 10,
    backgroundColor: 'rgba(0,0,0,.3)',
  },

  stimate: {
    position: 'absolute',
    bottom: 70,
    left: 20,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 7,
  },
});

export default RestaurantCard;

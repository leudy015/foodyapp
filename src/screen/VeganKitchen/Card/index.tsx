import React from 'react';
import {
  View,
  FlatList,
  Image,
  ImageBackground,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { Query, useMutation } from 'react-apollo';
import { query, mutations } from '../../../GraphQL';
import { dimensions, colors, stylesText } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import { useTranslation } from 'react-i18next';
import {
  DynamicStyleSheet,
  useDynamicValue,
  DynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import Loading from '../../../Components/PlaceHolder/LoadingKichen';
import { formaterPrice } from '../../../Utils/formaterPRice';
import LinearGradient from 'react-native-linear-gradient';
import { RatingCalculator } from '../../../Utils/rating';
import {
  AddStoreToFavorite,
  DeleteStoreToFavorite,
} from '../../../Utils/AddFavourite';
import { scheduleTime } from '../../../Utils/scheduleTime';
import Navigation from '../../../services/Navigration';
import { Grayscale } from 'react-native-color-matrix-image-filters';
import LottieView from 'lottie-react-native';
import * as Animatable from 'react-native-animatable';
import Scooter from '../../../Components/scooter';

const PlaceHolder = require('../imges/placeholder.png');
const source = require('../../../Assets/Animate/salten.json');

export default function Card(props: any) {
  const { id, city, lat, lgn, localeCode, currecy, category, llevar } = props;
  const styles = useDynamicValue(dynamicStyles);
  const { t } = useTranslation();
  const [crearFavorito] = useMutation(mutations.ANADIR_RESTAURANT_FAVORITE);
  const [eliminarFavorito] = useMutation(
    mutations.ELIMINAR_RESTAURANT_FAVORITE,
  );

  const backgroundColors = {
    light: colors.back_suave_dark,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const renderItem = ({ item }, refetch) => {
    const averageRating = RatingCalculator(item.Valoracion);

    const dat = {
      id: item._id,
      yo: id,
      autoshipping: item.autoshipping,
      city: city,
      lat: lat,
      lgn: lgn,
      localeCode: localeCode,
      currecy: currecy,
      store: item,
      refetch: refetch,
    };
    const a = item.schedule;

    const isOK = () => {
      if (scheduleTime(a) && item.open) {
        return true;
      }
      return false;
    };

    const NavToDetails = () => {
      Navigation.navigate('DestailsStore', { data: dat });
    };

    return (
      <TouchableOpacity onPress={() => NavToDetails()}>
        <Animatable.View
          animation="fadeInUp"
          style={styles.card}
          duration={500}
          iterationCount={1}>
          <View>
            <ImageBackground
              style={styles.image}
              imageStyle={{ borderRadius: 20 }}
              resizeMode="cover"
              source={PlaceHolder}>
              <Grayscale amount={isOK() ? 0 : 1}>
                <Image source={{ uri: item.image }} style={styles.image} />
              </Grayscale>
            </ImageBackground>
          </View>
          <View
            style={{
              marginLeft: 15,
              width: dimensions.IsIphoneX()
                ? dimensions.Width(45)
                : dimensions.Width(40),
            }}>
            <View>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryTextBold]}>
                {item.title}
              </CustomText>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.terciaryText,
                  { marginTop: 5, paddingBottom: 5 },
                ]}>
                {item.type}
              </CustomText>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <CustomText
                light={colors.white}
                dark={colors.white}
                style={[stylesText.terciaryText]}>
                <Icon
                  name="clockcircleo"
                  type="AntDesign"
                  size={12}
                  color={colors.white}
                />{' '}
                {item.stimateTime} ·{' '}
              </CustomText>
              <CustomText
                style={{
                  marginLeft: 10,
                  marginTop: Platform.select({
                    ios: 4,
                    android: -10,
                  }),
                }}>
                <Scooter white width={20} />
              </CustomText>
              <CustomText
                light={colors.white}
                dark={colors.white}
                style={[stylesText.terciaryText]}>
                {item.shipping === 0
                  ? 'Gratis'
                  : formaterPrice(
                      item.shipping / 100,
                      localeCode,
                      currecy,
                    )}{' '}
                ·{' '}
              </CustomText>
              <CustomText
                light={colors.white}
                dark={colors.white}
                style={[stylesText.terciaryText, { marginLeft: 10 }]}>
                <Icon
                  type="AntDesign"
                  name="star"
                  size={18}
                  color={colors.orange}
                />{' '}
                {averageRating.toFixed(1)}
              </CustomText>
            </View>

            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={['#2D008D', '#7334F9']}
              style={[styles.tagshipping, { marginRight: 10 }]}>
              <CustomText
                light={colors.white}
                dark={colors.white}
                style={stylesText.terciaryText}>
                Mín {formaterPrice(item.minime / 100, localeCode, currecy)}
              </CustomText>
            </LinearGradient>
          </View>
          <View style={{ marginLeft: 'auto' }}>
            <TouchableOpacity
              style={[
                styles.hearto,
                {
                  backgroundColor: item.anadidoFavorito
                    ? colors.ERROR
                    : backgroundColor,
                },
              ]}
              onPress={() => {
                if (item.anadidoFavorito) {
                  DeleteStoreToFavorite(item._id, refetch, eliminarFavorito);
                } else {
                  AddStoreToFavorite(id, item._id, refetch, crearFavorito);
                }
              }}>
              <Icon
                type="AntDesign"
                name="heart"
                size={18}
                color={item.anadidoFavorito ? colors.white : colors.rgb_153}
              />
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <Query
        query={query.RESTAURANT_H_K}
        variables={{ city: city, category: category, llevar: llevar }}>
        {(response) => {
          if (response.loading) {
            return <Loading />;
          }
          if (response.error) {
            return <Loading />;
          }
          if (response) {
            response.refetch();
            const data =
              response &&
              response.data &&
              response.data.getRestaurantHighkitchen
                ? response.data.getRestaurantHighkitchen.data
                : [];
            return (
              <FlatList
                data={data}
                renderItem={(item: any) => renderItem(item, response.refetch)}
                keyExtractor={(item: any) => item._id}
                showsVerticalScrollIndicator={false}
                ListEmptyComponent={
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      width: dimensions.Width(92),
                      marginTop: dimensions.Height(5),
                    }}>
                    <LottieView
                      source={source}
                      autoPlay
                      loop
                      style={{ width: 200 }}
                    />
                    <CustomText
                      ligth={colors.white}
                      dark={colors.white}
                      style={[
                        stylesText.secondaryText,
                        {
                          marginTop: 20,
                          color: colors.rgb_235,
                          letterSpacing: 3,
                        },
                      ]}>
                      No hay restaurantes disponibles
                    </CustomText>
                  </View>
                }
              />
            );
          }
        }}
      </Query>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    flexDirection: 'row',
    marginVertical: 5,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: colors.back_dark,
  },

  image: {
    width: 100,
    height: 100,
    resizeMode: 'cover',
    borderRadius: 20,
  },

  tagshipping: {
    zIndex: 200,
    marginLeft: 0,
    marginTop: 10,
    padding: 3,
    width: 120,
    height: 22,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 0,
    borderRadius: 100,
    marginBottom: 5,
    backgroundColor: colors.green,
  },

  hearto: {
    width: 40,
    height: 40,
    marginTop: 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    shadowColor: new DynamicValue(colors.back_suave_dark, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});

import React from 'react';
import { TouchableOpacity } from 'react-native';
import { CustomText } from '../CustomTetx';
import { useTranslation } from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors, stylesText } from '../../theme';
import Icon from 'react-native-dynamic-vector-icons';

export default function ApplePayBottom(props) {
  const styles = useDynamicValue(dynamicStyles);
  const { onPress, border, padding, noTop } = props;
  const { t } = useTranslation();

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.btn,
        {
          borderRadius: border,
          padding: padding ? padding : 15,
          marginTop: noTop ? 20 : dimensions.Height(5),
          alignSelf: 'center',
        },
      ]}>
      <Icon name="apple1" type="AntDesign" size={26} style={styles.icon} />
      <CustomText
        light={colors.white}
        dark={colors.black}
        style={[stylesText.mainText, { marginTop: 5 }]}>
        {t('applepay:title')}
      </CustomText>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  btn: {
    width: dimensions.Width(92),
    flexDirection: 'row',
    padding: 15,
    backgroundColor: new DynamicValue(colors.black, colors.white),
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  icon: {
    color: new DynamicValue(colors.white, colors.black),
    marginRight: 10,
  },
});

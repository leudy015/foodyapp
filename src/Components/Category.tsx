import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {CustomText} from '../Components/CustomTetx';
import {useQuery} from 'react-apollo';
import {query} from '../GraphQL';
import {stylesText} from '../theme/TextStyle';
import {dimensions, colors} from '../theme';
import AsyncStorage from '@react-native-community/async-storage';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Loading from '../Components/PlaceHolder/Category';

const Category = (props: any) => {
  const [active, setActive] = useState('5f25f836e63b4e09048aa2d3');

  const {loading, error, data} = useQuery(query.CATEGORY);
  if (loading) return <Loading />;
  if (error) return console.log(error);
  const category =
    data && data ? data.getCategory.data : data.getCategory.data.data;

  const activar = (id: any) => {
    setActive(id);
    AsyncStorage.setItem('category', id);
    props.getCategory();
  };

  const styles = useDynamicValue(dynamicStyles);

  return (
    <View style={styles.container}>
      {category.map((index: any, i: any) => {
        return (
          <TouchableOpacity
            key={i}
            style={active === index._id ? styles.catActive : styles.cat}
            onPress={() => activar(index._id)}>
            <CustomText
              light={active === index._id ? colors.white : colors.black}
              dark={colors.white}
              style={
                active === index._id
                  ? stylesText.secondaryTextBold
                  : stylesText.secondaryText
              }>
              {index.title}
            </CustomText>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    paddingHorizontal: dimensions.Width(3),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(1),
  },

  cat: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 10,
    borderRadius: 100,
    width: 'auto',
    textAlign: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },

  catActive: {
    backgroundColor: colors.main,
    padding: 10,
    borderRadius: 100,
    width: 'auto',
    textAlign: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
});

export default Category;

import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity, Modal, Platform } from 'react-native';
import { CustomText } from '../CustomTetx';
import { dimensions, colors, stylesText } from '../../theme';
import Complement from './complement';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import DetailProducto from './DetailsProduct';
import AsyncStorage from '@react-native-community/async-storage';
import { formaterPrice } from '../../Utils/formaterPRice';
import { revoveItemInCart, AddItemsToCadt } from './AddAndRemove';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export interface Producto {
  item: any;
  storeID: string;
  localeCode: string;
  currecy: string;
  inCartItem: [];
  setinCartItem?: any;
  itemId: number;
  quantityProdt: number;
  addToCart: boolean;
  quantity: number;
  setinCartItems?: any;
}
export default function Card(props: Producto) {
  const [visibleModal, setModalVisible] = useState(false);
  const [ids, setID] = useState(null);
  const [dataProducto, setdataProducto] = useState(null);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {
    storeID,
    localeCode,
    currecy,
    inCartItem,
    setinCartItem,
    item,
    itemId,
    quantityProdt,
    addToCart,
    quantity,
    setinCartItems,
  } = props;

  const openModal = (datos: any) => {
    setdataProducto(datos);
    setModalVisible(true);
  };

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const getId = async () => {
    const id = await AsyncStorage.getItem('id');
    setID(id);
  };

  useEffect(() => {
    getId();
  }, [ids]);

  const addToCarstItems = async (items: any, quantity: number) => {
    Object.assign(items, {
      quantity: quantity,
    });
    const input = {
      id: itemId,
      userId: ids,
      storeId: storeID,
      productId: items._id,
      addToCart: true,
      items: items,
    };
    AddItemsToCadt(
      inCartItem,
      setinCartItem,
      items._id,
      input,
      setModalVisible,
      2,
    );
  };

  const totaly = (item.price * quantityProdt) / 100;

  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => openModal(item)}>
          <Animatable.View
            animation="bounceInLeft"
            style={styles.cardActive}
            duration={700}
            iterationCount={1}>
            <View
              style={{
                marginLeft: 15,
                marginRight: 15,
              }}>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  {
                    width: dimensions.IsIphoneX()
                      ? dimensions.Width(63)
                      : dimensions.Width(60),
                    marginBottom: 10,
                  },
                ]}>
                <CustomText
                  numberOfLines={1}
                  light={colors.main}
                  dark={colors.main}
                  style={[stylesText.secondaryTextBold]}>
                  {quantityProdt} ×{' '}
                </CustomText>
                {item.name}
              </CustomText>
              <Complement
                localeCode={localeCode}
                currecy={currecy}
                data={item}
              />
              <View
                style={{
                  marginTop: 5,
                  width: dimensions.Width(88),
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <CustomText
                  numberOfLines={2}
                  light={colors.main}
                  dark={colors.main}
                  style={[stylesText.secondaryTextBold]}>
                  {formaterPrice(totaly, localeCode, currecy)}
                </CustomText>

                <View style={styles.sumaResta}>
                  <TouchableOpacity
                    onPress={() => {
                      //@ts-ignore
                      ReactNativeHapticFeedback.trigger(type, optiones);

                      if (quantity === 1) {
                        setinCartItems([]);
                      }

                      if (addToCart && quantityProdt === 1) {
                        revoveItemInCart(
                          inCartItem,
                          setinCartItem,
                          itemId,
                          null,
                          quantity,
                        );
                      } else {
                        addToCarstItems(item, quantityProdt - 1);
                      }
                    }}
                    style={[
                      styles.plus,
                      {
                        backgroundColor:
                          addToCart && quantityProdt === 1
                            ? '#f5365c5a'
                            : 'rgba(197, 248, 116, 0.3)',
                      },
                    ]}>
                    {addToCart && quantityProdt === 1 ? (
                      <Icon
                        name="delete"
                        type="AntDesign"
                        size={20}
                        color={colors.ERROR}
                      />
                    ) : (
                      <Icon
                        name="minus"
                        type="AntDesign"
                        size={20}
                        color={colors.main}
                      />
                    )}
                  </TouchableOpacity>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.white}
                    style={stylesText.secondaryTextBold}>
                    {quantityProdt}
                  </CustomText>
                  <TouchableOpacity
                    onPress={() => {
                      //@ts-ignore
                      ReactNativeHapticFeedback.trigger(type, optiones);
                      addToCarstItems(item, quantityProdt + 1);
                    }}
                    style={[
                      styles.plus,
                      {
                        backgroundColor: 'rgba(197, 248, 116, 0.3)',
                      },
                    ]}>
                    <Icon
                      name="plus"
                      type="AntDesign"
                      size={20}
                      color={colors.main}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Animatable.View>
        </TouchableOpacity>
      </View>
      {dataProducto ? (
        <Modal
          animationType="slide"
          visible={visibleModal}
          presentationStyle="formSheet"
          statusBarTranslucent={true}
          collapsable={true}
          onRequestClose={() => setModalVisible(false)}>
          <View style={styles.centeredView}>
            <DetailProducto
              datas={dataProducto}
              quantityTotal={item.quantity}
              option={item.subItems}
              itemId={itemId}
              setModalVisible={setModalVisible}
              localeCode={localeCode}
              currecy={currecy}
              addToCarts={true}
              userId={ids}
              storeId={storeID}
              inCartItem={inCartItem}
              setinCartItem={setinCartItem}
            />
          </View>
        </Modal>
      ) : null}
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.ScreenWidth,
  },
  centeredView: {
    backgroundColor: new DynamicValue(colors.colorInput, 'black'),
    flex: 1,
  },
  card: {
    padding: 0,
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  cardActive: {
    padding: 0,
    paddingTop: 20,
    paddingBottom: 20,
    paddingRight: 20,
    paddingLeft: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderLeftWidth: 7,
    borderLeftColor: colors.main,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  proIMG: {
    width: 100,
    height: 100,
    borderRadius: 15,
    backgroundColor: colors.white,
  },

  btn: {
    height: 30,
    minWidth: 100,
    width: 'auto',
    paddingHorizontal: 10,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    borderRadius: 5,
  },

  tagshipping: {
    zIndex: 200,
    marginHorizontal: 5,
    marginTop: 10,
    padding: 3,
    width: 60,
    height: 22,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 0,
    borderRadius: 100,
    marginBottom: 5,
    backgroundColor: colors.green,
  },

  sumaResta: {
    width: 140,
    height: 45,
    borderRadius: 7,
    marginLeft: 'auto',
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
  },

  iconos: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  plus: {
    padding: 7,
    borderRadius: 100,
  },
});

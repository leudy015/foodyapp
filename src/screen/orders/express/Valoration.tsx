import React, { useEffect, useState } from 'react';
import {
  View,
  Image,
  Modal,
  ActionSheetIOS,
  Platform,
  Linking,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import { stylesText } from '../../../theme/TextStyle';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-dynamic-vector-icons';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_URL,
} from '../../../Config/config';
import { RatingCalculator } from '../../../Utils/ratingRider';
import Star from '../../../Components/star';
import { useMutation } from 'react-apollo';
import { mutations } from '../../../GraphQL';
import StarRating from 'react-native-star-rating';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Valoration(props) {
  const {
    refetch,
    visibleModal,
    setvisibleModal,
    setModalVisible,
    dataModal,
  } = props;
  const [ids, setID] = useState(null);
  const [riderVal, setriderVal] = useState(0);

  const [actualizarRiders] = useMutation(mutations.ACTUALIZAR_RIDER);
  const [actualizarOrderProcess] = useMutation(mutations.CUSTOM_ORDER_PROCESS);

  const onStarRatingPressRiders = (rating: any) => {
    setriderVal(rating);
  };

  const SendPushNotificationNeworden = (OnesignalID, messageTexteNeworden) => {
    fetch(
      `${NETWORK_INTERFACE_URL}/send-push-notification-riders?IdOnesignal=${OnesignalID}&textmessage=${messageTexteNeworden}`,
    ).catch((err) => err);
  };

  const OrderProcesso = (datas, state, status, progress, refetch, message) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', options);
    const input = {
      id: datas.id,
      riders: datas.riders,
      estado: state,
      status: status,
      progreso: progress,
    };
    actualizarOrderProcess({ variables: { input: input } }).then((res) => {
      if (res.data.actualizarOrderProcess.success) {
        ReactNativeHapticFeedback.trigger('notificationSuccess', options);
        refetch();
        setModalVisible(false);
        if (state != 'Rechazada por el rider') {
          SendPushNotificationNeworden(datas.Riders.OnesignalID, message);
        }
      }
    });
  };

  const valorateRIder = (id, rating, dataRider, refetch) => {
    const input = {
      _id: id,
      rating: rating.concat(riderVal),
    };
    actualizarRiders({ variables: { input: input } }).then((res) => {
      if (res.data.actualizarRiders._id) {
        setvisibleModal(false);
        OrderProcesso(
          dataRider,
          'Valorada',
          'success',
          '100',
          refetch,
          `🌟 El cliente a valorado tu entrega con un ${riderVal} de 5`,
        );
      }
    });
  };

  const styles = useDynamicValue(dynamicStyles);

  useEffect(() => {
    const getId = async () => {
      const id = await AsyncStorage.getItem('id');
      setID(id);
    };
    getId();
  }, [ids]);

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const onPress = () => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: 'Contactar con soporte',
          options: ['Cancelar', 'Asistencia teléfonica', 'Enviar un email'],
          cancelButtonIndex: 0,
        },
        (buttonIndex) => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            OpenURLButton(`tel:+34669124487`);
          } else if (buttonIndex === 2) {
            OpenURLButton('mailto:info@wilbby.com');
          }
        },
      );
    } else {
      OpenURLButton(`tel:+34669124487`);
    }
  };

  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={visibleModal}
      statusBarTranslucent={true}
      presentationStyle="formSheet">
      <View style={styles.headerCont}>
        <View>
          <TouchableOpacity onPress={() => setvisibleModal(false)}>
            <CustomText>
              <Icon
                name="close"
                type="AntDesign"
                size={24}
                style={styles.icons}
              />
            </CustomText>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={[
          styles.const,
          {
            paddingHorizontal: 20,
            paddingTop: 50,
            height: dimensions.ScreenHeight,
          },
        ]}>
        {dataModal.riders ? (
          <>
            <View style={{ marginHorizontal: 10 }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.titleText,
                  { width: dimensions.Width(80), fontSize: 32 },
                ]}>
                Valora tu experiencia con el repartidor
              </CustomText>
            </View>
            <View
              style={[
                styles.cards,
                {
                  flexDirection: 'row',
                  alignItems: 'center',
                  padding: 15,
                },
              ]}>
              <View>
                <Image
                  source={{
                    uri:
                      NETWORK_INTERFACE_LINK_AVATAR + dataModal.Riders.avatar,
                  }}
                  style={styles.logos}
                />
              </View>
              <View>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.titleText]}>
                  {dataModal.Riders.name} {dataModal.Riders.lastName}
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryText]}>
                  Rider
                </CustomText>
                <View style={{ flexDirection: 'row' }}>
                  <Star
                    star={RatingCalculator(dataModal.Riders.rating)}
                    styles={{ marginTop: 5 }}
                  />
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.terciaryText,
                      { marginTop: 7, marginLeft: 5 },
                    ]}>
                    {RatingCalculator(dataModal.Riders.rating).toFixed(1)} / + (
                    {dataModal.Riders.rating.length})
                  </CustomText>
                </View>
              </View>
            </View>

            <View
              style={{
                padding: 20,
                marginTop: dimensions.Height(2),
              }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryText, { marginRight: 15 }]}>
                Valora el servio brindado por el repartidor para ayudar a otros
                clientes como tu.
              </CustomText>
            </View>

            <View
              style={{
                width: dimensions.Width(60),
                justifyContent: 'center',
                marginTop: dimensions.Height(2),
                marginBottom: 50,
                alignSelf: 'center',
              }}>
              <StarRating
                disabled={false}
                maxStars={5}
                rating={riderVal}
                selectedStar={(rating) => onStarRatingPressRiders(rating)}
                fullStarColor={colors.orange}
              />
            </View>

            <TouchableOpacity
              onPress={() =>
                valorateRIder(
                  dataModal.Riders._id,
                  dataModal.Riders.rating,
                  dataModal,
                  refetch,
                )
              }
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                width: dimensions.Width(90),
                height: 60,
                backgroundColor: colors.main,
                borderRadius: 10,
              }}>
              <CustomText
                light={colors.white}
                dark={colors.white}
                numberOfLines={1}
                style={stylesText.mainText}>
                Valorar repartidor
              </CustomText>
            </TouchableOpacity>
          </>
        ) : null}
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  const: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  container: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    height: 'auto',
    minHeight: dimensions.Height(100),
  },

  centeredView: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    flex: 1,
  },

  itemss: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    paddingVertical: 15,
    paddingTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },

  itemssActive: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    paddingVertical: 20,
    paddingTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  img: {
    width: 100,
    height: 100,
    borderRadius: 10,
    marginLeft: 15,
    marginRight: dimensions.Height(2),
    backgroundColor: colors.white,
  },

  headerCont: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 100,
    paddingHorizontal: dimensions.Width(4),
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  closet: {
    color: new DynamicValue(colors.main, colors.white),
    paddingRight: 15,
    paddingLeft: 15,
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  btnhelp: {
    backgroundColor: colors.main,
    paddingHorizontal: 30,
    padding: 7,
    borderRadius: 100,
  },

  textInputContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
  },

  cardscuston: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    marginTop: 30,
    borderRadius: 20,
    shadowColor: new DynamicValue(colors.black, colors.black),
    marginHorizontal: 10,
    shadowOffset: {
      width: 0,
      height: 15,
    },
    shadowOpacity: 0.34,
    shadowRadius: 8.27,
    elevation: 20,
    overflow: 'hidden',
  },

  cards: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    marginTop: 30,
    padding: 20,
    borderRadius: 20,
    shadowColor: new DynamicValue(colors.rgb_235, colors.black),
    marginHorizontal: 10,
    shadowOffset: {
      width: 0,
      height: 15,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  distance: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: -40,
    marginLeft: 'auto',
    marginRight: 15,
    marginBottom: 10,
    borderRadius: 15,
    flexDirection: 'row',
  },

  logos: {
    width: 60,
    height: 60,
    marginRight: 15,
    borderRadius: 50,
  },
});

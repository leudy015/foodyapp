import React, { useState, useEffect } from 'react';
import {
  View,
  Alert,
  Linking,
  Platform,
  DeviceEventEmitter,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import CloseCity from './closeCity';
import { Query } from 'react-apollo';
import { query } from '../../GraphQL';
import checkVersion from 'react-native-store-version';
import HomeScreen from './home';
import { useNavigationParam } from 'react-navigation-hooks';
import OneSignal from 'react-native-onesignal';
import { NETWORK_INTERFACE_URL } from '../../Config/config';
import Loadings from '../../screen/Loading';
import Toast from 'react-native-toast-message';
import QuickActions from 'react-native-quick-actions';

export default function Home() {
  const [city, setCity] = useState(null);
  const [ruteName, setruteName] = useState(null);
  const [id, setid] = useState(null);
  const userID = useNavigationParam('data');

  if (Platform.OS === 'ios') {
    QuickActions.setShortcutItems([
      //@ts-ignore
      {
        type: 'Order', // Required
        title: 'Pedidos', // Optional, if empty, `type` will be used instead
        icon: 'Date', // Icons instructions below
      },
      //@ts-ignore
      {
        type: 'Favourites', // Required
        title: 'Favoritos', // Optional, if empty, `type` will be used instead
        icon: 'Love', // Icons instructions below
      },
      //@ts-ignore
      {
        type: 'Account', // Required
        title: 'Mi cuenta', // Optional, if empty, `type` will be used instead
        icon: 'Contact', // Icons instructions below
      },
    ]);
  }

  DeviceEventEmitter.addListener('quickActionShortcut', (data) => {
    setruteName(data.type);
  });

  const getLocation = async () => {
    const city = await AsyncStorage.getItem('city');
    setCity(city);
  };

  useEffect(() => {
    getLocation();
  }, [city]);

  const getUser = async () => {
    const ids = await AsyncStorage.getItem('id');
    const deviceState = await OneSignal.getDeviceState();
    if (ids) {
      fetch(
        `${NETWORK_INTERFACE_URL}/save-userid-notification?OnesignalID=${deviceState.userId}&id=${ids}&city=${city}`,
      ).catch((err) => console.log(err));
    }
    setid(ids);
  };

  useEffect(() => {
    getUser();
  }, [id]);

  const newVersion = Platform.select({
    ios: '1.4.2',
    android: '21.4',
  });

  useEffect(() => {
    const init = async () => {
      try {
        const check = await checkVersion({
          version: newVersion, // app local version
          iosStoreURL:
            'https://apps.apple.com/es/app/wilbby-comida-a-domicilio/id1553798083',
          androidStoreURL:
            'https://play.google.com/store/apps/details?id=com.foodyapp',
          country: 'es', // default value is 'jp'
        });
        if (check.result === 'new') {
          Alert.alert(
            'Nueva versión de la app',
            'Hay una nueva versión de Wilbby actualízala para disfrutar las nuevas funcionalidades incorporada',
            [
              {
                text: 'Actualizar App',
                onPress: () => Linking.openURL('http://onelink.to/mf5tg4'),
              },
            ],

            { cancelable: false },
          );
        }
      } catch (e) {
        console.log(e);
      }
    };
    init();
  }, []);

  return (
    <Query query={query.GET_CITY} variables={{ city: city }}>
      {(response) => {
        if (response.loading) {
          return <Loadings />;
        }
        if (response) {
          const ciudad =
            response && response.data && response.data.getCity
              ? response.data.getCity.data
              : {};
          response.refetch();

          return (
            <View style={{ flex: 1 }}>
              <Toast ref={(ref) => Toast.setRef(ref)} style={{ zIndex: 100 }} />
              {ciudad && ciudad.close ? (
                <CloseCity data={ciudad} ciudad={city} />
              ) : (
                <HomeScreen
                  cities={city}
                  id={userID}
                  close={ciudad && ciudad.close}
                  refetch={response.refetch}
                  ruteNames={ruteName}
                />
              )}
            </View>
          );
        }
      }}
    </Query>
  );
}

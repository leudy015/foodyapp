import gql from 'graphql-tag';

const USER_DETAIL = gql`
  query getUsuario {
    getUsuario {
      messages
      success
      data {
        _id
        name
        lastName
        email
        avatar
        city
        telefono
        created_at
        updated_at
        termAndConditions
        verifyPhone
        StripeID
        PaypalID
        OnesignalID
        birthdayDate
      }
    }
  }
`;

const CATEGORY = gql`
  query getCategory {
    getCategory {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

const TIPO = gql`
  query getTipo {
    getTipo {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

const HighkitchenCategory = gql`
  query getHighkitchenCategory {
    getHighkitchenCategory {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

const TIPO_TIENDAS = gql`
  query getTipoTienda {
    getTipoTienda {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

const RESTAURANT = gql`
  query getRestaurant($city: String) {
    getRestaurant(city: $city) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        stimateTime
        ispartners
        highkitchen
        shipping
        previous_shipping
        stimateTime
        highkitchen
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        ispartners
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const RESTAURANT_H_K = gql`
  query getRestaurantHighkitchen(
    $city: String
    $category: String
    $llevar: Boolean
  ) {
    getRestaurantHighkitchen(
      city: $city
      category: $category
      llevar: $llevar
    ) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        shipping
        previous_shipping
        stimateTime
        ispartners
        highkitchen
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const RESTAURANT_ID = gql`
  query getRestaurantForID($id: ID) {
    getRestaurantForID(id: $id) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        shipping
        previous_shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const RESTAURANT_FAVORITO = gql`
  query getRestaurantFavorito($id: ID) {
    getRestaurantFavorito(id: $id) {
      message
      success
      list {
        _id
        restaurantID
        usuarioId
        restaurant {
          _id
          title
          image
          llevar
          description
          rating
          anadidoFavorito
          slug
          inOffert
          stimateTime
          ispartners
          highkitchen
          type
          Valoracion {
            id
            user
            comment
            value
            created_at
            restaurant
            Usuario {
              _id
              name
              lastName
              email
              avatar
            }
          }
          salvingPack
          noScheduled
          socialLink {
            instagram
            facebook
            twitter
            web
          }
          scheduleOnly {
            available
            hour
          }
          CategoryNew {
            _id
            name
            description
            account
            products
            posLocationId
            posCategoryType
            subCategories
            posCategoryId
            imageUrl
            Products {
              _id
              name
              description
              account
              location
              productType
              plu
              price
              sortOrder
              isOpen
              deliveryTax
              takeawayTax
              multiply
              multiMax
              posProductId
              posProductCategoryId
              subProducts
              productTags
              posCategoryIds
              imageUrl
              max
              min
              capacityUsages
              parentId
              visible
              snoozed
              recomended
              quantity
              new
              popular
              offert
              previous_price
              subProductSortOrder
              internalId
              cartItems {
                _id
                userId
                storeId
                productId
                items
                addToCart
              }
            }
          }
          adress {
            calle
            numero
            codigoPostal
            ciudad
            lat
            lgn
          }
          city
          categoryID
          autoshipping
          collections
          isDeliverectPartner
          alegeno_url
          categoryName
          minime
          shipping
          previous_shipping
          extras
          menu
          tipo
          isnew
          phone
          email
          logo
          apertura
          cierre
          aperturaMin
          cierreMin
          diaslaborales
          open
          OnesignalID
          schedule {
            Monday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Tuesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Wednesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Thursday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Friday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Saturday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Sunday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }
          }
        }
      }
    }
  }
`;

const RESTAURANT_FOR_CATEGORY = gql`
  query getRestaurantForCategory(
    $city: String
    $category: String
    $tipo: String
    $llevar: Boolean
    $search: String
    $page: Int
    $limit: Int
  ) {
    getRestaurantForCategory(
      city: $city
      category: $category
      tipo: $tipo
      llevar: $llevar
      search: $search
      page: $page
      limit: $limit
    ) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        stimateTime
        ispartners
        highkitchen
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        shipping
        previous_shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const GET_CUPON = gql`
  {
    getCuponAll {
      id
      clave
      tipo
      descuento
    }
  }
`;

const GET_VALORACION = gql`
  query getValoraciones($restaurant: ID) {
    getValoraciones(restaurant: $restaurant) {
      messages
      success
      data {
        id
        comment
        value
        user
        created_at
        Usuario {
          _id
          name
          lastName
          email
          city
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          OnesignalID
        }
      }
    }
  }
`;

const RESTAURANT_SEARCH = gql`
  query getRestaurantSearch(
    $search: String
    $city: String
    $category: String
    $price: Int
    $llevar: Boolean
  ) {
    getRestaurantSearch(
      search: $search
      city: $city
      category: $category
      price: $price
      llevar: $llevar
    ) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        shipping
        previous_shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const GET_OPINIONES = gql`
  query getOpinion($id: ID) {
    getOpinion(id: $id) {
      messages
      success
      data {
        id
        rating
        comment
        plato
        created_at
        user
        Usuario {
          _id
          name
          lastName
          avatar
        }
      }
    }
  }
`;

const GET_ADRESS = gql`
  query getAdress($id: ID) {
    getAdress(id: $id) {
      success
      message
      data {
        id
        formatted_address
        puertaPiso
        city
        postalcode
        type
        usuario
        lat
        lgn
      }
    }
  }
`;

const GET_STORE_IN_OFFERT = gql`
  query getStoreInOffert($city: String, $page: Int, $limit: Int) {
    getStoreInOffert(city: $city, page: $page, limit: $limit) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        shipping
        previous_shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const GET_TIENDAS_IN_OFFERT = gql`
  query getTiendaInOffert($city: String, $page: Int, $limit: Int) {
    getTiendaInOffert(city: $city, page: $page, limit: $limit) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        shipping
        previous_shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const GET_SALVING_IN_OFFERT = gql`
  query getSalvingPackInOffert($city: String) {
    getSalvingPackInOffert(city: $city) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        shipping
        previous_shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const GET_FARMACY_IN_OFFERT = gql`
  query getFarmacyOffert($city: String) {
    getFarmacyOffert(city: $city) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        Valoracion {
          id
          user
          comment
          value
          created_at
          restaurant
          Usuario {
            _id
            name
            lastName
            email
            avatar
          }
        }
        salvingPack
        noScheduled
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        CategoryNew {
          _id
          name
          description
          account
          products
          posLocationId
          posCategoryType
          subCategories
          posCategoryId
          imageUrl
          Products {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            isOpen
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        shipping
        previous_shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const GET_CUSTOM_ORDER = gql`
  query getCustomOrder($id: ID) {
    getCustomOrder(id: $id) {
      success
      messages
      data {
        id
        display_id
        riders
        Riders {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          rating
          OnesignalID
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          OnesignalID
        }
        origin {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        destination {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        schedule
        distance
        nota
        date
        city
        userID
        estado
        status
        progreso
        created_at
        total
        product_stimate_price
      }
    }
  }
`;

const GET_CITY = gql`
  query getCity($city: String) {
    getCity(city: $city) {
      success
      messages
      data {
        id
        close
        city
        title
        subtitle
        imagen
      }
    }
  }
`;

const OFFERT = gql`
  query getOfferts($city: String) {
    getOfferts(city: $city) {
      messages
      success
      data {
        id
        imagen
        city
        store
        cierre
        apertura
        open
      }
    }
  }
`;

const COLLECTIONS = gql`
  query getCollection($store: String) {
    getCollection(store: $store) {
      message
      success
      data {
        _id
        title
        image
        subCollectionItems {
          _id
          title
          collectiontype
          Product {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            deliveryTax
            takeawayTax
            multiply
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            capacityUsages
            parentId
            visible
            snoozed
            recomended
            quantity
            new
            popular
            offert
            previous_price
            subProductSortOrder
            internalId
            cartItems {
              _id
              userId
              storeId
              productId
              items
              addToCart
            }
          }
        }
      }
    }
  }
`;

export const query = {
  USER_DETAIL,
  OFFERT,
  CATEGORY,
  RESTAURANT,
  RESTAURANT_ID,
  RESTAURANT_FAVORITO,
  RESTAURANT_FOR_CATEGORY,
  GET_CITY,
  GET_CUPON,
  GET_VALORACION,
  RESTAURANT_SEARCH,
  GET_OPINIONES,
  GET_ADRESS,
  TIPO,
  TIPO_TIENDAS,
  GET_STORE_IN_OFFERT,
  GET_CUSTOM_ORDER,
  HighkitchenCategory,
  RESTAURANT_H_K,
  COLLECTIONS,
  GET_TIENDAS_IN_OFFERT,
  GET_SALVING_IN_OFFERT,
  GET_FARMACY_IN_OFFERT,
};

import React from 'react';
import { View, FlatList } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Products from '../../Components/NewCardProduct';
import ProductsStore from '../../Components/NewCardProduct/CardStore';

export default function Producs(props) {
  const { res, localeCode, currecy, inCartItem, setinCartItem } = props;
  const styles = useDynamicValue(dynamicStyles);

  const renderItemCategory = ({ item, index }) => {
    return (
      <View>
        <View
          style={[
            styles.itemsMenu,
            { flexDirection: 'row', alignItems: 'center' },
          ]}>
          <View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.titleText, { width: dimensions.Width(60) }]}>
              {item.name}
            </CustomText>
            {item.description ? (
              <CustomText
                numberOfLines={4}
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    width: dimensions.Width(60),
                    paddingTop: 5,
                    textAlign: 'justify',
                    paddingBottom: 5,
                    lineHeight: 17,
                  },
                ]}>
                {item.description}
              </CustomText>
            ) : null}
          </View>
        </View>
        <View
          style={{
            width: dimensions.Width(100),
            alignSelf: 'center',
          }}>
          {res.categoryName == 'Tiendas' ? (
            <ProductsStore
              search={item.name}
              datosProducto={item.Products}
              storeID={res._id}
              localeCode={localeCode}
              currecy={currecy}
              inCartItem={inCartItem}
              setinCartItem={setinCartItem}
            />
          ) : (
            <Products
              search={item.name}
              datosProducto={item.Products}
              storeID={res._id}
              localeCode={localeCode}
              currecy={currecy}
              inCartItem={inCartItem}
              setinCartItem={setinCartItem}
            />
          )}
        </View>
      </View>
    );
  };

  return (
    <FlatList
      data={res.CategoryNew}
      renderItem={(item: any) => renderItemCategory(item)}
      keyExtractor={(item: any) => item.channelLinkId}
      showsVerticalScrollIndicator={false}
      scrollEnabled={false}
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  itemsMenu: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 10,
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    paddingLeft: dimensions.Width(4),
  },

  vertodo: {
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 10,
  },
});

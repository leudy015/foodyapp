import React from 'react';
import {
  TextInput,
  View,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { dimensions, colors } from '../theme';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';

const height = Dimensions.get('window').height;

export const CustomTextInput = (props: any) => {
  const styles = useDynamicStyleSheet(dynamicStyles);
  let prop = { ...props };
  let right = prop.right;
  let left = prop.left;
  delete prop.right;
  delete prop.left;
  return (
    <View style={[styles.content, props.containerStyle]}>
      {right ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 10,
            marginRight: 10,
          }}>
          <Icon
            style={{ alignSelf: 'center' }}
            type={props.type}
            name={props.rightIcon}
          />
        </View>
      ) : null}
      <TextInput
        secureTextEntry={props.secrurity}
        clearButtonMode="while-editing"
        placeholder={props.placeHolder}
        placeholderTextColor={colors.rgb_153}
        autoCompleteType={props.autoCompleteType}
        keyboardType={props.keyboardType}
        {...prop}
        style={[
          props.textInputStyle,
          styles.textInputStyle,
          props.left ? { width: height < 600 ? '75%' : '80%' } : {},
        ]}
      />
      {left ? (
        <TouchableOpacity
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10,
          }}
          onPress={() => props.onPress()}>
          <Icon
            style={{ alignSelf: 'center', marginRight: 10 }}
            type={props.typeLeft}
            name={props.leftIcon}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  textInputStyle: {
    paddingVertical: 10,
    borderColor: colors.grey,
    width: '90%',
    paddingHorizontal: dimensions.Width(0),
    color: new DynamicValue(colors.black, colors.white),
  },
  hashTrix: {
    color: colors.red,
    marginVertical: dimensions.Height(1),
  },
  text: {
    color: new DynamicValue(colors.white, colors.black),
    fontSize: dimensions.FontSize(14),
    justifyContent: 'center',
    textAlignVertical: 'center',
    alignContent: 'center',
    marginVertical: dimensions.Height(1),
  },
  content: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    padding: Platform.select({
      android: 5,
      ios: dimensions.IsIphoneX() ? 8 : 10,
    }),
    marginTop: 10,
    borderRadius: 5,
    color: colors.rgb_153,
    borderColor: new DynamicValue(colors.rgb_153, colors.rgb_153),
    borderWidth: 0.5,
  },
});

import React from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { image, dimensions, colors } from '../../theme';
import { stylesText } from '../../theme/TextStyle';

export default function Horario(props: any) {
  const styles = useDynamicValue(dynamicStyles);
  const { res } = props;
  return (
    <View
      style={{
        marginLeft: 15,
        marginTop: 15,
        paddingBottom: dimensions.Height(5),
      }}>
      <CustomText
        light={colors.black}
        dark={colors.white}
        style={[stylesText.secondaryTextBold, { marginBottom: 10 }]}>
        HORARIO
      </CustomText>

      <View style={styles.horas}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.titleText200, { marginBottom: 5, marginTop: 5 }]}>
          {res.schedule.Monday.day}
        </CustomText>

        {res.schedule.Monday.isOpen ? (
          <View style={styles.item}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginBottom: 5 }]}>
              De{' '}
              {`${res.schedule.Monday.openHour1}:${
                res.schedule.Monday.openMinute1
              }${res.schedule.Monday.openMinute1 === 0 ? '0' : ''}`}{' '}
              a{' '}
              {`${res.schedule.Monday.closetHour1}:${
                res.schedule.Monday.closetMinute1
              }${res.schedule.Monday.closetMinute1 === 0 ? '0' : ''}`}
            </CustomText>
            {res.schedule.Monday.openHour2 ? (
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                Y de{' '}
                {`${res.schedule.Monday.openHour2}:${
                  res.schedule.Monday.openMinute2
                }${res.schedule.Monday.openMinute2 === 0 ? '0' : ''}`}{' '}
                a{' '}
                {`${res.schedule.Monday.closetHour2}:${
                  res.schedule.Monday.closetMinute2
                }${res.schedule.Monday.closetMinute2 === 0 ? '0' : ''}`}
              </CustomText>
            ) : null}
          </View>
        ) : (
          <View style={styles.close}>
            <CustomText
              light={colors.ERROR}
              dark={colors.ERROR}
              style={[stylesText.secondaryText]}>
              Cerrado
            </CustomText>
          </View>
        )}
      </View>

      <View style={styles.horas}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.titleText200, { marginBottom: 5, marginTop: 5 }]}>
          {res.schedule.Tuesday.day}
        </CustomText>

        {res.schedule.Tuesday.isOpen ? (
          <View style={styles.item}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginBottom: 5 }]}>
              De{' '}
              {`${res.schedule.Tuesday.openHour1}:${
                res.schedule.Tuesday.openMinute1
              }${res.schedule.Tuesday.openMinute1 === 0 ? '0' : ''}`}{' '}
              a{' '}
              {`${res.schedule.Tuesday.closetHour1}:${
                res.schedule.Tuesday.closetMinute1
              }${res.schedule.Tuesday.closetMinute1 === 0 ? '0' : ''}`}
            </CustomText>
            {res.schedule.Tuesday.openHour2 ? (
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                Y de{' '}
                {`${res.schedule.Tuesday.openHour2}:${
                  res.schedule.Tuesday.openMinute2
                }${res.schedule.Tuesday.openMinute2 === 0 ? '0' : ''}`}{' '}
                a{' '}
                {`${res.schedule.Tuesday.closetHour2}:${
                  res.schedule.Tuesday.closetMinute2
                }${res.schedule.Tuesday.closetMinute2 === 0 ? '0' : ''}`}
              </CustomText>
            ) : null}
          </View>
        ) : (
          <View style={styles.close}>
            <CustomText
              light={colors.ERROR}
              dark={colors.ERROR}
              style={[stylesText.secondaryText]}>
              Cerrado
            </CustomText>
          </View>
        )}
      </View>

      <View style={styles.horas}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.titleText200, { marginBottom: 5, marginTop: 5 }]}>
          {res.schedule.Wednesday.day}
        </CustomText>

        {res.schedule.Wednesday.isOpen ? (
          <View style={styles.item}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginBottom: 5 }]}>
              De{' '}
              {`${res.schedule.Wednesday.openHour1}:${
                res.schedule.Wednesday.openMinute1
              }${res.schedule.Wednesday.openMinute1 === 0 ? '0' : ''}`}{' '}
              a{' '}
              {`${res.schedule.Wednesday.closetHour1}:${
                res.schedule.Wednesday.closetMinute1
              }${res.schedule.Wednesday.closetMinute1 === 0 ? '0' : ''}`}
            </CustomText>
            {res.schedule.Wednesday.openHour2 ? (
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                Y de{' '}
                {`${res.schedule.Wednesday.openHour2}:${
                  res.schedule.Wednesday.openMinute2
                }${res.schedule.Wednesday.openMinute2 === 0 ? '0' : ''}`}{' '}
                a{' '}
                {`${res.schedule.Wednesday.closetHour2}:${
                  res.schedule.Wednesday.closetMinute2
                }${res.schedule.Wednesday.closetMinute2 === 0 ? '0' : ''}`}
              </CustomText>
            ) : null}
          </View>
        ) : (
          <View style={styles.close}>
            <CustomText
              light={colors.ERROR}
              dark={colors.ERROR}
              style={[stylesText.secondaryText]}>
              Cerrado
            </CustomText>
          </View>
        )}
      </View>

      <View style={styles.horas}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.titleText200, { marginBottom: 5, marginTop: 5 }]}>
          {res.schedule.Thursday.day}
        </CustomText>

        {res.schedule.Thursday.isOpen ? (
          <View style={styles.item}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginBottom: 5 }]}>
              De{' '}
              {`${res.schedule.Thursday.openHour1}:${
                res.schedule.Thursday.openMinute1
              }${res.schedule.Thursday.openMinute1 === 0 ? '0' : ''}`}{' '}
              a{' '}
              {`${res.schedule.Thursday.closetHour1}:${
                res.schedule.Thursday.closetMinute1
              }${res.schedule.Thursday.closetMinute1 === 0 ? '0' : ''}`}
            </CustomText>
            {res.schedule.Thursday.openHour2 ? (
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                Y de{' '}
                {`${res.schedule.Thursday.openHour2}:${
                  res.schedule.Thursday.openMinute2
                }${res.schedule.Thursday.openMinute2 === 0 ? '0' : ''}`}{' '}
                a{' '}
                {`${res.schedule.Thursday.closetHour2}:${
                  res.schedule.Thursday.closetMinute2
                }${res.schedule.Thursday.closetMinute2 === 0 ? '0' : ''}`}
              </CustomText>
            ) : null}
          </View>
        ) : (
          <View style={styles.close}>
            <CustomText
              light={colors.ERROR}
              dark={colors.ERROR}
              style={[stylesText.secondaryText]}>
              Cerrado
            </CustomText>
          </View>
        )}
      </View>

      <View style={styles.horas}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.titleText200, { marginBottom: 5, marginTop: 5 }]}>
          {res.schedule.Friday.day}
        </CustomText>

        {res.schedule.Friday.isOpen ? (
          <View style={styles.item}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginBottom: 5 }]}>
              De{' '}
              {`${res.schedule.Friday.openHour1}:${
                res.schedule.Friday.openMinute1
              }${res.schedule.Friday.openMinute1 === 0 ? '0' : ''}`}{' '}
              a{' '}
              {`${res.schedule.Friday.closetHour1}:${
                res.schedule.Friday.closetMinute1
              }${res.schedule.Friday.closetMinute1 === 0 ? '0' : ''}`}
            </CustomText>
            {res.schedule.Friday.openHour2 ? (
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                Y de{' '}
                {`${res.schedule.Friday.openHour2}:${
                  res.schedule.Friday.openMinute2
                }${res.schedule.Friday.openMinute2 === 0 ? '0' : ''}`}{' '}
                a{' '}
                {`${res.schedule.Friday.closetHour2}:${
                  res.schedule.Friday.closetMinute2
                }${res.schedule.Friday.closetMinute2 === 0 ? '0' : ''}`}
              </CustomText>
            ) : null}
          </View>
        ) : (
          <View style={styles.close}>
            <CustomText
              light={colors.ERROR}
              dark={colors.ERROR}
              style={[stylesText.secondaryText]}>
              Cerrado
            </CustomText>
          </View>
        )}
      </View>

      <View style={styles.horas}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.titleText200, { marginBottom: 5, marginTop: 5 }]}>
          {res.schedule.Saturday.day}
        </CustomText>

        {res.schedule.Saturday.isOpen ? (
          <View style={styles.item}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginBottom: 5 }]}>
              De{' '}
              {`${res.schedule.Saturday.openHour1}:${
                res.schedule.Saturday.openMinute1
              }${res.schedule.Saturday.openMinute1 === 0 ? '0' : ''}`}{' '}
              a{' '}
              {`${res.schedule.Saturday.closetHour1}:${
                res.schedule.Saturday.closetMinute1
              }${res.schedule.Saturday.closetMinute1 === 0 ? '0' : ''}`}
            </CustomText>
            {res.schedule.Saturday.openHour2 ? (
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                Y de{' '}
                {`${res.schedule.Saturday.openHour2}:${
                  res.schedule.Saturday.openMinute2
                }${res.schedule.Saturday.openMinute2 === 0 ? '0' : ''}`}{' '}
                a{' '}
                {`${res.schedule.Saturday.closetHour2}:${
                  res.schedule.Saturday.closetMinute2
                }${res.schedule.Saturday.closetMinute2 === 0 ? '0' : ''}`}
              </CustomText>
            ) : null}
          </View>
        ) : (
          <View style={styles.close}>
            <CustomText
              light={colors.ERROR}
              dark={colors.ERROR}
              style={[stylesText.secondaryText]}>
              Cerrado
            </CustomText>
          </View>
        )}
      </View>

      <View style={styles.horas}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.titleText200, { marginBottom: 5, marginTop: 5 }]}>
          {res.schedule.Sunday.day}
        </CustomText>

        {res.schedule.Sunday.isOpen ? (
          <View style={styles.item}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginBottom: 5 }]}>
              De{' '}
              {`${res.schedule.Sunday.openHour1}:${
                res.schedule.Sunday.openMinute1
              }${res.schedule.Sunday.openMinute1 === 0 ? '0' : ''}`}{' '}
              a{' '}
              {`${res.schedule.Sunday.closetHour1}:${
                res.schedule.Sunday.closetMinute1
              }${res.schedule.Sunday.closetMinute1 === 0 ? '0' : ''}`}
            </CustomText>
            {res.schedule.Sunday.openHour2 ? (
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                Y de{' '}
                {`${res.schedule.Sunday.openHour2}:${
                  res.schedule.Sunday.openMinute2
                }${res.schedule.Sunday.openMinute2 === 0 ? '0' : ''}`}{' '}
                a{' '}
                {`${res.schedule.Sunday.closetHour2}:${
                  res.schedule.Sunday.closetMinute2
                }${res.schedule.Sunday.closetMinute2 === 0 ? '0' : ''}`}
              </CustomText>
            ) : null}
          </View>
        ) : (
          <View style={styles.close}>
            <CustomText
              light={colors.ERROR}
              dark={colors.ERROR}
              style={[stylesText.secondaryText]}>
              Cerrado
            </CustomText>
          </View>
        )}
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  horas: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 15,
    marginTop: 15,
  },

  item: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },

  close: {
    backgroundColor: '#f5365c37',
    width: 110,
    height: 35,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

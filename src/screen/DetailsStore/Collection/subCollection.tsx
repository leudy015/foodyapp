import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  Animated,
  ScrollView,
  SafeAreaView,
  RefreshControl,
  Platform,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { useTranslation } from 'react-i18next';
import CardCollection from './subCollectionCard';
import DetailsCollection from './ModalCollectionDetails';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import SearcProducts from '../SearchProducts';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function subCollection(props) {
  const {
    datos,
    store,
    localeCode,
    currecy,
    inCartItem,
    setinCartItem,
    setModalVisible,
  } = props;
  const { t } = useTranslation();
  const styles = useDynamicValue(dynamicStyles);
  const [scrollY] = useState(new Animated.Value(0));
  const [refreshing, setRefreshing] = useState(false);
  const [ModalVisibles, setModalVisibles] = useState(false);
  const [datasModal, setdatasModal] = useState(null);
  const [modalVisibleSearch, setmodalVisibleSearch] = useState(false);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const imageContainerHeight = scrollY.interpolate({
    inputRange: [-200, 0],
    outputRange: [450, 250],
    extrapolate: 'extend',
  });

  const headerContainerWidth = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [dimensions.Width(100), dimensions.Width(100)],
    extrapolate: 'clamp',
  });

  const headerContainerHeight = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [60, 100],
    extrapolate: 'clamp',
  });

  const opacity = scrollY.interpolate({
    inputRange: [0, 50, 100],
    outputRange: [100, 50, 0],
    extrapolate: 'clamp',
  });

  const font = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [32, 16],
    extrapolate: 'clamp',
  });

  const top = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [0, 30],
    extrapolate: 'clamp',
  });

  const setDetailsCollection = (ites) => {
    setdatasModal(ites);
    setModalVisibles(true);
    ReactNativeHapticFeedback.trigger('selection', optiones);
  };

  const renderItems = ({ item }) => {
    return (
      <CardCollection
        data={item}
        localeCode={localeCode}
        currecy={currecy}
        onPress={() => setDetailsCollection(item)}
      />
    );
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.containerIcon}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={() => setModalVisible(false)}
            style={styles.icon}>
            <Icon
              name="arrow-left"
              type="Feather"
              size={24}
              color={colors.back_dark}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setmodalVisibleSearch(true)}
            style={styles.icon}>
            <Icon
              type="Feather"
              name="search"
              size={24}
              color={colors.back_dark}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <Animated.View
        style={[styles.imageContainer, { height: imageContainerHeight }]}>
        <Animated.Image
          style={[
            styles.image,
            { height: imageContainerHeight, opacity: opacity },
          ]}
          source={{ uri: datos.image }}
        />
      </Animated.View>
      <ScrollView
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: scrollY } } },
        ])}
        scrollEventThrottle={16}
        stickyHeaderIndices={[1]}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        style={styles.scrollViewContainer}>
        <View
          style={{
            marginTop: 250,
            marginBottom: dimensions.Height(10),
          }}>
          <FlatList
            data={datos.subCollectionItems}
            renderItem={(item: any) => renderItems(item)}
            keyExtractor={(item: any) => item._id}
            showsVerticalScrollIndicator={false}
            style={{ marginTop: 20, paddingBottom: 30 }}
          />
        </View>
        <View style={styles.stickyHeaderContainer}>
          <Animated.View
            style={[
              styles.headerContainer,
              {
                width: headerContainerWidth,
                height: headerContainerHeight,
              },
            ]}>
            <Animated.Text
              numberOfLines={1}
              style={[
                styles.text,
                {
                  fontSize: font,
                  marginTop: top,
                  fontWeight: '700',
                  marginHorizontal: 20,
                },
              ]}>
              {datos.title}
            </Animated.Text>
          </Animated.View>
        </View>
      </ScrollView>
      {datasModal ? (
        <DetailsCollection
          data={datasModal}
          store={store}
          category={datos._id}
          title={datos.title}
          localeCode={localeCode}
          currecy={currecy}
          inCartItem={inCartItem}
          setinCartItem={setinCartItem}
          ModalVisible={ModalVisibles}
          setModalVisible={setModalVisibles}
        />
      ) : null}

      <SearcProducts
        data={store}
        category=""
        title="Buscar productos"
        localeCode={localeCode}
        currecy={currecy}
        inCartItem={inCartItem}
        setinCartItem={setinCartItem}
        modalVisible={modalVisibleSearch}
        setModalVisible={setmodalVisibleSearch}
      />
    </View>
  );
}

const color1 = 'rgba(255, 255, 255, 0.5)';

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  containerIcon: {
    width: dimensions.Width(95),
    position: 'absolute',
    marginTop: Platform.select({
      ios: dimensions.IsIphoneX() ? dimensions.Height(0) : dimensions.Height(4),
      android: dimensions.Height(5),
    }),
    marginHorizontal: 10,
    zIndex: 100,
  },

  icon: {
    width: 40,
    height: 40,
    backgroundColor: color1,
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 100,
  },

  image: {
    flex: 1,
    width: dimensions.ScreenWidth,
  },

  scrollViewContainer: {
    flex: 1,
  },

  imageContainer: {
    position: 'absolute',
    top: 0,
    width: dimensions.ScreenWidth,
    height: 200,
  },

  stickyHeaderContainer: {
    position: 'absolute',
    top: 200,
    left: 0,
    right: 0,
  },

  headerContainer: {
    width: dimensions.Width(100),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  text: {
    color: new DynamicValue(colors.black, colors.white),
  },

  recomended: {
    marginTop: dimensions.Height(5),
  },

  icons: {
    color: new DynamicValue(colors.white, colors.white),
  },

  filters: {
    width: 60,
    height: 30,
    marginLeft: 'auto',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: new DynamicValue(
      colors.back_suave_dark,
      colors.back_suave_dark,
    ),
    borderRadius: 100,
  },

  items: {
    width: 30,
    height: 30,
    backgroundColor: colors.main,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  itemsInac: {
    width: 30,
    height: 30,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});

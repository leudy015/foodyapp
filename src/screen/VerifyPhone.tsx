import React, { useState } from 'react';
import { View, TouchableOpacity, Alert, ScrollView } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import Navigation from '../services/Navigration';
import IntlPhoneInput from 'react-native-intl-phone-input';
import { useNavigationParam } from 'react-navigation-hooks';
import { dimensions, colors } from '../theme';
import { NETWORK_INTERFACE_URL } from '../Config/config';
import { stylesText } from '../theme/TextStyle';
import Loader from '../Components/ModalLoading';
import Icon from 'react-native-dynamic-vector-icons';

const VerifyPhone = () => {
  const [phone, setPhone] = useState('');
  const [Loading, setLoading] = useState(false);
  const [isVerified, setisVerified] = useState(null);
  const data = useNavigationParam('data');

  const dynamicStyles = new DynamicStyleSheet({
    container: {
      flex: 1,
      backgroundColor: new DynamicValue('white', 'black'),
    },
    formView: {
      marginHorizontal: dimensions.Width(8),
      marginTop: dimensions.Height(4),
      marginBottom: dimensions.Height(15),
    },
    rememberMeView: {
      marginTop: dimensions.Height(2),
      flexDirection: 'row',
    },
    rememberText: {
      fontSize: dimensions.FontSize(18),
    },

    containerModal: {
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
      height: dimensions.ScreenHeight,
      alignSelf: 'center',
    },

    phoneInputStyle: {
      backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    },

    impu: {
      backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
      borderRadius: 40,
      marginBottom: 40,
      padding: 20,
      marginTop: 55,
    },

    conu: {
      color: new DynamicValue(colors.back_dark, colors.light_white),
    },

    code: { color: new DynamicValue(colors.back_dark, colors.light_white) },

    bntc1: {
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
      color: colors.rgb_153,
      marginTop: 15,
    },

    bntc: {
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
      color: colors.rgb_153,
    },

    signupButtonContainer: {
      marginTop: dimensions.Height(5),
      alignSelf: 'center',
    },
    buttonView: {
      backgroundColor: isVerified
        ? new DynamicValue(colors.main, colors.main)
        : colors.rgb_153,
      width: dimensions.Width(85),
      borderRadius: dimensions.Width(2),
    },
    buttonTitle: {
      alignSelf: 'center',
      paddingVertical: dimensions.Height(2),
      paddingHorizontal: dimensions.Width(5),
      color: colors.white,
      fontWeight: '400',
      fontSize: dimensions.FontSize(17),
    },
    contenedor: {
      width: dimensions.ScreenWidth,
      height: dimensions.ScreenHeight,
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    },

    close: {
      color: new DynamicValue('black', 'white'),
      marginTop: dimensions.Height(8),
      marginLeft: dimensions.Width(4),
    },
  });

  const styles = useDynamicValue(dynamicStyles);

  const onChangeText = ({
    dialCode,
    unmaskedPhoneNumber,
    phoneNumber,
    isVerified,
  }) => {
    setisVerified(isVerified);
    setPhone(dialCode + unmaskedPhoneNumber);
  };

  const VerifyPhone = async (phone: any, id: any) => {
    setLoading(true);
    const datas = {
      phone,
      id,
      data,
    };
    fetch(`${NETWORK_INTERFACE_URL}/verify-phone`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ phone }),
    })
      .then(async (res) => {
        const verify = await res.json();
        if (verify.success) {
          setLoading(false);
          Navigation.navigate('SendCode', { data: datas });
        } else {
          setLoading(false);
          Alert.alert(
            'Algo no salio bien',
            'Algo no fue como esperabamos intenlalo de nuevo por favor',
            [
              {
                text: 'Volver a enviar',
                onPress: () => console.log('ok'),
              },
            ],

            { cancelable: false },
          );
        }
      })
      .catch((err) =>
        Alert.alert(
          'Algo no salio bien',
          'Algo no fue como esperabamos intenlalo de nuevo por favor',
          [
            {
              text: 'Volver a enviar',
              onPress: () => console.log('ok'),
            },
          ],

          { cancelable: false },
        ),
      );
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Loader loading={Loading} color={colors.main} />
        <View style={styles.contenedor}>
          <TouchableOpacity onPress={() => Navigation.goBack()}>
            <Icon
              name="close"
              type="AntDesign"
              size={32}
              style={styles.close}
            />
          </TouchableOpacity>
          <View
            style={{
              marginTop: dimensions.Height(5),
              marginLeft: dimensions.Width(5),
              marginBottom: dimensions.Height(1),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={stylesText.titleText}>
              Verifica tu teléfono
            </CustomText>

            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                { marginTop: 30, textAlign: 'center' },
              ]}>
              Te enviaremos un código para verificar tu teléfono.
            </CustomText>
          </View>
          <View style={styles.formView}>
            <IntlPhoneInput
              filterInputStyle={styles.impu}
              dialCodeTextStyle={styles.code}
              closeButtonStyle={styles.bntc}
              searchIconStyle={styles.bntc1}
              containerStyle={styles.phoneInputStyle}
              modalCountryItemCountryNameStyle={styles.conu}
              modalContainer={styles.containerModal}
              defaultCountry="ES"
              lang="ES"
              closeText="Cerrar"
              filterText="Buscar"
              onChangeText={onChangeText}
            />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryText,
                { marginTop: 30, textAlign: 'left' },
              ]}>
              Te enviaremos un mensaje de texto para confirmar tu número de
              teléfono. Obtendrás un código. ¡No compartas el código con nadie!
            </CustomText>
            <View style={styles.signupButtonContainer}>
              <TouchableOpacity
                style={styles.buttonView}
                onPress={() => (isVerified ? VerifyPhone(phone, data.id) : '')}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  style={styles.buttonTitle}>
                  Enviar código
                </CustomText>
              </TouchableOpacity>
            </View>
            <View />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default VerifyPhone;

import React from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions, stylesText } from '../../theme';
import { formaterPrice } from '../../Utils/formaterPRice';

export default function Total(props) {
  const {
    subtotals,
    descuentos,
    extras,
    envio,
    propinas,
    totalFinal,
    localeCode,
    currecy,
    isShipping,
  } = props;

  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={[styles.items, { marginTop: 20, flexDirection: 'row' }]}>
      <View style={{ marginBottom: 15 }}>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { marginTop: 10, fontWeight: 'bold' },
          ]}>
          Subtotal:
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { marginTop: 10, fontWeight: 'bold' },
          ]}>
          Descuento:
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { marginTop: 10, fontWeight: 'bold' },
          ]}>
          Tarifa de servicio:
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { marginTop: 10, fontWeight: 'bold' },
          ]}>
          Envío:
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { marginTop: 10, fontWeight: 'bold' },
          ]}>
          Propina:
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { marginTop: 10, fontWeight: 'bold' },
          ]}>
          Total:
        </CustomText>
      </View>
      <View style={{ marginLeft: 'auto', marginRight: 15 }}>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { textAlign: 'right', marginTop: 10 },
          ]}>
          {formaterPrice(subtotals, localeCode, currecy)}
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { textAlign: 'right', marginTop: 10 },
          ]}>
          {descuentos}
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { textAlign: 'right', marginTop: 10 },
          ]}>
          {formaterPrice(extras, localeCode, currecy)}
        </CustomText>

        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { textAlign: 'right', marginTop: 10 },
          ]}>
          {isShipping ? 'GRATIS' : formaterPrice(envio, localeCode, currecy)}
        </CustomText>

        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { textAlign: 'right', marginTop: 10 },
          ]}>
          {formaterPrice(propinas, localeCode, currecy)}
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            { textAlign: 'right', marginTop: 10 },
          ]}>
          {formaterPrice(totalFinal, localeCode, currecy)}
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },
});

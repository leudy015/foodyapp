import React, { useState } from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
  Modal,
  Platform,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import Header from '../Components/Header';
import { useNavigationParam } from 'react-navigation-hooks';
import { Avatar } from 'react-native-elements';
import { dimensions, colors } from '../theme';
import { stylesText } from '../theme/TextStyle';
import moment from 'moment';
import { CustomTextInput } from '../Components/CustomInput';
import { Button } from '../Components/Button';
import Loader from '../Components/ModalLoading';
import ImagePicker from 'react-native-image-picker';
import Toast from 'react-native-toast-message';
import { mutations } from '../GraphQL';
import { Mutation } from 'react-apollo';
import Icon from 'react-native-dynamic-vector-icons';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_URL,
} from '../Config/config';
import HerderModal from '../Components/HeaderModal';
import IntlPhoneInput from 'react-native-intl-phone-input';
import CountDown from 'react-native-countdown-component';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { validateEmail } from '../Utils/EmailsValidator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LottieView from 'lottie-react-native';
import source from '../Assets/Animate/lf30_editor_zr5nhk6x.json';
import DatePicker from 'react-native-date-picker';

const UpdateProfile = () => {
  const dats = useNavigationParam('data');
  const location = dats.city;
  const data = dats.user;

  const [Loading, setLoading] = useState(false);
  const [avatar, setAvatar] = useState(data.avatar);
  const styles = useDynamicValue(dynamicStyles);
  const [city, setCity] = useState(location);
  const [name, setName] = useState(data.name);
  const [lastName, setLastName] = useState(data.lastName);
  const [email, setEmail] = useState(data.email);
  const [telefono] = useState(data.telefono);
  const [modalVisible, setModalVisible] = useState(false);
  const [phone, setPhone] = useState('');
  const [isVerified, setisVerified] = useState(null);
  const [visibleView, setVisibleView] = useState(0);
  const [value, setValue] = useState('');
  const [isError, setIsError] = useState(false);
  const [birthdayDate, setbirthdayDate] = useState(data.birthdayDate);
  const [date, setdate] = useState(new Date());

  const [open, setOpen] = useState(false);

  const selectPhotoTapped = (singleUpload: any) => {
    const options = {
      quality: 0.5,
      title: 'Seleccionar Avatar',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Hacer foto',
      chooseFromLibraryButtonTitle: 'Seleccionar foto existente',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        storageOptions: {
          skipBackup: true,
        },
      },
    };
    ImagePicker.showImagePicker(options, async (response) => {
      if (response) {
        const imgBlob = 'data:image/jpeg;base64,' + response.data;
        if (imgBlob) {
          console.log(response);
          singleUpload({ variables: { imgBlob } })
            .then((res: any) => {
              console.log('filemane: ', res.data.singleUpload.filename);
              setAvatar(res.data.singleUpload.filename);
            })
            .catch((error) => {
              console.log('fs error: que me arroja', error);
            });
        }
      }
    });
  };

  const handleSubmit = async (mutation: any) => {
    setLoading(true);
    const input = {
      _id: data._id,
      name: name,
      lastName: lastName,
      city: city,
      email: email,
      avatar: avatar,
      birthdayDate: birthdayDate,
    };
    if (!validateEmail(email)) {
      setIsError(true);
      setLoading(false);
      return null;
    } else {
      setIsError(false);
      if (!avatar) {
        Toast.show({
          text1: 'Debes añadir una foto de perfil',
          text2:
            'Para guardar los cambios en tu perfil debes añadir una foto de perfil.',
          position: 'top',
          type: 'info',
          topOffset: 50,
          visibilityTime: 4000,
        });
        setLoading(false);
        return null;
      } else {
        mutation({ variables: { input } })
          .then((res: any) => {
            console.log('done', res);
            setLoading(false);
            Toast.show({
              text1: 'Datos actualizado con éxito',
              text2: 'Tus datos han sido actualizado con éxito',
              position: 'top',
              type: 'success',
              topOffset: 50,
              visibilityTime: 4000,
            });
          })
          .catch((err: any) => console.log(err));
        setLoading(false);
      }
    }
  };

  const onChangeText = ({
    dialCode,
    unmaskedPhoneNumber,
    phoneNumber,
    isVerified,
  }) => {
    setisVerified(isVerified);
    setPhone(dialCode + unmaskedPhoneNumber);
  };

  const reSend = async (phone: any) => {
    setLoading(true);
    fetch(`${NETWORK_INTERFACE_URL}/verify-phone`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ phone }),
    })
      .then(async (res) => {
        const confirm = await res.json();
        if (confirm.success) {
          setLoading(false);
        } else {
          Toast.show({
            text1: 'Algo salio mal',
            text2: 'Algo salio mal intentalo de nuevo',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 4000,
          });
          setLoading(false);
        }
      })
      .catch((err) => console.log(err));
  };

  const VerifyPhone = async (phone: any) => {
    setLoading(true);
    fetch(`${NETWORK_INTERFACE_URL}/verify-phone`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ phone }),
    })
      .then(async (res) => {
        const verify = await res.json();
        if (verify.success) {
          setLoading(false);
          setVisibleView(1);
        } else {
          setLoading(false);

          Toast.show({
            text1: 'Algo salio mal',
            text2: 'Algo salio mal intentalo de nuevo',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 4000,
          });
        }
      })
      .catch((err) => {
        setLoading(false);
        Toast.show({
          text1: 'Algo salio mal',
          text2: 'Algo salio mal intentalo de nuevo',
          position: 'top',
          type: 'error',
          topOffset: 50,
          visibilityTime: 4000,
        });
      });
  };

  const CELL_COUNT = 4;

  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const sendCode = async () => {
    setLoading(true);
    const input = {
      phone: phone,
      code: value,
      id: data._id,
    };
    fetch(`${NETWORK_INTERFACE_URL}/verify-code`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(input),
    })
      .then(async (res) => {
        const confirm = await res.json();
        if (confirm.success) {
          setLoading(false);
          setModalVisible(false);
          setPhone('');
          setValue('');
          setVisibleView(0);
        } else {
          setLoading(false);
          Toast.show({
            text1: 'Hubo un error con tu código',
            text2: 'Hubo un error con tu código por favor vuelve a intentarlo.',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 4000,
          });
        }
      })
      .catch((err) => {
        console.log(err), setLoading(false);
      });
  };

  const finisehd = () => {
    Toast.show({
      text1: 'Se ha vencido el código',
      text2:
        'El código enviado se ha vencido para verificar tu teléfono vuelve a intentarlo.',
      position: 'top',
      type: 'error',
      topOffset: 50,
      visibilityTime: 4000,
    });
  };

  const selectDate = (date) => {
    setbirthdayDate(date);
    setdate(date);
  };

  const colorsText = {
    light: colors.black,
    dark: colors.white,
  };

  const mode = useColorSchemeContext();
  const backgroundColor = colorsText[mode];

  return (
    <Mutation mutation={mutations.ACTUALIZAR_USUARIO}>
      {(mutation: any) => {
        return (
          <View style={styles.container}>
            <Toast ref={(ref) => Toast.setRef(ref)} style={{ zIndex: 100 }} />
            <Loader loading={Loading} color={colors.main} />
            <Header title="Actualizar datos" />
            <KeyboardAwareScrollView
              keyboardShouldPersistTaps="always"
              showsVerticalScrollIndicator={false}>
              <View style={styles.infoContact}>
                <Mutation mutation={mutations.UPLOAD_FILE}>
                  {(singleUpload: any) => (
                    <TouchableOpacity
                      onPress={() => selectPhotoTapped(singleUpload)}>
                      <Avatar
                        size={120}
                        showAccessory
                        rounded
                        source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + avatar }}
                      />
                    </TouchableOpacity>
                  )}
                </Mutation>
                <CustomText
                  numberOfLines={1}
                  light={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.titleText,
                    {
                      marginTop: dimensions.Height(1),
                      width: dimensions.Width(80),
                      textAlign: 'center',
                    },
                  ]}>
                  {data.name} {data.lastName}
                </CustomText>
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[stylesText.terciaryText, { marginTop: 10 }]}>
                  En Wilbby desde {moment(data.created_at).format('L')}
                </CustomText>
              </View>
              <View style={styles.formView}>
                <View style={styles.cardscuston}>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryTextBold,
                      { paddingVertical: 10 },
                    ]}>
                    Datos Personales
                  </CustomText>
                  <CustomTextInput
                    value={name}
                    right
                    rightIcon="user"
                    type="AntDesign"
                    placeHolder="Nombre"
                    autoCompleteType="name"
                    onChangeText={(clave: any) => setName(clave)}
                    containerStyle={{ marginTop: 20 }}
                  />

                  <CustomTextInput
                    value={lastName}
                    right
                    rightIcon="user"
                    type="AntDesign"
                    placeHolder="Apellidos"
                    autoCompleteType="name"
                    onChangeText={(clave: any) => setLastName(clave)}
                    containerStyle={{ marginTop: 20 }}
                  />
                  <CustomTextInput
                    value={city}
                    right
                    rightIcon="enviromento"
                    type="AntDesign"
                    placeHolder="Ciudad"
                    autoCompleteType="email"
                    onChangeText={(clave: any) => setCity(clave)}
                    containerStyle={{ marginTop: 20, marginBottom: 20 }}
                  />
                </View>

                <View style={styles.cardscuston}>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryTextBold,
                      { paddingVertical: 10 },
                    ]}>
                    Datos de contacto
                  </CustomText>
                  <CustomTextInput
                    value={email}
                    right
                    rightIcon="mail"
                    type="AntDesign"
                    placeHolder="Correo Electrónico"
                    autoCompleteType="email"
                    onChangeText={(clave: any) => setEmail(clave)}
                    containerStyle={{ marginTop: 20 }}
                  />
                  {isError ? (
                    <CustomText
                      style={{ marginTop: 3 }}
                      light={colors.ERROR}
                      dark={colors.ERROR}>
                      ¡Por favor introduce un email válido!
                    </CustomText>
                  ) : null}
                  <TouchableOpacity
                    onPress={() => setModalVisible(true)}
                    style={styles.input}>
                    <Icon
                      style={{ alignSelf: 'center' }}
                      type="AntDesign"
                      name="phone"
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.secondaryText,
                        { marginLeft: 10, paddingTop: 5 },
                      ]}>
                      {telefono}
                    </CustomText>
                  </TouchableOpacity>
                </View>

                <View style={styles.cardscuston}>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryTextBold,
                      { paddingVertical: 10 },
                    ]}>
                    Cumpleaños
                  </CustomText>

                  <TouchableOpacity
                    onPress={() => setOpen(true)}
                    style={styles.input}>
                    <Icon
                      style={{ alignSelf: 'center' }}
                      name="gift"
                      type="Feather"
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.secondaryText,
                        { marginLeft: 10, paddingTop: 5 },
                      ]}>
                      {moment(birthdayDate).format('LL')}
                    </CustomText>
                  </TouchableOpacity>
                  {open ? (
                    <>
                      <DatePicker
                        //@ts-ignore
                        modal
                        open={open}
                        style={{ marginLeft: 30 }}
                        mode="date"
                        locale="es"
                        date={date}
                        androidVariant={
                          Platform.OS === 'android'
                            ? 'nativeAndroid'
                            : 'iosClone'
                        }
                        onDateChange={selectDate}
                        onCancel={() => {
                          setOpen(false);
                        }}
                        pickerContainerStyleIOS={{
                          justifyContent: 'center',
                          paddingHorizontal: 40,
                        }}
                        textColor={backgroundColor}
                      />

                      <Button
                        light={colors.white}
                        dark={colors.white}
                        containerStyle={[
                          styles.buttonView,
                          {
                            backgroundColor: colors.green,
                            width: dimensions.Width(85),
                            marginTop: 20,
                          },
                        ]}
                        onPress={() => setOpen(false)}
                        title="Confirmar"
                        titleStyle={styles.buttonTitle}
                      />
                    </>
                  ) : null}
                </View>

                <View style={styles.signupButtonContainer}>
                  <Button
                    light={colors.white}
                    dark={colors.white}
                    containerStyle={[
                      styles.buttonView,
                      { backgroundColor: colors.green },
                    ]}
                    onPress={() => handleSubmit(mutation)}
                    title="Guardar cambios"
                    titleStyle={styles.buttonTitle}
                  />
                </View>
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[stylesText.secondaryText, { paddingVertical: 10 }]}>
                  No compartimos tus datos personales y de contacto con nadie,
                  nuestra prioridad es tu privacidad.
                </CustomText>
              </View>
            </KeyboardAwareScrollView>
            <Modal
              animationType="slide"
              transparent={false}
              visible={modalVisible}
              statusBarTranslucent={true}
              presentationStyle="formSheet">
              <View style={styles.centeredView}>
                <HerderModal
                  title="Verificar teléfono"
                  OnClosetModal={() => setModalVisible(false)}
                />
                {visibleView === 0 ? (
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <Loader loading={Loading} color={colors.main} />
                    <View style={styles.contenedor}>
                      <View
                        style={{
                          marginTop: dimensions.Height(5),
                          marginLeft: dimensions.Width(5),
                          marginBottom: dimensions.Height(1),
                        }}>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={stylesText.titleText}>
                          Verifica tu teléfono
                        </CustomText>

                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={[
                            stylesText.secondaryText,
                            { marginTop: 30, textAlign: 'center' },
                          ]}>
                          Te enviaremos un código para verificar tu teléfono.
                        </CustomText>
                      </View>
                      <View style={styles.formView}>
                        <IntlPhoneInput
                          filterInputStyle={styles.impu}
                          dialCodeTextStyle={styles.code}
                          closeButtonStyle={styles.bntc}
                          searchIconStyle={styles.bntc1}
                          containerStyle={styles.phoneInputStyle}
                          modalCountryItemCountryNameStyle={styles.conu}
                          modalContainer={styles.containerModal}
                          defaultCountry="ES"
                          lang="ES"
                          closeText="Cerrar"
                          filterText="Buscar"
                          onChangeText={onChangeText}
                        />
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={[
                            stylesText.secondaryText,
                            { marginTop: 30, textAlign: 'left' },
                          ]}>
                          Te enviaremos un mensaje de texto para confirmar su
                          número. Obtendrás un código. ¡No compartas el código
                          con nadie!
                        </CustomText>
                        <View style={styles.signupButtonContainer}>
                          <TouchableOpacity
                            style={[
                              styles.buttonView,
                              {
                                backgroundColor: isVerified
                                  ? colors.main
                                  : colors.rgb_153,
                              },
                            ]}
                            onPress={() =>
                              isVerified ? VerifyPhone(phone) : null
                            }>
                            <CustomText
                              light={colors.back_dark}
                              dark={colors.rgb_153}
                              style={styles.buttonTitle}>
                              Enviar código
                            </CustomText>
                          </TouchableOpacity>
                        </View>
                        <View />
                      </View>
                    </View>
                  </ScrollView>
                ) : (
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <Loader loading={Loading} color={colors.main} />
                    <View style={styles.contenedor}>
                      <View
                        style={{
                          marginTop: dimensions.Height(6),
                          marginBottom: dimensions.Height(5),
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <LottieView
                          source={source}
                          autoPlay
                          loop
                          style={{ width: 100, height: 100 }}
                        />
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={stylesText.titleText}>
                          Verifica tu teléfono
                        </CustomText>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={[
                            stylesText.secondaryText,
                            {
                              marginTop: 30,
                              paddingHorizontal: dimensions.Width(4),
                              textAlign: 'center',
                            },
                          ]}>
                          Ingresa el código de verificación que te enviamos a su
                          número de teléfono {''} {phone}
                        </CustomText>
                      </View>
                      <View style={styles.formView}>
                        <CodeField
                          ref={ref}
                          {...props}
                          value={value}
                          onChangeText={setValue}
                          cellCount={CELL_COUNT}
                          rootStyle={styles.codeFiledRoot}
                          keyboardType="number-pad"
                          textContentType="oneTimeCode"
                          renderCell={({ index, symbol, isFocused }) => (
                            <CustomText
                              light={colors.back_dark}
                              dark={colors.rgb_153}
                              key={index}
                              style={[
                                styles.cell,
                                isFocused && styles.focusCell,
                              ]}
                              onLayout={getCellOnLayoutHandler(index)}>
                              {symbol || (isFocused ? <Cursor /> : null)}
                            </CustomText>
                          )}
                        />
                        <View style={{ flexDirection: 'row' }}>
                          <CustomText
                            style={{
                              color: colors.rgb_153,
                              fontWeight: '200',
                              fontSize: 14,
                              paddingTop: 30,
                            }}>
                            El código vence en:{' '}
                          </CustomText>

                          <CountDown
                            until={60 * 5 + 0}
                            size={8}
                            onFinish={() => finisehd()}
                            digitStyle={{
                              backgroundColor: 'transparent',
                              marginTop: 29,
                            }}
                            digitTxtStyle={{
                              color: colors.rgb_153,
                              fontWeight: '200',
                              fontSize: 14,
                            }}
                            timeToShow={['M', 'S']}
                            timeLabels={false}
                          />
                        </View>
                        <View style={styles.signupButtonContainer}>
                          <TouchableOpacity
                            style={[
                              styles.buttonView,
                              {
                                backgroundColor:
                                  value.length === 4
                                    ? colors.main
                                    : colors.rgb_153,
                              },
                            ]}
                            onPress={() =>
                              value.length === 4 ? sendCode() : ''
                            }>
                            <CustomText
                              light={colors.back_dark}
                              dark={colors.rgb_153}
                              style={styles.buttonTitle}>
                              Verificar
                            </CustomText>
                          </TouchableOpacity>
                        </View>

                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={[
                            stylesText.secondaryText,
                            {
                              marginTop: 50,
                              paddingHorizontal: dimensions.Width(4),
                              textAlign: 'center',
                            },
                          ]}>
                          ¿No recibiste el código?
                        </CustomText>
                        <TouchableOpacity
                          onPress={() => reSend(phone)}
                          style={{
                            alignSelf: 'center',
                            marginTop: dimensions.Height(2),
                            marginBottom: dimensions.Height(10),
                          }}>
                          <CustomText
                            light={colors.main}
                            dark={colors.main}
                            style={stylesText.secondaryText}>
                            Reenviar
                          </CustomText>
                        </TouchableOpacity>
                        <View />
                      </View>
                    </View>
                  </ScrollView>
                )}
              </View>
            </Modal>
          </View>
        );
      }}
    </Mutation>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  impu: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 40,
    marginBottom: 40,
    padding: 20,
    marginTop: 55,
  },

  conu: {
    color: new DynamicValue(colors.back_dark, colors.light_white),
  },

  code: { color: new DynamicValue(colors.back_dark, colors.light_white) },

  bntc1: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    color: colors.rgb_153,
    marginTop: 15,
  },

  bntc: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    color: colors.rgb_153,
  },

  infoContact: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: dimensions.Height(5),
  },

  formView: {
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(15),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  oot: { flex: 1, padding: 20 },
  title: { textAlign: 'center', fontSize: 30 },
  codeFiledRoot: { marginTop: 20 },
  cell: {
    width: 50,
    height: 50,
    lineHeight: 48,
    fontSize: 24,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: new DynamicValue(colors.back_dark, colors.rgb_153),
    textAlign: 'center',
  },
  focusCell: {
    borderColor: colors.main,
  },

  containerModal: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: dimensions.ScreenHeight,
    alignSelf: 'center',
  },

  phoneInputStyle: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },

  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  input: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    padding: Platform.select({
      android: 20,
      ios: dimensions.IsIphoneX() ? 17 : 20,
    }),
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 5,
    color: colors.rgb_153,
    borderColor: new DynamicValue(colors.rgb_153, colors.rgb_153),
    borderWidth: 0.5,
    flexDirection: 'row',
  },

  cardscuston: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    marginTop: 30,
    borderRadius: 20,
    padding: 15,
    shadowColor: new DynamicValue(colors.rgb_235, colors.black),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});

export default UpdateProfile;

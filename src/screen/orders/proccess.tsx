import React from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors } from '../../theme';
import Timeline from 'react-native-timeline-flatlist';
import moment from 'moment';

const Process = (props: any) => {
  const styles = useDynamicValue(dynamicStyles);

  const { data } = props;

  const d = data ? data : [];

  const datas = d.map((x) => {
    return {
      time: moment(x.date).format('hh:mm'),
      title: x.status,
      circleColor: colors.main,
      lineColor: colors.main,
    };
  });

  return (
    <View style={styles.container}>
      <Timeline
        titleStyle={styles.text}
        style={styles.list}
        data={datas}
        separator={true}
        circleSize={20}
        circleColor="rgba(197,248,116,.3)"
        lineColor="rgba(197,248,116,.3)"
        timeContainerStyle={{ minWidth: 53, marginTop: -5 }}
        timeStyle={{
          textAlign: 'center',
          backgroundColor: 'rgba(197,248,116,.3)',
          color: colors.main,
          padding: 5,
          borderRadius: 13,
          overflow: 'hidden',
        }}
        descriptionStyle={{ color: 'gray' }}
        options={{
          style: { paddingTop: 5 },
        }}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 65,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  list: {
    flex: 1,
    marginTop: 20,
  },

  text: {
    color: colors.rgb_153,
  },
});

export default Process;

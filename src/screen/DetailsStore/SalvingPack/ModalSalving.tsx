import React, { useState } from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  Alert,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors, image, stylesText } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
// @ts-ignore
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Star from '../../../Components/star';
import LinearGradient from 'react-native-linear-gradient';
import { formaterPrice } from '../../../Utils/formaterPRice';
import { RatingCalculator } from '../../../Utils/rating';
import moment from 'moment';
import { AddItemsToCadt } from '../../../Components/NewCardProduct/AddAndRemove';

const window = Dimensions.get('window');

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ModalSalving(props: any) {
  const {
    visibleModal,
    setModalVisible,
    datas,
    user,
    inCartItem,
    setinCartItem,
  } = props;
  const [quantity, setQuantity] = useState(1);
  const styles = useDynamicValue(dynamicStyles);

  const restante = moment(datas.salvingPack.pickUpDate).from(new Date());

  const rescantidad = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    if (quantity < 2) {
      null;
    } else {
      setQuantity(quantity - 1);
    }
  };

  const sumcantidad = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    if (quantity > datas.salvingPack.quantity - 1) {
      Alert.alert(
        `${datas.title} sólo tiene ${datas.salvingPack.quantity} packs disponible`,
      );
    } else {
      setQuantity(quantity + 1);
    }
  };

  Object.assign(datas.salvingPack.item, {
    quantity: quantity,
    subItems: [],
  });

  const input = {
    userId: user,
    storeId: datas._id,
    productId: datas._id,
    addToCart: true,
    items: datas.salvingPack.item,
  };

  const addTocardItems = () => {
    AddItemsToCadt(
      inCartItem,
      setinCartItem,
      datas._id,
      input,
      setModalVisible,
      2,
    );
  };

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const averageRating = RatingCalculator(datas.Valoracion);

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.container}>
        <ParallaxScrollView
          showsVerticalScrollIndicator={false}
          headerBackgroundColor="transparent"
          stickyHeaderHeight={STICKY_HEADER_HEIGHT}
          parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
          backgroundSpeed={10}
          renderBackground={() => (
            <View key="background" style={{ backgroundColor: colors.white }}>
              {datas.image ? (
                <>
                  <ImageBackground
                    style={styles.imagenbg}
                    source={image.PlaceHolder}>
                    <Image
                      style={styles.imagenbg}
                      source={{ uri: datas.image }}
                      resizeMode="cover"
                    />
                    <View
                      style={{
                        position: 'absolute',
                        top: 0,
                        width: window.width,
                        height: PARALLAX_HEADER_HEIGHT,
                      }}
                    />
                  </ImageBackground>
                </>
              ) : (
                <>
                  <Image
                    style={styles.imagenbg}
                    source={image.PlaceHolder}
                    resizeMode="cover"
                  />
                  <View
                    style={{
                      position: 'absolute',
                      top: 0,
                      width: window.width,
                      height: PARALLAX_HEADER_HEIGHT,
                    }}
                  />
                </>
              )}
            </View>
          )}
          renderFixedHeader={() => (
            <View key="fixed-header" style={styles.fixedSection}>
              <TouchableOpacity
                style={styles.left}
                onPress={() => setModalVisible(false)}>
                <CustomText>
                  <Icon
                    name="arrow-left"
                    type="Feather"
                    size={24}
                    style={styles.close}
                  />
                </CustomText>
              </TouchableOpacity>
            </View>
          )}
          renderStickyHeader={() => (
            <View key="sticky-header" style={styles.stickySection}>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  { marginLeft: 65, width: dimensions.Width(75) },
                ]}>
                {datas.title}
              </CustomText>
            </View>
          )}>
          <View style={{ position: 'absolute', zIndex: 100, top: -120 }}>
            <View style={{ marginRight: 15, marginLeft: 15 }}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[colors.orange, '#fcbe4a']}
                style={[styles.tagshipping, { width: 110 }]}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={stylesText.secondaryTextBold}>
                  {datas.salvingPack.quantity} Pack
                </CustomText>
              </LinearGradient>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#510090', '#4600DA']}
                style={[styles.tagshipping]}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={stylesText.secondaryTextBold}>
                  {formaterPrice(datas.salvingPack.item.price / 100, '', '')}
                </CustomText>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={[stylesText.secondaryTextBold, { marginLeft: 10 }]}>
                  -
                </CustomText>
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.secondaryTextBold,
                    { marginLeft: 10, textDecorationLine: 'line-through' },
                  ]}>
                  {formaterPrice(
                    datas.salvingPack.item.previous_price / 100,
                    '',
                    '',
                  )}
                </CustomText>
              </LinearGradient>
            </View>
          </View>
          <View style={styles.contenedor}>
            <View
              style={[styles.items, { borderBottomColor: backgroundColor }]}>
              <CustomText
                style={stylesText.titleText}
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}>
                {datas.title}
              </CustomText>

              <CustomText
                style={[stylesText.secondaryText, { marginTop: 5 }]}
                numberOfLines={1}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                <Icon
                  name="clock"
                  type="Feather"
                  size={14}
                  color={colors.rgb_153}
                />{' '}
                Disponible para{' '}
                {datas.salvingPack.pickUpDate
                  ? `las ${moment(datas.salvingPack.pickUpDate).format(
                      'hh:mm',
                    )}`
                  : 'hoy'}
              </CustomText>
            </View>
            <View
              style={[
                styles.items,
                { marginTop: 30, borderBottomColor: backgroundColor },
              ]}>
              <CustomText
                style={stylesText.secondaryTextBold}
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}>
                CONTENIDO DEL PACK
              </CustomText>
              <CustomText
                style={[stylesText.secondaryText, { marginTop: 10 }]}
                numberOfLines={10}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {datas.salvingPack.item.description}
              </CustomText>

              <View style={styles.btn_small}>
                <CustomText
                  style={stylesText.secondaryTextBold}
                  numberOfLines={1}
                  light={colors.main}
                  dark={colors.main}>
                  {datas.salvingPack.tag}
                </CustomText>
              </View>
            </View>
            <View
              style={[
                styles.items,
                { marginTop: 30, borderBottomColor: backgroundColor },
              ]}>
              <CustomText
                style={stylesText.secondaryTextBold}
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}>
                OPINIONES DE LOS CLIENTES
              </CustomText>
              <View style={styles.totalWrap}>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Star star={averageRating} />
                </View>
                <CustomText light={colors.rgb_153} dark={colors.rgb_153}>
                  {averageRating} de 5
                </CustomText>
              </View>
            </View>

            <View
              style={[
                styles.items,
                {
                  marginTop: 30,
                  borderBottomColor: backgroundColor,
                  marginBottom: dimensions.Height(20),
                },
              ]}>
              <CustomText
                style={stylesText.secondaryTextBold}
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}>
                INFO IMPORTANTE
              </CustomText>
              <CustomText
                style={[stylesText.secondaryText, { marginTop: 10 }]}
                numberOfLines={10}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                Recuerda que estás comprando un pack sorpresa y que el contenido
                de este pack dependerá de los alimentos no vendido cada día. En
                caso de alergias o intolerancias pregunta al establecimiento.
              </CustomText>
            </View>
          </View>
        </ParallaxScrollView>
        {datas.salvingPack.pickUpDate ? (
          <View
            style={{
              width: dimensions.ScreenWidth,
              height: 40,
              backgroundColor: colors.ERROR,
              position: 'absolute',
              zIndex: 100,
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 15,
              bottom: dimensions.IsIphoneX() ? 120 : 80,
            }}>
            <CustomText
              numberOfLines={1}
              light={colors.white}
              dark={colors.white}
              style={stylesText.secondaryTextBold}>
              Este pack se termina {restante}
            </CustomText>
          </View>
        ) : null}

        <View style={styles.selected}>
          <View style={styles.sumaResta}>
            <TouchableOpacity
              onPress={() => rescantidad()}
              style={[
                styles.plus,
                {
                  backgroundColor: 'rgba(197, 248, 116, 0.3)',
                },
              ]}>
              <Icon
                name="minus"
                type="AntDesign"
                size={24}
                color={colors.main}
              />
            </TouchableOpacity>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={stylesText.secondaryTextBold}>
              {quantity}
            </CustomText>
            <TouchableOpacity
              onPress={() => sumcantidad()}
              style={[
                styles.plus,
                {
                  backgroundColor: 'rgba(197, 248, 116, 0.3)',
                },
              ]}>
              <Icon
                name="plus"
                type="AntDesign"
                size={20}
                color={colors.main}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => addTocardItems()}
            style={[
              styles.btnAdd,
              {
                backgroundColor: colors.main,
              },
            ]}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              style={[stylesText.secondaryTextBold, { paddingBottom: -5 }]}>
              RESERVAR
            </CustomText>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}

const color1 = 'rgba(255, 255, 255, 0.5)';
const color2 = 'rgba(0, 0, 0, 0.5)';

const PARALLAX_HEADER_HEIGHT = 300;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contenedor: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    minHeight: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  imagenbg: {
    width: dimensions.Width(100),
    height: PARALLAX_HEADER_HEIGHT,
  },

  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: '100%',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 30,
  },

  fixedSection: {
    position: 'absolute',
    bottom: 20,
    left: 15,
    flexDirection: 'row',
    paddingBottom: 5,
    top: 40,
  },

  left: {
    marginRight: dimensions.Width(4),
    backgroundColor: new DynamicValue(color1, color2),
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },

  close: {
    color: colors.main,
  },

  items: {
    marginTop: 15,
    paddingHorizontal: 15,
    paddingBottom: 30,
    borderBottomWidth: 0.5,
  },

  btn_small: {
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
    padding: 5,
    borderRadius: 7,
    width: 170,
    height: 40,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  iconos: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  selected: {
    width: dimensions.ScreenWidth,
    height: dimensions.IsIphoneX() ? 120 : 80,
    borderRadius: 5,
    bottom: 0,
    position: 'absolute',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    flexDirection: 'row',
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  sumaResta: {
    width: 150,
    height: 50,
    borderRadius: 7,
    marginLeft: 15,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
  },

  btnAdd: {
    height: 50,
    paddingHorizontal: dimensions.Width(5),
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
    borderRadius: 10,
    marginRight: 15,
  },
  totalWrap: {
    marginTop: 20,
    marginBottom: 5,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 15,
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
  },

  tagshipping: {
    zIndex: 200,
    marginLeft: 0,
    marginTop: 10,
    padding: 3,
    width: 180,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 0,
    borderRadius: 5,
    marginBottom: 5,
    backgroundColor: colors.green,
  },

  plus: {
    padding: 7,
    borderRadius: 100,
  },
});

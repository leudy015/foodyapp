import React from 'react';
import {SafeAreaView, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dynamic';
import {colors, dimensions} from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <View style={{alignSelf: 'center', backgroundColor: back}}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            height: dimensions.Height(30),
            flexDirection: 'row',
            marginHorizontal: 20,
          }}>
          <View
            style={{
              marginTop: 20,
              width: dimensions.Width(70),
              marginRight: 20,
            }}>
            <View
              style={{
                width: dimensions.Width(70),
                height: 160,
                borderRadius: 7,
              }}
            />

            <View style={{flexDirection: 'row'}}>
              <View>
                <View
                  style={{
                    width: dimensions.Width(30),
                    height: 15,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(20),
                    height: 12,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(10),
                    height: 8,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
              </View>

              <View style={{marginLeft: 'auto', marginRight: 15}}>
                <View
                  style={{
                    width: dimensions.Width(10),
                    height: 20,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(5),
                    height: 10,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
              </View>
            </View>
          </View>

          <View
            style={{
              marginTop: 20,
              width: dimensions.Width(70),
              marginRight: 20,
            }}>
            <View
              style={{
                width: dimensions.Width(70),
                height: 160,
                borderRadius: 7,
              }}
            />

            <View style={{flexDirection: 'row'}}>
              <View>
                <View
                  style={{
                    width: dimensions.Width(30),
                    height: 15,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(20),
                    height: 12,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(10),
                    height: 8,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
              </View>

              <View style={{marginLeft: 'auto', marginRight: 15}}>
                <View
                  style={{
                    width: dimensions.Width(10),
                    height: 20,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(5),
                    height: 10,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
              </View>
            </View>
          </View>

          <View
            style={{
              marginTop: 20,
              width: dimensions.Width(70),
              marginRight: 20,
            }}>
            <View
              style={{
                width: dimensions.Width(70),
                height: 160,
                borderRadius: 7,
              }}
            />

            <View style={{flexDirection: 'row'}}>
              <View>
                <View
                  style={{
                    width: dimensions.Width(30),
                    height: 15,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(20),
                    height: 12,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(10),
                    height: 8,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
              </View>

              <View style={{marginLeft: 'auto', marginRight: 15}}>
                <View
                  style={{
                    width: dimensions.Width(10),
                    height: 20,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(5),
                    height: 10,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
              </View>
            </View>
          </View>

          <View style={{marginTop: 20, width: dimensions.Width(70)}}>
            <View
              style={{
                width: dimensions.Width(70),
                height: 160,
                borderRadius: 7,
              }}
            />

            <View style={{flexDirection: 'row'}}>
              <View>
                <View
                  style={{
                    width: dimensions.Width(30),
                    height: 15,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(20),
                    height: 12,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(10),
                    height: 8,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
              </View>
              <View style={{marginLeft: 'auto', marginRight: 15}}>
                <View
                  style={{
                    width: dimensions.Width(10),
                    height: 20,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(5),
                    height: 10,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

import React, { useState } from 'react';
import { View, TextInput } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, dimensions, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';

export const Comments = (prosp) => {
  const { quequieres, nota, setnota } = prosp;
  const styles = useDynamicValue(dynamicStyles);

  const borderColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const border = borderColors[mode];

  return (
    <View style={[styles.cards, { borderColor: border }]}>
      <View style={[styles.textInputContent, { alignItems: 'flex-start' }]}>
        <Icon
          name="edit"
          type="AntDesign"
          size={24}
          color={colors.rgb_153}
          style={{ marginTop: 15, marginRight: 10 }}
        />
        <TextInput
          style={[
            styles.textInput,
            {
              width: dimensions.Width(70),
              height: 'auto',
              minHeight: 120,
            },
          ]}
          placeholder={quequieres}
          multiline={true}
          defaultValue={nota}
          onChangeText={(value) => setnota(value)}
          selectionColor={colors.main}
          autoCompleteType="name"
          placeholderTextColor={colors.rgb_153}
          autoCorrect={false}
          clearButtonMode="while-editing"
        />
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  textInputContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
  },

  textInput: {
    height: 'auto',
    minHeight: 50,
    width: dimensions.Width(70),
    paddingHorizontal: 5,
    marginVertical: 10,
    fontSize: dimensions.FontSize(18),
    fontWeight: '300',
    color: new DynamicValue(colors.black, colors.white),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  cards: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: 30,
    padding: 10,
    borderRadius: 20,
    marginHorizontal: 15,
    borderWidth: 1,
  },
});

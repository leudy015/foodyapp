import React from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  Platform,
  TextInput,
} from 'react-native';
import { CustomText } from './CustomTetx';
import { colors, dimensions, stylesText } from '../theme';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Navigation from '../services/Navigration';

const height = Dimensions.get('window').height;

export interface IHeader {
  title: string;
  search?: boolean;
  autoFocus?: boolean;
  left?: boolean;
  onChangeText?: any;
  Loading?: boolean;
  value?: string;
  placeholder?: string;
  onFocus?: () => void;
  filter?: boolean;
  onProgressFilter?: any;
  fromPDF?: boolean;
  onPresPDF?: any;
  fromSearch?: boolean;
  setModalVisible?: any;
  setdatos?: any;
}

export default function Header(props: IHeader) {
  const {
    title,
    autoFocus,
    search,
    left,
    onChangeText,
    value,
    placeholder,
    onFocus,
    fromPDF,
    onPresPDF,
    fromSearch,
    setModalVisible,
    setdatos,
  } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <SafeAreaView
      style={[
        styles.container,
        {
          height: Platform.select({
            ios: dimensions.IsIphoneX() ? (search ? 100 : 80) : 70,
            android: 90,
          }),
        },
      ]}>
      <View style={styles.headerCont}>
        <View style={{ marginTop: search ? 8 : 0 }}>
          <TouchableOpacity
            onPress={() => {
              if (setdatos) {
                setdatos([]);
              }
              fromSearch ? setModalVisible(false) : Navigation.goBack();
            }}
            style={styles.contentIcon}>
            <Icon
              name="arrow-left"
              type="Feather"
              size={24}
              color={colors.main}
            />
          </TouchableOpacity>
        </View>
        {search ? (
          <View style={styles.input}>
            <Icon
              type="AntDesign"
              name="search1"
              size={22}
              color={colors.main}
              style={{ marginLeft: 15 }}
            />

            <TextInput
              style={styles.textInput}
              placeholder={placeholder}
              onChangeText={onChangeText}
              onFocus={onFocus}
              autoFocus={autoFocus}
              placeholderTextColor={colors.rgb_153}
              selectionColor={colors.main}
              value={value}
              returnKeyType="search"
              clearButtonMode="while-editing"
            />
          </View>
        ) : (
          <>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                width: dimensions.Width(50),
              }}>
              <CustomText
                numberOfLines={1}
                ligth={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryTextBold, { textAlign: 'center' }]}>
                {title}
              </CustomText>
            </View>
            <View>
              {fromPDF ? (
                <TouchableOpacity
                  onPress={onPresPDF}
                  style={styles.contentIcon}>
                  <Icon
                    name="share-outline"
                    type="Ionicons"
                    size={24}
                    color={colors.main}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    if (setdatos) {
                      setdatos([]);
                    }
                    Navigation.navigate('ComparteyGana', 0);
                  }}
                  style={styles.contentIcon}>
                  <Icon
                    name="gift"
                    type="AntDesign"
                    size={24}
                    color={colors.main}
                  />
                </TouchableOpacity>
              )}
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.ScreenWidth,
    marginBottom: 15,
  },

  headerCont: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: dimensions.Width(4),
    marginTop: Platform.select({
      ios: dimensions.IsIphoneX() ? 0 : 10,
      android: dimensions.Height(5),
    }),
  },

  closet: {
    color: new DynamicValue(colors.main, colors.white),
    paddingRight: 15,
    paddingLeft: height < 600 ? 15 : 0,
  },

  contentIcon: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 100,
    width: 36,
    height: 36,
    justifyContent: 'center',
    alignItems: 'center',
  },

  input: {
    height: 40,
    width: dimensions.Width(80),
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },

  textInput: {
    height: 40,
    width: dimensions.Width(70),
    borderRadius: 100,
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    paddingLeft: 15,
    color: new DynamicValue(colors.black, colors.white),
  },

  filters: {
    width: 80,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 100,
  },
});

import React from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Header from '../Components/Header';
import { useNavigationParam } from 'react-navigation-hooks';
import Loader from '../Components/ModalLoading';
import { WebView } from 'react-native-webview';
import { colors } from '../theme';

const WebViews = () => {
  const url = useNavigationParam('data');
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.container}>
      <Header title="Wilbby" />
      <WebView
        source={{
          uri: url,
        }}
        startInLoadingState={true}
        renderLoading={() => <Loader color={colors.main} />}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },
});

export default WebViews;

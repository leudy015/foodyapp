import React from 'react';
import { View, TouchableOpacity, FlatList } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions, stylesText } from '../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { formaterPrice } from '../../Utils/formaterPRice';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const propi = [
  {
    id: 400,
    cantidad: 50,
  },
  {
    id: 300,
    cantidad: 100,
  },
  {
    id: 0,
    cantidad: 150,
  },
  {
    id: 1,
    cantidad: 200,
  },
  {
    id: 2,
    cantidad: 250,
  },
  {
    id: 298,
    cantidad: 300,
  },
  {
    id: 3,
    cantidad: 500,
  },
  {
    id: 4,
    cantidad: 750,
  },
  {
    id: 5,
    cantidad: 1000,
  },

  {
    id: 6,
    cantidad: 1500,
  },
  {
    id: 7,
    cantidad: 2000,
  },
];

export default function PropinaRider(props) {
  const styles = useDynamicValue(dynamicStyles);
  const { setPropina, propina, localeCode, currecy } = props;

  const renderPripina = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.propinaCont}
        onPress={() => {
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          if (propina) {
            setPropina(0);
            if (propina != item.cantidad) {
              setPropina(item.cantidad);
            }
          } else {
            setPropina(item.cantidad);
          }
        }}>
        <CustomText
          light={colors.rgb_153}
          dark={colors.white}
          style={stylesText.secondaryTextBold}>
          {formaterPrice(item.cantidad / 100, localeCode, currecy)}
        </CustomText>
        <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <Icon
            name={propina === item.cantidad ? 'close' : 'plus'}
            type="AntDesign"
            size={18}
            color={propina === item.cantidad ? colors.ERROR : colors.green}
            style={{ marginRight: 5 }}
          />
          <CustomText
            light={propina === item.cantidad ? colors.ERROR : colors.green}
            dark={propina === item.cantidad ? colors.ERROR : colors.green}
            style={stylesText.secondaryText}>
            {propina === item.cantidad ? 'Eliminar' : 'Añadir'}
          </CustomText>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{ marginTop: 20, marginBottom: 20, marginLeft: 15 }}>
      <CustomText
        numberOfLines={2}
        light={colors.black}
        dark={colors.white}
        style={[
          stylesText.secondaryTextBold,
          { marginBottom: 5, width: dimensions.Width(80) },
        ]}>
        ¿Quieres ayudar a nuestro repartidor con una propina? 😇
      </CustomText>
      <FlatList
        data={propi}
        renderItem={(item: any) => renderPripina(item)}
        keyExtractor={(item: any) => item.id}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  propinaCont: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginHorizontal: dimensions.Width(2),
    padding: 10,
    width: 150,
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 10,
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});

import React from 'react';
import { View, TouchableOpacity, TextInput } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions } from '../../theme';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import { ActivityIndicator } from 'react-native-paper';

export default function Cupon(props) {
  const { descuento, Loading, setCupon, handleSubmit } = props;

  const styles = useDynamicValue(dynamicStyles);
  return (
    <View>
      {descuento.descuento ? (
        <View
          style={{
            padding: 10,
            width: dimensions.Width(96),
            height: 70,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            backgroundColor: 'rgba(197, 248, 116, 0.3)',
            marginTop: 20,
            borderRadius: 10,
          }}>
          <CustomText
            light={colors.main}
            dark={colors.main}
            style={stylesText.secondaryTextBold}>
            <Icon
              name="checkcircle"
              type="AntDesign"
              size={20}
              color={colors.main}
              style={{ marginTop: 10 }}
            />{' '}
            Cupón aplicado con éxito
          </CustomText>
        </View>
      ) : (
        <>
          <View
            style={{
              padding: 20,
              flexDirection: 'row',
              marginTop: 20,
              alignItems: 'center',
            }}>
            <View>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  { marginLeft: 0, marginTop: 7 },
                ]}>
                ¿Tienes un cupón?
              </CustomText>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    marginLeft: 0,
                    marginTop: 7,
                    width: 300,
                    paddingBottom: 5,
                    lineHeight: 18,
                  },
                ]}>
                Los cupones sólo se aplican al precio de los poductos no incluye
                el precio del envío, al menos que este cupón sea para envíos
                gratuitos.
              </CustomText>
            </View>
          </View>
          <View style={styles.formView}>
            <Icon
              name="ticket-percent-outline"
              type="MaterialCommunityIcons"
              size={24}
              color={colors.main}
            />
            <TextInput
              placeholder="Código de descuento"
              autoCompleteType="name"
              autoCapitalize="none"
              autoCorrect={false}
              clearButtonMode="always"
              selectionColor={colors.main}
              placeholderTextColor={colors.rgb_153}
              style={styles.input}
              onChangeText={(clave: any) =>
                setCupon(clave.toUpperCase().replace(/\s/g, ''))
              }
            />
            <View style={styles.btn}>
              <TouchableOpacity onPress={() => handleSubmit()}>
                {Loading ? (
                  <ActivityIndicator color={colors.main} size="small" />
                ) : (
                  <CustomText light={colors.rgb_153} dark={colors.rgb_153}>
                    Aceptar
                  </CustomText>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </>
      )}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  formView: {
    marginTop: dimensions.Height(1),
    marginHorizontal: 15,
    paddingHorizontal: 10,
    borderRadius: 5,
    height: 40,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: new DynamicValue(colors.rgb_153, colors.rgb_153),
    borderWidth: 0.5,
  },

  input: {
    width: dimensions.Width(60),
    marginLeft: 10,
    color: colors.rgb_153,
    flexDirection: 'row',
  },

  btn: {
    width: dimensions.Width(18),
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 0.5,
    borderLeftColor: new DynamicValue(colors.rgb_153, colors.rgb_153),
  },
});

import React from 'react';
import { View } from 'react-native';
import { useTranslation } from 'react-i18next';
import { colors, dimensions, image, stylesText } from '../../theme';
import { CustomText } from '../CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';

export default function HeightDemand() {
  const { t } = useTranslation();
  return (
    <View
      style={{
        backgroundColor: 'rgba(197,248,116,.3764705882352941)',
        marginHorizontal: 10,
        marginVertical: 20,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 15,
      }}>
      <Icon name="warning" type="AntDesign" color={colors.main} size={24} />
      <CustomText
        style={[
          stylesText.secondaryTextBold,
          { marginLeft: 10, fontSize: 16, width: dimensions.Width(70) },
        ]}
        light={colors.main}
        dark={colors.main}>
        {t('heigthDemand:title')}
      </CustomText>
    </View>
  );
}

import React, { useState, useEffect } from 'react';
import { Text, View, Animated } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors } from '../../theme';
import { CustomText } from '../CustomTetx';

export const PercentageBar = ({ starText, percentage }) => {
  const styles = useDynamicValue(dynamicStyles);
  const [animation] = useState(new Animated.Value(0));
  useEffect(() => {
    //@ts-ignore
    Animated.timing(animation, {
      toValue: percentage,
      duration: 2000,
    }).start();
  }, [percentage]);

  return (
    <View
      style={{
        flexDirection: 'row',
      }}>
      <CustomText
        light={colors.black}
        dark={colors.white}
        style={styles.progressText}>
        {starText}
      </CustomText>
      <View style={styles.progressMiddle}>
        <View style={styles.progressWrap}>
          <Animated.View
            style={[
              styles.progressBar,
              {
                width: animation.interpolate({
                  inputRange: [0, 100],
                  outputRange: ['0%', '100%'],
                }),
              },
            ]}
          />
        </View>
      </View>
      <CustomText
        light={colors.rgb_153}
        dark={colors.rgb_153}
        style={styles.progressPercentText}>
        {percentage}%
      </CustomText>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  progressText: {
    width: 10,
    fontSize: 14,
  },
  progressPercentText: { width: 40, fontSize: 14 },
  progressMiddle: {
    height: 15,
    flex: 1,
    marginHorizontal: 10,
  },
  progressWrap: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 18,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    padding: 2,
  },
  progressBar: {
    flex: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: '#ffcc48',
    shadowOpacity: 1.0,
    shadowRadius: 4,
    backgroundColor: '#FFCC48',
    borderRadius: 18,
    minWidth: 5,
  },
});

import React from 'react';
import { View, Image } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, image } from '../theme';
import LoadingAnimated from '../Components/LoadingAnimated';

const NoResul = () => {
  const styles = useDynamicValue(dynamicStyles);

  const Burguer = new DynamicValue(image.Logo, image.LogoWhite);

  const source = useDynamicValue(Burguer);

  return (
    <View style={styles.container}>
      <Image source={source} style={{ resizeMode: 'contain', width: 200 }} />
      <LoadingAnimated name="Wilbby by EnCaminoo" />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default NoResul;

import React, { useState } from 'react';
import { View, FlatList, Image, ImageBackground } from 'react-native';
import { CustomText } from '../../CustomTetx';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors, stylesText, image } from '../../../theme';
import * as Animatable from 'react-native-animatable';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { formaterPrice } from '../../../Utils/formaterPRice';
import CheckBox from '@react-native-community/checkbox';
import Pagination from '../../Pagination';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Bundled(props: any) {
  const { datos, localeCode, currecy, animated, options, setOptions } = props;
  const styles = useDynamicValue(dynamicStyles);

  const [modifier, setModifier] = useState(datos.ModifierGroups);

  function removeItemFromArr(arr: any, item: any) {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    var i = arr.findIndex((x: any) => x._id === item._id);
    if (i !== -1) {
      arr.splice(i, 1);
      setOptions(options.concat());
    }
  }

  const onSelectedItemsChange = (item: any, quantitys: number) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    Object.assign(datos, {
      mid: datos._id,
      quantity: quantitys,
      subItems: datos.subItems.concat(item),
    });
    setOptions(options.concat());
  };

  const plusQuantity = (quantity, index, subIndex, multiMax) => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    if (quantity < multiMax) {
      let newModifier = [...modifier];
      newModifier[index]['Modifiers'][subIndex]['quantity'] = quantity + 1;
      setModifier(newModifier);
    }
  };

  const minusQuantity = (quantity, index, subIndex) => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    if (quantity !== 1) {
      let newModifier = [...modifier];
      newModifier[index]['Modifiers'][subIndex]['quantity'] = quantity - 1;
      setModifier(newModifier);
    }
  };

  const _itemsRenderList = (index1, { item, index }) => {
    const filtrar = datos && datos.subItems ? datos.subItems : [];
    const ifselected = filtrar.filter((e) => e._id === item._id).length === 1;
    return (
      <>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={styles.complemento}>
            <View style={{ flexDirection: 'row' }}>
              <View
                style={{
                  width: item.imageUrl
                    ? dimensions.Width(40)
                    : dimensions.Width(45),
                }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  {item.imageUrl ? (
                    <ImageBackground
                      imageStyle={styles.image}
                      source={image.PlaceHolder}
                      style={styles.image}>
                      <Image
                        source={{ uri: item.imageUrl }}
                        style={styles.image}
                      />
                    </ImageBackground>
                  ) : null}

                  <View>
                    <CustomText
                      numberOfLines={5}
                      light={ifselected ? colors.black : colors.rgb_153}
                      dark={ifselected ? colors.white : colors.rgb_153}
                      style={ifselected ? styles.textActive : styles.text}>
                      {item.name}
                    </CustomText>
                    {item.price > 0 ? (
                      <CustomText
                        numberOfLines={4}
                        light={colors.main}
                        dark={colors.main}
                        style={[
                          stylesText.secondaryText,
                          {
                            fontWeight: '300',
                            marginRight: 15,
                            marginTop: 10,
                          },
                        ]}>
                        (+{' '}
                        {formaterPrice(item.price / 100, localeCode, currecy)})
                      </CustomText>
                    ) : null}
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              marginLeft: 'auto',
              marginRight: 15,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View style={{ width: 100, marginRight: 10 }}>
              {ifselected && item.multiMax > 1 ? (
                <Pagination
                  page={item.quantity}
                  onPressMas={() =>
                    plusQuantity(item.quantity, index1, index, item.multiMax)
                  }
                  onPressMenos={() =>
                    minusQuantity(item.quantity, index1, index)
                  }
                  width={100}
                />
              ) : null}
            </View>
            {!item.snoozed ? (
              <CheckBox
                key={item._id}
                disabled={ifselected ? false : item.snoozed}
                value={ifselected}
                onValueChange={(value) =>
                  value
                    ? onSelectedItemsChange(item, 1)
                    : removeItemFromArr(datos.subItems, item)
                }
                onTintColor={colors.main}
                tintColors={{ true: colors.main, false: colors.rgb_153 }}
                onCheckColor={colors.main}
                onAnimationType="fill"
                offAnimationType="fill"
              />
            ) : (
              <View
                style={[styles.requerido, { backgroundColor: colors.ERROR }]}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={[stylesText.secondaryText, { paddingBottom: 1 }]}>
                  Agotado
                </CustomText>
              </View>
            )}
          </View>
        </View>
        <View style={styles.separator} />
      </>
    );
  };

  const _renderItem = ({ item, index }) => {
    return (
      <View>
        <View
          style={[
            styles.items,
            {
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'center',
            },
          ]}>
          <View>
            <Animatable.Text
              animation={animated}
              iterationCount={5}
              direction="alternate"
              numberOfLines={2}
              style={styles.animateText}>
              {item.name}
            </Animatable.Text>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                { paddingBottom: 3, marginTop: 3 },
              ]}>
              Elige {item.min} {item.min > 1 ? 'productos' : 'producto'}
            </CustomText>
          </View>
        </View>
        <FlatList
          data={item.Modifiers}
          renderItem={(items: any) => _itemsRenderList(index, items)}
          keyExtractor={(item: any) => item._id}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  };
  return (
    <FlatList
      data={modifier}
      renderItem={(item: any) => _renderItem(item)}
      keyExtractor={(item: any) => item._id}
      showsVerticalScrollIndicator={false}
      scrollEnabled={false}
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  animateText: {
    width: dimensions.Width(60),
    fontSize: dimensions.FontSize(18),
    fontWeight: 'bold',
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 30,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },

  requerido: {
    marginLeft: 'auto',
    marginRight: 15,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    paddingHorizontal: 20,
    padding: 7,
    borderRadius: 100,
  },

  complemento: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 30,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    paddingLeft: dimensions.Width(4),
    flexDirection: 'row',
    marginRight: 15,
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  image: {
    width: 40,
    height: 40,
    borderRadius: 100,
    marginRight: 10,
  },

  textActive: {
    fontSize: dimensions.FontSize(14),
    fontWeight: 'bold',
    fontFamily: 'Hiragino Sans',
    paddingBottom: 5,
  },

  text: {
    fontSize: dimensions.FontSize(14),
    fontWeight: '300',
    fontFamily: 'Hiragino Sans',
    paddingBottom: 5,
    lineHeight: 20,
  },
});

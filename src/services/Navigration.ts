import { NavigationActions, StackActions } from 'react-navigation';

let _container: any; // eslint-disable-line

function setContainer(container: any) {
  _container = container;
}

let location: any; // eslint-disable-line

function setLocation(data: any) {
  location = data;
}

function reset(routeName: any, params: any) {
  _container.dispatch(
    StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName, params })],
    }),
  );
}

function navigate(routeName: any, params: any) {
  _container.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
}
function goBack() {
  _container.dispatch(NavigationActions.back());
}

function getCurrentRoute() {
  if (!_container || !_container.state.nav) {
    return null;
  }

  return _container.state.nav.routes[_container.state.nav.index] || null;
}

export default {
  setContainer,
  setLocation,
  navigate,
  reset,
  getCurrentRoute,
  goBack,
};

import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  Animated,
  Alert,
  Keyboard,
  Image,
  TextInput,
  Platform,
  Linking,
  Dimensions,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { stylesText } from '../theme/TextStyle';
import { dimensions } from '../theme/dimension';
import Icon from 'react-native-dynamic-vector-icons';
import { colors, image } from '../theme';
import Navigation from '../services/Navigration';
import { Button } from '../Components/Button';
import { NETWORK_INTERFACE_LINK } from '../Config/config';
import AsyncStorage from '@react-native-community/async-storage';
import LoginSocial from './SocialLogin';
import { validateEmail } from '../Utils/EmailsValidator';
import Toast from 'react-native-toast-message';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import LottieView from 'lottie-react-native';
import Regiter from './Register';
import {
  getModel,
  getSystemName,
  getIpAddress,
} from 'react-native-device-info';
import * as RNLocalize from 'react-native-localize';
import Mailer from 'react-native-mail';

const Login = () => {
  const styles = useDynamicValue(dynamicStyles);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [Loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [ip, setip] = useState(null);
  const [activeIndex, setactiveIndex] = useState('Social Login');

  const hi = Dimensions.get('window').height;

  const initialBottom =
    hi > 750 ? dimensions.Height(30) : dimensions.Height(35);

  const initH = dimensions.IsIphoneX()
    ? dimensions.Height(40)
    : dimensions.Height(40);

  const hig = dimensions.IsIphoneX()
    ? dimensions.Height(70)
    : Platform.select({
        ios: dimensions.Height(70),
        android:
          activeIndex === 'Social Login'
            ? dimensions.Height(80)
            : dimensions.Height(70),
      });

  const [h] = useState(new Animated.Value(hig));
  const [opacity] = useState(new Animated.Value(100));

  const setheight = () => {
    Animated.sequence([
      //@ts-ignore
      Animated.timing(opacity, {
        toValue: 0,
        duration: 0,
      }),
      //@ts-ignore
      Animated.timing(h, {
        toValue: initH,
        duration: 500,
      }),
    ]).start();
  };

  const setdisMis = () => {
    Keyboard.dismiss();
    Animated.sequence([
      //@ts-ignore
      Animated.timing(h, {
        toValue: hig,
        duration: 500,
      }),
      //@ts-ignore
      Animated.timing(opacity, {
        toValue: 100,
        duration: 0,
      }),
    ]).start();
  };

  const optiones = {
    enableVibrateFallback: true,
    ignoreAndroidSystemSettings: false,
  };

  let model = getModel();
  let systemName = getSystemName();
  const country = RNLocalize.getLocales();

  getIpAddress().then((ip) => {
    setip(ip);
  });

  const dataLogin = {
    date: new Date(),
    divice: model,
    system: systemName,
    location: country[0].countryCode,
    IPaddress: ip,
  };

  const input = {
    email,
    password,
    dataLogin: dataLogin,
  };

  const onLogin = async () => {
    setLoading(true);
    if (!validateEmail(email)) {
      setIsError(true);
      setLoading(false);
      return null;
    } else {
      setIsError(false);
      setLoading(false);
      let res = await fetch(`${NETWORK_INTERFACE_LINK}/auth`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(input),
      });
      const user = await res.json();
      if (user.success) {
        setLoading(false);
        Toast.show({
          text1: user.messages,
          text2: user.messages,
          position: 'top',
          type: 'success',
          topOffset: 50,
          visibilityTime: 4000,
        });
        const us = JSON.stringify(user.data.user);
        if (user.data.verifyPhone) {
          AsyncStorage.setItem('token', user.data.token);
          AsyncStorage.setItem('id', user.data.id);
          AsyncStorage.setItem('user', us);
          setTimeout(() => {
            Navigation.navigate('Inicio', { data: user.data.id });
          }, 2000);
        } else {
          const dats = {
            id: user.data.id,
            token: user.data.token,
            Register: false,
          };
          Navigation.navigate('VerifyPhone', { data: dats });
        }
      } else {
        setLoading(false);
        ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
        Toast.show({
          text1: user.messages,
          text2: user.messages,
          position: 'top',
          type: 'info',
          topOffset: 50,
          visibilityTime: 4000,
        });
      }
    }
  };

  const handleEmail = () => {
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: `Necesito ayuda para acceder a Wilbby`,
          recipients: ['info@wilbby.com'],
          body: `<b>Hola necesito ayuda para acceder a wilbby</b>`,
          // Android only (defaults to "Send Mail")
          isHTML: true,
        },
        (error, event) => {},
      );
    } else {
      Linking.openURL('mailto:info@wilbby.com');
    }
  };

  const renderContent = () => {
    switch (activeIndex) {
      case 'Login':
        return (
          <View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryText, { textAlign: 'center' }]}>
              Inicia sesión en tu cuenta
            </CustomText>
            <TouchableOpacity
              onPress={() => setactiveIndex('Social Login')}
              style={styles.btnBack}>
              <Icon
                name="arrow-left"
                type="Feather"
                size={24}
                color={colors.main}
                style={{ marginRight: 0 }}
              />
            </TouchableOpacity>
            <View style={styles.formView}>
              <TextInput
                style={styles.textInput}
                onFocus={() => setheight()}
                autoCapitalize="none"
                selectionColor={colors.main}
                placeholder="Correo electrónico"
                autoCompleteType="username"
                placeholderTextColor={colors.rgb_153}
                autoCorrect={false}
                onChangeText={(clave: any) => setEmail(clave)}
              />
              {isError ? (
                <CustomText
                  style={{ marginTop: 3 }}
                  light={colors.ERROR}
                  dark={colors.ERROR}>
                  ¡Por favor introduce un email válido!
                </CustomText>
              ) : null}
              <TextInput
                onFocus={() => setheight()}
                autoCompleteType="password"
                placeholder="Contraseña"
                autoCapitalize="none"
                placeholderTextColor={colors.rgb_153}
                secureTextEntry={true}
                autoCorrect={false}
                selectionColor={colors.main}
                style={styles.textInput}
                onChangeText={(clave: any) => setPassword(clave)}
              />

              <View>
                <TouchableOpacity
                  onPress={() => Navigation.navigate('Forgot', 0)}
                  style={{
                    marginLeft: 'auto',
                    marginTop: dimensions.Height(2),
                  }}>
                  <CustomText
                    light={colors.main}
                    dark={colors.main}
                    style={stylesText.secondaryText}>
                    ¿Obtener nueva contraseña?
                  </CustomText>
                </TouchableOpacity>
              </View>

              <View style={styles.signupButtonContainer}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() => onLogin()}
                  title="Iniciar sesión"
                  loading={Loading}
                  titleStyle={styles.buttonTitle}
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => setModalVisible(true)}
              style={{
                alignSelf: 'center',
                alignItems: 'center',
                marginTop: 20,
              }}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[stylesText.secondaryTextBold, { textAlign: 'center' }]}>
                ¿No tienes una cuenta?
              </CustomText>
            </TouchableOpacity>
          </View>
        );
      case 'Social Login':
        return (
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryText]}>
              Inicia sesión en tu cuenta
            </CustomText>
            <LoginSocial
              setEmailScreen={() => setactiveIndex('Login')}
              dataLogin={dataLogin}
            />
          </View>
        );
      default:
        break;
    }
  };
  return (
    <View style={styles.container}>
      <Toast ref={(ref) => Toast.setRef(ref)} style={{ zIndex: 100 }} />
      <Animated.View style={[styles.content, { height: h }]}>
        <TouchableOpacity activeOpacity={100} onPress={() => setdisMis()}>
          <TouchableOpacity
            onPress={() =>
              Alert.alert(
                '¿Necesitas ayuda?',
                'No dudes en contactarnos estamos aquí para ayudarte',
                [
                  {
                    text: 'Ayuda teléfonica',
                    onPress: () => Linking.openURL('tel:+34669124487'),
                  },

                  {
                    text: 'Envíar un email',
                    onPress: () => handleEmail(),
                  },
                  {
                    text: 'Cancelar',
                    onPress: () => {},
                  },
                ],

                { cancelable: false },
              )
            }
            style={{
              marginLeft: 'auto',
              marginTop: dimensions.Height(5),
              zIndex: 100,
              marginRight: dimensions.Width(5),
            }}>
            <Icon
              name="questioncircleo"
              type="AntDesign"
              size={24}
              color={colors.white}
            />
          </TouchableOpacity>
          <Animated.View
            style={{
              marginTop: dimensions.Height(0),
              justifyContent: 'center',
              opacity: opacity,
            }}>
            <View
              style={{
                marginTop: dimensions.Height(2),
                marginHorizontal: 20,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <CustomText
                light={colors.white}
                dark={colors.white}
                style={[
                  styles.text,
                  { textAlign: 'left', width: dimensions.Width(60) },
                ]}>
                Bienvenido a Wilbby
              </CustomText>
              <TouchableOpacity
                onPress={() => Navigation.navigate('Inicio', 0)}
                style={{
                  marginLeft: 'auto',
                  backgroundColor: 'rgba(197,248,116,.3)',
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                  borderRadius: 100,
                }}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={stylesText.secondaryText}>
                  Explorar
                </CustomText>
              </TouchableOpacity>
            </View>
            <LottieView
              source={require('../Assets/Animate/chica.json')}
              autoPlay
              loop
              style={{
                width: hi > 750 ? 250 : 170,
                alignSelf: 'center',
                alignItems: 'center',
              }}
            />
          </Animated.View>
        </TouchableOpacity>
      </Animated.View>
      <Image source={image.Line} style={styles.line} />
      <Animated.View style={[styles.content_Login, { bottom: initialBottom }]}>
        {renderContent()}
      </Animated.View>
      <Regiter
        setheight={() => setheight()}
        setModalVisible={setModalVisible}
        modalVisible={modalVisible}
        setdisMis={() => setdisMis()}
        dataLogin={dataLogin}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  content: {
    width: dimensions.ScreenWidth,
    backgroundColor: colors.main,
  },

  line: {
    width: dimensions.ScreenWidth,
    height: 100,
  },

  content_Login: {
    width: dimensions.Width(90),
    height: 'auto',
    padding: 15,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignSelf: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
    alignItems: 'center',
    borderRadius: 10,
  },

  title: {
    marginTop: dimensions.Height(5),
    marginLeft: dimensions.Width(4),
  },

  text: {
    fontSize: dimensions.FontSize(30),
    fontWeight: '900',
  },

  textInput: {
    backgroundColor: new DynamicValue(
      colors.colorInput,
      colors.back_suave_dark,
    ),
    padding: Platform.select({
      android: 10,
      ios: dimensions.IsIphoneX() ? 15 : 10,
    }),
    marginTop: 10,
    borderRadius: 5,
    color: colors.rgb_153,
    borderColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    borderWidth: 0.3,
  },

  close: {
    color: new DynamicValue('black', 'white'),
    marginTop: dimensions.Height(8),
    marginLeft: dimensions.Width(4),
  },

  formView: {
    marginHorizontal: dimensions.Width(4),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(2),
    alignSelf: 'center',
    width: dimensions.Width(80),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(80),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: Platform.select({
      android: 10,
      ios: dimensions.IsIphoneX() ? 10 : 10,
    }),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(16),
    textAlign: 'center',
  },

  btnBack: {
    marginBottom: 10,
    marginLeft: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: new DynamicValue(
      colors.colorInput,
      colors.back_suave_dark,
    ),
    borderRadius: 100,
    width: 36,
    height: 36,
    justifyContent: 'center',
  },
});

export default Login;

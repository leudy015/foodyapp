import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Animated,
  FlatList,
  Alert,
  Dimensions,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Toast from 'react-native-toast-message';
import Header from '../../Components/Header';
import { dimensions } from '../../theme';
import { colors, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { useNavigationParam } from 'react-navigation-hooks';
import { useMutation, Query } from 'react-apollo';
import { mutations, query } from '../../GraphQL';
import Icon from 'react-native-dynamic-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import Navigation from '../../services/Navigration';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import LottieView from 'lottie-react-native';
import LoadingAnimated from '../../Components/LoadingAnimated';
import Card from './Card';
import Maps from './Maps';
import CurrentLocation from './current';
import ManualDirection from './AdressManual';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const he = Dimensions.get('window').height;

const h = he > 750 ? 470 : 420;

const Direccion = () => {
  const data = useNavigationParam('data');
  const styles = useDynamicValue(dynamicStyles);
  const [lat, setLat] = useState(data.lat);
  const [lgn, setLgn] = useState(data.lgn);
  const [type, setType] = useState('Casa');
  const [formatted_address, setFormatted_address] = useState('');
  const [puertaPiso, setpuertaPiso] = useState('');
  const [direccionModal, setDireccionModal] = useState(false);
  const [editable, seteditable] = useState(false);
  const [adressId, setadressId] = useState('');
  const [city, setcity] = useState(data.city);
  const [id, setID] = useState(null);
  const [selectAdr, setselectAdr] = useState(true);
  const [isModalVisible, setModalVisible] = useState(false);
  const [postalCode, setpostalCode] = useState('');
  const [heightAnimate, setheightAnimate] = useState(new Animated.Value(h));
  const [modalManualDirection, setmodalManualDirection] = useState(false);

  const [createAdress] = useMutation(mutations.CREATE_ADRESS);
  const [eliminarAdress] = useMutation(mutations.DELETE_ADRESS);

  const getId = async () => {
    const id = await AsyncStorage.getItem('id');
    setID(id);
  };

  useEffect(() => {
    getId();
  }, [id]);

  const direccionDefecto = async () => {
    const adressID = await AsyncStorage.getItem('adressId');
    setadressId(adressID);
  };

  useEffect(() => {
    direccionDefecto();
  }, [adressId]);

  const createUpdateAdress = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    const input = {
      formatted_address: formatted_address,
      puertaPiso: puertaPiso,
      city: city,
      postalcode: postalCode,
      type: type,
      usuario: id,
      lat: String(lat),
      lgn: String(lgn),
    };
    AsyncStorage.setItem('adressName', formatted_address);
    if (puertaPiso && formatted_address) {
      createAdress({ variables: { input: input } })
        .then((res) => {
          if (res.data.createAdress.success) {
            const dat = res.data.createAdress.data;
            AsyncStorage.setItem('adressName', dat.formatted_address);
            AsyncStorage.setItem('adressId', dat.id);
            setDireccionModal(false);
            seteditable(false);
            setheightAnimate(new Animated.Value(470));
            direccionDefecto();
            if (data.fromHome) {
              Navigation.goBack();
            }

            if (data.getAdreesID) {
              data.getAdreesID();
            }
            Toast.show({
              text1: 'Dirección añadida',
              text2: 'Dirección añadida con éxito',
              position: 'top',
              type: 'success',
              topOffset: 50,
              visibilityTime: 4000,
            });
          }
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      Alert.alert(
        'Falta información',
        'Debes añadir un número de piso, puerta o escalera, para continuar',
        [
          {
            text: 'Añadir información',
            onPress: () => setModalVisible(true),
          },
        ],
      );
    }
  };

  const eliminarDireccion = (id: string, refetch: any) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    eliminarAdress({
      variables: {
        id: id,
      },
    })
      .then(async (res) => {
        if (res.data.eliminarAdress.success) {
          await AsyncStorage.removeItem('adressName');
          await AsyncStorage.removeItem('adressId');
          refetch();
          Toast.show({
            text1: 'Direción eliminada',
            text2: 'Direción eliminada con exito',
            position: 'top',
            type: 'success',
            topOffset: 50,
            visibilityTime: 4000,
          });
          if (data.getAdreesID) {
            data.getAdreesID();
            Navigation.goBack();
          }
        } else {
          Toast.show({
            text1: 'Algo va mal',
            text2: 'Algo va mal intentalo de nuevo',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 4000,
          });
        }
      })

      .catch((e) => {
        console.log(e);
      });
  };

  const setDireccions = (sdress: string, id: string, city: string) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    AsyncStorage.setItem('adressName', sdress);
    AsyncStorage.setItem('adressId', id);
    AsyncStorage.setItem('city', city);

    if (data.getRidersAvailable) {
      data.getRidersAvailable(city);
    }
    if (data.setcity) {
      data.setcity(city);
    }
    if (data.fromHome) {
      Navigation.goBack();
    }

    if (data.getAdreesID) {
      data.getAdreesID();
    }
  };

  const _renderItem = ({ item }, refetch) => {
    return (
      <Card
        item={item}
        editable={editable}
        setDireccions={setDireccions}
        adressId={adressId}
        eliminarDireccion={eliminarDireccion}
        refetch={refetch}
      />
    );
  };

  return (
    <View style={styles.container}>
      <Toast ref={(ref) => Toast.setRef(ref)} style={{ zIndex: 100 }} />
      <Header title="Dirección de entrega" />
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled">
        <View style={{ alignSelf: 'center', marginTop: dimensions.Height(0) }}>
          <Query query={query.GET_ADRESS} variables={{ id: id }}>
            {(response: any) => {
              if (response.loading) {
                return (
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      width: dimensions.ScreenWidth,
                      height: dimensions.Height(70),
                    }}>
                    <LoadingAnimated name="Buscando tus direcciones" />
                  </View>
                );
              }
              if (response) {
                const adress =
                  response && response.data && response.data.getAdress
                    ? response.data.getAdress.data
                    : [];
                response.refetch();
                return (
                  <View style={styles.container}>
                    <View>
                      <View
                        style={{
                          marginBottom: 25,
                          width: dimensions.Width(100),
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginTop: dimensions.Height(3),
                        }}>
                        <TouchableOpacity
                          style={styles.btnAdd}
                          onPress={() => {
                            setDireccionModal(true);
                          }}>
                          <Icon
                            name="pluscircle"
                            type="AntDesign"
                            size={26}
                            color={colors.rgb_153}
                          />
                          <CustomText
                            light={colors.rgb_153}
                            dark={colors.rgb_153}
                            style={[
                              stylesText.secondaryTextBold,
                              {
                                fontSize: 18,
                                marginLeft: 10,
                                color: colors.rgb_153,
                              },
                            ]}>
                            Añadir dirección de entrega
                          </CustomText>
                        </TouchableOpacity>
                      </View>

                      <View
                        style={{ marginHorizontal: 15, marginVertical: 30 }}>
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={[
                            stylesText.secondaryTextBold,
                            { marginTop: 5, fontSize: 18 },
                          ]}>
                          Cerca de mi
                        </CustomText>
                      </View>

                      <CurrentLocation setDireccionModal={setDireccionModal} />

                      <View
                        style={{ marginHorizontal: 15, marginVertical: 30 }}>
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={[
                            stylesText.secondaryTextBold,
                            { marginTop: 5, fontSize: 18 },
                          ]}>
                          Ubicaciones recientes
                        </CustomText>
                      </View>

                      <FlatList
                        data={adress}
                        renderItem={(item: any) =>
                          _renderItem(item, response.refetch)
                        }
                        style={{ marginBottom: dimensions.Height(10) }}
                        keyExtractor={(item: any) => item._id}
                        showsVerticalScrollIndicator={false}
                        ListEmptyComponent={
                          <View
                            style={{
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <LottieView
                              source={require('../../Assets/Animate/1342-location.json')}
                              autoPlay
                              loop
                              style={{ width: 250 }}
                            />
                            <CustomText
                              ligth={colors.black}
                              dark={colors.white}
                              style={stylesText.secondaryText}>
                              Aún no sabemos donde llevarte tus pedidos
                            </CustomText>
                          </View>
                        }
                      />
                    </View>
                  </View>
                );
              }
            }}
          </Query>
        </View>
      </ScrollView>
      <Maps
        direccionModal={direccionModal}
        createUpdateAdress={createUpdateAdress}
        selectAdr={selectAdr}
        setFormatted_address={setFormatted_address}
        setselectAdr={setselectAdr}
        setLat={setLat}
        setLgn={setLgn}
        setDireccionModal={setDireccionModal}
        lat={lat}
        lgn={lgn}
        setheightAnimate={setheightAnimate}
        type={type}
        setcity={setcity}
        formatted_address={formatted_address}
        setpuertaPiso={setpuertaPiso}
        heightAnimate={heightAnimate}
        setType={setType}
        puertaPiso={puertaPiso}
        setpostalCode={setpostalCode}
        LocalCity={data.setcity}
        isModalVisible={isModalVisible}
        setModalVisible={setModalVisible}
        setmodalManualDirection={setmodalManualDirection}
      />
      <ManualDirection
        visible={modalManualDirection}
        setModalVisible={setmodalManualDirection}
        city={data.city}
        lat={data.lat}
        lgn={data.lgn}
        user={id}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  inputAdress: {
    height: 'auto',
    fontSize: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    color: new DynamicValue(colors.black, colors.white),
  },

  header: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  back: {
    marginLeft: 10,
    marginTop: 30,
    marginBottom: 20,
    position: 'absolute',
    padding: 10,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
  },

  listAdress: {
    width: dimensions.Width(95),
    paddingHorizontal: 15,
    height: 'auto',
    paddingVertical: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(1.5),
    marginBottom: dimensions.Height(1.5),
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 0.5,
  },

  fixDireccion: {
    position: 'absolute',
    bottom: 0,
    borderTopStartRadius: 25,
    borderTopEndRadius: 25,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  tipos: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderWidth: 2,
    marginHorizontal: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: 100,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  btnAdd: {
    width: dimensions.Width(96),
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    padding: 15,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Direccion;

import React, { useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Alert,
  Platform,
  NativeModules,
} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicValue,
  DynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { stylesText } from '../theme/TextStyle';
import { dimensions } from '../theme/dimension';
import { colors, image } from '../theme';
import Navigation from '../services/Navigration';
import {
  NETWORK_INTERFACE_LINK,
  NETWORK_INTERFACE_URL,
} from '../Config/config';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from '../Components/ModalLoading';
import {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import { GoogleSignin } from '@react-native-community/google-signin';
import {
  AppleButton,
  appleAuth,
} from '@invertase/react-native-apple-authentication';
import Toast from 'react-native-toast-message';

GoogleSignin.configure({
  iosClientId:
    '700864121165-0pm8if62pbdr40lba0vuo3oduacvcg1u.apps.googleusercontent.com',
});

const { RNTwitterSignIn } = NativeModules;

const Constants = {
  TWITTER_COMSUMER_KEY: 'qxbputK7JSrMl5uXHAGI52bVj',
  TWITTER_CONSUMER_SECRET: 'sr5jAh27A1itXqpy4AY8HcMEEW0IZbIUrlYAXCN3sCSj81D5cQ',
};

const LoginSocial = (props) => {
  const [Loading, setLoading] = useState(false);
  const [isLoggedIn, setisLoggedIn] = useState(false);
  const styles = useDynamicValue(dynamicStyles);

  useEffect(() => {
    RNTwitterSignIn.init(
      Constants.TWITTER_COMSUMER_KEY,
      Constants.TWITTER_CONSUMER_SECRET,
    );
  }, []);

  const onLogin = async (email: string, password: string) => {
    setLoading(true);
    const input = {
      email,
      password,
      dataLogin: props.dataLogin,
    };
    let res = await fetch(`${NETWORK_INTERFACE_LINK}/auth`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify(input),
    });
    const user = await res.json();
    if (user.success) {
      setLoading(false);
      Toast.show({
        text1: 'Bienvenido a Wilbby',
        text2: 'Bienvenido a Wilbby',
        position: 'top',
        type: 'success',
        topOffset: 50,
        visibilityTime: 4000,
      });
      const us = JSON.stringify(user.data.user);
      if (user.data.verifyPhone) {
        AsyncStorage.setItem('token', user.data.token);
        AsyncStorage.setItem('id', user.data.id);
        AsyncStorage.setItem('user', us);
        setTimeout(() => {
          Navigation.navigate('Inicio', { data: user.data.id });
        }, 2000);
      } else {
        const dats = {
          id: user.data.id,
          token: user.data.token,
          Register: false,
        };
        Navigation.navigate('VerifyPhone', { data: dats });
      }
    } else {
      Toast.show({
        text1: user.messages,
        text2: user.messages,
        position: 'top',
        type: 'error',
        topOffset: 50,
        visibilityTime: 4000,
      });
      setLoading(false);
    }
  };

  useEffect(() => {
    if (Platform.OS === 'ios') {
      return appleAuth.onCredentialRevoked(async () => {
        console.warn(
          'If this function executes, User Credentials have been Revoked',
        );
      });
    }
  }, []);

  async function onAppleButtonPress() {
    if (Platform.OS === 'ios') {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      const credentialState = await appleAuth.getCredentialStateForUser(
        appleAuthRequestResponse.user,
      );

      if (credentialState === appleAuth.State.AUTHORIZED) {
        let email = appleAuthRequestResponse.email;
        let token = appleAuthRequestResponse.user;
        let firstName = appleAuthRequestResponse.fullName.givenName;
        let lastName = appleAuthRequestResponse.fullName.familyName;
        let provide = 'Apple App';

        socialLogin({ firstName, lastName, email, token, city: '', provide });
      }
    }
  }

  const googleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      let firstName = userInfo.user.name;
      let lastName = userInfo.user.givenName;
      let email = userInfo.user.email;
      let token = userInfo.user.id;
      let provide = 'Google App';
      socialLogin({ firstName, lastName, email, token, city: '', provide });
    } catch (error) {
      console.log(error);
    }
  };

  const facebookLogin = () => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log(
            'Login success with permissions: ' +
              result.grantedPermissions.toString(),
          );

          const responseInfoCallback = (error: any, result: any) => {
            if (error) {
              console.log(error);
              Alert.alert('Facebook request failed. Try Again.');
            } else {
              //alert(JSON.stringify(result))
              getFacebookUserInfo(result);
            }
          };

          const infoRequest = new GraphRequest(
            '/me?fields=email,name,picture.height(480)',
            null,
            responseInfoCallback,
          );
          // Start the graph request.
          console.log('start  graph request');
          new GraphRequestManager().addRequest(infoRequest).start();
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  const _twitterSignIn = () => {
    RNTwitterSignIn.logIn()
      .then((loginData) => {
        let email = loginData.email;
        let firstName = loginData.name;
        let lastName = loginData.userName;
        let token = loginData.userID;
        let provide = 'Twitter App';
        socialLogin({ firstName, lastName, email, token, city: '', provide });
        const { authToken, authTokenSecret } = loginData;
        if (authToken && authTokenSecret) {
          setisLoggedIn(true);
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  const handleLogout = () => {
    RNTwitterSignIn.logOut();
    setisLoggedIn(false);
  };

  const getFacebookUserInfo = (result: any) => {
    let name = result.name.split(' ');
    let firstName = name[0];
    let lastName = name[1] || '';
    let email = result.email;
    let token = result.id;
    let provide = 'Facebook App';
    socialLogin({ firstName, lastName, email, token, city: '', provide });
  };

  const socialLogin = async (postData: any) => {
    await fetch(`${NETWORK_INTERFACE_URL}/api/v1/auth/social/mobile`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(postData),
    })
      .then(async (res: any) => {
        const data = await res.json();
        onLogin(data.nuevoUsuario.email, data.token);
      })
      .catch((err: any) => {
        console.log(err);
        Toast.show({
          text1: 'Upps lo sentimos',
          text2:
            'Hubo un problema con su solicitud vuelve a intentarlo por favor',
          position: 'top',
          type: 'error',
          topOffset: 50,
          visibilityTime: 4000,
        });
      });
  };

  const lightLogo = AppleButton.Style.WHITE;
  const darkLogo = AppleButton.Style.BLACK;
  const DynamicColor = new DynamicValue(darkLogo, lightLogo);
  const Colord = useDynamicValue(DynamicColor);

  const sty = Platform.select({
    ios: styles.socialBTN,
    android: styles.socialBTNAndroid,
  });

  return (
    <View style={{ marginTop: 20 }}>
      <Loader loading={Loading} color={colors.main} />
      {Platform.OS === 'ios' ? (
        <View style={[styles.appleBtn, { borderRadius: 100 }]}>
          <AppleButton
            buttonStyle={Colord}
            buttonType={AppleButton.Type.SIGN_IN}
            style={{
              height: dimensions.IsIphoneX() ? 50 : 40,
              width: dimensions.Width(79),
              borderRadius: 100,
            }}
            onPress={() => onAppleButtonPress()}
          />
        </View>
      ) : null}

      <TouchableOpacity style={sty} onPress={() => facebookLogin()}>
        <Image
          source={image.FaceBook}
          style={{ width: 35, height: 35, marginRight: 10, marginLeft: 15 }}
        />
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={stylesText.secondaryTextBold}>
          Continúa con Facebook
        </CustomText>
      </TouchableOpacity>

      <TouchableOpacity style={sty} onPress={() => googleSignIn()}>
        <Image
          source={image.Google}
          style={{ width: 30, height: 30, marginRight: 10, marginLeft: 15 }}
        />
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          Continúa con Google
        </CustomText>
      </TouchableOpacity>

      <TouchableOpacity
        style={sty}
        onPress={() => {
          if (isLoggedIn) {
            handleLogout();
          } else {
            _twitterSignIn();
          }
        }}>
        <Image
          source={image.Twitter}
          style={{
            width: 35,
            height: 25,
            marginRight: 10,
            marginLeft: 15,
            resizeMode: 'cover',
          }}
        />
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          {isLoggedIn ? 'Salir de Twitter' : 'Continúa con Twitter'}
        </CustomText>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => props.setEmailScreen()}
        style={{
          alignSelf: 'center',
          marginVertical: dimensions.IsIphoneX() ? 10 : 5,
        }}>
        <CustomText
          light={colors.main}
          dark={colors.main}
          style={[stylesText.secondaryTextBold]}>
          Continúa con tu email
        </CustomText>
      </TouchableOpacity>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  socialBTN: {
    width: dimensions.Width(80),
    height: dimensions.IsIphoneX() ? 50 : 40,
    flexDirection: 'row',
    borderRadius: 7,
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    marginBottom: dimensions.Height(1.5),
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
  },

  socialBTNAndroid: {
    width: dimensions.Width(80),
    height: dimensions.IsIphoneX() ? 50 : 40,
    flexDirection: 'row',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: colors.rgb_153,
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginBottom: dimensions.Height(1.5),
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
  },

  appleBtn: {
    height: dimensions.IsIphoneX() ? 60 : 50,
    width: dimensions.Width(80),
    marginBottom: dimensions.Height(1),
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
  },
});

export default LoginSocial;

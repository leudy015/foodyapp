import React from 'react';
import { Image, View, TouchableOpacity, Alert, Modal } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors, image } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import AlgoEspecial from '../AlgoEspecial';
import { validateDates } from '../../Utils/CalculateHour';

export default function Express(props) {
  const { lat, lgn, ModalVisibleEsp, setModalVisibleEsp, id, city } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.noProduct}>
      <Image source={image.Loquesea} style={styles.loqueseaIMG} />
      <View>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={stylesText.secondaryTextBold}>
          ¿No lo encuentras?
        </CustomText>
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            {
              width: dimensions.Width(50),
              paddingTop: 15,
              paddingBottom: 5,
              lineHeight: 18,
            },
          ]}>
          Lo que sea, si existe lo buscamos y te lo llevamos donde estés.
        </CustomText>

        <TouchableOpacity
          style={styles.btn}
          onPress={() =>
            validateDates(9, 30, 23, 59)
              ? setModalVisibleEsp(true)
              : Alert.alert('No disponible o fuera de horario')
          }>
          <CustomText
            numberOfLines={1}
            light={colors.white}
            dark={colors.white}
            style={[stylesText.secondaryText]}>
            Envío personalizado
          </CustomText>
        </TouchableOpacity>
        <Modal
          animationType="slide"
          visible={ModalVisibleEsp}
          presentationStyle="formSheet"
          statusBarTranslucent={true}
          onRequestClose={() => setModalVisibleEsp(false)}>
          <View style={styles.centeredView}>
            <View style={styles.HeaderModal}>
              <TouchableOpacity onPress={() => {}}></TouchableOpacity>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  width: dimensions.Width(50),
                }}>
                <CustomText
                  numberOfLines={1}
                  ligth={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.mainText,
                    { textAlign: 'center', marginLeft: 15 },
                  ]}>
                  Algo especial
                </CustomText>
              </View>
              <TouchableOpacity onPress={() => setModalVisibleEsp(false)}>
                <Icon
                  name="closecircleo"
                  type="AntDesign"
                  size={24}
                  style={styles.icons}
                />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
              <AlgoEspecial
                lat={lat}
                lgn={lgn}
                user={id}
                city={city}
                setModalVisible={setModalVisibleEsp}
                quequieres="¿Qué quieres que te compremos?"
                donde="¿Dónde lo compramos?"
                vamos="¿Dónde te lo llevamos?"
                especial={true}
              />
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  centeredView: {
    backgroundColor: new DynamicValue(colors.colorInput, 'black'),
    flex: 1,
  },
  conts: {
    width: dimensions.Width(100),
    height: 80,
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    marginTop: -50,
    borderRadius: 20,
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  noProduct: {
    width: dimensions.Width(90),
    height: 'auto',
    padding: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginBottom: 50,
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 30,
  },

  loqueseaIMG: {
    width: 100,
    height: 120,
    alignItems: 'center',
  },

  btn: {
    height: 40,
    paddingHorizontal: 10,
    backgroundColor: colors.main,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    borderRadius: 5,
  },

  HeaderModal: {
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    height: 90,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});

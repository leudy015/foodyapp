import React, { useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  Linking,
  Alert,
  Platform,
  TextInput,
  Modal,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { stylesText } from '../theme/TextStyle';
import { dimensions } from '../theme/dimension';
import Icon from 'react-native-dynamic-vector-icons';
import { colors } from '../theme';
import Navigation from '../services/Navigration';
import { Button } from '../Components/Button';
import { NETWORK_INTERFACE_LINK } from '../Config/config';
import { validateEmail } from '../Utils/EmailsValidator';
import Toast from 'react-native-toast-message';
import SafariView from 'react-native-safari-view';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const Register = (props) => {
  const {
    setheight,
    modalVisible,
    setModalVisible,
    setdisMis,
    dataLogin,
  } = props;
  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [Loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const input = {
    name,
    lastName,
    email,
    password,
  };

  const onRegister = async () => {
    setLoading(true);
    if (!validateEmail(email)) {
      setIsError(true);
      setLoading(false);
      return null;
    } else {
      if (!name && !lastName && !email && !password) {
        Toast.show({
          text1: 'Upss falta algo',
          text2: 'Para registrarte debes rellenar todos los datos',
          position: 'top',
          type: 'info',
          topOffset: 50,
          visibilityTime: 4000,
        });
        setLoading(false);
        return null;
      } else {
        let res = await fetch(`${NETWORK_INTERFACE_LINK}`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(input),
        });
        if (res) {
          const user = await res.json();
          if (!user.success) {
            setLoading(false);
            Toast.show({
              text1: user.messages,
              text2: user.messages,
              position: 'top',
              type: 'error',
              topOffset: 50,
              visibilityTime: 4000,
            });
          } else {
            setLoading(false);
            const datas = {
              id: user.data._id,
              Register: true,
              email: email,
              password: password,
              dataLogin: dataLogin,
            };
            setModalVisible(false);
            setdisMis();
            Navigation.navigate('VerifyPhone', { data: datas });
          }
        } else {
          Toast.show({
            text1: 'Ups hubo un error',
            text2: 'Hubo un problema interno con el sistema',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 4000,
          });
          setLoading(false);
        }
      }
      setIsError(false);
    }
  };

  const supportedURL = 'https://wilbby.com/condiciones-de-uso';

  const OpenURLButton = () => {
    if (Platform.OS === 'ios') {
      SafariView.show({
        url: supportedURL,
      });
    } else {
      Navigation.navigate('Web', {
        data: supportedURL,
      });
    }
  };
  const styles = useDynamicValue(dynamicStyles);

  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        visible={modalVisible}
        presentationStyle="formSheet"
        statusBarTranslucent={true}
        onRequestClose={() => setModalVisible(false)}>
        <View style={styles.centeredView}>
          <View style={styles.HeaderModal}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
                setdisMis();
              }}
              style={{ marginLeft: 'auto' }}>
              <Icon
                name="closecircleo"
                type="AntDesign"
                size={30}
                color={colors.rgb_153}
              />
            </TouchableOpacity>
          </View>
          <KeyboardAwareScrollView
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.h1,
                {
                  width: dimensions.Width(70),
                  marginLeft: dimensions.Height(3),
                },
              ]}>
              Crea una cuenta en Wilbby
            </CustomText>
            <View style={styles.formView}>
              <TextInput
                style={styles.textInput}
                onFocus={setheight}
                selectionColor={colors.main}
                placeholderTextColor={colors.rgb_153}
                placeholder="Nombre"
                autoCompleteType="name"
                autoCorrect={false}
                onChangeText={(clave: any) => setName(clave)}
              />
              <TextInput
                style={styles.textInput}
                onFocus={setheight}
                selectionColor={colors.main}
                placeholderTextColor={colors.rgb_153}
                placeholder="Apellidos"
                autoCompleteType="name"
                autoCorrect={false}
                onChangeText={(clave: any) => setLastName(clave)}
              />
              <TextInput
                style={styles.textInput}
                onFocus={setheight}
                autoCapitalize="none"
                placeholderTextColor={colors.rgb_153}
                selectionColor={colors.main}
                placeholder="Correo electrónico"
                autoCompleteType="email"
                autoCorrect={false}
                onChangeText={(clave: any) => setEmail(clave)}
              />
              {isError ? (
                <CustomText
                  style={{ marginTop: 3 }}
                  light={colors.ERROR}
                  dark={colors.ERROR}>
                  ¡Por favor introduce un email válido!
                </CustomText>
              ) : null}
              <TextInput
                onFocus={setheight}
                autoCompleteType="password"
                placeholder="Contraseña"
                autoCapitalize="none"
                secureTextEntry={true}
                placeholderTextColor={colors.rgb_153}
                autoCorrect={false}
                selectionColor={colors.main}
                style={styles.textInput}
                onChangeText={(clave: any) => setPassword(clave)}
              />
              <View>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.secondaryText,
                    { marginTop: dimensions.Height(2) },
                  ]}>
                  Al regístrarme he leído y estoy de acuerdo con los
                </CustomText>
                <TouchableOpacity onPress={() => OpenURLButton()}>
                  <CustomText
                    light={colors.main}
                    dark={colors.main}
                    style={stylesText.secondaryText}>
                    Téminos y Condiciones
                  </CustomText>
                </TouchableOpacity>
              </View>
              <View style={styles.signupButtonContainer}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  loading={Loading}
                  containerStyle={styles.buttonView}
                  onPress={() => onRegister()}
                  title="Crear cuenta"
                  titleStyle={styles.buttonTitle}
                />
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </Modal>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    paddingHorizontal: dimensions.Width(4),
  },

  centeredView: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    flex: 1,
  },

  HeaderModal: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    height: 90,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  textInput: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    padding: Platform.select({
      android: 10,
      ios: dimensions.IsIphoneX() ? 20 : 10,
    }),
    marginTop: 20,
    borderRadius: 5,
    color: colors.rgb_153,
    borderColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    borderWidth: 0.3,
  },

  formView: {
    marginTop: dimensions.Height(2),
    marginHorizontal: dimensions.Width(6),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(6),
    alignSelf: 'center',
    width: dimensions.Width(90),
    paddingBottom: dimensions.IsIphoneX() ? 20 : 0,
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(16),
    textAlign: 'center',
  },
});

export default Register;

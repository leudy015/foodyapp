import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../../services/Navigration';

export default function Description(props) {
  const { res, dist, adress, maxLine, up, localeCode, currecy } = props;
  const styles = useDynamicValue(dynamicStyles);

  const naviData = {
    data: res,
    km: dist,
    time: '5 min',
    datos: adress,
    localeCode: localeCode,
    currecy: currecy,
  };
  return (
    <View style={[styles.items, { marginTop: 10 }]}>
      <View style={{ flexDirection: 'row' }}>
        <CustomText
          numberOfLines={maxLine}
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            {
              marginTop: 15,
              paddingBottom: dimensions.Height(1.5),
              width: dimensions.Width(85),
            },
          ]}>
          {res.description}
        </CustomText>
        <TouchableOpacity
          style={{ alignItems: 'center', alignSelf: 'center' }}
          onPress={up}>
          <Icon
            name={maxLine === 2 ? 'down' : 'up'}
            type="AntDesign"
            size={20}
            color={colors.rgb_153}
            style={{
              marginRight: 15,
              marginLeft: 10,
            }}
          />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={styles.infoStore}
        onPress={() => Navigation.navigate('Restaurant', { data: naviData })}>
        <Icon
          name="infocirlceo"
          type="AntDesign"
          size={18}
          color={colors.main}
          style={{
            marginLeft: 0,
            marginRight: 10,
          }}
        />
        <CustomText
          light={colors.main}
          dark={colors.main}
          style={[stylesText.secondaryTextBold]}>
          Más info del establecimiento{' '}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    paddingLeft: dimensions.Width(4),
  },

  infoStore: {
    backgroundColor: 'rgba(197, 248, 116, 0.3764705882352941)',
    padding: 15,
    width: dimensions.Width(60),
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    marginTop: 10,
  },
});

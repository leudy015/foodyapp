import React, { useState } from 'react';
import { View, ScrollView, RefreshControl } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { Query } from 'react-apollo';
import { query } from '../GraphQL';
import StoreCard from '../Components/RestaurantCard/RestaurantCardFaforito';
import { dimensions, colors, stylesText } from '../theme';
import { CustomText } from '../Components/CustomTetx';
import LoadingAnimated from '../Components/LoadingAnimated';

const Favourites = (props) => {
  const styles = useDynamicValue(dynamicStyles);
  const { city, id, lat, lgn, localeCode, currecy, riders } = props;
  const [refreshing, setRefreshing] = useState(false);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  return (
    <View style={styles.container}>
      <ScrollView
        style={{ marginTop: dimensions.Height(1) }}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }>
        <View
          style={{
            marginHorizontal: dimensions.Width(4),
            marginTop: dimensions.Height(1),
            marginBottom: dimensions.Height(2),
            width: dimensions.Width(70),
          }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={stylesText.titleText}>
            Tus tiendas y restaurantes favoritos
          </CustomText>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_235}
            style={[
              stylesText.secondaryText,
              { marginTop: 10, paddingBottom: 5, lineHeight: 17 },
            ]}>
            Este es el lugar donde te guardamos tus restaurantes y tiendas
            favoritas para que este más cerca de ti
          </CustomText>
        </View>
        <Query query={query.RESTAURANT_FAVORITO} variables={{ id: id }}>
          {(response) => {
            if (response.loading) {
              return (
                <View style={{ marginTop: dimensions.Height(10) }}>
                  <LoadingAnimated name="Buscando tus establecimientos favoritos" />
                </View>
              );
            }
            if (response) {
              const data =
                response && response.data && response.data.getRestaurantFavorito
                  ? response.data.getRestaurantFavorito.list
                  : [];
              response.refetch();
              return (
                <View>
                  <StoreCard
                    data={data}
                    city={city}
                    lat={lat}
                    lgn={lgn}
                    refetch={response.refetch}
                    localeCode={localeCode}
                    currecy={currecy}
                    riders={riders}
                  />
                </View>
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    flex: 1,
  },
});

export default Favourites;

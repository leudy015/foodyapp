import React from 'react';
import { View, Button, TextInput, Alert } from 'react-native';
import Modal from 'react-native-modal';
import { dimensions } from '../../theme';
import { colors, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';

export default function ExtraInfo(props: any) {
  const styles = useDynamicValue(dynamicStyles);
  const { isModalVisible, setModalVisible, setpuertaPiso, puertaPiso } = props;

  const toggleModal = () => {
    if (puertaPiso) {
      setModalVisible(!isModalVisible);
    } else {
      Alert.alert(
        'Falta información',
        'Debes añadir un número de piso, puerta o escalera, para continuar',
      );
    }
  };
  return (
    <Modal isVisible={isModalVisible}>
      <View style={styles.modalContent}>
        <CustomText
          ligth={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.secondaryTextBold,
            { textAlign: 'center', color: colors.rgb_153, lineHeight: 20 },
          ]}>
          Añadir puerta, piso etc ...
        </CustomText>

        <TextInput
          placeholder="Puerta, piso, etc..."
          autoCompleteType="name"
          placeholderTextColor={colors.rgb_153}
          style={[
            styles.inputAdress,
            {
              marginTop: 20,
              borderWidth: 0.5,
              borderColor: colors.rgb_235,
              padding: 15,
              width: dimensions.Width(80),
              borderRadius: 10,
            },
          ]}
          onChangeText={(clave: any) => setpuertaPiso(clave)}
        />

        <View style={{ marginTop: 40 }}>
          <Button
            title="Añadir información"
            onPress={toggleModal}
            color={colors.main}
          />
        </View>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modalContent: {
    width: dimensions.Width(90),
    height: dimensions.Height(30),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
  },

  inputAdress: {
    height: 'auto',
    fontSize: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    color: new DynamicValue(colors.black, colors.white),
  },
});

import React from 'react';
import { View, FlatList, TouchableOpacity, Alert } from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { CustomText } from '../../../Components/CustomTetx';
import { colors } from '../../../theme';
import Category from './MainCategory';
import { query } from '../../../GraphQL';
import { Query } from 'react-apollo';
import { stylesText } from '../../../theme/TextStyle';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Navigation from '../../../services/Navigration';
import ModaExpress from './ModalExpress';
import CategoryLoading from '../../../Components/PlaceHolder/Category';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function CategoryScreen(props: any) {
  const {
    city,
    lat,
    lgn,
    localeCode,
    currecy,
    modalizeRef,
    id,
    countryCode,
    setCategory,
    onPenAllCategory,
    setModalVisible,
    modalVisible,
    settitleCat,
    titleCat,
    setNotification,
    getUser,
    riders,
  } = props;

  const styles = useDynamicValue(dynamicStyles);

  const onOpen = () => {
    getUser();
    setNotification();
    modalizeRef.current?.open();
    ReactNativeHapticFeedback.trigger('selection', optiones);
  };

  const openModal = (items: any) => {
    getUser();
    setNotification();
    settitleCat(items.title);
    setModalVisible(true);
  };

  const noRider = () => {
    Alert.alert(
      'No hay repartidores disponible',
      'En este momento no tenemos repartidores disponible te avisaremos cuento vuelvan a estarlo',
    );
  };

  const itemsRender = ({ item }) => {
    const actionCategory = () => {
      setCategory(item);
      switch (item.title) {
        case 'Algo Especial':
          if (id) {
            if (riders) {
              openModal(item);
            } else {
              noRider();
            }
          } else {
            Navigation.navigate('Login', 0);
          }
          break;
        case 'Envío Express':
          if (id) {
            if (riders) {
              openModal(item);
            } else {
              noRider();
            }
          } else {
            Navigation.navigate('Login', 0);
          }
          break;
        default:
          onOpen();
          break;
      }
    };

    return <Category data={item} onPress={() => actionCategory()} />;
  };
  return (
    <View style={{ paddingHorizontal: 5 }}>
      <View style={styles.titles}>
        <CustomText
          light={colors.black}
          dark={colors.rgb_153}
          style={stylesText.secondaryTextBold}>
          Secciones
        </CustomText>
        <TouchableOpacity onPress={() => onPenAllCategory()}>
          <CustomText
            style={stylesText.secondaryTextBold}
            light={colors.main}
            dark={colors.main}>
            Ver todas
          </CustomText>
        </TouchableOpacity>
      </View>
      <Query query={query.CATEGORY}>
        {(response) => {
          if (response) {
            response.refetch();
            const data =
              response && response.data && response.data.getCategory
                ? response.data.getCategory.data
                : [];
            return (
              <FlatList
                data={data}
                renderItem={(item: any) => itemsRender(item)}
                keyExtractor={(item: any) => item._id}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              />
            );
          }
        }}
      </Query>
      <ModaExpress
        city={city}
        titleCat={titleCat}
        lat={lat}
        lgn={lgn}
        id={id}
        localeCode={localeCode}
        currecy={currecy}
        countryCode={countryCode}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  titles: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 15,
    marginVertical: 20,
  },
});

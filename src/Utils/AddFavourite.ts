import { Alert } from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Toast from 'react-native-simple-toast';
import Navigation from '../services/Navigration';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export const AddStoreToFavorite = (
  userID: string,
  StoreID: string,
  refetch: any,
  crearFavorito: any,
) => {
  if (userID) {
    crearFavorito({
      variables: { restaurantID: StoreID, usuarioId: userID },
    }).then(async (res: any) => {
      if (res.data.crearFavorito.success) {
        ReactNativeHapticFeedback.trigger('notificationSuccess', options);
        Toast.showWithGravity(
          'Tienda añadida a favoritos',
          Toast.LONG,
          Toast.TOP,
        );
        if (refetch) {
          refetch();
        }
      } else {
        Toast.showWithGravity(
          'Algo no va bien intentalo de nuevo',
          Toast.LONG,
          Toast.TOP,
        );
      }
    });
  } else {
    Alert.alert(
      'Debes iniciar sesión',
      'Para añadir este articulo a la cesta debes iniciar sesión',
      [
        {
          text: 'Iniciar sesión',
          onPress: () => Navigation.navigate('Login', 0),
        },

        {
          text: 'Regístrarme',
          onPress: () => Navigation.navigate('Login', 0),
        },
      ],

      { cancelable: false },
    );
  }
};

export const DeleteStoreToFavorite = (
  StoreID: string,
  refetch: any,
  eliminarFavorito: any,
) => {
  eliminarFavorito({ variables: { id: StoreID } }).then(async (res: any) => {
    if (res.data.eliminarFavorito.success) {
      ReactNativeHapticFeedback.trigger('notificationError', options);
      Toast.showWithGravity(
        'Tienda eliminada de favoritos',
        Toast.LONG,
        Toast.TOP,
      );
      if (refetch) {
        refetch();
      }
    } else {
      Toast.showWithGravity(
        'Algo no va bien intentalo de nuevo',
        Toast.LONG,
        Toast.TOP,
      );
    }
  });
};

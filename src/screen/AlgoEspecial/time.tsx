import React from 'react';
import { View, Platform } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, dimensions, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import moment from 'moment';
import CheckBox from '@react-native-community/checkbox';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import DatePicker from 'react-native-date-picker';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const opciones = [
  {
    id: 1,
    title: 'Lo antes posible',
    icon: 'enviromento',
    type: 'AntDesign',
  },
  {
    id: 2,
    title: 'Programar entrega',
    icon: 'clockcircleo',
    type: 'AntDesign',
  },
];

export const TimeSelect = (prosp) => {
  const { date, selecte, setDate, setselecte } = prosp;
  const styles = useDynamicValue(dynamicStyles);

  const borderColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const border = borderColors[mode];

  const colorsText = {
    light: colors.black,
    dark: colors.white,
  };
  const backgroundColor = colorsText[mode];

  const d = new Date();
  const day = d.getDate();
  const year = d.getFullYear();
  const month = d.getMonth() + 1;
  const month2 = d.getMonth() + 2;

  const o = month > 9 ? '' : '0';
  const o2 = month2 > 9 ? '' : '0';

  const jsCoreDateCreator = (dateString: string) => {
    let dateParam = dateString.split(/[\s-:]/);
    dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString();
    //@ts-ignore
    return new Date(...dateParam);
  };

  const onSeleAhora = (items: any) => {
    ReactNativeHapticFeedback.trigger('selection', options);
    if (items === 'Programar entrega') {
      setselecte(items);
    } else {
      setselecte(items);
      setDate(null);
    }
  };

  return (
    <View style={[styles.cards, { borderColor: border }]}>
      <View
        style={[
          styles.textInputContent,
          { alignItems: 'flex-start', flexDirection: 'column' },
        ]}>
        <View style={{ marginVertical: 20, marginHorizontal: 10 }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryText, { fontSize: 20 }]}>
            {date ? `${moment(date).format('lll')}` : 'Lo antes posible'}
          </CustomText>
        </View>
        <View
          style={{
            marginTop: 10,
          }}>
          {opciones.map((d, e) => (
            <View key={e} style={styles.items}>
              <View style={{ flexDirection: 'row', paddingVertical: 15 }}>
                <Icon
                  name={d.icon}
                  type="AntDesign"
                  size={22}
                  color={colors.rgb_153}
                  style={{ marginRight: 15 }}
                />
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={stylesText.titleText200}>
                  {d.title}
                </CustomText>
              </View>
              <CheckBox
                key={e}
                value={d.title === selecte ? true : false}
                disabled={false}
                onValueChange={() => onSeleAhora(d.title)}
                onTintColor={colors.main}
                tintColors={{ true: colors.main, false: colors.rgb_153 }}
                onCheckColor={colors.main}
                onAnimationType="fill"
                offAnimationType="fill"
              />
            </View>
          ))}
          {selecte === 'Programar entrega' ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <DatePicker
                date={date ? date : new Date()}
                onDateChange={setDate}
                mode="datetime"
                androidVariant={
                  Platform.OS === 'android' ? 'nativeAndroid' : 'iosClone'
                }
                minimumDate={jsCoreDateCreator(`${year}-${o + month}-${day}`)}
                maximumDate={jsCoreDateCreator(`${year}-${o2 + month2}-${day}`)}
                locale="es"
                textColor={backgroundColor}
              />
            </View>
          ) : null}
        </View>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  textInputContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
  },

  cards: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: 30,
    padding: 10,
    borderRadius: 20,
    marginHorizontal: 15,
    borderWidth: 1,
  },

  items: {
    width: dimensions.Width(82),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

import React from 'react';
import { Modal, View, Alert, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import LottieView from 'lottie-react-native';
import { colors, stylesText, dimensions } from '../../theme';
import { Button } from '../Button';
import { CustomText } from '../CustomTetx';
import { openSettings } from 'react-native-permissions';
import RNLocation from 'react-native-location';
import { SetCity } from '../../Utils/getCiti';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-community/async-storage';

export default function Location(props) {
  const { visibleModal, setModalVisible, setisLocation, setcity } = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const currentPosition = () => {
    Geolocation.getCurrentPosition((info) => {
      SetCity(info.coords.latitude, info.coords.longitude, setcity);
    });
  };

  const alertOpen = () => {
    Alert.alert(
      'Activa tu ubicación',
      'Para mostrarte restaurantes y tiendas cerca de ti necesitamos que active tu ubicación, no la utilzamos para rastrearte.',
      [
        {
          text: 'Ir a los ajustes',
          onPress: () => openSettings(),
        },
        {
          text: 'Cancelar',
          onPress: () => {},
        },
      ],
      { cancelable: false },
    );
  };

  const getPermision = () => {
    RNLocation.requestPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
        rationale: {
          title: 'Wilbby necesita usar tu ubicación',
          message:
            'Necesitamos tu ubicación para mostrarte tiendas cerca de ti',
          buttonPositive: 'OK',
          buttonNegative: 'Cancelar',
        },
      },
    }).then((res) => {
      if (Number(res) === 0) {
        alertOpen();
      } else {
        setModalVisible(false);
        currentPosition();
        if (setisLocation) {
          setisLocation(true);
        }
      }
    });
  };

  const getStatus = async () => {
    RNLocation.checkPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
      },
    }).then((per) => {
      if (!per) {
        getPermision();
      } else {
        setModalVisible(false);
      }
    });
  };

  const noActivate = async () => {
    await AsyncStorage.setItem('location', 'no_activate');
    setModalVisible(false);
  };

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.container}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: dimensions.ScreenHeight,
          }}>
          <LottieView
            source={require('../../Assets/Animate/location.json')}
            autoPlay
            loop
            style={{ width: 230 }}
          />
          <View style={{ marginHorizontal: 30, marginTop: 30 }}>
            <CustomText
              ligth={colors.black}
              dark={colors.white}
              style={[stylesText.titleText, { textAlign: 'center' }]}>
              Activa tu ubicación
            </CustomText>
            <CustomText
              ligth={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                { textAlign: 'center', marginTop: 20 },
              ]}>
              Para mostrarte restaurantes y tiendas cerca de ti necesitamos que
              active tu ubicación, no la utilzamos para rastrearte.
            </CustomText>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={[
                  styles.buttonView,
                  { backgroundColor: colors.main },
                ]}
                onPress={() => getStatus()}
                title="Continuar"
                titleStyle={styles.buttonTitle}
              />
            </View>
            <View style={{ paddingTop: 20 }}>
              <TouchableOpacity onPress={() => noActivate()}>
                <CustomText
                  ligth={colors.main}
                  dark={colors.main}
                  style={[
                    stylesText.secondaryTextBold,
                    { textAlign: 'center', color: colors.main },
                  ]}>
                  Cancelar
                </CustomText>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

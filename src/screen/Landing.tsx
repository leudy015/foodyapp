import React from 'react';
import { View, ScrollView, Image } from 'react-native';
import Navigation from '../services/Navigration';
import { dimensions, colors, image } from '../theme';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { Button } from '../Components/Button';
import { stylesText } from '../theme/TextStyle';
import LottieView from 'lottie-react-native';

function Landing() {
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ marginTop: dimensions.Height(15) }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <LottieView
              source={require('../Assets/Animate/chica.json')}
              autoPlay
              loop
              style={{ width: 300 }}
            />
          </View>
        </View>

        <View style={{ alignSelf: 'center' }}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.text}>
            Bienvenid@ a Wilbby
          </CustomText>
        </View>

        <View
          style={{
            alignSelf: 'center',
            marginTop: 10,
            paddingHorizontal: dimensions.Width(4),
          }}>
          <CustomText
            light={colors.back_dark}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText, { textAlign: 'center' }]}>
            Todas las tiendas, restaurantes, supermercados y farmacias de tu
            barrio en una app y a domicilio.
          </CustomText>

          <CustomText
            light={colors.main}
            dark={colors.main}
            style={[
              stylesText.secondaryText,
              { textAlign: 'center', marginTop: 15 },
            ]}>
            #ConWilbbyLoTienesTodo
          </CustomText>
        </View>

        <View
          style={{
            marginTop: dimensions.Height(5),
            marginBottom: dimensions.Height(8),
          }}>
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => Navigation.navigate('Inicio', 0)}
              title="Explorar"
              titleStyle={styles.buttonTitle}
            />
          </View>

          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => Navigation.navigate('Login', 0)}
              title="Iniciar sesión"
              titleStyle={styles.buttonTitle}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  text: {
    fontSize: dimensions.FontSize(30),
    fontWeight: 'bold',
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(100),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(20),
  },
});

export default Landing;

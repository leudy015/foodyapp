import React from 'react';
import { View, TouchableOpacity, Alert, Platform, Linking } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import AsyncStorage from '@react-native-community/async-storage';
import Navigation from '../services/Navigration';
import { Avatar } from 'react-native-elements';
import { dimensions, colors } from '../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { Query } from 'react-apollo';
import { query } from '../GraphQL';
import { stylesText } from '../theme/TextStyle';
import moment from 'moment';
import InAppReview from 'react-native-in-app-review';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../Config/config';
import LoadingPRofile from '../Components/PlaceHolder/profile';
import { useTranslation } from 'react-i18next';
import { openSettings } from 'react-native-permissions';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const Profile = (props) => {
  const { t } = useTranslation();
  const styles = useDynamicValue(dynamicStyles);
  const {
    city,
    localeCode,
    currecy,
    lat,
    lgn,
    setcity,
    getRidersAvailable,
  } = props;

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const ope = () => {
    openSettings().catch(() => console.warn('cannot open settings'));
  };

  const review = () => {
    if (InAppReview.isAvailable()) {
      InAppReview.RequestInAppReview();
    } else {
      Alert.alert('Ya has valorado nuestra app, Gracias');
    }
  };
  const LogOut = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    Alert.alert(
      t('profile:logout'),
      t('profile:logoutmessage'),
      [
        {
          text: t('profile:cancel'),
          onPress: () => console.log('OK Pressed'),
          style: 'cancel',
        },
        {
          text: 'Ok',
          onPress: async () => {
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            await AsyncStorage.removeItem('id');
            await AsyncStorage.removeItem('token');
            await AsyncStorage.removeItem('user');
            await AsyncStorage.removeItem('adressName');
            await AsyncStorage.removeItem('adressId');
            Navigation.navigate('Login', 0);
          },
        },
        ,
      ],
      { cancelable: false },
    );
  };

  const confiAdres = {
    fromHome: false,
    getAdreesID: null,
    city: city,
    lat: lat,
    lgn: lgn,
    setcity: setcity,
    getRidersAvailable: getRidersAvailable,
  };

  return (
    <Query query={query.USER_DETAIL}>
      {(response: any) => {
        if (response.loading) {
          return <LoadingPRofile />;
        }
        if (response) {
          const user =
            response && response.data && response.data.getUsuario
              ? response.data.getUsuario.data
              : {};
          const cupData = {
            user: user,
            localeCode: localeCode,
            currecy: currecy,
            city: city,
          };
          return (
            <View style={styles.container}>
              <View>
                <View style={styles.infoContact}>
                  <View>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={stylesText.titleText}>
                      {t('profile:hello')}!
                    </CustomText>
                    <CustomText
                      numberOfLines={1}
                      light={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.h1,
                        {
                          marginBottom: dimensions.Height(0.5),
                          width: dimensions.Width(60),
                          textAlign: 'left',
                        },
                      ]}>
                      {user.name} {user.lastName}
                    </CustomText>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[stylesText.terciaryText, { marginTop: 5 }]}>
                      En Wilbby {t('profile:from')}{' '}
                      {moment(user.created_at).format('L')}
                    </CustomText>
                  </View>
                  <View style={{ marginLeft: 'auto' }}>
                    <Avatar
                      size={80}
                      rounded
                      source={{
                        uri: NETWORK_INTERFACE_LINK_AVATAR + user.avatar,
                      }}
                    />
                  </View>
                </View>
              </View>
              <View style={styles.contenedor}>
                <View style={{ marginTop: dimensions.Height(3) }}>
                  <TouchableOpacity
                    style={styles.item}
                    onPress={() =>
                      Navigation.navigate('UpdateProfile', {
                        data: {
                          city: city,
                          user: user,
                        },
                      })
                    }>
                    <Icon
                      name="user"
                      type="Feather"
                      size={20}
                      style={styles.icons}
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:profile')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        style={styles.icons}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.item}
                    onPress={() =>
                      Navigation.navigate('Direccion', { data: confiAdres })
                    }>
                    <Icon
                      name="map-pin"
                      type="Feather"
                      size={20}
                      style={styles.icons}
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:address')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        style={styles.icons}
                      />
                    </CustomText>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.item}
                    onPress={() =>
                      Navigation.navigate('Wallet', { data: user })
                    }>
                    <Icon
                      name="credit-card"
                      type="Feather"
                      size={20}
                      style={styles.icons}
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:payment')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        style={styles.icons}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <View style={styles.separator} />
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.titleText,
                      { marginLeft: 20, marginBottom: 30, marginTop: 30 },
                    ]}>
                    {t('profile:promotion')}
                  </CustomText>
                  <TouchableOpacity
                    style={styles.item}
                    onPress={() => Navigation.navigate('ComparteyGana', 0)}>
                    <Icon
                      name="gift"
                      type="Feather"
                      size={20}
                      style={styles.icons}
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:share')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        style={styles.icons}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.item}
                    onPress={() =>
                      Navigation.navigate('Cupones', { data: cupData })
                    }>
                    <Icon
                      name="ticket-percent-outline"
                      type="MaterialCommunityIcons"
                      size={20}
                      style={styles.icons}
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:promo')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        style={styles.icons}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.item}
                    onPress={() => Navigation.navigate('Ayuda', 0)}>
                    <Icon
                      name="customerservice"
                      type="AntDesign"
                      size={18}
                      style={styles.icons}
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:help')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        style={styles.icons}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.item} onPress={() => ope()}>
                    <Icon
                      name="setting"
                      type="AntDesign"
                      size={18}
                      style={styles.icons}
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:setting')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        style={styles.icons}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  {Platform.OS === 'ios' ? (
                    <>
                      <TouchableOpacity
                        style={styles.item}
                        onPress={() => Navigation.navigate('Apariencia', 0)}>
                        <Icon
                          name="appstore-o"
                          type="AntDesign"
                          size={20}
                          style={styles.icons}
                        />
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={[stylesText.mainText, styles.text]}>
                          {t('profile:aparence')}
                        </CustomText>
                        <CustomText
                          style={{
                            marginLeft: 'auto',
                            marginRight: 15,
                          }}>
                          <Icon
                            name="right"
                            type="AntDesign"
                            size={20}
                            style={styles.icons}
                          />
                        </CustomText>
                      </TouchableOpacity>
                    </>
                  ) : null}
                  <TouchableOpacity
                    style={styles.item}
                    onPress={() => review()}>
                    <Icon
                      name="staro"
                      type="AntDesign"
                      size={20}
                      style={styles.icons}
                    />
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:rating')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        style={styles.icons}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.item}
                    onPress={() => LogOut()}>
                    <CustomText
                      light={colors.ERROR}
                      dark={colors.ERROR}
                      style={[stylesText.mainText, styles.text]}>
                      {t('profile:logout')}
                    </CustomText>
                    <CustomText
                      style={{
                        marginLeft: 'auto',
                        marginRight: 15,
                      }}>
                      <Icon
                        name="logout"
                        type="AntDesign"
                        size={20}
                        color={colors.ERROR}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={{
                      textAlign: 'center',
                      marginTop: 30,
                    }}>
                    Sigue a Wilbby
                  </CustomText>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      paddingBottom: dimensions.Height(20),
                      marginTop: 30,
                      width: dimensions.Width(100),
                    }}>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginHorizontal: 15,
                      }}>
                      <TouchableOpacity
                        style={[styles.btnSocial]}
                        onPress={() =>
                          OpenURLButton('https://www.instagram.com/wilbby_es')
                        }>
                        <Icon
                          name="instagram"
                          type="Feather"
                          size={24}
                          color={colors.rgb_153}
                        />
                      </TouchableOpacity>
                      <CustomText
                        light={colors.black}
                        dark={colors.white}
                        style={[stylesText.secondaryText, { marginTop: 5 }]}>
                        Instagram
                      </CustomText>
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginHorizontal: 15,
                      }}>
                      <TouchableOpacity
                        style={styles.btnSocial}
                        onPress={() =>
                          OpenURLButton(
                            'https://www.facebook.com/wilbbyapp.es/',
                          )
                        }>
                        <Icon
                          name="facebook"
                          type="FontAwesome"
                          size={24}
                          color={colors.rgb_153}
                        />
                      </TouchableOpacity>
                      <CustomText
                        light={colors.black}
                        dark={colors.white}
                        style={[stylesText.secondaryText, { marginTop: 5 }]}>
                        Facebook
                      </CustomText>
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginHorizontal: 15,
                      }}>
                      <TouchableOpacity
                        style={styles.btnSocial}
                        onPress={() =>
                          OpenURLButton('https://twitter.com/wilbby_es')
                        }>
                        <Icon
                          name="twitter"
                          type="AntDesign"
                          size={24}
                          color={colors.rgb_153}
                        />
                      </TouchableOpacity>
                      <CustomText
                        light={colors.black}
                        dark={colors.white}
                        style={[stylesText.secondaryText, { marginTop: 5 }]}>
                        Twitter
                      </CustomText>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          );
        }
      }}
    </Query>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contenedor: {
    flex: 1,
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  hert: {
    color: colors.main,
    marginTop: dimensions.Height(8),
    marginLeft: dimensions.Width(1),
  },

  infoContact: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: dimensions.Height(5),
    width: dimensions.Width(90),
  },

  item: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: dimensions.Width(4),
  },

  icons: {
    color: new DynamicValue(colors.black, colors.white),
    marginRight: 10,
    paddingBottom: 5,
  },

  separator: {
    width: dimensions.Width(96),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'center',
    alignItems: 'center',
  },

  text: {
    paddingBottom: 5,
  },

  btnSocial: {
    width: 40,
    height: 40,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
  },
});

export default Profile;

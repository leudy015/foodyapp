import React from 'react';
import {SafeAreaView, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dynamic';
import {colors, dimensions} from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <View style={{alignSelf: 'center', backgroundColor: back}}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            height: 200,
          }}>
          <View
            style={{
              marginTop: 70,
              marginLeft: 15,
              flexDirection: 'row',
              width: dimensions.Width(100),
            }}>
            <View style={{flexDirection: 'row', width: dimensions.Width(80)}}>
              <View style={{width: 50, height: 50, borderRadius: 50}} />
              <View>
                <View
                  style={{
                    width: 150,
                    height: 10,
                    borderRadius: 50,
                    marginLeft: 10,
                    marginTop: 3,
                  }}
                />
                <View
                  style={{
                    width: 150,
                    height: 20,
                    borderRadius: 50,
                    marginTop: 10,
                    marginLeft: 10,
                  }}
                />
              </View>
            </View>
            <View
              style={{
                width: 40,
                height: 40,
                borderRadius: 50,
                marginLeft: 'auto',
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(96),
              height: 70,
              borderRadius: 7,
              marginTop: 20,
              marginLeft: 10,
            }}
          />
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

import React from 'react';
import { Dimensions, View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Pdf from 'react-native-pdf';
import Header from '../Components/Header';
import { useNavigationParam } from 'react-navigation-hooks';
import { colors } from '../theme';
export default function PdfView() {
  const url = useNavigationParam('data');
  const source = { uri: url, cache: true };
  const styles = useDynamicValue(dynamicStyles);

  return (
    <View style={styles.container}>
      <Header title="Infromación sobre alérgenos" />
      <Pdf
        source={source}
        onLoadComplete={(numberOfPages, filePath) => {
          console.log(`number of pages: ${numberOfPages}`);
        }}
        onPageChanged={(page, numberOfPages) => {
          console.log(`current page: ${page}`);
        }}
        onError={(error) => {
          console.log(error);
        }}
        onPressLink={(uri) => {
          console.log(`Link presse: ${uri}`);
        }}
        style={styles.pdf}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    flex: 1,
  },

  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    marginTop: 0,
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
  },
});

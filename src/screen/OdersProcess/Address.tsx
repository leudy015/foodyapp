import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions } from '../../theme';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../../services/Navigration';

export default function Address(props) {
  const styles = useDynamicValue(dynamicStyles);
  const { formatted_address, confiAdres, adressID } = props;
  return (
    <View style={[styles.items, { marginTop: 20, flexDirection: 'row' }]}>
      <View
        style={{
          width: dimensions.Width(70),
          paddingTop: 15,
          marginBottom: 15,
        }}>
        <CustomText
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold, { marginBottom: 5 }]}>
          Dirección de entrega
        </CustomText>
        {!formatted_address && !adressID ? (
          <CustomText
            numberOfLines={3}
            light={colors.ERROR}
            dark={colors.ERROR}
            style={stylesText.secondaryText}>
            Añadir dirección de entrega.
          </CustomText>
        ) : (
          <>
            <CustomText
              numberOfLines={3}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                [
                  stylesText.secondaryText,
                  { paddingBottom: 5, lineHeight: 18 },
                ],
              ]}>
              {formatted_address}
            </CustomText>
          </>
        )}

        {adressID ? (
          <CustomText
            numberOfLines={3}
            light={colors.main}
            dark={colors.main}
            style={stylesText.secondaryText}>
            Dirección completa
          </CustomText>
        ) : (
          <CustomText
            numberOfLines={3}
            light={colors.ERROR}
            dark={colors.ERROR}
            style={stylesText.secondaryText}>
            Falta información
          </CustomText>
        )}
      </View>
      <TouchableOpacity
        onPress={() => Navigation.navigate('Direccion', { data: confiAdres })}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          marginLeft: 'auto',
          marginRight: 15,
        }}>
        <Icon
          name="pluscircle"
          type="AntDesign"
          size={30}
          color={colors.main}
        />
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },
});

import React, { useState } from 'react';
import {
  View,
  Animated,
  TouchableOpacity,
  TextInput,
  Platform,
  Linking,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../../../services/Navigration';
import Mailer from 'react-native-mail';

export default function Adress(props: any) {
  const {
    top,
    city,
    lat,
    lgn,
    localeCode,
    currecy,
    isLocation,
    setNotification,
    riders,
    getRidersAvailable,
  } = props;
  const [onsearch, setonsearch] = useState('');
  const styles = useDynamicValue(dynamicStyles);

  const nav = {
    city: city,
    lat: lat,
    lgn: lgn,
    localeCode: localeCode,
    currecy: currecy,
    isLocation: isLocation,
    onsearch: onsearch,
    riders: riders,
  };

  return (
    <Animated.View style={[styles.searchCont, { marginTop: top }]}>
      <View style={[styles.search]}>
        <View style={styles.input}>
          <TextInput
            style={styles.textInput}
            placeholder="Busca en Wilbby"
            placeholderTextColor={colors.rgb_153}
            selectionColor={colors.main}
            returnKeyType="search"
            clearButtonMode="while-editing"
            onChangeText={(value) => setonsearch(value)}
            onSubmitEditing={() => {
              Navigation.navigate('Search', { data: nav });
              setNotification();
              getRidersAvailable(city);
            }}
          />
          <TouchableOpacity
            style={styles.btns_search}
            onPress={() => {
              Navigation.navigate('Search', { data: nav });
              setNotification();
              getRidersAvailable(city);
            }}>
            <Icon type="Feather" name="search" size={22} color={colors.white} />
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity
        style={styles.btns}
        onPress={() => {
          Navigation.navigate('ComparteyGana', 0);
          getRidersAvailable(city);
        }}>
        <Icon type="Feather" name="gift" size={24} color={colors.main} />
      </TouchableOpacity>
    </Animated.View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  search: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  searchCont: {
    width: dimensions.Width(92),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
  },

  btns: {
    width: 45,
    height: 45,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 10.27,
    elevation: 15,
  },

  input: {
    height: 60,
    width: dimensions.Width(75),
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },

  textInput: {
    height: 50,
    width: dimensions.IsIphoneX() ? dimensions.Width(62) : dimensions.Width(60),
    borderRadius: 15,
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    paddingLeft: 15,
    color: new DynamicValue(colors.rgb_153, colors.white),
  },

  btns_search: {
    backgroundColor: colors.main,
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
  },
});

export const validateDates = (
  OPEN_HOUR: number,
  OPEN_MINUTE: number,
  CLOSE_HOUR: number,
  CLOSE_MINUTE: number,
) => {
  const now = new Date();
  const time1 = new Date(now);
  const time2 = new Date(now);
  // validamos que la fecha de ingreso sea menor que la de salida
  const open = new Date(now);
  open.setHours(OPEN_HOUR);
  open.setMinutes(OPEN_MINUTE);
  const close = new Date(now);
  close.setHours(CLOSE_HOUR);
  close.setMinutes(CLOSE_MINUTE);
  if (time1 >= open && time2 <= close) {
    return true;
  }
  return false;
};

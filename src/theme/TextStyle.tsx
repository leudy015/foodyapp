import { dimensions } from './dimension';
import { Platform } from 'react-native';
const fontFamily = Platform.select({
  ios: 'Helvetica',
  android: 'sans-serif',
});

const mainText = {
  fontSize: dimensions.FontSize(18),
  fontWeight: '400',
  fontFamily: fontFamily,
};

const titleText2 = {
  fontSize: dimensions.FontSize(18),
  fontWeight: 'bold',
  fontFamily: fontFamily,
};

const titleText = {
  fontSize: dimensions.FontSize(22),
  fontWeight: 'bold',
  fontFamily: fontFamily,
};

const titleText200 = {
  fontSize: dimensions.FontSize(18),
  fontWeight: '100',
  fontFamily: fontFamily,
};

const titleTextNoBold = {
  fontSize: dimensions.FontSize(22),
  fontWeight: 'bold',
  fontFamily: fontFamily,
};

const secondaryText = {
  fontSize: dimensions.FontSize(14),
  fontWeight: '300',
  fontFamily: fontFamily,
};

const secondaryTextBold = {
  fontSize: dimensions.FontSize(18),
  fontWeight: 'bold',
  fontFamily: fontFamily,
};

const miniTitle = {
  fontSize: dimensions.FontSize(16),
  fontWeight: '300',
  fontFamily: fontFamily,
};

const terciaryText = {
  fontSize: dimensions.FontSize(12),
  fontWeight: '200',
  fontFamily: fontFamily,
};

const placeholderText = {
  fontSize: dimensions.FontSize(12),
  fontWeight: '100',
  fontFamily: fontFamily,
};

const h1 = {
  fontSize: dimensions.IsIphoneX()
    ? dimensions.FontSize(30)
    : dimensions.FontSize(22),
  fontWeight: 'bold',
  fontFamily: fontFamily,
};

export const stylesText = {
  mainText,
  secondaryText,
  terciaryText,
  placeholderText,
  h1,
  titleText,
  titleTextNoBold,
  titleText200,
  secondaryTextBold,
  titleText2,
  miniTitle,
};

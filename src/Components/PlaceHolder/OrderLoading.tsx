import React from 'react';
import { View } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions } from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  return (
    <View
      style={{
        alignSelf: 'center',
        backgroundColor: 'transparent',
        marginBottom: dimensions.Height(10),
      }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            height: dimensions.ScreenHeight,
            alignSelf: 'center',
            alignItems: 'center',
            marginHorizontal: 20,
          }}>
          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 100,
              flexDirection: 'row',
              marginTop: 30,
            }}>
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 7,
                marginTop: 10,
              }}
            />
            <View style={{ alignSelf: 'center', marginLeft: 15 }}>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 150,
                  height: 15,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 10,
                  borderRadius: 7,
                  marginTop: 10,
                }}
              />
            </View>
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

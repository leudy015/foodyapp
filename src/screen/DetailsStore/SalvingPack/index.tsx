import React, { useState, useEffect } from 'react';
import { TouchableOpacity, Animated, Image } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors, image } from '../../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import Content from './ModalSalving';

export default function SalvingPack(props: any) {
  const [visibleModal, setVisibleModal] = useState(false);
  const [heigth] = useState(new Animated.Value(45));
  const [width] = useState(new Animated.Value(90));
  const [widthText] = useState(new Animated.Value(0));
  const { data, refetch, user, inCartItem, setinCartItem } = props;
  const styles = useDynamicValue(dynamicStyles);

  useEffect(() => {
    //@ts-ignore
    Animated.sequence([
      //@ts-ignore
      Animated.timing(heigth, {
        toValue: 45,
        duration: 200,
      }),
      //@ts-ignore
      Animated.timing(width, {
        toValue: 90,
        duration: 200,
      }),
      //@ts-ignore
      Animated.timing(width, {
        toValue: dimensions.Width(90),
        duration: 500,
      }),
      //@ts-ignore
      Animated.timing(widthText, {
        toValue: dimensions.Width(80),
        duration: 500,
      }),
    ]).start();
  }, []);
  return (
    <>
      <Animated.View
        style={[
          styles.container,
          {
            height: heigth,
            width: width,
            alignItems: 'center',
            flexDirection: 'row',
          },
        ]}>
        <TouchableOpacity
          onPress={() => setVisibleModal(true)}
          style={{
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Animated.Text
            style={[styles.text, { width: widthText }]}
            numberOfLines={1}>
            {data.title} tiene {data.salvingPack.quantity}{' '}
            {data.salvingPack.quantity > 1 ? 'packs' : 'pack'} para salvar
          </Animated.Text>
          <TouchableOpacity
            style={styles.info}
            onPress={() => setVisibleModal(true)}>
            <Image
              source={image.SaveIcon}
              style={{ width: 40, height: 40, resizeMode: 'cover' }}
            />
          </TouchableOpacity>
        </TouchableOpacity>
      </Animated.View>
      <Content
        visibleModal={visibleModal}
        setModalVisible={setVisibleModal}
        datas={data}
        refetch={refetch}
        user={user}
        inCartItem={inCartItem}
        setinCartItem={setinCartItem}
      />
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.Width(90),
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 20,
    marginTop: 10,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
  },

  info: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 100,
    marginRight: 3,
  },

  text: {
    paddingHorizontal: 20,
    color: new DynamicValue(colors.back_dark, colors.white),
    fontSize: dimensions.FontSize(14),
    fontWeight: 'bold',
    fontFamily: 'Helvetica',
  },
});

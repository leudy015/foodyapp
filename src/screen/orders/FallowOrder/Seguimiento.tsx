import React, { useState } from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  Linking,
  Alert,
  Platform,
  Animated,
  RefreshControl,
  TouchableOpacity,
  SafeAreaView,
  ActionSheetIOS,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors, stylesText, image } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import { useNavigationParam } from 'react-navigation-hooks';
import { formaterPrice } from '../../../Utils/formaterPRice';
import InfoOrder from './info';
import Status from './status';
import Adress from './Adress';
import Products from './Product';
import Icon from 'react-native-dynamic-vector-icons';
import Mailer from 'react-native-mail';
import moment from 'moment';
import Navigation from '../../../services/Navigration';
const heigth = Dimensions.get('window').height;

const FallowOrder = () => {
  const [scrollY] = useState(new Animated.Value(0));
  const [refreshing, setRefreshing] = useState(false);
  const datas = useNavigationParam('data');
  const styles = useDynamicValue(dynamicStyles);

  const { item, refetch, localeCode, currecy } = datas;

  refetch();

  const barcolor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const mode = useColorSchemeContext();
  const barColor = barcolor[mode];

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const alto = heigth > 720 ? 110 : 90;

  const headerContainerWidth = scrollY.interpolate({
    inputRange: [0, 125],
    outputRange: [dimensions.Width(96), dimensions.Width(100)],
    extrapolate: 'clamp',
  });

  const headerContainerHeight = scrollY.interpolate({
    inputRange: [0, 200],
    outputRange: [210, alto],
    extrapolate: 'clamp',
  });

  const heightsHiden = scrollY.interpolate({
    inputRange: [0, 200],
    outputRange: ['100%', '0%'],
    extrapolate: 'clamp',
  });

  const headerContainerBorder = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [15, 0],
    extrapolate: 'clamp',
  });

  const opacity = scrollY.interpolate({
    inputRange: [0, 50, 90],
    outputRange: [1, 0.5, 0],
    extrapolate: 'clamp',
  });

  const opacityTitle = scrollY.interpolate({
    inputRange: [90, 100, 100],
    outputRange: [0, 50, 100],
    extrapolate: 'clamp',
  });

  const opacityText = scrollY.interpolate({
    inputRange: [140, 180, 200],
    outputRange: [0, 30, 100],
    extrapolate: 'clamp',
  });

  const imageContainerHeight = scrollY.interpolate({
    inputRange: [-200, 0],
    outputRange: [450, 250],
    extrapolate: 'extend',
  });

  const minute1 = item.storeData.categoryName === 'Supermercados' ? 120 : 45;

  const handleEmail = () => {
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: `Necesito ayuda con un pedido de wilbby`,
          recipients: ['info@wilbby.com'],
          body: `<b>Hola Necesito ayuda con el pedido número ${item.channelOrderDisplayId} mi nombre es ${item.customerData.name} y mi teléfono es ${item.customerData.telefono} gracias</b>`,
          // Android only (defaults to "Send Mail")
          isHTML: true,
        },
        (error, event) => {},
      );
    } else {
      OpenURLButton('mailto:info@wilbby.com');
    }
  };

  const onPress = () => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: 'Contactar con soporte',
          options: ['Cancelar', 'Asistencia teléfonica', 'Enviar un email'],
          cancelButtonIndex: 0,
        },
        (buttonIndex) => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            OpenURLButton(`tel:+34669124487`);
          } else if (buttonIndex === 2) {
            handleEmail();
          }
        },
      );
    } else {
      OpenURLButton(`tel:+34669124487`);
    }
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.containerIcon}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={() => Navigation.goBack()}
            style={styles.icon}>
            <Icon
              name="arrow-left"
              type="Feather"
              size={24}
              color={colors.white}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onPress()} style={styles.btnhelp}>
            <CustomText style={[stylesText.secondaryText, { color: 'white' }]}>
              <Icon
                name="message1"
                type="AntDesign"
                size={12}
                color={colors.white}
              />{' '}
              Ayuda
            </CustomText>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <Animated.View
        style={[
          styles.imageContainer,
          { height: imageContainerHeight },
        ]}></Animated.View>
      <ScrollView
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: scrollY } } },
        ])}
        scrollEventThrottle={16}
        stickyHeaderIndices={[1]}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        style={styles.scrollViewContainer}>
        <View
          style={{
            marginTop: 300,
            marginBottom: 120,
          }}>
          <View style={{ marginTop: 30 }}>
            <Status item={item} />
            {item.orderType === 'delivery' ? <Adress item={item} /> : null}
          </View>
          <View style={{ paddingBottom: dimensions.Height(20) }}>
            <Products
              item={item}
              localeCode={localeCode}
              currecy={currecy}
              OpenURLButton={OpenURLButton}
            />
          </View>
        </View>
        <View style={styles.stickyHeaderContainer}>
          <Animated.View
            style={[
              styles.headerContainer,
              {
                width: headerContainerWidth,
                height: headerContainerHeight,
                borderRadius: headerContainerBorder,
              },
            ]}>
            <Animated.View
              style={[
                styles.HeaderTitle,
                {
                  opacity: opacityTitle,
                  zIndex: 1,
                },
              ]}>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.titleText,
                  {
                    opacity: opacityText,
                    width: dimensions.Width(40),
                    marginLeft: 10,
                  },
                ]}>
                {item.scheduled
                  ? moment(item.deliveryTime)
                      .add(30, 'minute')
                      .format('hh:mm A')
                  : moment(item.created_at)
                      .add(minute1, 'minute')
                      .format('hh:mm A')}
              </CustomText>
            </Animated.View>
            <Animated.View
              style={{
                opacity: opacity,
                zIndex: 2,
                height: heightsHiden,
              }}>
              <InfoOrder item={item} heigth={heigth} />
            </Animated.View>
          </Animated.View>
        </View>
      </ScrollView>
      <View
        style={[
          styles.selected,
          { borderColor: barColor, paddingHorizontal: 30 },
        ]}>
        <View style={{ paddingVertical: 30 }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryTextBold]}>
            Total del pedido:
          </CustomText>
        </View>
        <View style={{ paddingVertical: 30 }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={
              dimensions.IsIphoneX()
                ? stylesText.h1
                : stylesText.secondaryTextBold
            }>
            {formaterPrice(item.payment / 100, localeCode, currecy)}
          </CustomText>
        </View>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
  },

  HeaderTitle: {
    width: dimensions.Width(96),
    marginTop: Platform.select({
      ios: dimensions.IsIphoneX()
        ? dimensions.Height(5.7)
        : dimensions.Height(5),
      android: dimensions.Height(5.3),
    }),
    position: 'absolute',
    height: 'auto',
    marginHorizontal: 50,
    zIndex: 1,
  },

  containerIcon: {
    width: dimensions.Width(96),
    position: 'absolute',
    marginTop: Platform.select({
      ios: dimensions.IsIphoneX() ? dimensions.Height(0) : dimensions.Height(4),
      android: dimensions.Height(5),
    }),
    marginHorizontal: 10,
    zIndex: 1,
  },
  scrollViewContainer: {
    flex: 1,
    height: dimensions.ScreenHeight,
    marginBottom: -150,
  },
  imageContainer: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: 200,
    backgroundColor: new DynamicValue(colors.main, colors.back_suave_dark),
  },
  stickyHeaderContainer: {
    position: 'absolute',
    top: 100,
    left: 0,
    right: 0,
  },
  headerContainer: {
    width: dimensions.Width(96),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
    padding: 20,
  },
  image: {
    flex: 1,
  },
  icon: {
    width: 36,
    height: 36,
    backgroundColor: new DynamicValue(colors.secundary, colors.main),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  tabs: {
    marginTop: dimensions.Height(2),
    marginLeft: -40,
  },

  btnhelp: {
    backgroundColor: new DynamicValue(colors.secundary, colors.main),
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 5,
  },

  selected: {
    width: dimensions.ScreenWidth,
    borderTopWidth: 0.5,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    bottom: 0,
    paddingBottom: dimensions.IsIphoneX() ? 20 : 0,
    position: 'absolute',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default FallowOrder;

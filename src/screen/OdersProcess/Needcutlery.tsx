import React from 'react';
import { View, Switch } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions, stylesText } from '../../theme';

export default function Needcutlery(props) {
  const { isEnabledCubiertos, toggleSwitch } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={[styles.items, { marginTop: 20, flexDirection: 'row' }]}>
      <View
        style={{
          width: dimensions.Width(70),
          paddingTop: 15,
          marginBottom: 15,
        }}>
        <CustomText
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold, { marginBottom: 5 }]}>
          ¿Necesitas cubiertos?
        </CustomText>
        <CustomText
          numberOfLines={3}
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            { paddingBottom: 5, lineHeight: 18 },
          ]}>
          Ayúdanos a reducir los residuos: pide cubiertos solo cuando los
          necesites.
        </CustomText>
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          marginLeft: 'auto',
          marginRight: 15,
        }}>
        <Switch
          trackColor={{ false: '#767577', true: colors.main }}
          thumbColor={isEnabledCubiertos ? colors.white : '#f4f3f4'}
          ios_backgroundColor={colors.rgb_153}
          onValueChange={toggleSwitch}
          value={isEnabledCubiertos}
        />
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },
});

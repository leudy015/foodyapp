import React, { useEffect, useState } from 'react';
import {
  View,
  Image,
  ScrollView,
  Modal,
  ActionSheetIOS,
  Platform,
  Linking,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors, image } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import { stylesText } from '../../../theme/TextStyle';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-dynamic-vector-icons';
import moment from 'moment';
import CustomAnimationProgress from '../../../Components/ProgressBar';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../../../Config/config';
import { GOOGLE_API_KEY } from '../../../Config/config';
import { customMaspStyles } from '../../../Components/MapStyle';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { RatingCalculator } from '../../../Utils/ratingRider';
import Star from '../../../Components/star';
import { formaterPrice } from '../../../Utils/formaterPRice';

export default function Details(props) {
  const {
    localeCode,
    currecy,
    dataModal,
    modalVisible,
    setModalVisible,
  } = props;
  const [ids, setID] = useState(null);

  const styles = useDynamicValue(dynamicStyles);
  const stylos = useDynamicValue(customMaspStyles);

  useEffect(() => {
    const getId = async () => {
      const id = await AsyncStorage.getItem('id');
      setID(id);
    };
    getId();
  }, [ids]);

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const onPress = () => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: 'Contactar con soporte',
          options: ['Cancelar', 'Asistencia teléfonica', 'Enviar un email'],
          cancelButtonIndex: 0,
        },
        (buttonIndex) => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            OpenURLButton(`tel:+34669124487`);
          } else if (buttonIndex === 2) {
            OpenURLButton('mailto:info@wilbby.com');
          }
        },
      );
    } else {
      OpenURLButton(`tel:+34669124487`);
    }
  };

  const borderColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const border = borderColors[mode];

  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={modalVisible}
      statusBarTranslucent={true}>
      <View style={styles.headerCont}>
        <View>
          <TouchableOpacity
            onPress={() => setModalVisible(false)}
            style={styles.closet}>
            <CustomText>
              <Icon
                name="arrow-left"
                type="Feather"
                size={24}
                color={colors.main}
              />
            </CustomText>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity onPress={() => onPress()} style={styles.btnhelp}>
            <CustomText style={[stylesText.secondaryText, { color: 'white' }]}>
              <Icon
                name="message1"
                type="AntDesign"
                size={16}
                color={colors.white}
              />{' '}
              Ayuda
            </CustomText>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.const}>
        <View
          style={[
            styles.centeredView,
            { marginBottom: dimensions.Height(10) },
          ]}>
          <MapView
            provider={PROVIDER_GOOGLE}
            customMapStyle={stylos}
            rotateEnabled={false}
            loadingEnabled={true}
            style={{
              height: 250,
              width: '100%',
            }}
            region={{
              latitude: dataModal.origin
                ? Number.parseFloat(dataModal.origin.lat)
                : 42.3441207,
              longitude: dataModal.origin
                ? Number.parseFloat(dataModal.origin.lgn)
                : -3.7297125,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}>
            <Marker
              coordinate={{
                latitude: dataModal.origin
                  ? Number.parseFloat(dataModal.origin.lat)
                  : 42.3441207,
                longitude: dataModal.origin
                  ? Number.parseFloat(dataModal.origin.lgn)
                  : -3.7297125,
              }}>
              <Image source={image.Maping} style={{ width: 50, height: 50 }} />
            </Marker>

            <MapViewDirections
              origin={{
                latitude: dataModal.origin
                  ? Number.parseFloat(dataModal.origin.lat)
                  : 42.3441207,
                longitude: dataModal.origin
                  ? Number.parseFloat(dataModal.origin.lgn)
                  : -3.7297125,
              }}
              destination={{
                latitude: dataModal.destination
                  ? Number.parseFloat(dataModal.destination.lat)
                  : 42.3441207,
                longitude: dataModal.destination
                  ? Number.parseFloat(dataModal.destination.lgn)
                  : -3.7297125,
              }}
              apikey={GOOGLE_API_KEY}
              strokeWidth={5}
              strokeColor={colors.main}
            />

            <Marker
              coordinate={{
                latitude: dataModal.destination
                  ? Number.parseFloat(dataModal.destination.lat)
                  : 42.3441207,
                longitude: dataModal.destination
                  ? Number.parseFloat(dataModal.destination.lgn)
                  : -3.7297125,
              }}>
              <Image
                source={image.Maping_Shop}
                style={{ width: 50, height: 50 }}
              />
            </Marker>
          </MapView>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{ alignItems: 'center' }}>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginTop: dimensions.Height(3),
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <CustomAnimationProgress
                    color1={colors.main}
                    color2={colors.main}
                    color3={colors.main}
                    maxpercent={100}
                    percent={Number(dataModal.progreso)}
                    width={dimensions.Width(90)} //numeric only
                    height={14}
                    style={{
                      marginHorizontal: 10,
                      marginBottom: 5,
                    }}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      width: dimensions.Width(90),
                      paddingHorizontal: 5,
                    }}>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryText, { paddingBottom: 5 }]}>
                      Pagado
                    </CustomText>

                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryText, { paddingBottom: 5 }]}>
                      En camino
                    </CustomText>

                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryText, { paddingBottom: 5 }]}>
                      Entregado
                    </CustomText>
                  </View>
                </View>
              </View>
            </View>
          </View>

          <View
            style={{
              marginTop: dimensions.Height(2),
            }}>
            <View style={{ marginHorizontal: 10, marginTop: 30 }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  { width: dimensions.Width(80), fontSize: 18 },
                ]}>
                Hora de entrega
              </CustomText>
            </View>
            <View style={[styles.cards, { borderColor: border }]}>
              <View style={{ marginLeft: 10 }}>
                <CustomText
                  light={dataModal.schedule ? colors.ERROR : colors.rgb_153}
                  dark={dataModal.schedule ? colors.ERROR : colors.rgb_153}
                  style={[
                    stylesText.titleText,
                    {
                      fontSize: 17,
                      width: dimensions.Width(65),
                    },
                  ]}>
                  {dataModal.schedule
                    ? 'Entrega programada'
                    : 'Hora estimada de la entrega'}
                </CustomText>
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.secondaryText,
                    {
                      marginTop: 5,
                      width: dimensions.Width(80),
                    },
                  ]}>
                  {moment(
                    dataModal.schedule ? dataModal.date : dataModal.created_at,
                  )
                    .add(dataModal.schedule ? 0 : 45, 'minute')
                    .format('lll')}
                </CustomText>
              </View>
            </View>

            <View style={{ marginHorizontal: 10, marginTop: 30 }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  { width: dimensions.Width(80), fontSize: 18 },
                ]}>
                Estado del envío
              </CustomText>
            </View>
            <View style={[styles.cards, { borderColor: border }]}>
              <View style={{ marginLeft: 10 }}>
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.titleText,
                    {
                      fontSize: 17,
                      width: dimensions.Width(65),
                    },
                  ]}>
                  Estado del envío: {dataModal.estado}
                </CustomText>
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.secondaryText,
                    {
                      width: dimensions.Width(80),
                      marginTop: 5,
                    },
                  ]}>
                  ID del envío: #{dataModal.display_id}
                </CustomText>
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.secondaryText,
                    {
                      marginTop: 5,
                      width: dimensions.Width(80),
                    },
                  ]}>
                  Fecha: {moment(dataModal.created_at).format('lll')}
                </CustomText>
              </View>
            </View>

            {dataModal.riders ? (
              <>
                <View style={{ marginHorizontal: 10 }}>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryTextBold,
                      { width: dimensions.Width(80), fontSize: 18 },
                    ]}>
                    Rider
                  </CustomText>
                </View>
                <View
                  style={[
                    styles.cards,
                    {
                      flexDirection: 'row',
                      alignItems: 'center',
                      padding: 15,
                      borderColor: border,
                    },
                  ]}>
                  <View>
                    <Image
                      source={{
                        uri:
                          NETWORK_INTERFACE_LINK_AVATAR +
                          dataModal.Riders.avatar,
                      }}
                      style={styles.logos}
                    />
                  </View>
                  <View>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.titleText]}>
                      {dataModal.Riders.name} {dataModal.Riders.lastName}
                    </CustomText>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryText]}>
                      Rider
                    </CustomText>
                    <View style={{ flexDirection: 'row' }}>
                      <Star
                        star={RatingCalculator(dataModal.Riders.rating)}
                        styles={{ marginTop: 5 }}
                      />
                      <CustomText
                        light={colors.black}
                        dark={colors.white}
                        style={[
                          stylesText.terciaryText,
                          { marginTop: 7, marginLeft: 5 },
                        ]}>
                        {RatingCalculator(dataModal.Riders.rating).toFixed(1)} /
                        + ({dataModal.Riders.rating.length})
                      </CustomText>
                    </View>
                  </View>
                </View>
              </>
            ) : null}
            <View style={{ marginHorizontal: 10 }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  {
                    width: dimensions.Width(80),
                    fontSize: 18,
                  },
                ]}>
                Dirección de recogida y entrega
              </CustomText>
            </View>
            <View style={[styles.cards, { borderColor: border }]}>
              <View
                style={[
                  styles.distance,
                  { paddingHorizontal: 15, paddingVertical: 5 },
                ]}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={stylesText.secondaryText}>
                  Distancia
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryTextBold, { marginLeft: 5 }]}>
                  {Number(dataModal.distance).toFixed(1)}{' '}
                  {Number(dataModal.distance) < 1 ? 'm' : 'Km'}
                </CustomText>
              </View>
              <View
                style={[
                  styles.textInputContent,
                  { padding: 10, marginTop: 20 },
                ]}>
                <Icon
                  name="flag"
                  type="AntDesign"
                  size={24}
                  color={colors.rgb_153}
                />
                <View>
                  <CustomText
                    light={colors.rgb_153}
                    dark={colors.rgb_153}
                    style={[
                      stylesText.secondaryText,
                      {
                        fontSize: 17,
                        marginLeft: 15,
                        paddingTop: 10,
                        width: dimensions.Width(65),
                      },
                    ]}>
                    {dataModal.origin.address_name}
                  </CustomText>
                  <CustomText
                    light={colors.main}
                    dark={colors.main}
                    style={[
                      stylesText.placeholderText,
                      { marginLeft: 15, marginTop: 5 },
                    ]}>
                    {dataModal.origin.address_number} {dataModal.origin.type}
                  </CustomText>
                </View>

                <Icon
                  name="right"
                  type="AntDesign"
                  size={16}
                  color={colors.rgb_153}
                  style={{ marginLeft: 'auto' }}
                />
              </View>
              <View style={[styles.textInputContent, { marginTop: 15 }]}>
                <Icon
                  name="enviromento"
                  type="AntDesign"
                  size={24}
                  color={colors.rgb_153}
                />
                <View>
                  <CustomText
                    light={colors.rgb_153}
                    dark={colors.rgb_153}
                    style={[
                      stylesText.secondaryText,
                      {
                        fontSize: 17,
                        marginLeft: 15,
                        paddingTop: 10,
                        width: dimensions.Width(65),
                      },
                    ]}>
                    {dataModal.destination.address_name}
                  </CustomText>
                  <CustomText
                    light={colors.main}
                    dark={colors.main}
                    style={[
                      stylesText.placeholderText,
                      { marginLeft: 15, marginTop: 5 },
                    ]}>
                    {dataModal.destination.address_number}{' '}
                    {dataModal.destination.type}
                  </CustomText>
                </View>
                <Icon
                  name="right"
                  type="AntDesign"
                  size={16}
                  color={colors.rgb_153}
                  style={{ marginLeft: 'auto' }}
                />
              </View>
            </View>

            <View style={{ marginHorizontal: 10 }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  { width: dimensions.Width(80), fontSize: 18 },
                ]}>
                Tu wilbby
              </CustomText>
            </View>

            <View style={[styles.cards, { borderColor: border }]}>
              <View
                style={[styles.textInputContent, { alignItems: 'flex-start' }]}>
                <Icon
                  name="edit"
                  type="AntDesign"
                  size={24}
                  color={colors.rgb_153}
                  style={{ marginTop: 15, marginRight: 10 }}
                />
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.secondaryText,
                    {
                      fontSize: 17,
                      marginLeft: 15,
                      paddingTop: 10,
                      width: dimensions.Width(65),
                    },
                  ]}>
                  {dataModal.nota}
                </CustomText>
              </View>
            </View>

            {dataModal.product_stimate_price ? (
              <>
                <View style={{ marginHorizontal: 10 }}>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryTextBold,
                      { width: dimensions.Width(80), fontSize: 18 },
                    ]}>
                    Precio estimado de producto
                  </CustomText>
                </View>
                <View style={[styles.cards, { borderColor: border }]}>
                  <View
                    style={[
                      styles.textInputContent,
                      { alignItems: 'flex-start' },
                    ]}>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[
                        stylesText.secondaryText,
                        {
                          fontSize: 17,
                          marginLeft: 15,
                          width: dimensions.Width(65),
                        },
                      ]}>
                      {formaterPrice(
                        dataModal.product_stimate_price,
                        localeCode,
                        currecy,
                      )}{' '}
                      Precio estimado de producto
                    </CustomText>
                  </View>
                </View>
              </>
            ) : null}

            <View style={{ marginHorizontal: 10 }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryTextBold,
                  { width: dimensions.Width(80), fontSize: 18 },
                ]}>
                Coste total del envío
              </CustomText>
            </View>
            <View style={[styles.cards, { borderColor: border }]}>
              <View
                style={{
                  marginTop: 20,
                  marginBottom: 20,
                  flexDirection: 'row',
                }}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryTextBold, { fontSize: 22 }]}>
                  Total envío
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.secondaryTextBold,
                    { fontSize: 22, marginLeft: 'auto' },
                  ]}>
                  {formaterPrice(dataModal.total, localeCode, currecy)}
                </CustomText>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  const: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  container: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    height: 'auto',
    minHeight: dimensions.Height(100),
  },

  centeredView: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    flex: 1,
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    alignSelf: 'flex-start',
  },

  img: {
    width: 100,
    height: 100,
    borderRadius: 10,
    marginLeft: 15,
    marginRight: dimensions.Height(2),
    backgroundColor: colors.white,
  },

  headerCont: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'absolute',
    height: 100,
    paddingHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(3),
    zIndex: 100,
    width: dimensions.Width(100),
  },

  closet: {
    color: new DynamicValue(colors.main, colors.white),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    padding: 10,
    borderRadius: 100,
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  btnhelp: {
    backgroundColor: colors.main,
    paddingHorizontal: 30,
    padding: 7,
    borderRadius: 100,
    marginLeft: 'auto',
  },

  textInputContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
  },

  cardscuston: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    marginTop: 30,
    borderRadius: 20,
    shadowColor: new DynamicValue(colors.black, colors.black),
    marginHorizontal: 10,
    shadowOffset: {
      width: 0,
      height: 15,
    },
    shadowOpacity: 0.34,
    shadowRadius: 8.27,
    elevation: 20,
    overflow: 'hidden',
  },

  cards: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginHorizontal: 10,
    marginVertical: 30,
    padding: 20,
    borderRadius: 15,
    borderWidth: 0.5,
  },

  distance: {
    backgroundColor: new DynamicValue(
      colors.colorInput,
      colors.back_suave_dark,
    ),
    marginTop: -40,
    marginLeft: 'auto',
    marginRight: 15,
    marginBottom: 10,
    borderRadius: 15,
    flexDirection: 'row',
  },

  logos: {
    width: 60,
    height: 60,
    marginRight: 15,
    borderRadius: 50,
  },
});

import React, { useState } from 'react';
import {
  View,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import Header from '../Components/Header';
import { dimensions, colors, image } from '../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { Query } from 'react-apollo';
import { query } from '../GraphQL';
import { useNavigationParam } from 'react-navigation-hooks';
import RestaurantCard from '../Components/RestaurantCard';
import { stylesText } from '../theme/TextStyle';
import CardLoading from '../Components/PlaceHolder/CardLoanding';
import Loadingss from '../Components/PlaceHolder/CategoryList';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Scooter from '../Components/scooter';
import HeightDemand from '../Components/HeightDemand';
import { validateDates } from '../Utils/CalculateHour';
import NoLocation from '../Components/NoLocation';
import Navigration from '../services/Navigration';
import LoadingAnimated from '../Components/LoadingAnimated';
import LottieView from 'lottie-react-native';
const source = require('../Assets/Animate/no_order.json');

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const Search = () => {
  const datos = useNavigationParam('data');
  const isLocation = datos.isLocation;
  const styles = useDynamicValue(dynamicStyles);
  const [categoria, setCategoria] = useState('');
  const [search, setSearch] = useState(datos.onsearch);
  const [price, setPrice] = useState();
  const [refreshing, setRefreshing] = useState(false);
  const [llevar, setLlevar] = useState(false);

  let loading: boolean;

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };
  const BorderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];
  const borderColor = BorderColor[mode];

  const renderItem = ({ item }) => {
    switch (item.title) {
      case 'Algo Especial':
        return null;
      case 'Envío Express':
        return null;
      default:
        return (
          <TouchableOpacity
            style={[
              styles.cat,
              {
                backgroundColor:
                  categoria === item._id ? colors.main : backgroundColor,
                borderColor: borderColor,
              },
            ]}
            onPress={() => {
              ReactNativeHapticFeedback.trigger(
                'notificationSuccess',
                optiones,
              );
              if (categoria) {
                setCategoria('');
                if (categoria != item._id) {
                  setCategoria(item._id);
                }
              } else {
                setCategoria(item._id);
              }
            }}>
            <CustomText
              light={
                categoria === item._id ? colors.white : colors.back_suave_dark
              }
              dark={categoria === item._id ? colors.white : colors.rgb_235}
              style={[stylesText.secondaryText, { fontWeight: '500' }]}>
              {item.title}
            </CustomText>
          </TouchableOpacity>
        );
    }
  };

  return (
    <View style={styles.container}>
      <Header
        title="Buscar"
        search
        autoFocus={true}
        value={search}
        Loading={loading}
        placeholder="Productos o tiendas"
        onChangeText={(value: any) => setSearch(value)}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={_onRefresh}
            enabled={true}
            title={datos.city}
          />
        }>
        <View style={{ flexDirection: 'row', paddingRight: 100 }}>
          <View style={styles.filters}>
            <TouchableOpacity
              onPress={() => {
                setLlevar(false);
                ReactNativeHapticFeedback.trigger(
                  'notificationSuccess',
                  optiones,
                );
              }}
              style={llevar ? styles.itemsInac : styles.items}>
              <Scooter />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setLlevar(true);
                ReactNativeHapticFeedback.trigger(
                  'notificationSuccess',
                  optiones,
                );
              }}
              style={llevar ? styles.items : styles.itemsInac}>
              <Icon
                name="run"
                type="MaterialCommunityIcons"
                size={20}
                style={styles.iconos}
              />
            </TouchableOpacity>
          </View>
          <Query query={query.CATEGORY}>
            {(response) => {
              if (response.loading) {
                return <Loadingss />;
              }
              if (response) {
                response.refetch();
                const data =
                  response && response.data && response.data.getCategory
                    ? response.data.getCategory.data
                    : [];
                return (
                  <View style={{ marginBottom: 15 }}>
                    <FlatList
                      data={data}
                      renderItem={(item: any) => renderItem(item)}
                      keyExtractor={(item: any) => item._id}
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                    />
                  </View>
                );
              }
            }}
          </Query>
        </View>

        {validateDates(23, 0, 23, 59) ? <HeightDemand /> : null}

        <Query
          query={query.RESTAURANT_SEARCH}
          variables={{
            search: search,
            city: datos.city,
            category: categoria,
            price: Math.round(price),
            llevar: llevar,
          }}>
          {(response: any) => {
            if (response.loading) {
              loading = response.loading;
              return (
                <View
                  style={{
                    width: dimensions.ScreenWidth,
                    height: dimensions.Height(50),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <LoadingAnimated name={`Estamos buscandote a ${search}`} />
                </View>
              );
            }
            if (response) {
              response.refetch();
              const data =
                response && response.data && response.data.getRestaurantSearch
                  ? response.data.getRestaurantSearch.data
                  : [];

              return (
                <View
                  style={{
                    marginTop: 20,
                  }}>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.titleText,
                      {
                        fontWeight: 'bold',
                        marginLeft: 15,
                        marginBottom: 20,
                        width: dimensions.Width(80),
                      },
                    ]}>
                    {search ? `Resultados para ${search}` : ''}
                  </CustomText>
                  {isLocation ? (
                    <>
                      {search ? (
                        <RestaurantCard
                          data={data}
                          city={datos.city}
                          lat={datos.lat}
                          lgn={datos.lgn}
                          refetch={response.refetch}
                          fromSearch={true}
                          localeCode={datos.localeCode}
                          currecy={datos.currecy}
                          riders={datos.riders}
                        />
                      ) : (
                        <View
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: dimensions.Width(96),
                            marginBottom: dimensions.Height(20),
                            paddingHorizontal: 30,
                          }}>
                          <LottieView
                            source={source}
                            autoPlay
                            loop
                            style={{ width: 200 }}
                          />
                          <CustomText
                            ligth={colors.black}
                            dark={colors.white}
                            style={[
                              stylesText.secondaryTextBold,
                              {
                                textAlign: 'center',
                              },
                            ]}>
                            Encuentra tu restaurante, supermercado o tienda
                            favorita, sólo escribe el nombre o tipo de comida
                          </CustomText>
                        </View>
                      )}
                    </>
                  ) : (
                    <NoLocation />
                  )}
                </View>
              );
            }
          }}
        </Query>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: dimensions.Height(6),
          }}>
          <TouchableOpacity
            style={{ borderRadius: 15 }}
            activeOpacity={100}
            onPress={() => {
              Navigration.navigate('ComparteyGana', 0);
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(type, optiones);
            }}>
            <Image
              source={image.FreeCredit}
              style={{
                width: dimensions.Width(96),
                height: 140,
                resizeMode: 'cover',
                borderRadius: 15,
              }}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  btnSmal: {
    borderTopEndRadius: dimensions.Width(5),
    borderTopStartRadius: dimensions.Width(5),
    borderBottomEndRadius: dimensions.Width(5),
    marginBottom: 10,
    backgroundColor: colors.ERROR,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginLeft: 'auto',
    marginRight: 15,
  },

  herderModal: {
    width: dimensions.Width(100),
    height: 80,
    backgroundColor: 'transparent',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: new DynamicValue(
      colors.colorBorder,
      colors.back_suave_dark,
    ),
    borderBottomWidth: 1,
  },

  noty: {
    marginLeft: 'auto',
    marginRight: dimensions.Width(4),
    backgroundColor: new DynamicValue('white', 'black'),
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  close: {
    color: new DynamicValue('black', 'white'),
    marginTop: dimensions.Height(8),
    marginLeft: dimensions.Width(4),
  },

  categoria: {
    margin: 12,
    padding: 10,
    paddingHorizontal: 10,
    borderRadius: 30,
  },

  cat: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 30,
    borderRadius: 100,
  },

  icons: {
    position: 'absolute',
    zIndex: 100,
    backgroundColor: new DynamicValue(colors.light_white, colors.black),
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    top: 15,
    right: 15,
  },

  iconos: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  filters: {
    width: 80,
    height: 40,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 100,
    paddingHorizontal: 5,
  },

  items: {
    width: 35,
    height: 35,
    backgroundColor: colors.main,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  itemsInac: {
    width: 35,
    height: 35,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});

export default Search;

import {colors} from './colors';
import {dimensions} from './dimension';
import {image} from './image';
import {stylesText} from './TextStyle';
export {colors, dimensions, image, stylesText};

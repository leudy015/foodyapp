import React from 'react';
import { View, TouchableOpacity, Image, ImageBackground } from 'react-native';
import { CustomText } from '../../../Components/CustomTetx';
import { colors, stylesText, dimensions, image } from '../../../theme';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { Grayscale } from 'react-native-color-matrix-image-filters';
import { RatingCalculator } from '../../../Utils/rating';
import Icon from 'react-native-dynamic-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import { mutations } from '../../../GraphQL';
import { useMutation } from 'react-apollo';
import {
  AddStoreToFavorite,
  DeleteStoreToFavorite,
} from '../../../Utils/AddFavourite';
import Navigation from '../../../services/Navigration';
import * as Animatable from 'react-native-animatable';
import { selectHour } from '../../../Utils/selectHour';
import moment from 'moment';

const jsCoreDateCreator = (dateString: string) => {
  let dateParam = dateString.split(/[\s-:]/);
  dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString();
  //@ts-ignore
  return new Date(...dateParam);
};

export default function Card(props: any) {
  const styles = useDynamicValue(dynamicStyles);
  const {
    datas,
    refetch,
    id,
    lat,
    lgn,
    localeCode,
    currecy,
    setNotification,
    riders,
  } = props;
  const [crearFavorito] = useMutation(mutations.ANADIR_RESTAURANT_FAVORITE);
  const [eliminarFavorito] = useMutation(
    mutations.ELIMINAR_RESTAURANT_FAVORITE,
  );

  const isOK = () => {
    if (datas.open) {
      return true;
    }
    return false;
  };

  const backgroundColors = {
    light: colors.white,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const averageRating = RatingCalculator(datas.Valoracion);

  const dat = {
    id: datas._id,
    yo: id,
    autoshipping: datas.autoshipping,
    city: datas.city,
    lat: lat,
    lgn: lgn,
    localeCode: localeCode,
    currecy: currecy,
    store: datas,
    refetch: refetch,
    delivery: riders,
  };

  const NavToDetails = () => {
    setNotification();
    Navigation.navigate('DestailsStore', { data: dat });
  };

  const data = datas.schedule;

  const scheduleOnly = datas.scheduleOnly;

  //@ts-ignore
  const gettheTime = selectHour(data).t;

  const finis = selectHour(data).finis;

  const moreDay: number = finis || scheduleOnly.hour > 0 ? 1 : 0;

  const d = new Date();
  const day = d.getDate() + moreDay;
  const year = d.getFullYear();
  const month = d.getMonth() + 1;

  const o = month > 9 ? '' : '0';

  //@ts-ignore
  const opentime = `${gettheTime.getHours()}-${gettheTime.getMinutes()}`;

  const min = jsCoreDateCreator(`${year}-${o + month}-${day}-${opentime}`);

  return (
    <TouchableOpacity onPress={() => NavToDetails()}>
      <Animatable.View
        animation="bounceIn"
        style={styles.card}
        duration={2000}
        iterationCount={1}>
        {datas.image ? (
          <ImageBackground
            style={[styles.mainImage]}
            imageStyle={{ borderRadius: 20 }}
            resizeMode="cover"
            source={image.PlaceHolder}>
            <Grayscale amount={isOK() ? 0 : 1}>
              <Image style={styles.mainImage} source={{ uri: datas.image }} />
            </Grayscale>
          </ImageBackground>
        ) : (
          <Image
            style={[styles.mainImage, { resizeMode: 'cover' }]}
            source={image.PlaceHolder}
          />
        )}

        <View style={[styles.types]}>
          <CustomText
            numberOfLines={1}
            light={colors.white}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              {
                alignItems: 'center',
                alignSelf: 'center',
                textAlign: 'center',
                paddingHorizontal: 20,
              },
            ]}>
            {datas.title}
          </CustomText>
          <View style={[styles.type, { marginTop: 5 }]}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              style={[
                stylesText.placeholderText,
                { alignItems: 'center', alignSelf: 'center', paddingBottom: 3 },
              ]}>
              {datas.type}
            </CustomText>
          </View>
          <View style={styles.volvemos}>
            <CustomText
              light={colors.black}
              dark={colors.black}
              style={[stylesText.secondaryTextBold, { fontSize: 14 }]}>
              <Icon
                name="clockcircleo"
                type="AntDesign"
                size={12}
                color={colors.black}
              />{' '}
              {datas.scheduleOnly.available
                ? `${datas.noScheduled ? 'Abre a las:' : 'Programar para:'} ${
                    finis
                      ? 'mañana'
                      : datas.scheduleOnly.hour > 23
                      ? 'mañana'
                      : moment(min).format('HH:mm')
                  }`
                : datas.stimateTime}
            </CustomText>
          </View>
        </View>

        <View style={styles.buttom}>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={[
                styles.hearto,
                {
                  backgroundColor: datas.anadidoFavorito
                    ? colors.ERROR
                    : backgroundColor,
                },
              ]}
              onPress={() => {
                if (datas.anadidoFavorito) {
                  DeleteStoreToFavorite(datas._id, refetch, eliminarFavorito);
                } else {
                  AddStoreToFavorite(id, datas._id, refetch, crearFavorito);
                }
              }}>
              <Icon
                type="AntDesign"
                name="heart"
                size={18}
                color={datas.anadidoFavorito ? colors.white : colors.rgb_153}
              />
            </TouchableOpacity>
            {datas.isnew ? (
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#510090', '#4600DA']}
                style={[styles.tag, { marginTop: 8 }]}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={stylesText.placeholderText}>
                  Nuevo
                </CustomText>
              </LinearGradient>
            ) : null}
          </View>
          <View style={styles.rating}>
            <CustomText
              ligth={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryText]}>
              <Icon
                name="star"
                type="AntDesign"
                size={16}
                color={colors.orange}
              />{' '}
              {averageRating.toFixed(1)}
            </CustomText>
          </View>
        </View>
      </Animatable.View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  mainImage: {
    width: dimensions.Width(60),
    height: dimensions.IsIphoneX()
      ? dimensions.Height(15)
      : dimensions.Height(19),
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },

  card: {
    marginHorizontal: 15,
    marginVertical: 15,
  },

  volvemos: {
    position: 'absolute',
    bottom: 5,
    left: 6,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  types: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: dimensions.Width(60),
    height: dimensions.IsIphoneX()
      ? dimensions.Height(15)
      : dimensions.Height(19),
    borderRadius: 15,
    backgroundColor: 'rgba(0,0,0,.3)',
  },

  type: {
    backgroundColor: 'rgba(0,0,0,.5)',
    paddingHorizontal: 10,
    paddingVertical: 3,
    borderRadius: 5,
  },

  tag: {
    zIndex: 200,
    width: 50,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 5,
    backgroundColor: colors.green,
  },

  hearto: {
    width: 30,
    height: 30,
    marginTop: 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginRight: 10,
    shadowColor: new DynamicValue(colors.light_grey, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  buttom: {
    marginTop: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },

  rating: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginLeft: 5,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: new DynamicValue(colors.light_grey, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});

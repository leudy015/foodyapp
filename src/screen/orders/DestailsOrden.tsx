import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  Dimensions,
  Alert,
  ActionSheetIOS,
  Linking,
  Modal,
  Platform,
  ImageBackground,
  FlatList,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors, image } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigationParam } from 'react-navigation-hooks';
// @ts-ignore
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../../services/Navigration';
import moment from 'moment';
import HerderModal from '../../Components/HeaderModal';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Complementos from '../../Components/NewCardProduct/complement';
import { RatingCalculator } from '../../Utils/ratingRider';
import Star from '../../Components/star';
import { scheduleTime } from '../../Utils/scheduleTime';
import { Grayscale } from 'react-native-color-matrix-image-filters';
import { formaterPrice } from '../../Utils/formaterPRice';
import Mailer from 'react-native-mail';
import {
  SiriShortcutsEvent,
  presentShortcut,
} from 'react-native-siri-shortcut';
import AddToSiriButton, {
  SiriButtonStyles,
} from 'react-native-siri-shortcut/AddToSiriButton';
import Proccess from './proccess';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../../Config/config';
import { Avatar } from 'react-native-elements';

const opts = {
  activityType: 'com.wilbby.gustash.SiriShortcutsModule.tengoHambre',
  title: 'Pedir en Wilbby',
  userInfo: {
    foo: 1,
    bar: 'baz',
    baz: 34.5,
  },
  keywords: ['kek', 'foo', 'bar'],
  persistentIdentifier: 'com.wilbby.gustash.SiriShortcutsModule.tengoHambre',
  isEligibleForSearch: true,
  isEligibleForPrediction: true,
  suggestedInvocationPhrase: 'Tengo hambre',
  needsSave: true,
};

const window = Dimensions.get('window');

const DetailsOrden = () => {
  const datas = useNavigationParam('data');
  const styles = useDynamicValue(dynamicStyles);
  const [modalVisible, setModalVisible] = useState(false);

  datas.refetch();

  SiriShortcutsEvent.addListener(
    'SiriShortcutListener',
    ({ userInfo, activityType }) => {
      console.log(userInfo, activityType);
    },
  );

  const setSiri = () => {
    presentShortcut(opts, ({ status }) => {
      console.log(`I was ${status}`);
    });
  };

  var totals = 0;

  datas.item.items.forEach(function (items: any) {
    const t = items.items.quantity * items.items.price;
    totals += t;
    items.items.subItems.forEach((x) => {
      const s = x.quantity * x.price;
      totals += s * items.items.quantity;
      const t = x && x.subItems ? x.subItems : [];
      t.forEach((y) => {
        const u = y.quantity * y.price;
        totals += u * items.items.quantity;
      });
    });
  });

  const restaurant = datas.item.storeData;
  const localeCode = datas.localeCode;
  const currecy = datas.currecy;
  const rat = datas.item.courierData ? datas.item.courierData.rating : [];

  const averageRating = RatingCalculator(rat);

  const schedule = restaurant.schedule;

  const backgroundColors = {
    light: SiriButtonStyles.blackOutline,
    dark: SiriButtonStyles.whiteOutline,
  };

  const mode = useColorSchemeContext();
  const type = backgroundColors[mode];

  const _renderItems = ({ item }) => {
    return (
      <View style={styles.itemss}>
        <View>
          <View style={{ flexDirection: 'row' }}>
            <CustomText
              light={colors.main}
              dark={colors.main}
              numberOfLines={1}
              style={[stylesText.secondaryTextBold, { marginRight: 10 }]}>
              {item.items.quantity} ×
            </CustomText>
            <CustomText
              light={colors.black}
              dark={colors.white}
              numberOfLines={1}
              style={[
                stylesText.secondaryTextBold,
                { width: dimensions.Width(60) },
              ]}>
              {item.items.name}
            </CustomText>
          </View>
          <CustomText
            numberOfLines={2}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              { width: dimensions.Width(60), paddingTop: 5, marginBottom: 5 },
            ]}>
            {item.items.description}
          </CustomText>
          <Complementos
            localeCode={localeCode}
            currecy={currecy}
            data={item.items}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            numberOfLines={1}
            style={[
              stylesText.secondaryTextBold,
              { width: dimensions.Width(60), marginTop: 20 },
            ]}>
            {formaterPrice(item.items.price / 100, localeCode, currecy)}
          </CustomText>
        </View>
        <Image
          source={{
            uri: item.items.imageUrl,
          }}
          style={styles.img}
        />
      </View>
    );
  };

  const isOK = () => {
    if (scheduleTime(schedule) && restaurant.open) {
      return true;
    }
    return false;
  };

  const OpenURLButton = async (url: any) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const handleEmail = () => {
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: `Necesito ayuda con un pedido de wilbby`,
          recipients: ['info@wilbby.com'],
          body: `<b>Hola Necesito ayuda con un pedido de Wilbby</b>`,
          // Android only (defaults to "Send Mail")
          isHTML: true,
        },
        (error, event) => {},
      );
    } else {
      OpenURLButton('mailto:info@wilbby.com');
    }
  };

  const onPress = () => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: 'Contactar con soporte',
          options: ['Cancelar', 'Asistencia teléfonica', 'Enviar un email'],
          cancelButtonIndex: 0,
        },
        (buttonIndex) => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            OpenURLButton(`tel:+34669124487`);
          } else if (buttonIndex === 2) {
            handleEmail();
          }
        },
      );
    } else {
      OpenURLButton(`tel:+34669124487`);
    }
  };

  const fill =
    datas.item.status === 'Finalizada' ? 100 : datas.item.IntegerValue;

  const datosNavigation = {
    data: datas.item.items,
    time: 'Lo antes posible',
    restaurants: restaurant,
    user: datas.item.customerData,
    city: datas.city,
    lat: datas.lat,
    lgn: datas.lgn,
    isOK: isOK,
    localeCode: localeCode,
    currecy: currecy,
    takeaway: false,
  };

  const isDone = (estado) => {
    switch (estado) {
      case 'Finalizada':
        return true;

      case 'Rechazado':
        return true;

      case 'Rechazada por la tienda':
        return true;

      case 'Resolución':
        return true;

      case 'Devuelto':
        return true;
      default:
        return false;
    }
  };

  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor="transparent"
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background">
            <ImageBackground
              style={styles.imagenbg}
              resizeMode="cover"
              source={image.PlaceHolder}>
              <Grayscale amount={isOK() ? 0 : 1}>
                <Image
                  style={styles.imagenbg}
                  source={{ uri: restaurant.image }}
                />
              </Grayscale>
            </ImageBackground>

            <View
              style={{
                position: 'absolute',
                top: 0,
                width: window.width,
                backgroundColor: 'rgba(0,0,0,.5)',
                height: PARALLAX_HEADER_HEIGHT,
              }}
            />
          </View>
        )}
        renderForeground={() => (
          <View key="parallax-header" style={styles.parallaxHeader}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              style={[stylesText.secondaryTextBold, styles.sectionSpeakerText]}>
              {restaurant.title}
            </CustomText>
          </View>
        )}
        renderFixedHeader={() => (
          <View
            key="fixed-header"
            style={[styles.fixedSection, { width: dimensions.ScreenWidth }]}>
            <TouchableOpacity
              style={styles.back}
              onPress={() => Navigation.goBack()}>
              <Icon
                name="arrow-left"
                type="Feather"
                size={24}
                style={styles.close}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.back,
                { right: 0, minWidth: 110, flexDirection: 'row' },
              ]}
              onPress={() => onPress()}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryText, { marginRight: 7 }]}>
                Ayuda
              </CustomText>
              <CustomText>
                <Icon
                  name="customerservice"
                  type="AntDesign"
                  size={24}
                  style={styles.close}
                />
              </CustomText>
            </TouchableOpacity>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                { width: dimensions.Width(40), textAlign: 'center' },
              ]}>
              {restaurant.title}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View style={styles.conts} />
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: dimensions.Width(4),
              marginTop: -30,
              marginBottom: dimensions.Height(2),
            }}>
            <AnimatedCircularProgress
              size={70}
              width={15}
              fill={fill}
              tintColor={colors.main}
              backgroundColor={colors.rgb_235}>
              {(fill) => (
                <CustomText light={colors.black} dark={colors.white}>
                  {fill}%
                </CustomText>
              )}
            </AnimatedCircularProgress>
            <View style={{ marginLeft: 15 }}>
              <TouchableOpacity
                onPress={() => setModalVisible(true)}
                style={{
                  backgroundColor: colors.main,
                  padding: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 5,
                }}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={[stylesText.secondaryText, { fontSize: 16 }]}>
                  {datas.item.status}
                </CustomText>
              </TouchableOpacity>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryText,
                  { textTransform: 'uppercase', marginTop: 10 },
                ]}>
                ID DE Wilbby #{datas.item.channelOrderDisplayId}
              </CustomText>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.terciaryText,
                  { fontSize: 14, marginTop: 5 },
                ]}>
                {moment(datas.item.created_at).format('lll')}
              </CustomText>
            </View>
          </View>
          <View style={styles.separator} />
          <View
            style={{
              paddingHorizontal: dimensions.Width(4),
              marginTop: dimensions.Height(4),
            }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText}>
              Tu Wilbby
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.terciaryText,
                {
                  marginTop: 5,
                  marginBottom: dimensions.Height(4),
                  paddingBottom: 5,
                },
              ]}>
              {datas.item.items.length}{' '}
              {datas.item.items.length > 1 ? 'productos' : 'producto'} de{' '}
              {restaurant.title}
            </CustomText>

            <View>
              <FlatList
                data={datas.item.items}
                renderItem={(item: any) => _renderItems(item)}
                keyExtractor={(items: any) => items._id}
                scrollEnabled={false}
              />
            </View>
            <View style={{ marginBottom: 30, marginTop: dimensions.Height(5) }}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[stylesText.titleText, { marginTop: 0 }]}>
                Nota del pedido
              </CustomText>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  { marginTop: 5, paddingBottom: 5 },
                ]}>
                {datas.nota ? datas.nota : 'No has añadido nota al pedido'}
              </CustomText>
            </View>

            {datas.item.orderType === 'delivery' ? (
              <>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.titleText, { marginTop: 0 }]}>
                  Entrega
                </CustomText>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View style={{ marginRight: 10 }}>
                    <Icon
                      name="location-pin"
                      type="MaterialIcons"
                      color={colors.main}
                      size={30}
                    />
                  </View>
                  <View>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryTextBold, { marginTop: 20 }]}>
                      {restaurant.title}
                    </CustomText>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[
                        stylesText.secondaryText,
                        {
                          marginTop: 5,
                          paddingBottom: 5,
                          width: dimensions.Width(80),
                        },
                      ]}>
                      {restaurant.adress.calle}, {restaurant.adress.numero},{' '}
                      {restaurant.adress.codigoPostal},{' '}
                      {restaurant.adress.ciudad}
                    </CustomText>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View style={{ marginRight: 10 }}>
                    <Icon
                      name="person-pin"
                      type="MaterialIcons"
                      color={colors.main}
                      size={30}
                    />
                  </View>
                  <View>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[stylesText.secondaryTextBold, { marginTop: 20 }]}>
                      {datas.item.customerData.name}{' '}
                      {datas.item.customerData.lastName}
                    </CustomText>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[
                        stylesText.secondaryText,
                        {
                          marginTop: 5,
                          paddingBottom: 5,
                          width: dimensions.Width(80),
                        },
                      ]}>
                      {datas.item.deliveryAddressData.formatted_address},{' '}
                      {datas.item.deliveryAddressData.puertaPiso},{' '}
                      {datas.item.deliveryAddressData.codigoPostal},{' '}
                      {datas.item.deliveryAddressData.city}
                    </CustomText>
                  </View>
                </View>
              </>
            ) : null}

            {datas.item.orderType === 'delivery' ? (
              <View style={{ marginTop: dimensions.Height(5) }}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.titleText,
                    { marginTop: dimensions.Height(0) },
                  ]}>
                  Rider
                </CustomText>

                {datas.item.courierData ? (
                  <View
                    style={{
                      marginBottom: dimensions.Height(4),
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginTop: 15,
                    }}>
                    <View style={{ marginRight: 10 }}>
                      <Avatar
                        size={50}
                        rounded
                        source={{
                          uri:
                            NETWORK_INTERFACE_LINK_AVATAR +
                            datas.item.courierData.avatar,
                        }}
                      />
                    </View>
                    <View>
                      <CustomText
                        light={colors.black}
                        dark={colors.white}
                        style={[stylesText.secondaryTextBold]}>
                        {datas.item.courierData.name}{' '}
                        {datas.item.courierData.lastName}
                      </CustomText>
                      <View style={{ flexDirection: 'row' }}>
                        <Star star={averageRating} styles={{ marginTop: 5 }} />
                        <CustomText
                          light={colors.black}
                          dark={colors.white}
                          style={[
                            stylesText.terciaryText,
                            { marginTop: 7, marginLeft: 5 },
                          ]}>
                          {averageRating.toFixed(1)} / + (
                          {datas.item.courierData.rating.length})
                        </CustomText>
                      </View>
                    </View>
                  </View>
                ) : (
                  <CustomText
                    light={colors.rgb_153}
                    dark={colors.rgb_153}
                    style={[
                      stylesText.terciaryText,
                      {
                        marginTop: 5,
                        marginBottom: dimensions.Height(4),
                        paddingBottom: 5,
                      },
                    ]}>
                    Sin Asignar
                  </CustomText>
                )}
              </View>
            ) : null}
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 40,
            }}>
            {Platform.OS === 'ios' ? (
              <>
                {
                  //@ts-ignore
                  <AddToSiriButton
                    style={{ flex: 1 }}
                    buttonStyle={type}
                    onPress={() => setSiri()}
                  />
                }
                <CustomText
                  light={colors.rgb_153}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.mainText,
                    {
                      width: 200,
                      textAlign: 'center',
                      marginTop: 10,
                      paddingBottom: 5,
                    },
                  ]}>
                  ¡Dile a Siri que vuelva a hacer este pedido!
                </CustomText>
              </>
            ) : null}
          </View>

          <View style={[styles.separator, { marginTop: 50 }]} />

          <View
            style={{
              paddingHorizontal: dimensions.Width(4),
              marginTop: dimensions.Height(4),
              marginBottom: dimensions.Height(20),
            }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText}>
              Resumen
            </CustomText>
            <View>
              <View style={{ flexDirection: 'row', marginTop: 20 }}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={stylesText.secondaryTextBold}>
                  Productos:
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryText, { marginLeft: 'auto' }]}>
                  {formaterPrice(totals / 100, localeCode, currecy)}
                </CustomText>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 20 }}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={stylesText.secondaryTextBold}>
                  Tarifa de servicio:
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryText, { marginLeft: 'auto' }]}>
                  {formaterPrice(
                    datas.item.serviceCharge / 100,
                    localeCode,
                    currecy,
                  )}
                </CustomText>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 20 }}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={stylesText.secondaryTextBold}>
                  Envío:
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryText, { marginLeft: 'auto' }]}>
                  {formaterPrice(
                    datas.item.deliveryCost / 100,
                    localeCode,
                    currecy,
                  )}
                </CustomText>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 20 }}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={stylesText.secondaryTextBold}>
                  Propina:
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.secondaryText, { marginLeft: 'auto' }]}>
                  {formaterPrice(datas.item.tip / 100, localeCode, currecy)}
                </CustomText>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 20 }}>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={stylesText.titleText}>
                  Total:
                </CustomText>
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.titleText, { marginLeft: 'auto' }]}>
                  {formaterPrice(datas.item.payment / 100, localeCode, currecy)}
                </CustomText>
              </View>
            </View>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.placeholderText, { marginTop: 30 }]}>
              Este total incluye descuento, propina al establecimiento y todo
              los cargos de transacción.
            </CustomText>
          </View>
        </View>
      </ParallaxScrollView>

      {isDone(datas.item.status) ? (
        <View
          style={[
            styles.selected,
            {
              backgroundColor: datas.isvalored ? colors.main : colors.main,
            },
          ]}>
          <TouchableOpacity
            onPress={() => {
              if (isOK()) {
                Navigation.navigate('Order', { data: datosNavigation });
              } else {
                Alert.alert(
                  'Tienda cerrada',
                  'Este establecimiento está cerrado te avisaremos cuando esté disponible',
                  [
                    {
                      text: 'Ok',
                      onPress: () => console.log('calcel'),
                    },
                  ],

                  { cancelable: false },
                );
              }
            }}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              width: dimensions.Width(90),
              height: 60,
            }}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              numberOfLines={1}
              style={stylesText.mainText}>
              Repetir pedido
            </CustomText>
          </TouchableOpacity>
        </View>
      ) : null}
      <Modal
        animationType="slide"
        visible={modalVisible}
        presentationStyle="formSheet"
        statusBarTranslucent={true}
        onRequestClose={() => setModalVisible(false)}>
        <View style={styles.centeredView}>
          <HerderModal
            title="Proceso del pedido"
            icon="close"
            OnClosetModal={() => setModalVisible(false)}
          />
          <Proccess data={datas.item.statusProcess} />
        </View>
      </Modal>
    </View>
  );
};

const AVATAR_SIZE = 70;
const ROW_HEIGHT = 60;
const PARALLAX_HEADER_HEIGHT = dimensions.IsIphoneX() ? 250 : 220;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contenedor: {
    flex: 1,
    width: dimensions.ScreenWidth,
    height: 'auto',
    minHeight: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: '100%',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 40,
  },
  stickySectionText: {
    color: '#95ca3e',
    fontSize: 20,
    margin: 10,
    marginTop: 15,
  },
  fixedSection: {
    color: '#95ca3e',
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 5,
  },
  fixedSectionText: {
    color: '#95ca3e',
    fontSize: 16,
    marginRight: 0,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  fixedSectionText1: {
    color: '#95ca3e',
    fontSize: 16,
    marginRight: 15,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    paddingTop: dimensions.IsIphoneX()
      ? dimensions.Height(12)
      : dimensions.Height(15),
  },
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    borderWidth: 2,
    borderColor: '#d6d7da',
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  sectionSpeakerText: {
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    paddingVertical: 5,
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  rowText: {
    fontSize: 20,
  },

  header1: {
    width: '100%',
    height: 80,
    marginTop: 'auto',
  },

  back: {
    marginLeft: 15,
    marginRight: dimensions.Width(2),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },

  hert: {
    color: colors.main,
    marginTop: dimensions.Height(8),
    marginLeft: dimensions.Width(4),
  },

  close: {
    color: colors.main,
  },

  separator: {
    width: dimensions.Width(100),
    height: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    alignSelf: 'flex-start',
  },

  modalConten: {
    flex: 1,
    width: dimensions.Width(100),
    height: dimensions.Height(50),
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  items: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 15,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    paddingLeft: dimensions.Width(4),
    flexDirection: 'row',
  },

  items1: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 25,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    paddingLeft: dimensions.Width(4),
  },

  itemss: {
    width: dimensions.Width(100),
    height: 'auto',
    paddingVertical: 25,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    paddingLeft: dimensions.Width(4),
    flexDirection: 'row',
  },

  img: {
    width: 80,
    height: 80,
    borderRadius: 5,
    marginLeft: 'auto',
    marginRight: dimensions.Height(4),
  },

  selected: {
    width: dimensions.Width(94),
    height: 60,
    borderRadius: 15,
    bottom: 20,
    position: 'absolute',
    flexDirection: 'row',
    flex: 1,
    backgroundColor: colors.main,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  conts: {
    width: dimensions.Width(100),
    height: 80,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: -50,
    borderRadius: 30,
  },

  logos: {
    width: 80,
    height: 80,
    marginRight: 15,
    borderRadius: 50,
    marginTop: 12,
  },

  selecteds: {
    width: dimensions.Width(94),
    height: 60,
    borderRadius: 15,
    flexDirection: 'row',
    flex: 1,
    backgroundColor: colors.main,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
  },

  input: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    padding: Platform.select({
      android: 10,
      ios: dimensions.IsIphoneX() ? 20 : 10,
    }),
    marginBottom: 30,
    marginHorizontal: 10,
    borderRadius: 5,
    color: colors.rgb_153,
    borderColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    borderWidth: 0.5,
    flexDirection: 'row',
  },

  btnPDF: {
    marginTop: 5,
    height: 25,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    marginRight: 15,
  },
});

export default DetailsOrden;

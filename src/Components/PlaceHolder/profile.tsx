import React from 'react';
import { View, ScrollView } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions } from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  return (
    <View
      style={{
        alignSelf: 'center',
        marginTop: dimensions.Height(5),
      }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 20,
            alignItems: 'center',
          }}>
          <View style={{ flexDirection: 'row', width: dimensions.Width(90) }}>
            <View>
              <View
                style={{
                  width: 180,
                  height: 30,
                  borderRadius: 10,
                  marginTop: 20,
                }}
              />
              <View
                style={{
                  width: 130,
                  height: 20,
                  borderRadius: 10,
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  width: 130,
                  height: 10,
                  borderRadius: 10,
                  marginTop: 10,
                }}
              />
            </View>

            <View style={{ marginLeft: 'auto' }}>
              <View
                style={{
                  width: 130,
                  height: 130,
                  borderRadius: 100,
                }}
              />
            </View>
          </View>

          <View
            style={{
              width: dimensions.Width(90),
              height: 80,
              borderRadius: 20,
              marginTop: dimensions.Height(5),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 80,
              borderRadius: 20,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 80,
              borderRadius: 20,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 80,
              borderRadius: 20,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 80,
              borderRadius: 20,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 80,
              borderRadius: 20,
              marginTop: dimensions.Height(2),
            }}
          />
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, dimensions, stylesText, image } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import { GOOGLE_API_KEY } from '../../Config/config';
import { customMaspStyles } from '../../Components/MapStyle';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { getDistacia } from '../../Utils/getDistance';

export const Maps = (prosp) => {
  const {
    originLat,
    originLng,
    detinationLat,
    detitationLgn,
    setDireccionModal,
    setDireccionModalDetination,
    adress1,
    adress2,
    puertaPiso1,
    puertaPiso2,
    localeCode,
    lat,
    lgn,
    dist,
    setdistance,
  } = prosp;
  const styles = useDynamicValue(dynamicStyles);
  const stylos = useDynamicValue(customMaspStyles);

  const origin = {
    latitude: originLat ? originLat : 40.4167754,
    longitude: originLng ? originLng : -3.7037902,
  };
  const destination = {
    latitude: detinationLat ? detinationLat : 40.4167754,
    longitude: detitationLgn ? detitationLgn : -3.7037902,
  };

  getDistacia(originLat, originLng, detinationLat, detitationLgn).then(
    async (res) => {
      const response = await res.json();

      if (response) {
        setdistance(response.rows[0].elements[0].distance.value);
      }
    },
  );

  //@ts-ignore
  const d = new Intl.NumberFormat(localeCode ? localeCode : 'es-ES').format(
    //@ts-ignore
    dist.toFixed(1),
  );

  const borderColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const border = borderColors[mode];

  return (
    <View style={[styles.cardscuston, { borderColor: border }]}>
      <MapView
        showsUserLocation={true}
        zoomEnabled={true}
        provider={PROVIDER_GOOGLE}
        customMapStyle={stylos}
        style={{
          height: 200,
          width: dimensions.Width(93),
          borderRadius: 20,
        }}
        region={{
          latitude: lat ? lat : 40.4167754,
          longitude: lgn ? lgn : -3.7037902,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}>
        <Marker title={adress1} coordinate={origin}>
          <Image source={image.Maping_Shop} style={{ width: 50, height: 50 }} />
        </Marker>
        <MapViewDirections
          origin={origin}
          destination={destination}
          apikey={GOOGLE_API_KEY}
          strokeWidth={5}
          strokeColor={colors.main}
        />
        <Marker title={adress2} coordinate={destination}>
          <Image source={image.Maping} style={{ width: 50, height: 50 }} />
        </Marker>
      </MapView>
      <View style={[styles.distance]}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={stylesText.secondaryText}>
          Distancia {d} {dist < 1 ? 'm' : 'Km'}
        </CustomText>
      </View>
      <TouchableOpacity
        style={[styles.textInputContent, { marginTop: 30 }]}
        onPress={() => setDireccionModal(true)}>
        <Icon name="flag" type="AntDesign" size={20} color={colors.rgb_153} />
        <View style={{ marginLeft: 10 }}>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              { width: dimensions.Width(70), fontSize: 18 },
            ]}>
            {adress1}
          </CustomText>
          <CustomText
            light={colors.main}
            dark={colors.main}
            style={[stylesText.secondaryText]}>
            {puertaPiso1}
          </CustomText>
        </View>

        <Icon
          name="right"
          type="AntDesign"
          size={16}
          color={colors.rgb_153}
          style={{ marginLeft: 'auto' }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.textInputContent]}
        onPress={() => setDireccionModalDetination(true)}>
        <Icon
          name="enviromento"
          type="AntDesign"
          size={24}
          color={colors.rgb_153}
        />
        <View style={{ marginLeft: 10 }}>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              { width: dimensions.Width(70), fontSize: 18 },
            ]}>
            {adress2}
          </CustomText>
          <CustomText
            light={colors.main}
            dark={colors.main}
            style={[stylesText.secondaryText]}>
            {puertaPiso2}
          </CustomText>
        </View>
        <Icon
          name="right"
          type="AntDesign"
          size={16}
          color={colors.rgb_153}
          style={{ marginLeft: 'auto' }}
        />
      </TouchableOpacity>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  cardscuston: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: 30,
    borderRadius: 20,
    shadowColor: new DynamicValue(colors.rgb_235, colors.black),
    marginHorizontal: 15,
    overflow: 'hidden',
    borderWidth: 1,
  },

  distance: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: -40,
    marginLeft: 'auto',
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginHorizontal: 10,
    borderRadius: 15,
    flexDirection: 'row',
  },

  textInputContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    marginTop: 20,
  },
});

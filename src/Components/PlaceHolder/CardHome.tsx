import React from 'react';
import { SafeAreaView, View } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions } from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <View style={{ alignSelf: 'center', backgroundColor: back }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            height: dimensions.Height(25),
            alignItems: 'center',
            marginHorizontal: 20,
          }}>
          <View style={{ marginTop: 20 }}>
            <View
              style={{
                width: dimensions.Width(94),
                height: 150,
                borderRadius: 20,
              }}
            />

            <View style={{ flexDirection: 'row' }}>
              <View>
                <View
                  style={{
                    width: dimensions.Width(50),
                    height: 18,
                    marginTop: 10,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(40),
                    height: 15,
                    marginTop: 15,
                    borderRadius: 30,
                    marginLeft: 5,
                  }}
                />
              </View>
              <View style={{ marginLeft: 'auto', marginRight: 15 }}>
                <View
                  style={{
                    width: dimensions.Width(20),
                    height: 20,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(15),
                    height: 10,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

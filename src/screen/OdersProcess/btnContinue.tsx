import React from 'react';
import { View, TouchableOpacity, Alert } from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions, stylesText } from '../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { formaterPrice } from '../../Utils/formaterPRice';

export default function BtnContinue(props) {
  const { totalFinal, Crear_Orden, localeCode, currecy, takeaway } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View
      style={[
        styles.selected,
        {
          backgroundColor: colors.main,
          justifyContent: 'center',
          alignSelf: 'center',
          alignItems: 'center',
        },
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 15,
        }}>
        <CustomText
          light={colors.white}
          dark={colors.white}
          numberOfLines={1}
          style={stylesText.titleText2}>
          {formaterPrice(totalFinal, localeCode, currecy)}
        </CustomText>
      </View>
      <View style={{ marginLeft: 'auto' }}>
        <TouchableOpacity
          onPress={() => {
            if (takeaway) {
              Alert.alert(
                'Para recoger en el establecimiento',
                'Este es un pedido para recoger. Recibirás un código de recogida para retirar los productos.',
                [
                  {
                    text: 'Cancelar',
                    onPress: () => {},
                    style: 'destructive',
                  },

                  {
                    text: '¡Entendido!',
                    onPress: () => Crear_Orden(true),
                  },
                ],

                { cancelable: false },
              );
            } else {
              Crear_Orden(true);
            }
          }}
          style={{
            height: 70,
            paddingHorizontal: 20,
            backgroundColor: colors.main,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 15,
            marginRight: -3,
          }}>
          <CustomText
            light={colors.white}
            dark={colors.white}
            style={stylesText.mainText}>
            Continuar al pago{' '}
            <Icon
              name="doubleright"
              type="AntDesign"
              color={colors.white}
              size={14}
            />
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  selected: {
    width: dimensions.Width(94),
    height: 70,
    borderRadius: 15,
    bottom: 20,
    position: 'absolute',
    flexDirection: 'row',
    flex: 1,
  },
});

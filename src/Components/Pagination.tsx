import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import { dimensions, colors, stylesText, image } from '../theme';
import { CustomText } from './CustomTetx';

export default function Pagination(props: any) {
  const { page, onPressMas, onPressMenos, width } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={[styles.cont, { width: width ? width : 200 }]}>
      <TouchableOpacity onPress={onPressMenos} style={styles.btn}>
        <Icon name="minus" type="AntDesign" size={24} color={colors.main} />
      </TouchableOpacity>
      <CustomText
        light={colors.black}
        dark={colors.white}
        style={[
          stylesText.secondaryTextBold,
          {
            textAlign: 'center',
          },
        ]}>
        {page}
      </CustomText>
      <TouchableOpacity onPress={onPressMas} style={styles.btn}>
        <Icon name="plus" type="AntDesign" size={24} color={colors.main} />
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  btn: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    borderRadius: 100,
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
  },
});

import React from 'react';
import { Modal, View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import LottieView from 'lottie-react-native';
import { colors, stylesText, dimensions } from '../../theme';
import { Button } from '../Button';
import { CustomText } from '../CustomTetx';
import source from '../../Assets/Animate/noInter.json';
import RNRestart from 'react-native-restart';

export default function Notification(props) {
  const { visibleModal } = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      collapsable={true}
      statusBarTranslucent={true}>
      <View style={styles.container}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: dimensions.ScreenHeight,
          }}>
          <LottieView
            source={source}
            autoPlay
            loop
            style={{
              width: 400,
              height: 300,
            }}
          />
          <View style={{ marginHorizontal: 30 }}>
            <CustomText
              ligth={colors.black}
              dark={colors.white}
              style={[stylesText.titleText, { textAlign: 'center' }]}>
              No hay conexión a internet
            </CustomText>
            <CustomText
              ligth={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                { textAlign: 'center', marginTop: 20 },
              ]}>
              Hay un problema con tu conexión a internet
            </CustomText>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={[
                  styles.buttonView,
                  { backgroundColor: colors.main },
                ]}
                onPress={() => RNRestart.Restart()}
                title="Volver a intentarlo"
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

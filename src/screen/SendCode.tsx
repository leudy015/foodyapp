import React, { useState } from 'react';
import { View, TouchableOpacity, Alert } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import AsyncStorage from '@react-native-community/async-storage';
import Navigation from '../services/Navigration';
import CountDown from 'react-native-countdown-component';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { useNavigationParam } from 'react-navigation-hooks';
import { NETWORK_INTERFACE_URL } from '../Config/config';
import { dimensions, colors } from '../theme';
import Loader from '../Components/ModalLoading';
import Icon from 'react-native-dynamic-vector-icons';
import { stylesText } from '../theme/TextStyle';
import { ScrollView } from 'react-native-gesture-handler';
import LottieView from 'lottie-react-native';
import source from '../Assets/Animate/lf30_editor_zr5nhk6x.json';
import { useMutation } from 'react-apollo';
import { mutations } from '../GraphQL';

const SendCode = () => {
  const [Loading, setLoading] = useState(false);
  const [value, setValue] = useState('');
  const [autenticarUsuario] = useMutation(mutations.AUTENTICAR_USUARIO);

  const data = useNavigationParam('data');

  const CELL_COUNT = 4;

  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const sendCode = async () => {
    setLoading(true);
    const input = {
      phone: data.phone,
      code: value,
      id: data.id,
    };
    fetch(`${NETWORK_INTERFACE_URL}/verify-code`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(input),
    })
      .then(async (res) => {
        const confirm = await res.json();
        if (confirm.success) {
          setLoading(false);
          if (data.data.Register) {
            autenticarUsuario({
              variables: {
                email: data.data.email,
                password: data.data.password,
                input: data.data.dataLogin,
              },
            })
              .then((res) => {
                if (res.data.autenticarUsuario.data) {
                  const user = res.data.autenticarUsuario.data;
                  const us = JSON.stringify(user.user);
                  AsyncStorage.setItem('token', user.token);
                  AsyncStorage.setItem('id', user.id);
                  AsyncStorage.setItem('user', us);
                  Navigation.navigate('Inicio', { data: user.data.id });
                } else {
                  Navigation.navigate('Login', 0);
                }
              })
              .catch(() => {
                Navigation.navigate('Login', 0);
              });
          } else {
            const us = JSON.stringify(data.data.user);
            AsyncStorage.setItem('token', data.data.token);
            AsyncStorage.setItem('id', data.data.id);
            AsyncStorage.setItem('user', us);
            Navigation.navigate('Inicio', { data: data.data.id });
          }
        } else {
          Alert.alert(
            'Algo salio mal',
            'Hubo un error con tu código por favor vuelve a intentarlo.',
            [
              {
                text: 'Volver a enviar',
                onPress: () => VerifyPhone(data.phone),
              },
            ],

            { cancelable: false },
          );
        }
      })
      .catch((err) => console.log(err));
  };

  const finisehd = () => {
    Alert.alert(
      'Se ha vencido el código',
      'El código enviado se ha vencido para verificar tu teléfono vuelve a intentarlo.',
      [
        {
          text: 'Volver a enviar',
          onPress: () => VerifyPhone(data.phone),
        },
        {
          text: 'Cancelar',
          onPress: () => {},
        },
      ],

      { cancelable: false },
    );
  };

  const VerifyPhone = async (phone: any) => {
    setLoading(true);
    fetch(`${NETWORK_INTERFACE_URL}/verify-phone`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ phone }),
    })
      .then(async (res) => {
        const confirm = await res.json();
        if (confirm.success) {
          setLoading(false);
        } else {
          Alert.alert(
            'Algo salio mal',
            'Algo salio mal intentalo de nuevo',
            [
              {
                text: 'Volver a enviar',
                onPress: () => console.log('ok'),
              },
            ],

            { cancelable: false },
          );
          setLoading(false);
        }
      })
      .catch((err) => console.log(err));
  };

  const dynamicStyles = new DynamicStyleSheet({
    container: {
      flex: 1,
      backgroundColor: new DynamicValue('white', 'black'),
    },
    formView: {
      marginHorizontal: dimensions.Width(8),
      marginTop: dimensions.Height(0),
      marginBottom: dimensions.Height(15),
    },
    rememberMeView: {
      marginTop: dimensions.Height(2),
      flexDirection: 'row',
    },
    rememberText: {
      fontSize: dimensions.FontSize(18),
    },
    signupButtonContainer: {
      marginTop: dimensions.Height(5),
      alignSelf: 'center',
    },
    buttonView: {
      backgroundColor:
        value.length === 4
          ? new DynamicValue(colors.main, colors.main)
          : colors.rgb_153,
      width: dimensions.Width(84),
      borderRadius: dimensions.Width(2),
    },
    buttonTitle: {
      alignSelf: 'center',
      paddingVertical: dimensions.Height(2),
      paddingHorizontal: dimensions.Width(5),
      color: colors.white,
      fontWeight: '400',
      fontSize: dimensions.FontSize(17),
    },
    contenedor: {
      width: dimensions.ScreenWidth,
      height: dimensions.ScreenHeight,
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    },
    h1: {
      fontSize: dimensions.FontSize(30),
      fontWeight: 'bold',
      textAlign: 'left',
    },

    root: { flex: 1, padding: 20 },
    title: { textAlign: 'center', fontSize: 30 },
    codeFiledRoot: { marginTop: 20 },
    cell: {
      width: 50,
      height: 50,
      lineHeight: 48,
      fontSize: 24,
      borderRadius: 5,
      borderWidth: 1,
      borderColor: new DynamicValue(colors.back_dark, colors.rgb_153),
      textAlign: 'center',
    },
    focusCell: {
      borderColor: colors.main,
    },
    close: {
      color: new DynamicValue('black', 'white'),
      marginTop: dimensions.Height(8),
      marginLeft: dimensions.Width(4),
    },
  });

  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Loader loading={Loading} color={colors.main} />
        <View style={styles.contenedor}>
          <TouchableOpacity onPress={() => Navigation.goBack()}>
            <Icon
              name="close"
              type="AntDesign"
              size={32}
              style={styles.close}
            />
          </TouchableOpacity>
          <View
            style={{
              marginTop: dimensions.Height(6),
              marginBottom: dimensions.Height(5),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <LottieView
              source={source}
              autoPlay
              loop
              style={{ width: 100, height: 100 }}
            />
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={stylesText.titleText}>
              Verifica tu teléfono
            </CustomText>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  marginTop: 30,
                  paddingHorizontal: dimensions.Width(4),
                  textAlign: 'center',
                },
              ]}>
              Ingresa el código de verificación que te enviamos a su número de
              teléfono {''} {data.phone}
            </CustomText>
          </View>
          <View style={styles.formView}>
            <CodeField
              ref={ref}
              {...props}
              value={value}
              onChangeText={setValue}
              cellCount={CELL_COUNT}
              rootStyle={styles.codeFiledRoot}
              keyboardType="number-pad"
              textContentType="oneTimeCode"
              renderCell={({ index, symbol, isFocused }) => (
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  key={index}
                  style={[styles.cell, isFocused && styles.focusCell]}
                  onLayout={getCellOnLayoutHandler(index)}>
                  {symbol || (isFocused ? <Cursor /> : null)}
                </CustomText>
              )}
            />
            <View style={{ flexDirection: 'row' }}>
              <CustomText
                style={{
                  color: colors.rgb_153,
                  fontWeight: '200',
                  fontSize: 14,
                  paddingTop: 30,
                }}>
                El código vence en:{' '}
              </CustomText>

              <CountDown
                until={60 * 5 + 0}
                size={8}
                onFinish={() => finisehd()}
                digitStyle={{
                  backgroundColor: 'transparent',
                  marginTop: 29,
                }}
                digitTxtStyle={{
                  color: colors.rgb_153,
                  fontWeight: '200',
                  fontSize: 14,
                }}
                timeToShow={['M', 'S']}
                timeLabels={false}
              />
            </View>
            <View style={styles.signupButtonContainer}>
              <TouchableOpacity
                style={styles.buttonView}
                onPress={() => (value.length === 4 ? sendCode() : '')}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  style={styles.buttonTitle}>
                  Verificar
                </CustomText>
              </TouchableOpacity>
            </View>

            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  marginTop: 50,
                  paddingHorizontal: dimensions.Width(4),
                  textAlign: 'center',
                },
              ]}>
              ¿No recibiste el código?
            </CustomText>
            <TouchableOpacity
              onPress={() => VerifyPhone(data.phone)}
              style={{
                alignSelf: 'center',
                marginTop: dimensions.Height(5),
                marginBottom: dimensions.Height(10),
              }}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={stylesText.secondaryText}>
                Reenviar
              </CustomText>
            </TouchableOpacity>
            <View />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default SendCode;

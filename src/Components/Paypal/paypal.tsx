import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, Image, Alert } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../CustomTetx';
import { dimensions, colors, image, stylesText } from '../../theme';

const Paypal = (props) => {
  const styles = useDynamicValue(dynamicStyles);

  const backgroundColor = {
    light: colors.white,
    dark: colors.back_dark,
  };
  const mode = useColorSchemeContext();
  const bg = backgroundColor[mode];

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const modes = useColorSchemeContext();
  const borderColot = backgroundColors[modes];

  return (
    <View
      style={[
        styles.cardinfo,
        { backgroundColor: bg, borderColor: borderColot },
      ]}>
      <Image
        source={image.Paypal}
        style={{
          width: 50,
          height: 50,
          resizeMode: 'contain',
          marginRight: 10,
        }}
      />
      <View>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.mainText, { fontWeight: 'bold' }]}>
          Pagos con Paypal
        </CustomText>
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[stylesText.secondaryText, { paddingBottom: 3 }]}>
          Usa paypal para pagar
        </CustomText>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  cardinfo: {
    width: dimensions.Width(92),
    padding: 15,
    height: 'auto',
    flexDirection: 'row',
    marginTop: dimensions.Height(1),
    marginBottom: dimensions.Height(1),
    borderRadius: 10,
    alignSelf: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
  },
});

export default Paypal;

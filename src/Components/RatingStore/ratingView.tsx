import React, { useState } from 'react';
import { View, TouchableOpacity, FlatList, Button } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Star from '../star';
import { PercentageBar } from './PercentageBar';
import { colors, stylesText } from '../../theme';
import { CustomText } from '../CustomTetx';
import { ComentsUser } from './Coments';
import { dimensions } from '../../theme/dimension';
import { RatingCalculator } from '../../Utils/rating';
import { DesgloseRating } from '../../Utils/DesgloseRating';
import LottieView from 'lottie-react-native';
import Modal from 'react-native-modal';
import Safe from './safe';
import 'moment/locale/es';

export default function RetingView(props: any) {
  const { data } = props;
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const averageRating = RatingCalculator(data.Valoracion);
  const styles = useDynamicValue(dynamicStyles);

  const value = DesgloseRating(data.Valoracion);
  const bar = value[1].length / data.Valoracion.length;
  const bar1 = value[2].length / data.Valoracion.length;
  const bar2 = value[3].length / data.Valoracion.length;
  const bar3 = value[4].length / data.Valoracion.length;
  const bar4 = value[5].length / data.Valoracion.length;

  const porcentaje4 = bar4 ? bar4 * 100 : 0;
  const porcentaje3 = bar3 ? bar3 * 100 : 0;
  const porcentaje2 = bar2 ? bar2 * 100 : 0;
  const porcentaje1 = bar1 ? bar1 * 100 : 0;
  const porcentaje = bar ? bar * 100 : 0;

  const _renderItem = ({ item }) => {
    return <ComentsUser data={item} />;
  };

  return (
    <View style={styles.container}>
      <View style={styles.reviewContainer}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={styles.title}>
          Opiniones de los clientes
        </CustomText>
        <View style={styles.totalWrap}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Star star={averageRating} />
          </View>
          <CustomText light={colors.rgb_153} dark={colors.rgb_153}>
            {averageRating} de 5
          </CustomText>
        </View>

        <CustomText
          light={colors.black}
          dark={colors.white}
          style={styles.amountText}>
          {data.Valoracion.length} Opiniones de los clientes
        </CustomText>

        <View style={{ marginTop: 40 }}>
          <View style={styles.spacer}>
            <PercentageBar starText="5" percentage={porcentaje4.toFixed(0)} />
          </View>
          <View style={styles.spacer}>
            <PercentageBar starText="4" percentage={porcentaje3.toFixed(0)} />
          </View>
          <View style={styles.spacer}>
            <PercentageBar starText="3" percentage={porcentaje2.toFixed(0)} />
          </View>
          <View style={styles.spacer}>
            <PercentageBar starText="2" percentage={porcentaje1.toFixed(0)} />
          </View>
          <View style={styles.spacer}>
            <PercentageBar starText="1" percentage={porcentaje.toFixed(0)} />
          </View>
        </View>

        <TouchableOpacity
          style={{ marginTop: 20 }}
          onPress={() => toggleModal()}>
          <CustomText
            light={colors.main}
            dark={colors.main}
            style={styles.howWeCalculate}>
            ¿Cómo calculamos las calificaciones?
          </CustomText>
        </TouchableOpacity>
      </View>

      <CustomText light={colors.black} dark={colors.white} style={styles.title}>
        Comentarios
      </CustomText>

      <Safe />
      <FlatList
        // @ts-ignore
        data={data.Valoracion}
        renderItem={(item) => _renderItem(item)}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
        ListEmptyComponent={
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 30,
            }}>
            <LottieView
              source={require('../../Assets/Animate/star.json')}
              autoPlay
              loop
              style={{ width: 250, height: 200 }}
            />
            <CustomText
              ligth={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryTextBold,
                { textAlign: 'center', color: colors.rgb_153, lineHeight: 20 },
              ]}>
              ¡Aún no hay opiniones para este establecimiento, sé el primero en
              hacer un pedido y comentar!
            </CustomText>
          </View>
        }
      />

      <Modal isVisible={isModalVisible}>
        <View style={styles.modalContent}>
          <LottieView
            source={require('../../Assets/Animate/star.json')}
            autoPlay
            loop
            style={{ width: 250, height: 200 }}
          />
          <CustomText
            ligth={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryTextBold,
              { textAlign: 'center', color: colors.rgb_153, lineHeight: 20 },
            ]}>
            Todas las opiniones son recopiladas de clientes que han realizado un
            pedido en éste restaurante, al finalizar el servicio, éste tiene la
            opción libre de valorar el servicio y la comida.
          </CustomText>

          <View style={{ marginTop: 40 }}>
            <Button
              title="Entendido"
              onPress={toggleModal}
              color={colors.main}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modalContent: {
    width: dimensions.Width(90),
    height: dimensions.Height(60),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
  },

  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: dimensions.Height(10),
  },
  reviewContainer: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    paddingHorizontal: 30,
    paddingVertical: 40,
    marginVertical: 30,
    minWidth: '94%',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 1.0,
    shadowRadius: 7,
    shadowColor: new DynamicValue('rgba(218, 218, 218, 0.5)', colors.black),
    elevation: 10,
  },

  spacer: {
    marginTop: 15,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  totalWrap: {
    marginTop: 20,
    marginBottom: 5,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  amountText: {
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10,
  },

  howWeCalculate: {
    fontSize: 15,
    textAlign: 'center',
  },
});

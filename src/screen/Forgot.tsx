import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  Alert,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { stylesText } from '../theme/TextStyle';
import { dimensions } from '../theme/dimension';
import Icon from 'react-native-dynamic-vector-icons';
import { colors } from '../theme';
import { Button } from '../Components/Button';
import { NETWORK_INTERFACE_URL } from '../Config/config';
import { validateEmail } from '../Utils/EmailsValidator';
import Toast from 'react-native-toast-message';
import { CustomTextInput } from '../Components/CustomInput';
import Navigation from '../services/Navigration';
import Loader from '../Components/ModalLoading';

const Forgot = () => {
  const [email, setEmail] = useState('');
  const [Loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const styles = useDynamicValue(dynamicStyles);

  const onReset = async () => {
    setLoading(true);
    if (!validateEmail(email)) {
      setIsError(true);
      setLoading(false);
      return null;
    } else {
      setIsError(false);
      if (email === '') {
        Toast.show({
          text1: 'Debes escribir tu Email',
          text2:
            'Para recuperar tu contraseña debes añadir tu email de Wilbby App®',
          position: 'top',
          type: 'info',
          topOffset: 50,
          visibilityTime: 4000,
        });

        setLoading(false);
        return null;
      } else {
        let res = await fetch(
          `${NETWORK_INTERFACE_URL}/forgotpassword?email=${email}`,
        );
        if (res) {
          const user = await res.json();

          console.log(user);
          if (!user.success) {
            setLoading(false);
            Toast.show({
              text1: 'No tenemos este email',
              text2: 'Aún no tenemos este email en Wilbby',
              position: 'top',
              type: 'info',
              topOffset: 50,
              visibilityTime: 4000,
            });
          } else {
            setLoading(false);
            Navigation.goBack();
          }
        } else {
          Toast.show({
            text1: 'No tenemos este email',
            text2: 'Aún no tenemos este email en Wilbby',
            position: 'top',
            type: 'info',
            topOffset: 50,
            visibilityTime: 4000,
          });
          setLoading(false);
        }
      }
    }
  };
  return (
    <View style={styles.container}>
      <Toast ref={(ref) => Toast.setRef(ref)} style={{ zIndex: 100 }} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity onPress={() => Navigation.goBack()}>
          <Icon
            name="arrow-left"
            type="Feather"
            size={32}
            style={styles.close}
          />
        </TouchableOpacity>

        <View style={styles.title}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.titleTextNoBold]}>
            Recuperar contraseña de Wilbby
          </CustomText>
        </View>
        <View style={styles.formView}>
          <CustomTextInput
            right
            rightIcon="mail"
            type="AntDesign"
            placeHolder="Correo electrónico"
            autoCompleteType="email"
            autoCapitalize="none"
            onChangeText={(clave: any) => setEmail(clave)}
          />
          {isError ? (
            <CustomText
              style={{ marginTop: 3 }}
              light={colors.ERROR}
              dark={colors.ERROR}>
              ¡Por favor introduce un email válido!
            </CustomText>
          ) : null}
          <View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryText,
                { marginTop: dimensions.Height(3) },
              ]}>
              Te enviaremos un enlace al Email con el que te has registrado en
              Wilbby para que reestablesca tu contraseña.
            </CustomText>
          </View>

          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => onReset()}
              title="Enviar enlace"
              titleStyle={styles.buttonTitle}
              loading={Loading}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },
  title: {
    marginTop: dimensions.Height(5),
    marginLeft: dimensions.Width(4),
  },

  close: {
    color: new DynamicValue('black', 'white'),
    marginTop: dimensions.Height(8),
    marginLeft: dimensions.Width(4),
  },

  formView: {
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(15),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  socialBTN: {
    width: dimensions.Width(90),
    height: 60,
    flexDirection: 'row',
    borderColor: colors.rgb_153,
    borderWidth: 1,
    borderRadius: dimensions.Width(1.5),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: dimensions.Height(3),
  },
});

export default Forgot;

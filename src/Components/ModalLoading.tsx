import React from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { BlurView, VibrancyView } from '@react-native-community/blur';

const ContainerView = Platform.select({
  ios: VibrancyView,
  //@ts-ignore
  android: BlurView,
});

const Loader = ({ loading = false, color, size, opacity = 0.4 }) => {
  return (
    <Modal
      transparent
      animationType={'none'}
      visible={loading}
      statusBarTranslucent={true}
      onRequestClose={() => null}>
      <View
        style={[
          styles.modalBackground,
          { backgroundColor: `rgba(0,0,0,${opacity})` },
        ]}>
        <ContainerView blurType="light" style={styles.activityIndicatorWrapper}>
          <ActivityIndicator
            animating={loading}
            color="gray"
            size={Platform.select({ ios: 'large', android: 'small' })}
          />
        </ContainerView>
      </View>
    </Modal>
  );
};

Loader.propTypes = {
  loading: PropTypes.bool.isRequired,
  color: PropTypes.string,
  size: PropTypes.string,
  opacity: (props, propName, componentName: any) => {
    if (props[propName] < 0 || props[propName] > 1) {
      return new Error('Opacity prop value out of range');
    }
  },
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activityIndicatorWrapper: {
    height: 100,
    width: 100,
    borderRadius: 10,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Loader;

import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { dimensions, colors, image, stylesText } from '../theme';

export default function AddCard(props) {
  const { onPress } = props;
  const styles = useDynamicValue(dynamicStyles);

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const modes = useColorSchemeContext();
  const borderColot = backgroundColors[modes];

  return (
    <TouchableOpacity
      style={[styles.cardinfo, { borderColor: borderColot }]}
      onPress={onPress}>
      <CustomText
        light={colors.back_dark}
        dark={colors.white}
        style={[stylesText.mainText, { fontWeight: 'bold' }]}>
        Añadir tarjeta
      </CustomText>
      <CustomText
        light={colors.rgb_153}
        dark={colors.rgb_153}
        style={[stylesText.secondaryText, { fontWeight: '200', marginTop: 5 }]}>
        Aceptamos todo tipo
      </CustomText>
      <Image
        source={image.Brand}
        style={{
          marginTop: 10,
          width: 200,
          resizeMode: 'contain',
          marginLeft: -15,
        }}
      />
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cardinfo: {
    width: dimensions.Width(92),
    padding: 15,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(1),
    marginBottom: dimensions.Height(1),
    borderRadius: 10,
    alignSelf: 'center',
    borderWidth: 0.5,
  },
});

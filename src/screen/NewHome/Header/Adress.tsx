import React, { useState, useEffect } from 'react';
import { Animated, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../../Components/CustomTetx';
import { dimensions, colors } from '../../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { stylesText } from '../../../theme/TextStyle';
import AsyncStorage from '@react-native-community/async-storage';
import { useTranslation } from 'react-i18next';
import Navigation from '../../../services/Navigration';
import * as Animatable from 'react-native-animatable';

export default function Search(props: any) {
  const [adressName, setadressName] = useState('');
  const { t } = useTranslation();
  const {
    opacity,
    id,
    city,
    lat,
    lgn,
    localeCode,
    currecy,
    setNotification,
    setcity,
    getRidersAvailable,
  } = props;
  const styles = useDynamicValue(dynamicStyles);

  const getAdress = async () => {
    const adressHome = await AsyncStorage.getItem('adressNameHome');
    const adress = await AsyncStorage.getItem('adressName');
    setadressName(adress ? adress : adressHome);
  };

  useEffect(() => {
    getAdress();
  }, [adressName]);

  const confiAdres = {
    fromHome: true,
    getAdreesID: getAdress,
    city: city,
    lat: lat,
    lgn: lgn,
    setcity: setcity,
    getRidersAvailable: getRidersAvailable,
  };

  return (
    <Animated.View style={[styles.adreesCont, { opacity: opacity }]}>
      <TouchableOpacity
        onPress={() => {
          if (id) {
            Navigation.navigate('Direccion', { data: confiAdres });
            setNotification();
            getRidersAvailable(city);
          } else {
            Navigation.navigate('Login', 0);
          }
        }}>
        <Animatable.View
          animation="bounceInDown"
          style={[styles.adrees]}
          duration={2000}
          iterationCount={1}>
          <Icon
            type="Feather"
            name="map-pin"
            size={16}
            color={colors.main}
            style={{ marginRight: 5 }}
          />
          <CustomText
            light={colors.black}
            dark={colors.rgb_153}
            numberOfLines={1}
            style={[
              stylesText.secondaryTextBold,
              { width: dimensions.Width(60) },
            ]}>
            {adressName ? adressName : `${t('home:currentLocation')}`}
          </CustomText>
          <Icon
            type="AntDesign"
            name="caretdown"
            size={12}
            color={colors.main}
            style={{ marginLeft: 5 }}
          />
        </Animatable.View>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.btns}
        onPress={() =>
          Navigation.navigate('Scaner', {
            data: {
              city: city,
              lat: lat,
              lgn: lgn,
              localeCode: localeCode,
              currecy: currecy,
            },
          })
        }>
        <Icon
          type="MaterialCommunityIcons"
          name="qrcode-scan"
          size={20}
          color={colors.rgb_153}
        />
      </TouchableOpacity>
    </Animated.View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  adrees: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  adreesCont: {
    width: dimensions.Width(92),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  btns: {
    width: 45,
    height: 45,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 10.27,
    elevation: 15,
  },
});

import React, { useState } from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Modal,
  Animated,
  Alert,
  Image,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions } from '../../theme';
import { colors, stylesText, image } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { customMaspStyles } from '../../Components/MapStyle';
import Icon from 'react-native-dynamic-vector-icons';
import { Button } from '../../Components/Button';
import { GOOGLE_API_KEY } from '../../Config/config';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import ExtraInfo from '../Adress/ExtraInfo';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const tipos = [
  {
    id: 1,
    cantidad: 'Casa',
    type: 'AntDesign',
    name: 'home',
  },
  {
    id: 2,
    cantidad: 'Trabajo',
    type: 'MaterialCommunityIcons',
    name: 'briefcase-outline',
  },
  {
    id: 3,
    cantidad: 'Mi churri',
    type: 'AntDesign',
    name: 'hearto',
  },

  {
    id: 3,
    cantidad: 'Otro',
    type: 'AntDesign',
    name: 'pushpino',
  },
];

const Direccion = (props: any) => {
  const { direccionModal, setDireccionModal, valueAdress, lgns, lats } = props;
  const styles = useDynamicValue(dynamicStyles);
  const [lat, setLat] = useState(lats);
  const [lgn, setLgn] = useState(lgns);
  const [isModalVisible, setModalVisible] = useState(false);
  const [type, setType] = useState('Casa');
  const [puertaPiso, setpuertaPiso] = useState('');
  const [selectAdr, setselectAdr] = useState(true);
  const [formatted_address, setFormatted_address] = useState('');
  const [heightAnimate, setheightAnimate] = useState(new Animated.Value(470));

  const stylos = useDynamicValue(customMaspStyles);

  const createUpdateAdress = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    if (formatted_address && puertaPiso) {
      const datos = {
        adress: formatted_address,
        lat: lat,
        lgn: lgn,
        type: type,
        puertaPiso: puertaPiso,
      };
      valueAdress(datos);
      setDireccionModal(false);
      setselectAdr(true);
    } else {
      Alert.alert('Debes añadir una dirección de recogida');
    }
  };

  const BorderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const borderColor = BorderColor[mode];

  const backgroundColors = {
    light: colors.colorInput,
    dark: colors.back_dark,
  };
  const modes = useColorSchemeContext();
  const backgroundColor = backgroundColors[modes];

  const textStyle = {
    container: {
      flex: 1,
    },
    textInputContainer: {
      flexDirection: 'row',
    },
    textInput: {
      backgroundColor: backgroundColor,
      height: 44,
      color: colors.rgb_102,
      borderRadius: 5,
      paddingVertical: 5,
      paddingHorizontal: 10,
      fontSize: 18,
      paddingLeft: 50,
      flex: 1,
    },
    poweredContainer: {
      justifyContent: 'flex-end',
      alignItems: 'center',
      borderBottomRightRadius: 5,
      borderBottomLeftRadius: 5,
      borderColor: borderColor,
      borderTopWidth: 0.5,
    },
    powered: {},
    listView: {},
    row: {
      backgroundColor: 'transparent',
      color: colors.white,
      flexDirection: 'row',
    },
    separator: {
      height: 0,
      backgroundColor: borderColor,
    },
    description: {
      color: colors.rgb_153,
      fontSize: 18,
    },
    loader: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      height: 20,
    },
  };

  return (
    <>
      <Modal
        animationType="slide"
        transparent={false}
        visible={direccionModal}
        statusBarTranslucent={true}
        onRequestClose={() => setDireccionModal(false)}>
        <View style={styles.centeredView}>
          {selectAdr ? (
            <View>
              <View style={styles.header}>
                <View
                  style={{
                    marginTop: 30,
                    flexDirection: 'row',
                    marginHorizontal: 20,
                    width: dimensions.Width(90),
                    marginLeft: 'auto',
                  }}>
                  <GooglePlacesAutocomplete
                    onFail={(error) => console.log('error' + error)}
                    placeholder="Buscar calle, ciudad, distrito..."
                    textInputProps={{ selectionColor: colors.main }}
                    currentLocation={true}
                    currentLocationLabel="Ubicación actual"
                    listViewDisplayed={false}
                    fetchDetails={true}
                    styles={textStyle}
                    onPress={(data, details = null) => {
                      setFormatted_address(data.description);
                      setselectAdr(false);
                      setLat(details.geometry.location.lat);
                      setLgn(details.geometry.location.lng);
                    }}
                    query={{
                      key: GOOGLE_API_KEY,
                      language: 'es',
                    }}
                  />
                </View>
                <TouchableOpacity
                  style={[styles.back, { marginTop: 30, marginLeft: 25 }]}
                  onPress={() => {
                    setDireccionModal(false), setselectAdr(true);
                  }}>
                  <Icon
                    name="arrow-left"
                    type="Feather"
                    size={24}
                    color={colors.main}
                  />
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <>
              <MapView
                showsUserLocation={true}
                provider={PROVIDER_GOOGLE}
                customMapStyle={stylos}
                style={{
                  height: dimensions.Height(50),
                  width: dimensions.ScreenWidth,
                }}
                region={{
                  latitude: lat,
                  longitude: lgn,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
                }}>
                <Marker
                  title={formatted_address}
                  coordinate={{
                    latitude: lat,
                    longitude: lgn,
                  }}>
                  <Image
                    source={image.Maping}
                    style={{ width: 50, height: 50 }}
                  />
                </Marker>
              </MapView>
              <TouchableOpacity
                style={styles.back}
                onPress={() => {
                  if (!selectAdr) {
                    setselectAdr(true);
                  } else {
                    setDireccionModal(false);
                  }
                  setheightAnimate(new Animated.Value(470));
                }}>
                <Icon
                  name="arrow-left"
                  type="Feather"
                  size={24}
                  color={colors.main}
                />
              </TouchableOpacity>
              <Animated.View
                style={[styles.fixDireccion, { height: heightAnimate }]}>
                <View style={styles.tap} />

                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 20,
                  }}>
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={stylesText.secondaryTextBold}>
                    Añade una dirección de recogida
                  </CustomText>
                </View>

                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 20,
                  }}>
                  <TouchableOpacity
                    style={styles.inputAdress}
                    activeOpacity={1}
                    onPress={() => setselectAdr(true)}>
                    <Icon
                      type="Feather"
                      name="search"
                      size={22}
                      color={colors.rgb_153}
                      style={{ marginRight: 10 }}
                    />
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={stylesText.mainText}>
                      Buscar calle, ciudad, distrito...
                    </CustomText>
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    marginTop: 30,
                    marginBottom: 20,
                    marginHorizontal: 20,
                  }}>
                  <View style={styles.location}>
                    <Icon
                      type="FontAwesome"
                      name="location-arrow"
                      size={25}
                      color={colors.main}
                    />
                  </View>
                  <View>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={[
                        stylesText.secondaryTextBold,
                        { width: dimensions.Width(76) },
                      ]}
                      numberOfLines={2}>
                      {formatted_address
                        ? formatted_address
                        : 'Buscar calle, ciudad, distrito...'}
                    </CustomText>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                        marginTop: 10,
                      }}>
                      <TouchableOpacity onPress={() => setModalVisible(true)}>
                        <CustomText
                          light={colors.main}
                          dark={colors.main}
                          style={[stylesText.secondaryText]}
                          numberOfLines={1}>
                          {puertaPiso
                            ? puertaPiso
                            : 'Añadir piso, puerta, etc...'}
                        </CustomText>
                      </TouchableOpacity>
                      <CustomText
                        light={colors.rgb_153}
                        dark={colors.rgb_153}
                        style={[stylesText.secondaryText, { marginLeft: 10 }]}
                        numberOfLines={1}>
                        {type}
                      </CustomText>
                    </View>
                  </View>
                </View>

                <View style={{ marginTop: 20 }}>
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}>
                    {tipos.map((t, i) => {
                      return (
                        <TouchableOpacity
                          onPress={() => setType(t.cantidad)}
                          style={[
                            styles.tipos,
                            {
                              borderColor: borderColor,
                              backgroundColor:
                                type === t.cantidad
                                  ? colors.main
                                  : 'transparent',
                            },
                          ]}
                          key={i}>
                          <Icon
                            name={t.name}
                            //@ts-ignore
                            type={t.type}
                            size={20}
                            color={
                              type === t.cantidad
                                ? colors.white
                                : colors.rgb_153
                            }
                            style={{ marginRight: 5 }}
                          />
                          <CustomText
                            light={
                              type === t.cantidad
                                ? colors.white
                                : colors.rgb_153
                            }
                            dark={
                              type === t.cantidad
                                ? colors.white
                                : colors.rgb_153
                            }
                            style={[stylesText.secondaryText]}>
                            {t.cantidad}
                          </CustomText>
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>
                </View>
                <View style={styles.signupButtonContainer}>
                  <Button
                    light={colors.white}
                    dark={colors.white}
                    containerStyle={[
                      styles.buttonView,
                      { backgroundColor: colors.main },
                    ]}
                    onPress={() => {
                      if (puertaPiso) {
                        createUpdateAdress();
                      } else {
                        setModalVisible(true);
                      }
                    }}
                    title="Confirmar dirección"
                    titleStyle={styles.buttonTitle}
                  />
                </View>
              </Animated.View>
              <ExtraInfo
                isModalVisible={isModalVisible}
                setModalVisible={setModalVisible}
                setpuertaPiso={setpuertaPiso}
                puertaPiso={puertaPiso}
              />
            </>
          )}
        </View>
      </Modal>
    </>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  inputAdress: {
    fontSize: 16,
    padding: 12,
    width: dimensions.Width(90),
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    color: new DynamicValue(colors.black, colors.white),
    flexDirection: 'row',
  },

  header: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: dimensions.Height(5),
  },

  listAdress: {
    width: dimensions.Width(95),
    paddingHorizontal: 15,
    height: 'auto',
    paddingVertical: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(1.5),
    marginBottom: dimensions.Height(1.5),
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  back: {
    marginLeft: 10,
    marginTop: dimensions.IsIphoneX() ? 60 : 40,
    marginBottom: 20,
    position: 'absolute',
    padding: 10,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
  },

  fixDireccion: {
    position: 'absolute',
    bottom: 0,
    borderTopStartRadius: 25,
    borderTopEndRadius: 25,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  tipos: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderWidth: 2,
    marginHorizontal: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  location: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(197,248,116,.3)',
    borderRadius: 100,
    marginRight: 10,
  },

  tap: {
    width: 60,
    height: 7,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    marginTop: 15,
    alignSelf: 'center',
    alignItems: 'center',
  },
});

export default Direccion;

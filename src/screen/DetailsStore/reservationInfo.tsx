import React from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import moment from 'moment';

export default function ReservationInfo(props) {
  const { open } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View
      style={{
        marginTop: 10,
        marginBottom: 5,
      }}>
      <CustomText
        ligth={colors.black}
        dark={colors.white}
        style={stylesText.secondaryText}>
        <Icon name="calendar" type="AntDesign" size={18} style={styles.icons} />{' '}
        Pídelo para mañana a las{' '}
        <CustomText
          ligth={colors.black}
          dark={colors.white}
          style={stylesText.secondaryTextBold}>
          {moment(open).format('LT')}
        </CustomText>
      </CustomText>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});

import React, { useState, useEffect } from 'react';
import { View, ScrollView, Share } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import Header from '../Components/Header';
import { dimensions } from '../theme';
import { colors } from '../theme';
import { stylesText } from '../theme/TextStyle';
import { Button } from '../Components/Button';
import Confetti from 'react-native-confetti';
import LottieView from 'lottie-react-native';
import source from '../Assets/Animate/gitf.json';

const Comparte = () => {
  const [_confettiView, set_confettiView] = useState(null);
  const styles = useDynamicValue(dynamicStyles);

  useEffect(() => {
    if (_confettiView) {
      _confettiView.startConfetti();
    }
  }, [_confettiView]);

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `¡Eh! Descárgate la app de Wilbby y utiliza mi código HOLAWILBBY para obtener un 10% de descuento para gastar en la app (promoción sujeta a condiciones). http://onelink.to/mf5tg4`,
        url: `http://onelink.to/mf5tg4`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <View style={styles.container}>
      <Confetti ref={(node) => set_confettiView(node)} />
      <Header title="Comparte y gana" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            alignSelf: 'center',
            marginTop: dimensions.Height(10),
            alignItems: 'center',
          }}>
          <LottieView
            source={source}
            autoPlay
            loop
            style={{ width: 250, height: 250 }}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={{
              textAlign: 'center',
              fontSize: dimensions.FontSize(24),
              fontWeight: '900',
              marginTop: 20,
              paddingHorizontal: 30,
              marginBottom: 15,
            }}>
            Consigue un 10% de descuento por cada amigo que traigas
          </CustomText>

          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.placeholderText,
              { paddingHorizontal: 20, textAlign: 'center' },
            ]}>
            Código promocional válido para un sólo uso por amigo, ciertas
            restricciones aplican.
          </CustomText>
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => onShare()}
              title="Compartir"
              titleStyle={styles.buttonTitle}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

export default Comparte;

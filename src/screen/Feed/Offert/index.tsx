import React from 'react';
import {
  FlatList,
  TouchableOpacity,
  Image,
  Alert,
  ImageBackground,
} from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { dimensions, image } from '../../../theme';
import Navigation from '../../../services/Navigration';
import { Query } from 'react-apollo';
import { query } from '../../../GraphQL';
import { validateDates } from '../../../Utils/CalculateHour';

export default function Offerts(props: any) {
  const styles = useDynamicValue(dynamicStyles);
  const { yo, city, lat, lgn, localeCode, currecy } = props;

  const renderItem = ({ item }) => {
    const dat = {
      id: item.store,
      yo: yo,
      autoshipping: false,
      city: city,
      lat: lat,
      lgn: lgn,
      localeCode: localeCode,
      currecy: currecy,
    };

    const a = item.apertura;
    const c = item.cierre;

    const isOK = () => {
      if (validateDates(a, 0, c, 0) && item.open) {
        return true;
      }
      return false;
    };

    const NavToDetails = () => {
      if (isOK() && props.city) {
        Navigation.navigate('DestailsStore', { data: dat });
      } else if (!props.city) {
        if (props.SetUbicacion) {
          props.SetUbicacion();
        }
        Alert.alert(
          'Tienda no disponible',
          'Estamos fuera de servicio de momento, volveremos pronto',
          [
            {
              text: 'Ok',
              onPress: () => console.log('calcel'),
            },
          ],

          { cancelable: false },
        );
      } else {
        Alert.alert(
          'Tienda cerrada',
          'Este establecimiento está cerrado te avisaremos cuando esté disponible',
          [
            {
              text: 'Ok',
              onPress: () => console.log('calcel'),
            },
          ],

          { cancelable: false },
        );
      }
    };

    return (
      <TouchableOpacity
        style={styles.kitchen}
        onPress={() => NavToDetails()}
        activeOpacity={100}>
        <ImageBackground
          imageStyle={{ borderRadius: 10 }}
          style={{
            flex: 1,
            width: dimensions.Width(96),
            height: 200,
            borderRadius: 10,
          }}
          resizeMode="cover"
          source={image.PlaceHolder}>
          <Image
            source={{ uri: item.imagen }}
            style={{
              flex: 1,
              width: dimensions.Width(96),
              height: 200,
              resizeMode: 'cover',
              borderRadius: 10,
            }}
          />
        </ImageBackground>
      </TouchableOpacity>
    );
  };

  return (
    <Query query={query.OFFERT} variables={{ city: city }}>
      {(response) => {
        if (response.loading) {
          return null;
        }
        if (response.error) {
          return null;
        }
        if (response) {
          response.refetch();
          const data =
            response && response.data && response.data.getOfferts
              ? response.data.getOfferts.data
              : [];
          return (
            <FlatList
              data={data}
              renderItem={(item: any) => renderItem(item)}
              keyExtractor={(item: any) => item.id}
              horizontal={true}
              onEndReachedThreshold={0}
              showsHorizontalScrollIndicator={false}
            />
          );
        }
      }}
    </Query>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  kitchen: {
    marginBottom: 30,
    marginHorizontal: 5,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

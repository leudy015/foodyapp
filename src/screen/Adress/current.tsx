import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions } from '../../theme';
import { colors, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';

export default function CurrentLocation(props: any) {
  const styles = useDynamicValue(dynamicStyles);
  const [adressName, setadressName] = useState('');
  const { setDireccionModal } = props;

  const getAdress = async () => {
    const adressHome = await AsyncStorage.getItem('adressNameHome');
    setadressName(adressHome);
  };

  useEffect(() => {
    getAdress();
  }, [adressName]);

  return (
    <TouchableOpacity
      onPress={() => setDireccionModal(true)}
      style={[styles.listAdress]}>
      <View style={styles.main}>
        <View>
          <Icon
            name="navigation"
            type="Feather"
            size={30}
            color={colors.main}
          />
        </View>
        <View style={styles.titles}>
          <View style={{ width: dimensions.Width(90), paddingRight: 30 }}>
            <CustomText
              numberOfLines={2}
              light={colors.black}
              dark={colors.white}
              style={stylesText.mainText}>
              Ubicación actual
            </CustomText>
            <CustomText
              numberOfLines={1}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginTop: 5 }]}>
              {adressName ? adressName : 'Ubicación actual'}
            </CustomText>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  inputAdress: {
    height: 'auto',
    fontSize: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    color: new DynamicValue(colors.black, colors.white),
  },

  header: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  back: {
    marginLeft: 10,
    marginTop: 30,
    marginBottom: 20,
    position: 'absolute',
    padding: 10,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
  },

  main: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  titles: {
    width: dimensions.Width(80),
    marginLeft: 15,
    borderBottomWidth: 0.5,
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    paddingBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  editButtom: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    padding: 15,
    borderRadius: 100,
  },

  listAdress: {
    width: dimensions.Width(100),
    paddingHorizontal: 15,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: dimensions.Height(1.5),
    marginBottom: dimensions.Height(1.5),
    flexDirection: 'row',
    alignSelf: 'center',
  },

  fixDireccion: {
    position: 'absolute',
    bottom: 0,
    borderTopStartRadius: 25,
    borderTopEndRadius: 25,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  tipos: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderWidth: 2,
    marginHorizontal: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: 100,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },
});

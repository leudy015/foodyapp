import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { dimensions, colors } from '../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { stylesText } from '../theme/TextStyle';

const HeaderModal = (props: any) => {
  const styles = useDynamicValue(dynamicStyles);

  return (
    <View style={styles.herderModal}>
      <CustomText
        light={colors.black}
        dark={colors.white}
        style={[
          stylesText.mainText,
          {
            marginLeft: 15,
          },
        ]}>
        {props.title}
      </CustomText>
      <TouchableOpacity
        style={styles.noty}
        onPress={() => props.OnClosetModal()}>
        <CustomText>
          <Icon
            name={props.icon ? props.icon : 'close'}
            type="AntDesign"
            size={24}
            style={styles.close}
          />
        </CustomText>
      </TouchableOpacity>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  herderModal: {
    width: dimensions.Width(100),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: 90,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: new DynamicValue(colors.colorBorder, colors.back_dark),
    borderBottomWidth: 1,
    paddingTop: 20,
  },

  noty: {
    marginLeft: 'auto',
    marginRight: dimensions.Width(4),
    backgroundColor: new DynamicValue('white', 'black'),
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },
  close: {
    color: new DynamicValue('black', 'white'),
    marginTop: dimensions.Height(8),
    marginLeft: dimensions.Width(4),
  },
});

export default HeaderModal;

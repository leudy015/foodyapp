import gql from 'graphql-tag';

export const AUTENTICAR_USUARIO = gql`
  mutation autenticarUsuario(
    $email: String!
    $password: String!
    $input: deviceInfo
  ) {
    autenticarUsuario(email: $email, password: $password, input: $input) {
      success
      message
      data {
        token
        id
        verifyPhone
        user
      }
    }
  }
`;

const ACTUALIZAR_USUARIO = gql`
  mutation actualizarUsuario($input: ActualizarUsuarioInput) {
    actualizarUsuario(input: $input) {
      _id
      name
      lastName
      city
      email
      avatar
    }
  }
`;

const ACTUALIZAR_RIDER = gql`
  mutation actualizarRiders($input: ActualizarUsuarioInput) {
    actualizarRiders(input: $input) {
      _id
      name
      lastName
      city
      email
      avatar
      rating
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

const ELIMINAR_USUARIO = gql`
  mutation eliminarUsuario($id: ID!) {
    eliminarUsuario(id: $id) {
      success
      messages
    }
  }
`;

const ANADIR_RESTAURANT_FAVORITE = gql`
  mutation crearFavorito($restaurantID: ID, $usuarioId: ID) {
    crearFavorito(restaurantID: $restaurantID, usuarioId: $usuarioId) {
      success
      messages
    }
  }
`;

const ELIMINAR_RESTAURANT_FAVORITE = gql`
  mutation eliminarFavorito($id: ID) {
    eliminarFavorito(id: $id) {
      success
      messages
    }
  }
`;

const CREAR_VALORACIONES = gql`
  mutation crearValoracion($input: ValoracionCreationInput) {
    crearValoracion(input: $input) {
      id
      comment
      value
      restaurant
      user
    }
  }
`;

const CREATE_OPINIO = gql`
  mutation createOpinion($input: OpinionInput) {
    createOpinion(input: $input) {
      messages
      success
    }
  }
`;

const CREATE_ADRESS = gql`
  mutation createAdress($input: adreesUser) {
    createAdress(input: $input) {
      messages
      success
      data {
        id
        formatted_address
        puertaPiso
        type
        usuario
        lat
        lgn
      }
    }
  }
`;

const EDIT_ADRESS = gql`
  mutation actualizarAdress($input: adreesUserEdit) {
    actualizarAdress(input: $input) {
      messages
      success
      data {
        id
        formatted_address
        puertaPiso
        type
        usuario
        lat
        lgn
      }
    }
  }
`;

const DELETE_ADRESS = gql`
  mutation eliminarAdress($id: ID) {
    eliminarAdress(id: $id) {
      messages
      success
    }
  }
`;

const CREATE_CUSTOM_ORDER = gql`
  mutation createCustonOrder($input: CustonOrderInput) {
    createCustonOrder(input: $input) {
      success
      messages
      data {
        id
        display_id
        riders
        Riders {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        origin {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        destination {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        schedule
        distance
        nota
        date
        city
        userID
        estado
        status
        progreso
        created_at
        total
        product_stimate_price
      }
    }
  }
`;

const CUSTOM_ORDER_PROCESS = gql`
  mutation actualizarOrderProcess($input: CustonOrderInput) {
    actualizarOrderProcess(input: $input) {
      success
      messages
      data {
        id
        display_id
        riders
        Riders {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        origin {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        destination {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        schedule
        distance
        nota
        date
        city
        userID
        estado
        status
        progreso
        created_at
        total
        product_stimate_price
      }
    }
  }
`;

export const mutations = {
  ACTUALIZAR_USUARIO,
  UPLOAD_FILE,
  ELIMINAR_USUARIO,
  AUTENTICAR_USUARIO,
  ANADIR_RESTAURANT_FAVORITE,
  ELIMINAR_RESTAURANT_FAVORITE,
  CREAR_VALORACIONES,
  CREATE_OPINIO,
  CREATE_ADRESS,
  EDIT_ADRESS,
  DELETE_ADRESS,
  CREATE_CUSTOM_ORDER,
  ACTUALIZAR_RIDER,
  CUSTOM_ORDER_PROCESS,
};

import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { dimensions, colors, stylesText } from '../../theme';
import { CustomText } from '../CustomTetx';
import UserAvatar from 'react-native-user-avatar';
import moment from 'moment';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../../Config/config';
import Stars from '../star';
import Icon from 'react-native-dynamic-vector-icons';

export const ComentsUser = (props: any) => {
  const { data } = props;
  const [line, setline] = useState(3);
  const styles = useDynamicValue(dynamicStyles);

  let consideracion = '';
  switch (data.value) {
    case 0:
      consideracion = 'Sin opinión';
      break;
    case 1:
      consideracion = 'Mala';
      break;
    case 2:
      consideracion = 'Regular';
      break;
    case 3:
      consideracion = 'Buena';
      break;
    case 4:
      consideracion = 'Muy buena';
      break;
    case 5:
      consideracion = 'Excelente';
      break;
  }

  return (
    <View style={styles.container}>
      <View style={styles.user}>
        <View style={{ width: 50, height: 50 }}>
          <UserAvatar
            size={50}
            bgColors={[colors.secundary, colors.orange1, colors.main]}
            name={`${data.Usuario.name} ${data.Usuario.lastName}`}
            src={NETWORK_INTERFACE_LINK_AVATAR + data.Usuario.avatar}
          />
        </View>
        <View style={{ marginLeft: 15, width: dimensions.Width(70) }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Stars star={data.value} />
            <CustomText
              numberOfLines={line}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { marginLeft: 10 }]}>
              {data.value} de 5 · {consideracion}
            </CustomText>
            <Icon
              name="verified"
              type="Octicons"
              size={12}
              color={colors.main}
              style={{ marginLeft: 10 }}
            />
          </View>
          <CustomText
            numberOfLines={line}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              { paddingTop: 10, lineHeight: 18 },
            ]}>
            {data.comment ? data.comment : 'No ha dejado comentario'}
          </CustomText>

          {data.comment ? (
            <TouchableOpacity
              style={{ marginVertical: 10 }}
              onPress={() => {
                if (line > 3) {
                  setline(3);
                } else {
                  setline(20);
                }
              }}>
              <CustomText light={colors.main} dark={colors.main}>
                {line > 3 ? 'Ver menos' : 'Ver más'}
              </CustomText>
            </TouchableOpacity>
          ) : null}

          <View style={{ flexDirection: 'row' }}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryText,
                {
                  paddingTop: 10,
                  lineHeight: 18,
                  width: 'auto',
                  maxWidth: dimensions.Width(35),
                },
              ]}>
              {data.Usuario.name} {data.Usuario.lastName}
            </CustomText>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryText,
                { paddingTop: 10, lineHeight: 18, marginHorizontal: 15 },
              ]}>
              ·
            </CustomText>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryText,
                { paddingTop: 10, lineHeight: 18 },
              ]}>
              {moment(data.created_at).fromNow()}
            </CustomText>
          </View>
        </View>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    marginHorizontal: 15,
    marginVertical: 20,
    width: dimensions.Width(94),
    height: 'auto',
  },

  user: {
    flexDirection: 'row',
  },
});

import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { dimensions, colors } from '../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { stylesText } from '../theme/TextStyle';

const HeaderModal = (props: any) => {
  const styles = useDynamicValue(dynamicStyles);

  return (
    <View style={styles.herderModal}>
      <CustomText
        light={colors.black}
        dark={colors.white}
        style={[
          stylesText.mainText,
          {
            marginLeft: 15,
          },
        ]}>
        {props.title}
      </CustomText>
      <TouchableOpacity
        style={styles.noty}
        onPress={() => props.OnClosetModal()}>
        <CustomText
          light={colors.main}
          dark={colors.main}
          style={[stylesText.secondaryTextBold, { marginTop: 3 }]}>
          Añadir
        </CustomText>
      </TouchableOpacity>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  herderModal: {
    width: dimensions.Width(100),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: 90,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: new DynamicValue(colors.colorBorder, colors.back_dark),
    borderBottomWidth: 1,
    paddingTop: 20,
  },

  noty: {
    marginLeft: 'auto',
    marginRight: dimensions.Width(4),
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
    width: 120,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  close: {
    color: new DynamicValue('black', 'white'),
    marginTop: dimensions.Height(8),
    marginLeft: dimensions.Width(4),
  },
});

export default HeaderModal;

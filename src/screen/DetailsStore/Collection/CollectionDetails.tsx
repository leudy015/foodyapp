import React, { useState } from 'react';
import { View, ScrollView, RefreshControl } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Header from '../../../Components/Header';
import { dimensions } from '../../../theme';
import { colors } from '../../../theme';
import { useNavigationParam } from 'react-navigation-hooks';
import { newQuery } from '../../../GraphQL';
import { Query } from 'react-apollo';
import Loadings from '../../../Components/PlaceHolder/OrderLoading';
import ProductCard from '../../../Components/NewCardProduct';
import Paginacion from '../../../Components/Pagination';

const CollectionDetails = (props) => {
  const {
    collection,
    store,
    localeCode,
    currecy,
    inCartItem,
    setinCartItem,
    setModalVisible,
  } = props;
  const [refreshing, setRefreshing] = useState(false);
  const styles = useDynamicValue(dynamicStyles);
  const [page, setpage] = useState(1);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  return (
    <View style={styles.container}>
      <Header
        title={collection.title}
        fromSearch={true}
        setModalVisible={setModalVisible}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        scrollsToTop={true}
        snapToEnd={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }>
        <Query
          query={newQuery.GET_PRODUCT}
          variables={{ id: collection._id, page: page, limit: 20 }}>
          {(response) => {
            if (response.loading) {
              return <Loadings />;
            }
            if (response) {
              const product =
                response && response.data && response.data.getProducto
                  ? response.data.getProducto.data
                  : [];
              response.refetch();
              return (
                <View
                  style={{
                    marginBottom: dimensions.Height(10),
                    marginTop: 30,
                    justifyContent: 'center',
                    alignSelf: 'center',
                    alignItems: 'center',
                  }}>
                  <ProductCard
                    datosProducto={product}
                    storeID={store._id}
                    search={collection.title}
                    localeCode={localeCode}
                    currecy={currecy}
                    inCartItem={inCartItem}
                    setinCartItem={setinCartItem}
                  />
                  <View style={styles.cont}>
                    <Paginacion
                      page={page}
                      onPressMas={() => setpage(page + 1)}
                      onPressMenos={() => {
                        if (page === 1) {
                          null;
                        } else {
                          setpage(page - 1);
                        }
                      }}
                    />
                  </View>
                </View>
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
    marginBottom: dimensions.Height(3),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(100),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(20),
  },

  cat: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderWidth: 2,
    marginHorizontal: 10,
    borderRadius: 10,
  },

  cont: {
    marginTop: 30,
    marginBottom: 40,
    width: dimensions.ScreenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});

export default CollectionDetails;

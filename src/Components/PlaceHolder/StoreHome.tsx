import React from 'react';
import { View } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions } from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  return (
    <View style={{ alignSelf: 'center', backgroundColor: 'transparent' }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            marginHorizontal: 20,
            marginBottom: 20,
            marginTop: 10,
          }}>
          <View style={{ flexDirection: 'row' }}>
            <View>
              <View
                style={{
                  width: dimensions.Width(43),
                  height: dimensions.IsIphoneX()
                    ? dimensions.Height(15)
                    : dimensions.Height(17),
                  borderRadius: 15,
                  marginHorizontal: 15,
                  marginVertical: 15,
                }}
              />

              <View
                style={{
                  width: dimensions.Width(30),
                  height: 15,
                  borderRadius: 15,
                  marginHorizontal: 20,
                  marginVertical: 3,
                }}
              />
            </View>
            <View>
              <View
                style={{
                  width: dimensions.Width(43),
                  height: dimensions.IsIphoneX()
                    ? dimensions.Height(15)
                    : dimensions.Height(17),
                  borderRadius: 15,
                  marginHorizontal: 15,
                  marginVertical: 15,
                }}
              />

              <View
                style={{
                  width: dimensions.Width(30),
                  height: 15,
                  borderRadius: 15,
                  marginHorizontal: 20,
                  marginVertical: 3,
                }}
              />
            </View>
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

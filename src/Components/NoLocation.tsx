import React from 'react';
import { View, Platform, Alert } from 'react-native';
import LottieView from 'lottie-react-native';
import source from '../Assets/Animate/location.json';
import { openSettings } from 'react-native-permissions';
import { colors, dimensions, stylesText } from '../theme';
import { CustomText } from './CustomTetx';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { Button } from './Button';
import Geolocation from '@react-native-community/geolocation';
import { SetCity } from '../Utils/getCiti';
import RNLocation from 'react-native-location';

export default function NoLocation(props: any) {
  const { setcity, setisLocation } = props;
  const styles = useDynamicValue(dynamicStyles);

  const alertOpen = () => {
    Alert.alert(
      'Activa tu ubicación',
      'Para mostrarte restaurantes y tiendas cerca de ti necesitamos que active tu ubicación, no la utilzamos para rastrearte.',
      [
        {
          text: 'Ir a los ajustes',
          onPress: () => openSettings(),
        },
        {
          text: 'Cancelar',
          onPress: () => {},
        },
      ],
      { cancelable: false },
    );
  };

  const ope = () => {
    RNLocation.checkPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
      },
    }).then((per) => {
      if (!per) {
        alertOpen();
      } else {
        setisLocation(true);
        Geolocation.getCurrentPosition((info) => {
          SetCity(info.coords.latitude, info.coords.longitude, setcity);
        });
      }
    });
  };

  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 30,
        marginTop: Platform.select({
          ios: 30,
          android: 0,
        }),
      }}>
      <LottieView
        source={source}
        autoPlay
        loop
        style={{
          width: Platform.select({
            ios: 200,
            android: 150,
          }),
          marginBottom: 20,
        }}
      />
      <CustomText
        ligth={colors.black}
        dark={colors.white}
        style={[stylesText.titleText2, { textAlign: 'center' }]}>
        Para mostrarte restaurantes y tiendas cerca de ti necesitamos que active
        tu ubicación, no la utilzamos para rastrearte.
      </CustomText>
      <View style={styles.signupButtonContainer}>
        <Button
          light={colors.white}
          dark={colors.white}
          containerStyle={[styles.buttonView, { backgroundColor: colors.main }]}
          onPress={() => ope()}
          title="Continuar"
          titleStyle={styles.buttonTitle}
        />
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  signupButtonContainer: {
    marginTop: Platform.select({
      ios: dimensions.Height(5),
      android: dimensions.Height(2),
    }),
    alignSelf: 'center',
    width: dimensions.Width(70),
  },
  buttonView: {
    width: dimensions.Width(70),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

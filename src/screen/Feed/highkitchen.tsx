import React from 'react';
import { TouchableOpacity, ImageBackground, View } from 'react-native';
import Navigation from '../../services/Navigration';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions, stylesText } from '../../theme';
import { useTranslation } from 'react-i18next';
import { CustomText } from '../../Components/CustomTetx';
const AltaCocina = require('./images/altaCocina.png');

export default function Highkitchen(props: any) {
  const {
    id,
    city,
    lat,
    lgn,
    localeCode,
    currecy,
    isLocation,
    setNotification,
    riders,
  } = props;
  const { t } = useTranslation();
  const styles = useDynamicValue(dynamicStyles);

  const dataNav = {
    id: id,
    city: city,
    lat: lat,
    lgn: lgn,
    localeCode: localeCode,
    currecy: currecy,
    isLocation: isLocation,
    riders: riders,
  };
  return (
    <TouchableOpacity
      style={styles.kitchen}
      onPress={() => {
        Navigation.navigate('Highkitchen', { data: dataNav });
        if (setNotification) {
          setNotification();
        }
      }}
      activeOpacity={100}>
      <ImageBackground
        source={AltaCocina}
        style={[styles.kitchen, { justifyContent: 'flex-start' }]}
        imageStyle={{ borderRadius: 20, resizeMode: 'cover' }}
        resizeMode="cover">
        <View
          style={{
            alignItems: 'flex-end',
            justifyContent: 'flex-start',
            flexDirection: 'row',
            marginBottom: 15,
            marginLeft: 15,
          }}>
          <CustomText
            light={colors.white}
            dark={colors.white}
            style={[stylesText.secondaryTextBold, { letterSpacing: 8 }]}>
            {t('highkitchen:highkitchen')}
          </CustomText>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  kitchen: {
    width: dimensions.Width(94),
    height: 140,
    marginBottom: 30,
    marginHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

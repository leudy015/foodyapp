import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import SegmentedControl from '@react-native-community/segmented-control';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import MyOrder from './MyOrders';
import Express from './express';
import { newQuery } from '../../GraphQL';
import { Query } from 'react-apollo';
import Loading from '../../Components/PlaceHolder/OrderLoading';
import InAppReview from 'react-native-in-app-review';

export default function OrderCompnment(props: any) {
  const { city, id, lat, lgn, localeCode, currecy, riders } = props;
  const styles = useDynamicValue(dynamicStyles);
  const [active, setActive] = useState(0);

  useEffect(() => {
    if (InAppReview.isAvailable()) {
      InAppReview.RequestInAppReview();
    }
  }, []);

  return (
    <View style={styles.container}>
      <View
        style={{
          marginHorizontal: dimensions.Width(2),
          marginTop: dimensions.Height(1),
          marginBottom: dimensions.Height(2),
        }}>
        <SegmentedControl
          values={['Nuevos pedidos', 'Envíos express', 'Anteriores']}
          selectedIndex={active}
          onChange={(event) => {
            setActive(event.nativeEvent.selectedSegmentIndex);
          }}
        />
      </View>
      <View>
        {active === 0 ? (
          <Query
            query={newQuery.GET_ORDER}
            variables={{
              userId: id,
              status: [
                'Nueva',
                'Confirmada',
                'En la cocina',
                'Listo para recoger',
                'Preparando para el envío',
                'En camino',
                'Entregada',
              ],
            }}>
            {(response: any) => {
              if (response.loading) {
                return <Loading />;
              }
              if (response) {
                const respuesta =
                  response && response.data && response.data.getNewOrder
                    ? response.data.getNewOrder.list
                    : [];
                return (
                  <MyOrder
                    id={id}
                    refetch={response.refetch}
                    respuesta={respuesta}
                    lat={lat}
                    lgn={lgn}
                    city={city}
                    localeCode={localeCode}
                    currecy={currecy}
                    riders={riders}
                  />
                );
              }
            }}
          </Query>
        ) : null}
        {active === 1 ? (
          <Express localeCode={localeCode} currecy={currecy} />
        ) : null}
        {active === 2 ? (
          <Query
            query={newQuery.GET_ORDER}
            variables={{
              userId: id,
              status: [
                'Rechazado',
                'Rechazada por la tienda',
                'Rechazada por el rider',
                'Devuelto',
                'Finalizada',
                'Resolución',
              ],
            }}>
            {(response: any) => {
              if (response.loading) {
                return <Loading />;
              }
              if (response) {
                const respuesta =
                  response && response.data && response.data.getNewOrder
                    ? response.data.getNewOrder.list
                    : [];
                return (
                  <MyOrder
                    id={id}
                    refetch={response.refetch}
                    respuesta={respuesta}
                    lat={lat}
                    lgn={lgn}
                    city={city}
                    localeCode={localeCode}
                    currecy={currecy}
                    riders={riders}
                  />
                );
              }
            }}
          </Query>
        ) : null}
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});

import gql from 'graphql-tag';

const GET_NEW_PRODUCT_SEARCH = gql`
  query getNewProductoSearch(
    $store: String
    $search: String
    $page: Int
    $limit: Int
    $products: [String]
  ) {
    getNewProductoSearch(
      store: $store
      search: $search
      page: $page
      limit: $limit
      products: $products
    ) {
      messages
      success
      data {
        _id
        name
        description
        account
        location
        productType
        plu
        price
        sortOrder
        isOpen
        deliveryTax
        takeawayTax
        multiply
        multiMax
        posProductId
        posProductCategoryId
        subProducts
        productTags
        posCategoryIds
        imageUrl
        max
        min
        capacityUsages
        parentId
        visible
        snoozed
        recomended
        quantity
        new
        popular
        offert
        previous_price
        subProductSortOrder
        internalId
        cartItems {
          _id
          userId
          storeId
          productId
          items
          addToCart
        }
      }
    }
  }
`;

const GET_NEW_PRODUCT_APP = gql`
  query getNewProductoForApp($products: [String], $storeID: ID) {
    getNewProductoForApp(products: $products, storeID: $storeID) {
      messages
      success
      data {
        _id
        name
        description
        account
        location
        productType
        plu
        price
        sortOrder
        isOpen
        deliveryTax
        takeawayTax
        multiply
        multiMax
        posProductId
        posProductCategoryId
        subProducts
        productTags
        posCategoryIds
        imageUrl
        max
        min
        capacityUsages
        parentId
        visible
        snoozed
        recomended
        quantity
        new
        popular
        offert
        previous_price
        subProductSortOrder
        internalId
        cartItems {
          _id
          userId
          storeId
          productId
          items
          addToCart
        }
      }
    }
  }
`;

const GET_BUNDLED = gql`
  query getBundled($products: [String], $storeID: ID) {
    getBundled(products: $products, storeID: $storeID) {
      messages
      success
      data {
        _id
        name
        description
        account
        location
        productType
        plu
        price
        sortOrder
        deliveryTax
        takeawayTax
        multiply
        posProductId
        posProductCategoryId
        subProducts
        productTags
        posCategoryIds
        imageUrl
        max
        min
        recomended
        quantity
        capacityUsages
        parentId
        visible
        snoozed
        subProductSortOrder
        internalId
        Products {
          _id
          name
          description
          account
          location
          productType
          plu
          price
          sortOrder
          deliveryTax
          takeawayTax
          multiply
          multiMax
          posProductId
          posProductCategoryId
          subProducts
          productTags
          posCategoryIds
          imageUrl
          max
          min
          capacityUsages
          parentId
          visible
          recomended
          quantity
          snoozed
          subProductSortOrder
          internalId
        }
      }
    }
  }
`;

const GET_MODIFIELD = gql`
  query getModifieldGroup($products: [String], $storeID: ID) {
    getModifieldGroup(products: $products, storeID: $storeID) {
      messages
      success
      data {
        _id
        name
        description
        account
        location
        productType
        plu
        price
        sortOrder
        deliveryTax
        takeawayTax
        multiply
        multiMax
        posProductId
        posProductCategoryId
        subProducts
        productTags
        posCategoryIds
        imageUrl
        max
        min
        recomended
        quantity
        capacityUsages
        parentId
        visible
        snoozed
        subProductSortOrder
        internalId
        Modifiers {
          _id
          name
          description
          account
          location
          productType
          plu
          price
          sortOrder
          deliveryTax
          takeawayTax
          multiply
          multiMax
          posProductId
          posProductCategoryId
          subProducts
          productTags
          posCategoryIds
          imageUrl
          max
          min
          recomended
          quantity
          capacityUsages
          parentId
          visible
          snoozed
          subProductSortOrder
          internalId
          ModifierGroups {
            _id
            name
            description
            account
            location
            productType
            plu
            price
            sortOrder
            deliveryTax
            takeawayTax
            multiply
            multiMax
            posProductId
            posProductCategoryId
            subProducts
            productTags
            posCategoryIds
            imageUrl
            max
            min
            recomended
            quantity
            capacityUsages
            parentId
            visible
            snoozed
            subProductSortOrder
            internalId
            Modifiers {
              _id
              name
              description
              account
              location
              productType
              plu
              price
              sortOrder
              deliveryTax
              takeawayTax
              multiply
              multiMax
              posProductId
              posProductCategoryId
              subProducts
              productTags
              posCategoryIds
              imageUrl
              max
              min
              recomended
              quantity
              capacityUsages
              parentId
              visible
              snoozed
              subProductSortOrder
              internalId
            }
          }
        }
      }
    }
  }
`;

const GET_CART = gql`
  query getNewCart($userId: String, $storeId: String) {
    getNewCart(userId: $userId, storeId: $storeId) {
      message
      success
      list {
        _id
        userId
        storeId
        productId
        items
        addToCart
      }
    }
  }
`;

const GET_ORDER = gql`
  query getNewOrder($userId: ID, $status: [String]) {
    getNewOrder(userId: $userId, status: $status) {
      message
      success
      list {
        _id
        channelOrderDisplayId
        orderType
        pickupTime
        estimatedPickupTime
        deliveryTime
        courier
        tip
        scheduled
        created_at
        courierData {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          OnesignalID
          rating
        }
        customerData {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          OnesignalID
        }
        customer
        store
        storeData
        deliveryAddressData {
          _id
          formatted_address
          puertaPiso
          city
          postalcode
          type
          usuario
          lat
          lgn
        }
        deliveryAddress
        orderIsAlreadyPaid
        payment
        note
        items
        numberOfCustomers
        deliveryCost
        serviceCharge
        discountTotal
        IntegerValue
        paymentMethod
        statusProcess
        Needcutlery
        status
        invoiceUrl
      }
    }
  }
`;

const GET_PRODUCT = gql`
  query getProducto($id: String, $page: Int, $limit: Int) {
    getProducto(id: $id, page: $page, limit: $limit) {
      messages
      success
      data {
        _id
        name
        description
        account
        location
        productType
        plu
        price
        sortOrder
        deliveryTax
        takeawayTax
        multiply
        multiMax
        posProductId
        posProductCategoryId
        subProducts
        productTags
        posCategoryIds
        imageUrl
        max
        min
        capacityUsages
        parentId
        visible
        snoozed
        recomended
        quantity
        new
        popular
        offert
        previous_price
        subProductSortOrder
        internalId
        cartItems {
          _id
          userId
          storeId
          productId
          items
          addToCart
        }
      }
    }
  }
`;

const GET_PRIVATE_PROMO_CODE = gql`
  query getPrivatePromocode($id: String) {
    getPrivatePromocode(id: $id) {
      messages
      success
      data {
        id
        clave
        descuento
        tipo
        usage
        expire
        exprirable
        user
        description
        private
        uniqueStore
        store
        Store
        Usuario
        canjeado
        vencido
        city
        isShipping
      }
    }
  }
`;

const GET_PROMO_CODE = gql`
  query getPromocode($city: String) {
    getPromocode(city: $city) {
      messages
      success
      data {
        id
        clave
        descuento
        tipo
        usage
        expire
        exprirable
        user
        description
        private
        uniqueStore
        store
        Store
        Usuario
        canjeado
        vencido
        city
        isShipping
      }
    }
  }
`;

const GET_RIDER = gql`
  query getRiderForAdmin($city: String) {
    getRiderForAdmin(city: $city) {
      messages
      success
      data {
        _id
        name
        lastName
        email
        avatar
        city
        telefono
        created_at
        updated_at
        termAndConditions
        verifyPhone
        StripeID
        OnesignalID
        rating
      }
    }
  }
`;

export const newQuery = {
  GET_NEW_PRODUCT_SEARCH,
  GET_CART,
  GET_ORDER,
  GET_PRODUCT,
  GET_PRIVATE_PROMO_CODE,
  GET_PROMO_CODE,
  GET_NEW_PRODUCT_APP,
  GET_RIDER,
  GET_BUNDLED,
  GET_MODIFIELD,
};

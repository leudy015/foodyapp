import React, { PureComponent } from 'react';
import { KeyboardAvoidingView, View, Platform, StyleSheet } from 'react-native';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { NETWORK_INTERFACE_URL, STRIPE_CLIENT } from '../Config/config';
import stripe from 'tipsi-stripe';
import { colors } from '../theme';
import Toast from 'react-native-toast-message';
import AddCard from './addCard';

const ContainerView = Platform.select({
  ios: KeyboardAvoidingView,
  //@ts-ignore
  android: View,
});

stripe.setOptions({
  publishableKey: STRIPE_CLIENT,
});

interface IRecipeProps {
  clientID?: string;
  getCard?: any;
  setFromPaypal?: any;
  fromPaypal?: boolean;
  setLoading?: any;
  primarycolor: string;
  secudandyColor: string;
}

export default class CardTextFieldScreen extends PureComponent<IRecipeProps> {
  static title = 'Guardar tarjeta en Wilbby';

  handleCardPayPress = async () => {
    const options = {
      smsAutofillDisabled: true,
      theme: {
        primaryBackgroundColor: this.props.primarycolor,
        secondaryBackgroundColor: this.props.secudandyColor,
        primaryForegroundColor: colors.rgb_153,
        secundarioForegroundColor: colors.rgb_153,
        accentColor: colors.main,
        errorColor: colors.ERROR,
      },
    };
    try {
      const token = await stripe.paymentRequestWithCardForm(options);
      let response = await fetch(
        `${NETWORK_INTERFACE_URL}/card-create?customers=${this.props.clientID}&token=${token.id}`,
      );
      const secret = await response.json();
      if (secret) {
        this.props.getCard();
        if (this.props.setLoading) {
          this.props.setLoading(false);
        }
        if (this.props.fromPaypal) {
          this.props.setFromPaypal(false);
        }
        Toast.show({
          text1: 'Tarjeta añadida',
          text2: 'Tarjeta añadida a tu wallet',
          position: 'top',
          type: 'success',
          topOffset: 50,
          visibilityTime: 4000,
        });
      }
    } catch (e) {
      console.log(e);
      this.props.setLoading(false);
      Toast.show({
        text1: 'Algo salio mal',
        text2: 'Algo salio mal intentalo de nuevo',
        position: 'top',
        type: 'error',
        topOffset: 50,
        visibilityTime: 4000,
      });
    }
  };

  render() {
    return (
      <ContainerView
        behavior="padding"
        style={styles.container}
        onResponderGrant={dismissKeyboard}
        onStartShouldSetResponder={() => true}>
        <AddCard onPress={() => this.handleCardPayPress()} />
      </ContainerView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 'auto',
    marginRight: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import Navigation from '../services/Navigration';
import AsyncStorage from '@react-native-community/async-storage';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors, dimensions } from '../theme';
import { CustomText } from '../Components/CustomTetx';
import { stylesText } from '../theme/TextStyle';
import { Button } from '../Components/Button';
import { useNavigationParam } from 'react-navigation-hooks';
import LottieView from 'lottie-react-native';
import source from '../Assets/Animate/camera.json';
import { openSettings } from 'react-native-permissions';

const Scaner = () => {
  const [ids, setID] = useState(null);
  const data = useNavigationParam('data');
  const styles = useDynamicValue(dynamicStyles);

  useEffect(() => {
    const getId = async () => {
      const id = await AsyncStorage.getItem('id');
      setID(id);
    };
    getId();
  }, []);

  const ope = () => {
    openSettings().catch(() => console.warn('cannot open settings'));
  };

  console.log(data);

  const onSuccess = (e: any) => {
    const datDetails = {
      id: e.data,
      yo: ids,
      autoshipping: false,
      city: data.city,
      lat: data.lat,
      lgn: data.lgn,
      localeCode: data.localeCode,
      currecy: data.currecy,
    };
    Navigation.navigate('DestailsStore', { data: datDetails });
  };
  return (
    <QRCodeScanner
      onRead={(data) => onSuccess(data)}
      // @ts-ignore
      flashMode={RNCamera.Constants.FlashMode.auto}
      showMarker={true}
      markerStyle={{
        borderColor: colors.main,
        borderWidth: 5,
        borderRadius: 20,
      }}
      containerStyle={styles.container}
      cameraStyle={{
        height: dimensions.Height(110),
        width: dimensions.ScreenWidth,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: -30,
      }}
      notAuthorizedView={
        <View style={styles.nopermiso}>
          <LottieView
            source={source}
            autoPlay
            loop
            style={{ width: 260, height: 260 }}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={{
              textAlign: 'center',
              fontSize: dimensions.FontSize(20),
              fontWeight: '200',
              marginTop: 20,
              paddingHorizontal: 30,
            }}>
            Debes dar permiso a la cámara para escanear el establecimiento.
          </CustomText>
          <TouchableOpacity onPress={() => ope()}>
            <CustomText
              light={colors.main}
              dark={colors.main}
              style={{
                textAlign: 'center',
                fontSize: dimensions.FontSize(20),
                fontWeight: 'bold',
                marginTop: 30,
                paddingHorizontal: 30,
                zIndex: 100,
              }}>
              Dar acceso
            </CustomText>
          </TouchableOpacity>
        </View>
      }
      fadeIn={false}
      bottomContent={
        <View>
          <CustomText
            style={[
              stylesText.secondaryTextBold,
              { textAlign: 'center', marginTop: dimensions.Height(-30) },
            ]}
            light={colors.white}
            dark={colors.white}>
            Apunta con la cámara al código QR del establecimineto para ver los
            productos.
          </CustomText>
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => Navigation.goBack()}
              title="Volver al inicio"
              titleStyle={styles.buttonTitle}
            />
          </View>
        </View>
      }
    />
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },

  nopermiso: {
    width: dimensions.Width(100),
    height: dimensions.Height(100),
    backgroundColor: new DynamicValue('white', 'black'),
    paddingTop: dimensions.Height(15),
    alignItems: 'center',
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
    position: 'absolute',
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

export default Scaner;

import React, { useState } from 'react';
import { View, Platform } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors, dimensions } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import { stylesText } from '../../../theme/TextStyle';
import { Modalize } from 'react-native-modalize';
import { Button } from '../../../Components/Button';
import { useTranslation } from 'react-i18next';

export default function Modals(props: any) {
  const { city, onClose, data, modalizeRef } = props;
  const styles = useDynamicValue(dynamicStyles);
  const { t } = useTranslation();

  const [toggle] = useState(true);

  return (
    <Modalize
      ref={modalizeRef}
      adjustToContentHeight={toggle}
      modalStyle={styles.modalContens}>
      <View style={styles.modalConten}>
        <View
          style={{
            alignSelf: 'center',
            marginTop: 20,
          }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={stylesText.mainText}>
            {t('home:confirmLocation')}
          </CustomText>
        </View>

        <View
          style={{
            marginVertical: 50,
            marginHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Icon
            name="location-arrow"
            type="FontAwesome"
            size={26}
            color={colors.main}
          />
          <View style={{ marginLeft: 15 }}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText}>
              {t('home:now')}
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={stylesText.secondaryTextBold}>
              {city ? city : 'Ciudad no disponible'}
            </CustomText>
          </View>
        </View>
        <View style={styles.signupButtonContainer}>
          <Button
            light={colors.white}
            dark={colors.white}
            containerStyle={[
              styles.buttonView,
              { backgroundColor: colors.main },
            ]}
            onPress={() => onClose(data)}
            title={`${t('home:continue')} ${
              city ? `${t('home:in')} ${city}` : ''
            }`}
            titleStyle={styles.buttonTitle}
          />
        </View>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modalContens: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  modalConten: {
    flex: 1,
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 12,
    borderTopStartRadius: 12,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
    width: dimensions.Width(70),
  },
  buttonView: {
    width: dimensions.Width(70),
    borderRadius: 10,
    marginBottom: dimensions.Height(5),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

import React from 'react';
import { View, TouchableOpacity, Platform, Animated } from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, dimensions, stylesText } from '../../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { CustomText } from '../../../Components/CustomTetx';
import { BlurView, VibrancyView } from '@react-native-community/blur';
import { Badge } from '@ant-design/react-native';
import { Query } from 'react-apollo';
import { newQuery } from '../../../GraphQL';

const ContainerView = Platform.select({
  ios: VibrancyView,
  //@ts-ignore
  android: BlurView,
});

export interface Tabs {
  ruteName: string;
  onPressHome: any;
  onPressFavourites: any;
  onPressOrder: any;
  onPressAccount: any;
  city: string;
  id: string;
  opacity: any;
}

export default function ButtomBar(props: Tabs) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {
    ruteName,
    onPressHome,
    onPressOrder,
    onPressFavourites,
    onPressAccount,
    id,
    opacity,
  } = props;

  const opacidad = opacity.interpolate({
    inputRange: [0, 250],
    outputRange: [100, 0],
    extrapolate: 'clamp',
  });

  const inact = {
    light: colors.black,
    dark: colors.white,
  };
  const modo = useColorSchemeContext();
  const inactiveColor = inact[modo];

  let Home = ruteName == 'Home' ? colors.main : inactiveColor;
  let Order = ruteName == 'Order' ? colors.main : inactiveColor;
  let Favourite = ruteName == 'Favourites' ? colors.main : inactiveColor;
  let Account = ruteName == 'Account' ? colors.main : inactiveColor;

  const blur = {
    light: 'light',
    dark: 'dark',
  };
  const mode = useColorSchemeContext();
  const blurtype = blur[mode];

  return (
    <View
      style={{
        width: dimensions.ScreenWidth,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Animated.View style={[styles.container, { opacity: opacidad }]}>
        <View style={styles.tabConten}>
          <TouchableOpacity style={styles.items} onPress={onPressHome}>
            <Icon type="Feather" name="home" size={26} color={Home} />

            <CustomText
              style={[stylesText.placeholderText, { marginTop: 5 }]}
              light={Home}
              dark={Home}>
              Inicio
            </CustomText>
          </TouchableOpacity>

          <Query
            query={newQuery.GET_ORDER}
            variables={{
              userId: id,
              status: [
                'Nueva',
                'Confirmada',
                'En la cocina',
                'Listo para recoger',
                'Preparando para el envío',
                'En camino',
                'Entregada',
              ],
            }}>
            {(response) => {
              if (response) {
                const respuesta =
                  response && response.data && response.data.getNewOrder
                    ? response.data.getNewOrder.list
                    : [];
                response.refetch();
                return (
                  <TouchableOpacity style={styles.items} onPress={onPressOrder}>
                    {respuesta && respuesta.length > 0 ? (
                      <View style={[styles.badge]}>
                        <CustomText
                          numberOfLines={1}
                          style={{ color: colors.white }}>
                          {respuesta && respuesta.length}
                        </CustomText>
                      </View>
                    ) : null}

                    <Icon
                      type="Feather"
                      name="shopping-bag"
                      size={26}
                      color={Order}
                    />
                    <CustomText
                      style={[stylesText.placeholderText, { marginTop: 5 }]}
                      light={Order}
                      dark={Order}>
                      Pedidos
                    </CustomText>
                  </TouchableOpacity>
                );
              }
            }}
          </Query>

          <TouchableOpacity style={styles.items} onPress={onPressFavourites}>
            <Icon type="Feather" name="heart" size={26} color={Favourite} />
            <CustomText
              style={[stylesText.placeholderText, { marginTop: 5 }]}
              light={Favourite}
              dark={Favourite}>
              Favoritos
            </CustomText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.items} onPress={onPressAccount}>
            <Icon type="Feather" name="user" size={26} color={Account} />
            <CustomText
              style={[stylesText.placeholderText, { marginTop: 5 }]}
              light={Account}
              dark={Account}>
              Mi cuenta
            </CustomText>
          </TouchableOpacity>
        </View>
        <ContainerView
          style={styles.absoluto}
          //@ts-ignore
          blurType={blurtype}
        />
      </Animated.View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  absoluto: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  badge: {
    height: 20,
    width: 20,
    backgroundColor: colors.ERROR,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    position: 'absolute',
    zIndex: 100,
    bottom: 30,
    left: 25,
  },

  container: {
    height: dimensions.IsIphoneX()
      ? dimensions.Height(8)
      : dimensions.Height(11),
    width: dimensions.Width(90),
    justifyContent: 'center',
    bottom: 20,
    borderRadius: 20,
    position: 'absolute',
    overflow: 'hidden',
  },

  tabConten: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginHorizontal: 20,
    zIndex: 100,
  },

  items: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

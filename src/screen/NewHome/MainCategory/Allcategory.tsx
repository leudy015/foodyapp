import React, { useState } from 'react';
import { View, FlatList } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors, dimensions } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import { stylesText } from '../../../theme/TextStyle';
import { Modalize } from 'react-native-modalize';
import { useTranslation } from 'react-i18next';
import CategoryLoading from '../../../Components/PlaceHolder/Category';
import { query } from '../../../GraphQL';
import { Query } from 'react-apollo';
import Category from './MainCategory';
import Navigation from '../../../services/Navigration';

export default function Modals(props: any) {
  const { onOpen, id, setCategory, modalizeRefCategory, openModal } = props;
  const styles = useDynamicValue(dynamicStyles);
  const { t } = useTranslation();

  const [toggle] = useState(true);

  const itemsRender = ({ item }) => {
    const actionCategory = () => {
      setCategory(item);
      switch (item.title) {
        case 'Algo Especial':
          if (id) {
            openModal(item);
          } else {
            Navigation.navigate('Login', 0);
          }
          break;
        case 'Envío Express':
          if (id) {
            openModal(item);
          } else {
            Navigation.navigate('Login', 0);
          }
          break;
        default:
          onOpen(item);
          break;
      }
    };
    return (
      <Category
        data={item}
        onPress={() => actionCategory()}
        width={dimensions.Width(35)}
        height={dimensions.Width(35)}
      />
    );
  };
  return (
    <Modalize
      ref={modalizeRefCategory}
      snapPoint={dimensions.Height(85)}
      adjustToContentHeight={toggle}
      modalStyle={styles.modalContens}>
      <View style={styles.modalConten}>
        <View
          style={{
            alignSelf: 'center',
            marginTop: 20,
            marginBottom: 20,
          }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={stylesText.h1}>
            Secciones
          </CustomText>
        </View>
        <Query query={query.CATEGORY}>
          {(response) => {
            if (response.loading) {
              return <CategoryLoading />;
            }
            if (response) {
              response.refetch();
              const data =
                response && response.data && response.data.getCategory
                  ? response.data.getCategory.data
                  : [];
              return (
                <FlatList
                  data={data}
                  style={{
                    paddingBottom: dimensions.Height(10),
                    paddingHorizontal: 20,
                  }}
                  keyboardShouldPersistTaps="handled"
                  renderItem={(item: any) => itemsRender(item)}
                  keyExtractor={(item: any) => item._id}
                  showsVerticalScrollIndicator={false}
                  numColumns={2}
                />
              );
            }
          }}
        </Query>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modalContens: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  modalConten: {
    flex: 1,
    width: dimensions.Width(100),
    height: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    borderTopEndRadius: 12,
    borderTopStartRadius: 12,
  },
});

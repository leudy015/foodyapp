import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import { Query } from 'react-apollo';
import { query } from '../../GraphQL';

export const SelectPayment = (prosp) => {
  const { fromPaypal, SetPayment, card } = prosp;
  const styles = useDynamicValue(dynamicStyles);

  const renderIcon = () => {
    switch (card.brand) {
      case 'visa':
        return (
          <>
            <Icon
              name="cc-visa"
              type="FontAwesome"
              size={24}
              color={colors.rgb_153}
              style={{ margin: 10 }}
            />
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { fontSize: 18 }]}>
              Tarjeta teminada en {card.last4}
            </CustomText>
          </>
        );
      case 'mastercard':
        return (
          <>
            <Icon
              name="cc-mastercard"
              type="FontAwesome"
              size={24}
              color={colors.rgb_153}
              style={{ margin: 10 }}
            />
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { fontSize: 18 }]}>
              Tarjeta teminada en {card.last4}
            </CustomText>
          </>
        );
      default:
        return (
          <>
            <Icon
              name="creditcard"
              type="AntDesign"
              size={24}
              color={colors.rgb_153}
              style={{ margin: 10 }}
            />
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, { fontSize: 18 }]}>
              {card.brand
                ? `Tarjeta teminada en ${card.last4}`
                : 'Método de pago'}
            </CustomText>
          </>
        );
    }
  };

  const renderPaypal = () => {
    return (
      <>
        <Icon
          name="cc-paypal"
          type="FontAwesome"
          size={24}
          color={colors.rgb_153}
          style={{ margin: 10 }}
        />
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[stylesText.secondaryText, { fontSize: 18 }]}>
          Paypal
        </CustomText>
      </>
    );
  };

  const borderColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const border = borderColors[mode];

  return (
    <Query query={query.USER_DETAIL}>
      {(response) => {
        if (response) {
          response.refetch();
          const user =
            response && response.data && response.data.getUsuario
              ? response.data.getUsuario.data
              : {};
          return (
            <View style={[styles.cards, { borderColor: border }]}>
              <TouchableOpacity
                onPress={() => SetPayment(user, response.refetch)}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                }}>
                {fromPaypal ? renderPaypal() : renderIcon()}
                <Icon
                  name="right"
                  type="AntDesign"
                  size={18}
                  color={colors.rgb_153}
                  style={{ marginLeft: 'auto' }}
                />
              </TouchableOpacity>
            </View>
          );
        }
      }}
    </Query>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  textInputContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
  },

  cards: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: 30,
    padding: 10,
    borderRadius: 20,
    marginHorizontal: 15,
    borderWidth: 1,
  },
});

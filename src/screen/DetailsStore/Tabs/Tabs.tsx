import React from 'react';
import { TouchableOpacity, FlatList } from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, stylesText } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
export default function Tabs(props: any) {
  const { res, selected, setSelected } = props;
  const styles = useDynamicValue(dynamicStyles);

  const border = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const bordercolor = border[mode];

  const _renderItem = ({ item, index }, indexSelected) => {
    return (
      <TouchableOpacity
        onPress={() => setSelected(index)}
        style={[
          styles.tabs,
          {
            backgroundColor:
              index === indexSelected ? colors.main : bordercolor,
          },
        ]}>
        <CustomText
          style={[stylesText.secondaryText, { paddingBottom: 2 }]}
          light={index === indexSelected ? colors.white : colors.rgb_153}
          dark={colors.white}>
          {item.name}
        </CustomText>
      </TouchableOpacity>
    );
  };
  return (
    <FlatList
      data={res.CategoryNew}
      renderItem={(item: any) => _renderItem(item, selected)}
      keyExtractor={(item: any) => item._id}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      horizontal={true}
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  tabs: {
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 30,
    borderRadius: 100,
  },

  vertodo: {
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 10,
  },
});

import React, { useState, useEffect } from 'react';
import { View, FlatList } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { NETWORK_INTERFACE_URL } from '../../Config/config';
import { dimensions, colors, image, stylesText } from '../../theme';
import FieldCard from '../Card';
import Paypal from '../../Components/Paypal/paypal';
import { CustomText } from '../../Components/CustomTetx';
import CardPayment from '../../Components/CardPaiment';
import PaypalCard from '../../Components/PaypalCard';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { Button } from '../../Components/Button';
import { ScrollView } from 'react-native-gesture-handler';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function selecPaymentMethod(props: any) {
  const { datas, selectedPayment, setvisibleModal } = props;
  const styles = useDynamicValue(dynamicStyles);
  const [cards, setcards] = useState([]);
  //const [paypalAccount, setpaypalAccount] = useState([]);
  const [fromPaypal, setFromPaypal] = useState(false);
  const [selecCard, setselecCard] = useState('');
  const [customer, setCustomer] = useState('');
  const [brand, setbrand] = useState('');
  const [last4, setlast4] = useState('');

  const getCard = async () => {
    let res = await fetch(
      `${NETWORK_INTERFACE_URL}/get-card?customers=${datas.StripeID}`,
    );
    const card = await res.json();
    //setpaypalAccount(pay.data);
    setcards(card);
  };

  useEffect(() => {
    getCard();
  }, [cards]);

  const _renderItem = ({ item }) => {
    let images = '';
    let brand = '';
    switch (item.card.brand) {
      case 'visa':
        images = image.Visa;
        brand = 'Visa';
        break;
      case 'mastercard':
        images = image.MasterCard;
        brand = 'Master Card';
        break;
      case 'amex':
        images = image.AmericanExpress;
        brand = 'American Express';
        break;
    }

    return (
      <CardPayment
        item={item}
        images={images}
        brand={brand}
        selecCard={selecCard}
        onPress={() => {
          if (selecCard) {
            setselecCard('');
            setCustomer('');
            if (selecCard != item.id) {
              setselecCard(item.id);
              setCustomer(item.customer);
              setbrand(item.card.brand);
              setlast4(item.card.last4);
            }
          } else {
            setselecCard(item.id);
            setCustomer(item.customer);
            setbrand(item.card.brand);
            setlast4(item.card.last4);
          }
        }}
      />
    );
  };

  const confirmsPayment = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    const da = {
      card: selecCard,
      customer: customer,
      brand: brand,
      last4: last4,
      fromPaypal: fromPaypal,
    };
    selectedPayment(da);
    setvisibleModal(false);
  };

  const primary = {
    light: colors.white,
    dark: colors.black,
  };
  const modes = useColorSchemeContext();
  const primario = primary[modes];

  const secundary = {
    light: colors.colorInput,
    dark: colors.back_dark,
  };
  const mode = useColorSchemeContext();
  const secundario = secundary[mode];

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.crecadr}>
          <CustomText
            numberOfLines={1}
            ligth={colors.black}
            dark={colors.white}
            style={[stylesText.titleText, { marginBottom: 10, marginLeft: 0 }]}>
            Añadidos
          </CustomText>
          {!fromPaypal ? (
            <FlatList
              // @ts-ignore
              data={cards ? cards.data : []}
              renderItem={(item: any) => _renderItem(item)}
              keyExtractor={(item: any) => item.id}
              showsVerticalScrollIndicator={false}
            />
          ) : null}
          {!selecCard ? (
            <PaypalCard
              fromPaypal={fromPaypal}
              onPress={() => setFromPaypal(!fromPaypal)}
            />
          ) : null}

          {/* <FlatList
              // @ts-ignore
              data={paypalAccount ? paypalAccount : []}
              renderItem={(item: any) => _renderItemPaypal(item)}
              keyExtractor={(item: any) => item.id}
              showsVerticalScrollIndicator={false}
            />
          )  */}

          <CustomText
            numberOfLines={1}
            ligth={colors.black}
            dark={colors.white}
            style={[stylesText.titleText, { marginBottom: 30, marginTop: 30 }]}>
            Métodos de pago
          </CustomText>

          {/* <Paypal user={datas} getData={getCard} /> */}

          <FieldCard
            clientID={datas.StripeID}
            getCard={getCard}
            primarycolor={primario}
            secudandyColor={secundario}
          />

          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={[
                styles.buttonView,
                { backgroundColor: colors.main },
              ]}
              onPress={() => confirmsPayment()}
              title="Continuar"
              titleStyle={styles.buttonTitle}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },

  crecadr: {
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: 'transparent',
    padding: dimensions.Width(3),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(2),
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  cardinfo: {
    width: dimensions.Width(95),
    height: 80,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 0.5,
    shadowColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    shadowOffset: {
      width: 10,
      height: 5,
    },
    shadowOpacity: 12.34,
    shadowRadius: 10.27,
    elevation: 10,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
    marginBottom: dimensions.Height(15),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

import React, { useState, useEffect } from 'react';
import { View, FlatList } from 'react-native';
import { colors } from '../../theme';
import { CustomText } from '../CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';
import Card from './Card';

const RestaurantCard = (props: any) => {
  const { data, city, lat, localeCode, currecy, refetch, riders, lgn } = props;
  const [id, setID] = useState(null);

  useEffect(() => {
    const getId = async () => {
      const id = await AsyncStorage.getItem('id');
      setID(id);
    };
    getId();
  }, []);

  const _renderItem = ({ item }) => {
    return (
      <Card
        item={item.restaurant}
        id={id}
        city={city}
        lat={lat}
        lgn={lgn}
        localeCode={localeCode}
        currecy={currecy}
        refetch={refetch}
        riders={riders}
      />
    );
  };

  return (
    <FlatList
      data={data}
      renderItem={(item: any) => _renderItem(item)}
      keyExtractor={(item: any) => item.restaurant._id}
      showsVerticalScrollIndicator={false}
      ListEmptyComponent={
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <LottieView
            source={require('../../Assets/Animate/hearto.json')}
            autoPlay
            loop
            style={{ width: 250 }}
          />
          <CustomText
            ligth={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              { textAlign: 'center', paggingHorizontal: 30, paddingBottom: 5 },
            ]}>
            Aún no has añadido tiendas o restaurantes a favoritos
          </CustomText>
        </View>
      }
    />
  );
};

export default RestaurantCard;

import React from 'react';
import { TouchableOpacity, View, ActivityIndicator } from 'react-native';
import { colors, dimensions } from '../theme';
import { CustomText } from './CustomTetx';

export const Button = (props: any) => {
  return (
    <View
      style={[
        props.containerStyle,
        {
          minHeight: dimensions.IsIphoneX() ? 50 : 40,
          justifyContent: 'center',
        },
      ]}>
      <TouchableOpacity onPress={() => props.onPress()}>
        {props.loading ? (
          <ActivityIndicator color={colors.white} size="small" />
        ) : (
          <CustomText style={props.titleStyle}>{props.title}</CustomText>
        )}
      </TouchableOpacity>
    </View>
  );
};

import { query } from './query';
import { mutations } from './mutation';
import { newQuery } from './newQuery';
import { newMutation } from './newMutation';

export { query, mutations, newQuery, newMutation };

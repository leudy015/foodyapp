import React from 'react';
import { View, TouchableOpacity, Linking } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import Scooter from '../../Components/scooter';

export default function Swich(props) {
  const { changeLlevar, res, takeaway, address } = props;
  const styles = useDynamicValue(dynamicStyles);

  return (
    <View>
      <View style={styles.filters}>
        <TouchableOpacity
          onPress={() => (res.llevar ? changeLlevar() : null)}
          style={takeaway ? styles.itemsInac : styles.itemsllevar}>
          <Scooter />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              { marginTop: 2, marginLeft: 10, paddingBottom: 2 },
            ]}>
            Entrega
          </CustomText>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => (res.llevar ? changeLlevar() : null)}
          style={takeaway ? styles.itemsllevar : styles.itemsInac}>
          <Icon
            name="run"
            type="MaterialCommunityIcons"
            size={24}
            style={styles.icons}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              { marginTop: 2, marginLeft: 0, paddingBottom: 2 },
            ]}>
            {res.llevar ? 'Para recoger' : 'No disponible'}
          </CustomText>
        </TouchableOpacity>
      </View>
      {takeaway ? (
        <View style={styles.adress}>
          <View>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  width: dimensions.Width(50),
                  paddingBottom: 5,
                  lineHeight: 18,
                },
              ]}>
              {address.calle} {address.numero}, {address.codigoPostal},{' '}
              {address.ciudad}
            </CustomText>
          </View>
          <TouchableOpacity
            style={{
              marginLeft: 'auto',
              backgroundColor: 'rgba(197, 248, 116, 0.3)',
              paddingHorizontal: 10,
              paddingVertical: 10,
              borderRadius: 100,
            }}
            onPress={() =>
              Linking.openURL(
                `https://www.google.com/maps/search/?api=1&query=${address.lat},${address.lgn}`,
              )
            }>
            <Icon
              name="enviromento"
              type="AntDesign"
              size={24}
              color={colors.main}
            />
          </TouchableOpacity>
        </View>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  filters: {
    width: 290,
    height: 50,
    flexDirection: 'row',
    marginTop: 20,
    paddingHorizontal: 7,
    justifyContent: 'space-between',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 100,
  },

  itemsllevar: {
    width: 130,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.main,
    borderRadius: 100,
    paddingVertical: 7,
    paddingHorizontal: 10,
  },

  itemsInac: {
    width: 130,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  adress: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: dimensions.Height(2),
    backgroundColor: new DynamicValue(
      colors.colorInput,
      colors.back_suave_dark,
    ),
    borderRadius: 10,
    padding: 10,
  },
});

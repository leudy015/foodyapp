import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { dimensions, colors, stylesText } from '../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { CustomText } from './CustomTetx';
import { Avatar } from 'react-native-elements';

export default function CardPayments(props) {
  const { images, onPress, brand, selecCard, item } = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const modes = useColorSchemeContext();
  const borderColot = backgroundColors[modes];

  return (
    <TouchableOpacity
      style={[styles.cardinfo, { borderColor: borderColot }]}
      onPress={onPress}>
      <View style={{ alignSelf: 'center' }}>
        <Avatar rounded size={50} source={images} />
      </View>
      <View style={{ alignSelf: 'center', marginLeft: dimensions.Width(4) }}>
        <CustomText
          light={colors.back_dark}
          dark={colors.white}
          style={[stylesText.mainText, { fontWeight: 'bold' }]}>
          Seleccionar {brand}
        </CustomText>
        <CustomText
          numberOfLines={2}
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            {
              width: dimensions.Width(65),
            },
          ]}>
          Termina en {item.card.last4} - Caduca {item.card.exp_month}/
          {item.card.exp_year}
        </CustomText>
      </View>
      <View
        style={{
          alignSelf: 'center',
          marginLeft: 'auto',
        }}>
        {selecCard === item.id ? (
          <Icon
            name="checkcircle"
            type="AntDesign"
            size={20}
            color={colors.main}
          />
        ) : null}
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cardinfo: {
    width: dimensions.Width(92),
    paddingHorizontal: 15,
    height: 'auto',
    padding: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(1),
    marginBottom: dimensions.Height(1),
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 0.5,
  },
});

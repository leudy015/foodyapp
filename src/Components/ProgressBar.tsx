import React, { Component } from 'react';
import { Animated, View } from 'react-native';
import Svg, { Defs, LinearGradient, Rect, Stop } from 'react-native-svg';

export interface IProps {
  percent: number;
  maxpercent: number;
  height: number;
  width: number;
  color1: string;
  color2: string;
  color3: string;
  style?: any;
}
const AnimatedRect = Animated.createAnimatedComponent(Rect);

export default class CustomAnimationProgress extends Component<IProps> {
  state = {
    width: new Animated.Value(0),
  };

  rectangleRef = React.createRef();

  componentDidMount() {
    let { percent } = this.props;
    if (percent >= this.props.maxpercent) percent = this.props.maxpercent;
    let totalValue = this.props.maxpercent;
    this.animate(percent, totalValue);
  }

  componentDidUpdate(currentValue, totalValue) {
    this.animate(currentValue, totalValue);
  }

  animate = (current, total) =>
    Animated.timing(this.state.width, {
      toValue: 1, //current / total,
      duration: 1000,
      useNativeDriver: true,
    }).start();

  render() {
    const { width } = this.state;
    const height = this.props.height;
    const width1 = this.props.width;
    const p = this.props.percent;
    const t_percent = p >= this.props.maxpercent ? this.props.maxpercent : p;
    const percent = t_percent + '%'; //"100%"//children//"50%"
    const fillWidth = (width1 * +t_percent) / this.props.maxpercent;

    return (
      <View style={this.props.style}>
        <Svg width={width1} height={height}>
          <Defs>
            <LinearGradient
              id={`HeathManaBar-gradient`}
              //@ts-ignore
              rx="8"
              ry="8"
              //id="grad"
              x1="0"
              y1="0"
              x2={fillWidth}>
              <Stop offset="1" stopColor={this.props.color1} stopOpacity="1" />
              <Stop offset="1" stopColor={this.props.color2} stopOpacity="0" />
              <Stop offset="0" stopColor={this.props.color3} stopOpacity="1" />
            </LinearGradient>
          </Defs>

          <LinearGradient id="grad1" x1="0" y1="0" x2={percent} y2="0">
            <Stop offset="1" stopColor="#E1E1E1" stopOpacity="1" />
          </LinearGradient>
          <Rect
            rx="8"
            ry="8"
            x="0"
            y="0"
            width={width1}
            height={height}
            fill="url(#grad1)"
          />

          <AnimatedRect
            x="0"
            y="0"
            rx="8"
            ry="8"
            //@ts-ignore
            rectwidth={width.interpolate({
              inputRange: [0, 1],
              outputRange: [0, fillWidth],
            })}
            width={width.interpolate({
              inputRange: [0, 1],
              outputRange: [0, fillWidth],
            })}
            height={height}
            fill={`url(#HeathManaBar-gradient)`}
          />
        </Svg>
      </View>
    );
  }
}

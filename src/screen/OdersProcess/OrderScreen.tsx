import React, { useState, useRef, useEffect } from 'react';
import { View, Alert } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Header from '../../Components/Header';
import { useNavigationParam } from 'react-navigation-hooks';
import { colors, dimensions, stylesText } from '../../theme';
import { Modalize } from 'react-native-modalize';
import Toast from 'react-native-toast-message';
import { newMutation } from '../../GraphQL';
import { useMutation } from 'react-apollo';
import Navigation from '../../services/Navigration';
import AsyncStorage from '@react-native-community/async-storage';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import ModalDate from '../DetailsStore/ModalSelecDate';
import PropinaRider from './PropinaRider';
import ItemCart from './ItemsCart';
import Needcutlery from './Needcutlery';
import Address from './Address';
import AddNota from './AddNota';
import Scheduled from '../DetailsStore/Scheduled';
import TermAndCond from './termAndCond';
import AppleCupon from './Cupon';
import AutoShipping from './AutoShipping';
import Totals from './Total';
import BtnContinue from './btnContinue';
import { formaterPrice } from '../../Utils/formaterPRice';
import Loader from '../../Components/ModalLoading';
import moment from 'moment';
import { CustomText } from '../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const OrderScreen = () => {
  const styles = useDynamicValue(dynamicStyles);
  const datas = useNavigationParam('data');
  const user = datas.user;
  const appleDate = datas.time !== 'Lo antes posible' ? datas.time : null;
  const [date, setDate] = useState(appleDate);
  const [selecte, setSelected] = useState(
    datas.time === 'Lo antes posible'
      ? 'Lo antes posible'
      : 'Programar entrega',
  );
  const [city, setCity] = useState(datas.restaurants.city);
  const modalizeRef = useRef<Modalize>(null);
  const [isTermino, setIsTermino] = useState(true);
  const [propina, setPropina] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [nota, setNota] = useState('');
  const [id, setID] = useState(user._id);
  const [crearModificarNewOrden] = useMutation(newMutation.CREATE_NEW_ORDER);
  const [cupon, setCupon] = useState('');
  const [ordenID, setOrdenID] = useState(null);
  const [descuento, setDescuento] = useState({
    clave: '',
    descuento: 0,
    tipo: '',
    isShipping: false,
  });
  const [Loading, setLoading] = useState(false);
  const [adressID, setAdressID] = useState('');
  const [formatted_address, setFormatted_address] = useState('');
  const [isEnabledCubiertos, setIsEnabledcubiertos] = useState(false);
  const [itemsCart, setitemsCart] = useState(datas.data);
  const toggleSwitch = () =>
    setIsEnabledcubiertos((previousState) => !previousState);

  console.log(descuento);

  const respuesta = itemsCart;

  var subTotals = 0;
  respuesta.forEach(function (items: any) {
    const t = items.items.quantity * items.items.price;
    subTotals += t;
    items.items.subItems.forEach((x) => {
      const s = x.quantity * x.price;
      subTotals += s * items.items.quantity;
      const t = x && x.subItems ? x.subItems : [];
      t.forEach((y) => {
        const u = y.quantity * y.price;
        subTotals += u * items.items.quantity;
      });
    });
  });

  const getAdreesID = async () => {
    const adressID = await AsyncStorage.getItem('adressId');
    const adressname = await AsyncStorage.getItem('adressName');
    setFormatted_address(adressname);
    setAdressID(adressID);
    const city = await AsyncStorage.getItem('city');
    setCity(city);
    setID(await AsyncStorage.getItem('id'));
  };

  useEffect(() => {
    getAdreesID();
  }, [adressID]);

  const ifNeedAddAdress = () => {
    if (datas.takeaway) {
      return false;
    } else if (!datas.takeaway && !adressID) {
      return true;
    } else {
      return false;
    }
  };

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  const valorDescuento = descuento.descuento > 0 ? descuento.descuento : 0;
  const tipo = descuento.tipo ? descuento.tipo : '';
  const subtotal = subTotals;
  let displayDescuento = formaterPrice(0, datas.localeCode, datas.currecy);
  let descuentoFinal = 0;
  let total = subtotal;
  if (valorDescuento && tipo) {
    switch (tipo) {
      case 'dinero':
        displayDescuento = formaterPrice(
          valorDescuento / 100,
          datas.localeCode,
          datas.currecy,
        );
        total = subtotal - valorDescuento;
        descuentoFinal = valorDescuento;
        break;
      case 'porcentaje':
        displayDescuento = `${valorDescuento}%`;
        descuentoFinal = valorDescuento;
        total = subtotal - (subtotal * valorDescuento) / 100;
        break;
    }
  }

  let subtotals = subtotal;
  let descuentos = descuentoFinal;
  let extras = datas.restaurants.extras;
  let envio = datas.takeaway
    ? 0
    : city === datas.restaurants.city
    ? datas.restaurants.shipping
    : 350;
  let propinas = propina;

  let finalShipping = descuento.isShipping ? 0 : envio;

  let totals = total + extras + finalShipping + propinas;

  const totalFinal = totals < 0 ? 100 : totals;

  const estimatedPickupTime =
    selecte === 'Lo antes posible'
      ? moment(new Date()).add(0.5, 'hour').format()
      : moment(date).add(0.5, 'hour').format();

  const hora =
    selecte === 'Lo antes posible'
      ? moment(new Date()).format()
      : moment(date).format();

  const confiAdres = {
    fromHome: true,
    getAdreesID: getAdreesID,
    city: datas.city,
    lat: datas.lat,
    lgn: datas.lgn,
  };

  const input = {
    _id: ordenID,
    channelOrderDisplayId: Number.parseInt(getRandomArbitrary(100, 1000000)),
    orderType: datas.takeaway ? 'pickup' : 'delivery',
    pickupTime: hora,
    estimatedPickupTime: estimatedPickupTime,
    deliveryTime: hora,
    customer: id,
    scheduled: selecte === 'Lo antes posible' ? false : true,
    store: datas.restaurants._id,
    deliveryAddress: adressID,
    payment: totalFinal,
    note: nota,
    items: itemsCart,
    tip: propinas,
    numberOfCustomers: 1,
    deliveryCost: finalShipping,
    serviceCharge: extras,
    discountTotal: descuentos,
    IntegerValue: 10,
    Needcutlery: isEnabledCubiertos,
    clave: cupon,
  };

  const handleSubmit = async () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    setLoading(true);
    if (!cupon) {
      Alert.alert(
        'Upps debes añadir un cupón',
        'Para obtener un descuento debes añadir un código promocional',
        [
          {
            text: 'OK',
            onPress: () => console.log('OK'),
          },
        ],
        { cancelable: false },
      );
      setLoading(false);
      return null;
    } else {
      crearModificarNewOrden({ variables: { input: input } })
        .then(async (results) => {
          const done = results.data.crearModificarNewOrden;
          if (done.success) {
            setLoading(false);
            // @ts-ignore
            setDescuento(done.data.descuento);
            setOrdenID(done.data._id);
            setLoading(false);
            setCupon('');
            if (datas.takeaway && done.data.descuento.isShipping) {
              Toast.show({
                text1: 'No obtuviste ningún descuento envío',
                text2:
                  'Este cupòn es para el envío y debido a que tu pedido es para recoger no se ha aplicando ningun descuento',
                position: 'top',
                type: 'success',
                topOffset: 50,
                visibilityTime: 20000,
              });
            } else if (envio === 0 && done.data.descuento.isShipping) {
              Toast.show({
                text1: 'No obtuviste ningún descuento en el envío',
                text2:
                  'Este establecimiento tiene el envío gratutito y el cupón aplicado es para obtener envio gratis por lo que no obtuviste ningún descuento',
                position: 'top',
                type: 'success',
                topOffset: 50,
                visibilityTime: 20000,
              });
            } else {
              Toast.show({
                text1: done.message,
                text2: done.message,
                position: 'top',
                type: 'success',
                topOffset: 50,
                visibilityTime: 5000,
              });
            }
          } else {
            Toast.show({
              text1: done.message,
              text2: done.message,
              position: 'top',
              type: 'error',
              topOffset: 50,
              visibilityTime: 5000,
            });
            setLoading(false);
          }
        })
        .catch((err: any) => {
          setLoading(false);
          Toast.show({
            text1: 'Cupón no válido',
            text2: 'Hay un problema con tu cupón, o no es válido',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 5000,
          });

          console.error(err);
        });
    }
  };

  const Crear_Orden = (isNavegate: boolean) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    setLoading(true);
    if (ifNeedAddAdress()) {
      setLoading(false);
      Alert.alert(
        'Añadir dirección',
        'Para continuar debes añadir una direccion de entrega',
        [
          {
            text: 'Añadir',
            onPress: () =>
              Navigation.navigate('Direccion', { data: confiAdres }),
          },
        ],
        { cancelable: false },
      );
    } else {
      crearModificarNewOrden({
        variables: {
          input: input,
        },
      })
        .then(async (results) => {
          setLoading(false);
          const res = results.data.crearModificarNewOrden;
          if (res.success) {
            Toast.show({
              text1: res.message,
              text2: res.message,
              position: 'top',
              type: 'success',
              topOffset: 50,
              visibilityTime: 4000,
            });
            setLoading(false);
            const dat = {
              data: datas,
              total: totalFinal,
              user: user,
              results: res.data,
              stripe: user.StripeID,
              city: datas.city,
              lat: datas.lat,
              lgn: datas.lgn,
              localeCode: datas.localeCode,
              currecy: datas.currecy,
            };
            if (isNavegate) {
              setTimeout(() => {
                Navigation.navigate('Payment', { data: dat });
              }, 2000);
            }
          }
        })
        .catch((err) => {
          Toast.show({
            text1: 'Algo va mal',
            text2: 'Algo va mal intentalo de nuevo',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 4000,
          });

          console.log('err====', err);
          setLoading(false);
        });
    }
  };

  return (
    <View style={styles.container}>
      <Toast ref={(ref) => Toast.setRef(ref)} style={{ zIndex: 100 }} />
      <Loader loading={Loading} color={colors.main} />
      <Header title={datas.restaurants.title} />
      <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
        <View style={{ marginBottom: dimensions.Height(15) }}>
          <View style={{ marginHorizontal: 10 }}>
            <Scheduled
              takeaway={datas.takeaway}
              isOK={datas.isOK}
              res={datas.restaurants}
              date={date}
              onOpen={onOpen}
              selecte={selecte}
              details={true}
              delivery={datas.delivery}
            />
          </View>
          {city !== datas.restaurants.city ? (
            <View style={styles.containers}>
              <Icon
                name="map-marker-distance"
                type="MaterialCommunityIcons"
                size={34}
                color={colors.main}
              />
              <View style={{ width: dimensions.Width(70), marginLeft: 10 }}>
                <CustomText
                  style={stylesText.secondaryTextBold}
                  light={colors.main}
                  dark={colors.main}>
                  Estás un poco lejos
                </CustomText>
                <CustomText
                  style={[stylesText.secondaryText, { lineHeight: 18 }]}
                  light={colors.main}
                  dark={colors.main}>
                  Estás un poco lejos del restaurante puede que el coste de
                  envío aumente o no cubramos esta zona.
                </CustomText>
              </View>
            </View>
          ) : null}

          {datas.takeaway ? (
            <View style={styles.containers}>
              <Icon
                name="run"
                type="MaterialCommunityIcons"
                size={34}
                color={colors.main}
              />
              <View style={{ width: dimensions.Width(75), marginLeft: 10 }}>
                <CustomText
                  numberOfLines={1}
                  style={stylesText.secondaryTextBold}
                  light={colors.main}
                  dark={colors.main}>
                  Para recoger en el establecimiento
                </CustomText>
                <CustomText
                  style={[stylesText.secondaryText, { lineHeight: 18 }]}
                  light={colors.main}
                  dark={colors.main}>
                  Este es un pedido para recoger. Recibirás un código de
                  recogida para retirar los productos.
                </CustomText>
              </View>
            </View>
          ) : null}

          {datas.restaurants.categoryName === 'Restaurantes' ? (
            <Needcutlery
              isEnabledCubiertos={isEnabledCubiertos}
              toggleSwitch={toggleSwitch}
            />
          ) : null}
          <AddNota
            setModalVisible={setModalVisible}
            modalVisible={modalVisible}
            nota={nota}
            setNota={setNota}
          />
          {!datas.takeaway || datas.restaurants.autoshipping ? (
            <Address
              formatted_address={formatted_address}
              confiAdres={confiAdres}
              adressID={adressID}
            />
          ) : null}

          {!datas.takeaway ? (
            <PropinaRider
              setPropina={setPropina}
              propina={propina}
              localeCode={datas.localeCode}
              currecy={datas.currecy}
            />
          ) : null}

          <View style={{ marginTop: datas.takeaway ? 30 : 0 }}>
            <ItemCart
              respuesta={respuesta}
              localeCode={datas.localeCode}
              currecy={datas.currecy}
              inCartItem={itemsCart}
              setinCartItem={setitemsCart}
              setinCartItems={datas.setinCartItem}
              storeID={datas.restaurants._id}
            />
          </View>

          <AppleCupon
            descuento={descuento}
            Loading={Loading}
            handleSubmit={handleSubmit}
            setCupon={setCupon}
          />

          {datas.restaurants.autoshipping ? <AutoShipping /> : null}
          <TermAndCond setIsTermino={setIsTermino} isTermino={isTermino} />
          <Totals
            subtotals={subtotals / 100}
            descuentos={displayDescuento}
            extras={extras / 100}
            envio={finalShipping / 100}
            propinas={propinas / 100}
            totalFinal={totalFinal / 100}
            localeCode={datas.localeCode}
            currecy={datas.currecy}
            isShipping={descuento.isShipping}
          />
        </View>
      </KeyboardAwareScrollView>
      <BtnContinue
        totalFinal={totalFinal / 100}
        Crear_Orden={Crear_Orden}
        localeCode={datas.localeCode}
        currecy={datas.currecy}
        takeaway={datas.takeaway}
      />
      <ModalDate
        res={datas.restaurants}
        setDate={setDate}
        setSelected={(select) => setSelected(select)}
        date={date}
        selecte={selecte}
        modalizeRef={modalizeRef}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.colorInput, 'black'),
    flex: 1,
  },

  containers: {
    marginHorizontal: 15,
    marginVertical: 20,
    width: dimensions.Width(94),
    borderRadius: 15,
    padding: 20,
    height: 'auto',
    backgroundColor: 'rgba(197, 248, 116, 0.3)',
    flexDirection: 'row',
  },

  user: {
    flexDirection: 'row',
  },
});

export default OrderScreen;

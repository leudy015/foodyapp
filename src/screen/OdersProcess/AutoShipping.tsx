import React from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../../Components/CustomTetx';
import { colors, dimensions, stylesText } from '../../theme';
import Icon from 'react-native-dynamic-vector-icons';

export default function AutoShipping() {
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.moreInfo}>
      <View style={{ flexDirection: 'row' }}>
        <Icon
          name="location-off"
          type="MaterialIcons"
          size={18}
          color={colors.rgb_153}
        />
        <View>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryTextBold,
              {
                textAlign: 'left',
                marginLeft: 15,
                width: dimensions.Width(70),
              },
            ]}>
            Seguimiento en vivo no disponible
          </CustomText>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.placeholderText,
              {
                textAlign: 'left',
                marginLeft: 15,
                width: dimensions.Width(70),
              },
            ]}>
            Este establecimiento entrega sus pedidos directamente.
          </CustomText>
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginTop: 10 }}>
        <Icon
          name="infocirlceo"
          type="AntDesign"
          size={18}
          color={colors.rgb_153}
        />
        <View>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryTextBold,
              {
                textAlign: 'left',
                marginLeft: 15,
                width: dimensions.Width(70),
              },
            ]}>
            Datos de contacto compartido con el establecimiento
          </CustomText>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.placeholderText,
              {
                textAlign: 'left',
                marginLeft: 15,
                width: dimensions.Width(70),
              },
            ]}>
            Compartiremos tus datos con el establecimiento para realizar la
            entrega.
          </CustomText>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  moreInfo: {
    paddingHorizontal: dimensions.Width(2),
    marginHorizontal: dimensions.Width(4),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingVertical: 15,
    marginTop: 15,
    borderRadius: 10,
  },
});

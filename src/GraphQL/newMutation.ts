import gql from 'graphql-tag';

const ADD_TO_CART = gql`
  mutation addToCart($input: NewCartInput) {
    addToCart(input: $input) {
      success
      messages
    }
  }
`;

const DELETE_TO_CART = gql`
  mutation deleteCartItem($id: ID) {
    deleteCartItem(id: $id) {
      success
      messages
    }
  }
`;

const CREATE_NEW_ORDER = gql`
  mutation crearModificarNewOrden($input: NewOrderInput) {
    crearModificarNewOrden(input: $input) {
      success
      message
      data {
        _id
        channelOrderDisplayId
        orderType
        pickupTime
        estimatedPickupTime
        deliveryTime
        courier
        customer
        store
        deliveryAddress
        orderIsAlreadyPaid
        payment
        note
        tip
        items
        scheduled
        numberOfCustomers
        deliveryCost
        serviceCharge
        discountTotal
        IntegerValue
        paymentMethod
        cupon
        Needcutlery
        status
        descuento {
          clave
          descuento
          tipo
          isShipping
        }
      }
    }
  }
`;

const NEW_ORDER_PROCEED = gql`
  mutation NewOrdenProceed(
    $ordenId: String
    $status: String
    $IntegerValue: Int
    $statusProcess: OrderProccessInput
  ) {
    NewOrdenProceed(
      ordenId: $ordenId
      status: $status
      IntegerValue: $IntegerValue
      statusProcess: $statusProcess
    ) {
      messages
      success
    }
  }
`;

export const newMutation = {
  ADD_TO_CART,
  DELETE_TO_CART,
  CREATE_NEW_ORDER,
  NEW_ORDER_PROCEED,
};

import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import { dimensions, colors } from '../theme';
import Navigation from '../services/Navigration';
import { CustomText } from '../Components/CustomTetx';
import { Button } from '../Components/Button';
import { stylesText } from '../theme/TextStyle';
import Confetti from 'react-native-confetti';
import LottieView from 'lottie-react-native';
import AsyncStorage from '@react-native-community/async-storage';
import InAppReview from 'react-native-in-app-review';

function Thank() {
  const [_confettiView, set_confettiView] = useState(null);
  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
    if (_confettiView) {
      _confettiView.startConfetti();
    }
  }, [_confettiView]);

  useEffect(() => {
    if (InAppReview.isAvailable()) {
      InAppReview.RequestInAppReview();
    }
  }, []);

  return (
    <View style={styles.container}>
      <Confetti ref={(node) => set_confettiView(node)} />
      <View
        style={{
          alignSelf: 'center',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: dimensions.Height(15),
        }}>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <LottieView
            source={require('../Assets/Animate/success.json')}
            autoPlay
            loop
            style={{ width: 300 }}
          />
        </View>
        <CustomText
          dark={colors.white}
          light={colors.rgb_153}
          style={[
            stylesText.secondaryTextBold,
            { textAlign: 'center', paddingHorizontal: 30, marginBottom: 20 },
          ]}>
          Pedido realizado con éxito
        </CustomText>
        <CustomText
          dark={colors.white}
          light={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            { textAlign: 'center', paddingHorizontal: 30 },
          ]}>
          ¡Gracias por tu pedido en menos de 30 minutos lo tendras en tus manos!
        </CustomText>
        <View style={styles.signupButtonContainer}>
          <Button
            dark={colors.white}
            light={colors.white}
            containerStyle={styles.buttonView}
            onPress={async () => {
              Navigation.navigate('Inicio', { data: 'Order' });
              await AsyncStorage.setItem('notification', 'activate');
            }}
            title="Ver seguimiento"
            titleStyle={styles.buttonTitle}
          />
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  signupButtonContainer: {
    alignSelf: 'center',
    width: dimensions.Width(90),
  },

  buttonView: {
    alignSelf: 'center',
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
    marginTop: dimensions.Height(3),
    backgroundColor: colors.main,
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
});

export default Thank;

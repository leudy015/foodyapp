import React from 'react';
import { TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, stylesText } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function TipoCategory(props) {
  const styles = useDynamicValue(dynamicStyles);

  const { setTipo, item, tipo, setpage, setdatos } = props;

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };
  const BorderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];
  const borderColor = BorderColor[mode];

  return (
    <TouchableOpacity
      style={[
        styles.cat,
        {
          backgroundColor: tipo === item._id ? colors.main : backgroundColor,
          borderColor: borderColor,
        },
      ]}
      onPress={() => {
        if (tipo) {
          setTipo('');
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          if (tipo != item._id) {
            setpage(1);
            setdatos([]);
            setTipo(item._id);
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          }
        } else {
          setpage(1);
          setdatos([]);
          setTipo(item._id);
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
        }
      }}>
      <CustomText
        light={tipo === item._id ? colors.white : colors.back_suave_dark}
        dark={tipo === item._id ? colors.white : colors.rgb_235}
        style={[stylesText.secondaryText, { fontWeight: '500' }]}>
        {item.title}
      </CustomText>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cat: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 30,
    borderRadius: 100,
  },
});

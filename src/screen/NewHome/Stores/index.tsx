import React, { useState } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import { CustomText } from '../../../Components/CustomTetx';
import { colors, stylesText, dimensions, image } from '../../../theme';
import { DynamicStyleSheet, useDynamicValue } from 'react-native-dynamic';
import { useTranslation } from 'react-i18next';
import StoreCard from '../../../Components/RestaurantCard/RestaurantCardMedium';
import StoreCardSmall from './Card';
import { Query, useQuery } from 'react-apollo';
import { query } from '../../../GraphQL';
import Nodata from '../../../Components/NoData';
import Highkitchen from '../../Feed/highkitchen';
import Navigation from '../../../services/Navigration';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import LoadingAnimated from '../../../Components/LoadingAnimated';
import AddAdres from './AddAdress';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Stores(props: any) {
  const [page, setpage] = useState(1);
  const [pageTienda, setPageTienda] = useState(1);
  const {
    city,
    id,
    lat,
    lgn,
    localeCode,
    currecy,
    isLocation,
    setNotification,
    riders,
    getRidersAvailable,
    getAdress,
    setcity,
  } = props;
  const { t } = useTranslation();
  const styles = useDynamicValue(dynamicStyles);

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const { data, refetch, loading } = useQuery(query.GET_STORE_IN_OFFERT, {
    variables: {
      city: city,
      page: page,
      limit: 3,
    },
  });

  const stores =
    data && data.getStoreInOffert ? data.getStoreInOffert.data : [];

  refetch();

  const _renderItems = ({ item }, refetch) => {
    return (
      <StoreCardSmall
        datas={item}
        refetch={refetch}
        id={id}
        lat={lat}
        lgn={lgn}
        localeCode={localeCode}
        currecy={currecy}
        setNotification={setNotification}
        riders={riders}
      />
    );
  };
  return (
    <View>
      {loading ? (
        <View style={{ marginTop: dimensions.Height(10) }}>
          <LoadingAnimated name="Bienvenido a Wilbby by EnCaminoo" />
        </View>
      ) : (
        <>
          {stores.length > 0 ? (
            <>
              <View style={styles.titles}>
                <Image
                  source={image.RestaurantIcon}
                  style={{ width: 40, height: 40, resizeMode: 'cover' }}
                />
                <CustomText
                  light={colors.black}
                  dark={colors.rgb_153}
                  style={[stylesText.titleText]}>
                  Restaurantes destacados
                </CustomText>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                {loading ? (
                  <View style={{ width: dimensions.ScreenWidth }}>
                    <LoadingAnimated />
                  </View>
                ) : (
                  <StoreCard
                    data={stores}
                    city={city}
                    refetch={refetch}
                    lat={lat}
                    lgn={lgn}
                    horizontal={true}
                    localeCode={localeCode}
                    currecy={currecy}
                    riders={riders}
                    getRidersAvailable={getRidersAvailable}
                  />
                )}
              </View>

              <View style={styles.titles}>
                <Image
                  source={image.StoreIcon}
                  style={{ width: 40, height: 40, resizeMode: 'cover' }}
                />
                <CustomText
                  light={colors.black}
                  dark={colors.rgb_153}
                  style={[stylesText.titleText]}>
                  Tiendas destacadas
                </CustomText>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Query
                  query={query.GET_TIENDAS_IN_OFFERT}
                  variables={{ city: city, page: pageTienda, limit: 3 }}>
                  {(response) => {
                    if (response.loading) {
                      return (
                        <View style={{ width: dimensions.ScreenWidth }}>
                          <LoadingAnimated />
                        </View>
                      );
                    }
                    if (response) {
                      const data =
                        response &&
                        response.data &&
                        response.data.getTiendaInOffert
                          ? response.data.getTiendaInOffert.data
                          : [];
                      response.refetch();
                      return (
                        <StoreCard
                          data={data}
                          city={city}
                          refetch={response.refetch}
                          lat={lat}
                          lgn={lgn}
                          horizontal={true}
                          localeCode={localeCode}
                          currecy={currecy}
                          riders={riders}
                          getRidersAvailable={getRidersAvailable}
                        />
                      );
                    }
                  }}
                </Query>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: dimensions.Height(2),
                }}>
                <TouchableOpacity
                  style={{ borderRadius: 15 }}
                  activeOpacity={100}
                  onPress={() => {
                    Navigation.navigate('ComparteyGana', 0);
                    //@ts-ignore
                    ReactNativeHapticFeedback.trigger(type, optiones);
                  }}>
                  <Image
                    source={image.FreeCredit}
                    style={{
                      width: dimensions.Width(96),
                      height: 140,
                      resizeMode: 'cover',
                      borderRadius: 15,
                    }}
                  />
                </TouchableOpacity>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: dimensions.Width(100),
                }}>
                <Query
                  query={query.GET_SALVING_IN_OFFERT}
                  variables={{ city: city }}>
                  {(response) => {
                    if (response.loading) {
                      return (
                        <View style={{ width: dimensions.ScreenWidth }}>
                          <LoadingAnimated />
                        </View>
                      );
                    }
                    if (response) {
                      const data =
                        response &&
                        response.data &&
                        response.data.getSalvingPackInOffert
                          ? response.data.getSalvingPackInOffert.data
                          : [];
                      response.refetch();
                      return (
                        <>
                          {data.length > 0 ? (
                            <View
                              style={{ flex: 1, width: dimensions.Width(100) }}>
                              <View style={styles.titles}>
                                <Image
                                  source={image.SaveIcon}
                                  style={{
                                    width: 40,
                                    height: 40,
                                    resizeMode: 'cover',
                                  }}
                                />
                                <CustomText
                                  light={colors.black}
                                  dark={colors.rgb_153}
                                  style={[stylesText.titleText]}>
                                  Salva comida al mejor precio
                                </CustomText>
                              </View>
                              <FlatList
                                data={data}
                                renderItem={(item: any) =>
                                  _renderItems(item, response.refetch)
                                }
                                keyExtractor={(item: any) => item._id}
                                showsHorizontalScrollIndicator={false}
                                horizontal={true}
                                ListEmptyComponent={<Nodata />}
                              />
                            </View>
                          ) : (
                            <View
                              style={{ flex: 1, width: dimensions.Width(100) }}>
                              <View style={styles.titles}>
                                <Image
                                  source={image.FarmacyIcon}
                                  style={{
                                    width: 40,
                                    height: 40,
                                    resizeMode: 'cover',
                                  }}
                                />
                                <CustomText
                                  light={colors.black}
                                  dark={colors.rgb_153}
                                  style={[stylesText.titleText]}>
                                  Farmacias cerca de ti
                                </CustomText>
                              </View>
                              <View
                                style={{
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}>
                                <Query
                                  query={query.GET_FARMACY_IN_OFFERT}
                                  variables={{ city: city }}>
                                  {(response) => {
                                    if (response.loading) {
                                      return (
                                        <View
                                          style={{
                                            width: dimensions.ScreenWidth,
                                          }}>
                                          <LoadingAnimated />
                                        </View>
                                      );
                                    }
                                    if (response) {
                                      const data =
                                        response &&
                                        response.data &&
                                        response.data.getFarmacyOffert
                                          ? response.data.getFarmacyOffert.data
                                          : [];
                                      response.refetch();
                                      return (
                                        <FlatList
                                          data={data}
                                          renderItem={(item: any) =>
                                            _renderItems(item, response.refetch)
                                          }
                                          keyExtractor={(item: any) => item._id}
                                          showsHorizontalScrollIndicator={false}
                                          horizontal={true}
                                          ListEmptyComponent={<Nodata />}
                                        />
                                      );
                                    }
                                  }}
                                </Query>
                              </View>
                            </View>
                          )}
                        </>
                      );
                    }
                  }}
                </Query>
              </View>
              {city === 'Tomelloso' || city === 'Alcázar de San Juan' ? null : (
                <View
                  style={{
                    marginTop: dimensions.Height(3),
                    paddingBottom: dimensions.Height(5),
                  }}>
                  <Highkitchen
                    id={id}
                    city={city}
                    lat={lat}
                    lgn={lgn}
                    localeCode={localeCode}
                    currecy={currecy}
                    isLocation={isLocation}
                    setNotification={setNotification}
                    riders={riders}
                  />
                </View>
              )}
            </>
          ) : (
            <AddAdres
              getAdress={getAdress}
              city={city}
              lat={lat}
              lgn={lgn}
              setcity={setcity}
              getRidersAvailable={getRidersAvailable}
              setNotification={setNotification}
              id={id}
            />
          )}
        </>
      )}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  titles: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginHorizontal: 10,
    marginVertical: 20,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(4),
    alignSelf: 'center',
    width: dimensions.Width(70),
  },
  buttonView: {
    width: dimensions.Width(70),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

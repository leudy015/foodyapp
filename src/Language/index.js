import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import locale from 'react-native-locale-detector/index';
import AsyncStorage from '@react-native-community/async-storage';

import es from './language/es.json';
import en from './language/en.json';

const languageDetector = {
  init: Function.prototype,
  type: 'languageDetector',
  async: true,
  detect: async (callback) => {
    const savedDataJSON = await AsyncStorage.getItem('languageCode');
    const lng = savedDataJSON ? savedDataJSON : null;
    const selectLanguage = locale || lng;
    callback(selectLanguage);
  },
  cacheUserLanguage: () => {},
};

i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'es',
    resources: { en, es },
    ns: ['common'],
    defaultNS: 'common',

    debug: true,
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;

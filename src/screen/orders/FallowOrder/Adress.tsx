import React from 'react';
import { View } from 'react-native';
import { dimensions, colors, stylesText } from '../../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { CustomText } from '../../../Components/CustomTetx';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';

export default function Adress(props: any) {
  const { item } = props;
  const styles = useDynamicValue(dynamicStyles);

  const barcolor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const barColor = barcolor[mode];

  return (
    <View
      style={[
        styles.cont,
        { borderBottomColor: barColor, borderTopColor: barColor },
      ]}>
      <View>
        <Icon name="map-pin" type="Feather" color={colors.main} size={32} />
      </View>
      <View style={{ marginLeft: 10, width: dimensions.Width(80) }}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={stylesText.secondaryTextBold}>
          Dirección de entrega
        </CustomText>
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[stylesText.secondaryText, { marginTop: 5 }]}>
          {item.deliveryAddressData.formatted_address},{' '}
          {item.deliveryAddressData.puertaPiso},{' '}
          {item.deliveryAddressData.postalcode}, {item.deliveryAddressData.city}
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: dimensions.ScreenWidth,
    height: dimensions.Height(10),
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    marginTop: 20,
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
  },
});

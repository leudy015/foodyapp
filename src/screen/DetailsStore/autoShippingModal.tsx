import React, { useState } from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import { stylesText } from '../../theme/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import { Modalize } from 'react-native-modalize';
import { Button } from '../../Components/Button';
import LottieView from 'lottie-react-native';
import source from '../../Assets/Animate/chica.json';

export default function AutoShippingModal(props: any) {
  const [handle, setHandle] = useState(false);
  const { modalizeRefInfo } = props;
  const styles = useDynamicValue(dynamicStyles);

  const HEADER_HEIGHT = 600;

  const handlePosition = (position) => {
    setHandle(position === 'top');
  };

  const haldleonClose = () => {
    modalizeRefInfo.current?.close();
  };

  return (
    <Modalize
      ref={modalizeRefInfo}
      snapPoint={HEADER_HEIGHT}
      scrollViewProps={{ showsVerticalScrollIndicator: false }}
      withHandle={handle}
      modalTopOffset={100}
      modalStyle={styles.modalContens}
      handlePosition="inside"
      handleStyle={{
        top: 25,
        width: 40,
        height: handle ? 6 : 6,
        backgroundColor: colors.main,
      }}
      onPositionChange={handlePosition}>
      <View style={{ marginTop: dimensions.Height(5) }}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.h1, { textAlign: 'center' }]}>
          SIN SEGUIMIENTO
        </CustomText>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: dimensions.Height(3),
            marginTop: dimensions.Height(3),
          }}>
          <LottieView source={source} autoPlay loop style={{ width: 200 }} />
        </View>

        <View
          style={{
            paddingHorizontal: 30,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: dimensions.Height(5),
          }}>
          <Icon
            name="shopping-outline"
            type="MaterialCommunityIcons"
            size={35}
            color={colors.rgb_153}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              {
                textAlign: 'left',
                marginLeft: 15,
                width: dimensions.Width(70),
              },
            ]}>
            Este establecimiento entrega sus pedidos, por lo que no podrás
            realizar el seguimiento.
          </CustomText>
        </View>

        <View
          style={{
            paddingHorizontal: 30,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: dimensions.Height(5),
          }}>
          <Icon
            name="location-off"
            type="MaterialIcons"
            size={35}
            color={colors.rgb_153}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              {
                textAlign: 'left',
                marginLeft: 15,
                width: dimensions.Width(70),
              },
            ]}>
            El seguimiento en vivo no está disponible para tu pedido.
          </CustomText>
        </View>

        <View
          style={{
            paddingHorizontal: 30,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: dimensions.Height(5),
          }}>
          <Icon
            name="customerservice"
            type="AntDesign"
            size={35}
            color={colors.rgb_153}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              {
                textAlign: 'left',
                marginLeft: 15,
                width: dimensions.Width(70),
              },
            ]}>
            Para cualquier cuestión relacionada con este pedido, contacta con la
            asistencia de Wilbby.
          </CustomText>
        </View>

        <View style={styles.signupButtonContainer}>
          <Button
            light={colors.white}
            dark={colors.white}
            containerStyle={[
              styles.buttonView,
              { backgroundColor: colors.main },
            ]}
            onPress={() => haldleonClose()}
            title="¡De acuerdo!"
            titleStyle={styles.buttonTitle}
          />
        </View>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modalContens: {
    flex: 1,
    width: dimensions.Width(100),
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.light_white, colors.back_dark),
    borderTopEndRadius: 12,
    borderTopStartRadius: 12,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

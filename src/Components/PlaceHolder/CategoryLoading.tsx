import React from 'react';
import { View } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { colors } from '../../theme';

export default function PlaceHolder() {
  return (
    <View style={{ alignSelf: 'center', backgroundColor: 'transparent' }}>
      <SkeletonPlaceholder
        backgroundColor={colors.back_dark}
        highlightColor={colors.back_suave_dark}>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 20,
            marginTop: 10,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 20,
            }}>
            <View
              style={{
                width: 75,
                height: 75,
                borderRadius: 100,
                marginBottom: 10,
              }}
            />
            <View
              style={{
                width: 50,
                height: 10,
                borderRadius: 10,
                marginBottom: 10,
              }}
            />
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 20,
            }}>
            <View
              style={{
                width: 75,
                height: 75,
                borderRadius: 100,
                marginBottom: 10,
              }}
            />
            <View
              style={{
                width: 50,
                height: 10,
                borderRadius: 10,
                marginBottom: 10,
              }}
            />
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 20,
            }}>
            <View
              style={{
                width: 75,
                height: 75,
                borderRadius: 100,
                marginBottom: 10,
              }}
            />
            <View
              style={{
                width: 50,
                height: 10,
                borderRadius: 10,
                marginBottom: 10,
              }}
            />
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 5,
            }}>
            <View
              style={{
                width: 75,
                height: 75,
                borderRadius: 100,
                marginBottom: 10,
              }}
            />
            <View
              style={{
                width: 50,
                height: 10,
                borderRadius: 10,
                marginBottom: 10,
              }}
            />
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

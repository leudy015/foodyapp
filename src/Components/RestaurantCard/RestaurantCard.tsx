import React, { useState, useEffect } from 'react';
import { FlatList } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NoData from '../NoData';
import Card from './Card';

const RestaurantCard = (props: any) => {
  const {
    data,
    horizontal,
    city,
    lat,
    localeCode,
    currecy,
    refetch,
    riders,
    lgn,
  } = props;
  const [id, setID] = useState(null);

  const getId = async () => {
    const id = await AsyncStorage.getItem('id');
    setID(id);
  };

  useEffect(() => {
    getId();
  }, [id]);

  const _renderItem = ({ item }) => {
    return (
      <Card
        item={item}
        id={id}
        city={city}
        lat={lat}
        lgn={lgn}
        localeCode={localeCode}
        currecy={currecy}
        refetch={refetch}
        riders={riders}
      />
    );
  };

  return (
    <FlatList
      data={data}
      style={{ flex: 1 }}
      contentContainerStyle={{ flex: 1 }}
      renderItem={(item: any) => _renderItem(item)}
      keyExtractor={(item: any) => item._id}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      horizontal={horizontal ? true : false}
      ListEmptyComponent={<NoData />}
    />
  );
};

export default RestaurantCard;

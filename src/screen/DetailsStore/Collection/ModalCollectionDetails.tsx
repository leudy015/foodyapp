import React from 'react';
import { View, ScrollView, Alert, Modal } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { colors } from '../../../theme';
import CollectionDetails from './CollectionDetails';

export default function CollectionDetailModal(props) {
  const {
    data,
    store,
    localeCode,
    currecy,
    inCartItem,
    setinCartItem,
    ModalVisible,
    setModalVisible,
  } = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <Modal
      animationType="slide"
      visible={ModalVisible}
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.centeredView}>
        <CollectionDetails
          collection={data}
          store={store}
          localeCode={localeCode}
          currecy={currecy}
          inCartItem={inCartItem}
          setinCartItem={setinCartItem}
          setModalVisible={setModalVisible}
        />
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  centeredView: {
    backgroundColor: new DynamicValue(colors.white, 'black'),
    flex: 1,
  },
});

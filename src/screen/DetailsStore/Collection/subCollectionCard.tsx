import React from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  Image,
  ImageBackground,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { dimensions, colors, stylesText, image } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';

export default function cardCollection(props: any) {
  const { data, onPress } = props;
  const styles = useDynamicValue(dynamicStyles);

  const renderItems = ({ item }) => {
    return (
      <View style={{ marginHorizontal: 15, marginVertical: 10 }}>
        <ImageBackground
          resizeMode="cover"
          source={image.PlaceHolder}
          imageStyle={{ borderRadius: 10 }}
          style={{
            width: dimensions.Width(20),
            height: dimensions.Height(9),
          }}>
          <Image
            source={{ uri: item.imageUrl }}
            resizeMode="cover"
            style={{
              width: dimensions.Width(20),
              height: dimensions.Height(9),
              borderRadius: 10,
            }}
          />
        </ImageBackground>
        <CustomText
          numberOfLines={1}
          style={[
            stylesText.secondaryText,
            { width: dimensions.Width(20), marginTop: 5 },
          ]}
          light={colors.black}
          dark={colors.white}>
          {item.name}
        </CustomText>
      </View>
    );
  };

  return (
    <TouchableOpacity style={styles.collection} onPress={onPress}>
      <View style={styles.prod}>
        <FlatList
          data={data.Product}
          renderItem={(item: any) => renderItems(item)}
          keyExtractor={(item: any) => item._id}
          horizontal={true}
          scrollEnabled={false}
        />
      </View>
      <CustomText
        style={[stylesText.secondaryTextBold, { marginTop: 15 }]}
        light={colors.black}
        dark={colors.white}>
        {data.title}
      </CustomText>
      <CustomText
        style={[stylesText.secondaryTextBold, { marginTop: 15 }]}
        light={colors.main}
        dark={colors.main}>
        Ir a la sección
      </CustomText>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  collection: {
    width: dimensions.Width(90),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
    padding: 15,
    borderRadius: 15,
    marginTop: 15,
  },

  prod: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
});

import React, { useState } from 'react';
import {
  View,
  Animated,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  RefreshControl,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { useNavigationParam } from 'react-navigation-hooks';
import { dimensions, colors, stylesText } from '../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../../services/Navigration';
import { CustomText } from '../../Components/CustomTetx';
import { useTranslation } from 'react-i18next';
import Category from './Category';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Restaurant from './Card';
import Scooter from '../../Components/scooter';
import NoLocation from '../../Components/NoLocation';
const img = require('./imges/imagen1.png');

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Highkitchen() {
  const datos = useNavigationParam('data');
  const { id, city, lat, lgn, localeCode, currecy, isLocation } = datos;
  const styles = useDynamicValue(dynamicStyles);
  const { t } = useTranslation();
  const [scrollY] = useState(new Animated.Value(0));
  const [category, setcategory] = useState('');
  const [llevar, setllevar] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const nav = {
    city: city,
    lat: lat,
    lgn: lgn,
    localeCode: localeCode,
    currecy: currecy,
    isLocation: isLocation,
  };

  const imageContainerHeight = scrollY.interpolate({
    inputRange: [-200, 0],
    outputRange: [450, 250],
    extrapolate: 'extend',
  });

  const headerContainerWidth = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [dimensions.Width(100), dimensions.Width(100)],
    extrapolate: 'clamp',
  });

  const headerContainerHeight = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [60, 100],
    extrapolate: 'clamp',
  });

  const opacity = scrollY.interpolate({
    inputRange: [0, 50, 100],
    outputRange: [100, 50, 0],
    extrapolate: 'clamp',
  });

  const font = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [22, 14],
    extrapolate: 'clamp',
  });

  const top = scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [0, 30],
    extrapolate: 'clamp',
  });

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.containerIcon}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={() => Navigation.goBack()}
            style={styles.icon}>
            <Icon
              name="arrow-left"
              type="Feather"
              size={24}
              color={colors.back_dark}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => Navigation.navigate('Search', { data: nav })}
            style={styles.icon}>
            <Icon
              type="AntDesign"
              name="search1"
              size={24}
              color={colors.back_dark}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <Animated.View
        style={[styles.imageContainer, { height: imageContainerHeight }]}>
        <Animated.Image
          style={[
            styles.image,
            { height: imageContainerHeight, opacity: opacity },
          ]}
          source={img}
        />
      </Animated.View>
      <ScrollView
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: scrollY } } },
        ])}
        scrollEventThrottle={16}
        stickyHeaderIndices={[1]}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        style={styles.scrollViewContainer}>
        <View
          style={{
            marginTop: 300,
            marginHorizontal: 20,
            marginBottom: dimensions.Height(20),
          }}>
          <Category
            onPress={(item: any) => {
              if (category) {
                ReactNativeHapticFeedback.trigger('selection', optiones);
                setcategory('');
                if (category != item._id) {
                  ReactNativeHapticFeedback.trigger('selection', optiones);
                  setcategory(item._id);
                }
              } else {
                ReactNativeHapticFeedback.trigger('selection', optiones);
                setcategory(item._id);
              }
            }}
            active={category}
          />

          <View
            style={[
              styles.recomended,
              {
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              },
            ]}>
            <View>
              <CustomText
                light={colors.white}
                dark={colors.white}
                style={[
                  stylesText.titleText,
                  { letterSpacing: 3, width: dimensions.Width(50) },
                ]}>
                {t('vegankitchen:recomended')}
              </CustomText>
            </View>
            <View style={styles.filters}>
              <TouchableOpacity
                onPress={() => {
                  setllevar(false);
                  ReactNativeHapticFeedback.trigger(
                    'notificationSuccess',
                    optiones,
                  );
                }}
                style={llevar ? styles.itemsInac : styles.items}>
                <Scooter white={true} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setllevar(true);
                  ReactNativeHapticFeedback.trigger(
                    'notificationSuccess',
                    optiones,
                  );
                }}
                style={llevar ? styles.items : styles.itemsInac}>
                <Icon
                  name="run"
                  type="MaterialCommunityIcons"
                  size={20}
                  style={styles.icons}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={[
              styles.recomended,
              { borderTopWidth: 1, borderTopColor: colors.back_dark },
            ]}>
            {isLocation ? (
              <Restaurant
                id={id}
                city={city}
                lat={lat}
                lgn={lgn}
                localeCode={localeCode}
                currecy={currecy}
                category={category}
                llevar={llevar}
              />
            ) : (
              <NoLocation />
            )}
          </View>
        </View>
        <View style={styles.stickyHeaderContainer}>
          <Animated.View
            style={[
              styles.headerContainer,
              {
                width: headerContainerWidth,
                height: headerContainerHeight,
              },
            ]}>
            <Animated.Text
              style={[
                styles.text,
                { fontSize: font, marginTop: top, fontWeight: '700' },
              ]}>
              {t('vegankitchen:vegankitchen')}
            </Animated.Text>
          </Animated.View>
        </View>
      </ScrollView>
    </View>
  );
}

const color1 = 'rgba(255, 255, 255, 0.5)';

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  containerIcon: {
    width: dimensions.Width(95),
    position: 'absolute',
    marginTop: Platform.select({
      ios: dimensions.IsIphoneX() ? dimensions.Height(0) : dimensions.Height(4),
      android: dimensions.Height(5),
    }),
    marginHorizontal: 10,
    zIndex: 100,
  },

  icon: {
    width: 40,
    height: 40,
    backgroundColor: color1,
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 100,
  },

  image: {
    flex: 1,
    width: dimensions.ScreenWidth,
  },

  scrollViewContainer: {
    flex: 1,
  },

  imageContainer: {
    position: 'absolute',
    top: 0,
    width: dimensions.ScreenWidth,
    height: 200,
  },

  stickyHeaderContainer: {
    position: 'absolute',
    top: 220,
    left: 0,
    right: 0,
  },

  headerContainer: {
    width: dimensions.Width(100),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  text: {
    letterSpacing: 7,
    color: new DynamicValue(colors.main, colors.main),
  },

  recomended: {
    marginTop: dimensions.Height(5),
  },

  icons: {
    color: new DynamicValue(colors.white, colors.white),
  },

  filters: {
    width: 60,
    height: 30,
    marginLeft: 'auto',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: new DynamicValue(
      colors.back_suave_dark,
      colors.back_suave_dark,
    ),
    borderRadius: 100,
  },

  items: {
    width: 30,
    height: 30,
    backgroundColor: colors.main,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  itemsInac: {
    width: 30,
    height: 30,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});

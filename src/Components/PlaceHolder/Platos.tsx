import React from 'react';
import { SafeAreaView, View } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useDynamicValue } from 'react-native-dynamic';
import { colors, dimensions } from '../../theme';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  const back = useDynamicValue(colors.colorInput, colors.black);
  return (
    <View style={{ alignSelf: 'center', backgroundColor: 'transparent' }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(90),
            height: 40,
            flexDirection: 'row',
            marginBottom: dimensions.Height(5),
            borderRadius: 100,
          }}
        />
        <View
          style={{
            width: dimensions.Width(90),
            height: 40,
            flexDirection: 'row',
            marginBottom: dimensions.Height(5),
            borderRadius: 100,
          }}
        />
        <View
          style={{
            width: dimensions.Width(90),
            height: 40,
            flexDirection: 'row',
            marginBottom: dimensions.Height(5),
            borderRadius: 100,
          }}
        />
      </SkeletonPlaceholder>
    </View>
  );
}

import React from 'react';
import { View } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import { dimensions, colors } from '../theme';
import Navigation from '../services/Navigration';
import { CustomText } from '../Components/CustomTetx';
import { Button } from '../Components/Button';
import { stylesText } from '../theme/TextStyle';
import LottieView from 'lottie-react-native';

function Errors() {
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <View
        style={{
          alignSelf: 'center',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: dimensions.Height(15),
        }}>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <LottieView
            source={require('../Assets/Animate/error.json')}
            autoPlay
            loop
            style={{ width: 300 }}
          />
        </View>
        <CustomText
          dark={colors.white}
          light={colors.rgb_153}
          style={[
            stylesText.secondaryTextBold,
            { textAlign: 'center', paddingHorizontal: 30, marginBottom: 20 },
          ]}>
          Método de pago rechazado
        </CustomText>
        <CustomText
          dark={colors.white}
          light={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            { textAlign: 'center', paddingHorizontal: 30 },
          ]}>
          ¡Hubo un problema con tu método de pago, vuelve a intentarlo por
          favor!
        </CustomText>
        <View style={styles.signupButtonContainer}>
          <Button
            dark={colors.white}
            light={colors.white}
            containerStyle={styles.buttonView}
            onPress={() => Navigation.goBack()}
            title="Volver a intentarlo"
            titleStyle={styles.buttonTitle}
          />
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  signupButtonContainer: {
    alignSelf: 'center',
    width: dimensions.Width(90),
  },

  buttonView: {
    alignSelf: 'center',
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
    marginTop: dimensions.Height(3),
    backgroundColor: colors.main,
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
});

export default Errors;

import React, { useState } from 'react';
import { TouchableOpacity, FlatList } from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, stylesText } from '../../../theme';
import { CustomText } from '../../../Components/CustomTetx';
import { query } from '../../../GraphQL';
import { Query } from 'react-apollo';

export default function TabsCollection(props: any) {
  const [active, setactive] = useState('');
  const { res } = props;
  const styles = useDynamicValue(dynamicStyles);

  const border = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const bordercolor = border[mode];

  const _renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => setactive(item._id)}
        style={[
          styles.tabs,
          { backgroundColor: active === item._id ? colors.main : bordercolor },
        ]}>
        <CustomText
          style={[stylesText.secondaryText, { paddingBottom: 2 }]}
          light={active === item._id ? colors.white : colors.rgb_153}
          dark={colors.white}>
          {item.title}
        </CustomText>
      </TouchableOpacity>
    );
  };
  return (
    <Query query={query.COLLECTIONS} variables={{ store: res._id }}>
      {(response) => {
        if (response) {
          response.refetch();
          const data =
            response && response.data && response.data.getCollection
              ? response.data.getCollection.data
              : [];
          return (
            <FlatList
              data={data}
              renderItem={(item: any) => _renderItem(item)}
              keyExtractor={(item: any) => item._id}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
            />
          );
        }
      }}
    </Query>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  tabs: {
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 30,
    borderRadius: 100,
  },

  vertodo: {
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 10,
  },
});

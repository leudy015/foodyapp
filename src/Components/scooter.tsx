import React from 'react';
import { Image } from 'react-native';
import { image } from '../theme';
import { DynamicValue, useDynamicValue } from 'react-native-dynamic';

export default function Scooter(props) {
  const { white, width } = props;
  const Burguer = new DynamicValue(image.Scooter, image.ScooterWhite);

  const source = useDynamicValue(Burguer);

  return (
    <Image
      source={white ? image.ScooterWhite : source}
      style={{
        width: width ? width : 25,
        height: width ? width : 25,
        resizeMode: 'contain',
      }}
    />
  );
}

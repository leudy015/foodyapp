import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import { CustomText } from '../Components/CustomTetx';
import { dimensions, colors } from '../theme';
import { stylesText } from '../theme/TextStyle';
import Navigation from '../services/Navigration';
import LottieView from 'lottie-react-native';
import source from '../Assets/Animate/salten.json';

const ErrorPage = () => {
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.container}>
      <View
        style={{
          marginTop: dimensions.Height(15),
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <LottieView
          source={source}
          autoPlay
          loop
          style={{ width: 250, height: 250 }}
        />
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={{
            textAlign: 'center',
            fontSize: dimensions.FontSize(20),
            fontWeight: '200',
            marginTop: 20,
            paddingHorizontal: 30,
          }}>
          Algo no va bien intentalo de nuevo
        </CustomText>
        <TouchableOpacity
          onPress={() => Navigation.navigate('Inicio', 0)}
          style={styles.selecteds}>
          <CustomText
            light={colors.white}
            dark={colors.white}
            numberOfLines={1}
            style={stylesText.mainText}>
            Volver al inicio
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  selecteds: {
    width: dimensions.Width(90),
    height: 60,
    borderRadius: 15,
    flexDirection: 'row',
    backgroundColor: colors.main,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
});

export default ErrorPage;

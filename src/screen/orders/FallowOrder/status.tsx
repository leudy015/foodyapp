import React from 'react';
import { View } from 'react-native';
import { dimensions, colors, stylesText } from '../../../theme';
import Icon from 'react-native-dynamic-vector-icons';
import { CustomText } from '../../../Components/CustomTetx';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';

export default function Status(props: any) {
  const { item } = props;
  const styles = useDynamicValue(dynamicStyles);

  const barcolor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const barColor = barcolor[mode];

  return (
    <View
      style={[
        styles.cont,
        { borderBottomColor: barColor, borderTopColor: barColor },
      ]}>
      <View>
        <Icon
          name="progress-check"
          type="MaterialCommunityIcons"
          color={colors.main}
          size={32}
        />
      </View>
      <View style={{ marginLeft: 10 }}>
        <View style={{ flexDirection: 'row' }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              { marginTop: 5, fontSize: dimensions.FontSize(19) },
            ]}>
            Número de pedido
          </CustomText>
          <CustomText
            light={colors.main}
            dark={colors.main}
            style={[
              stylesText.secondaryTextBold,
              { marginLeft: 7, marginTop: 7 },
            ]}>
            #{item.channelOrderDisplayId}
          </CustomText>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              { marginTop: 5, fontSize: dimensions.FontSize(19) },
            ]}>
            Tu pedido está
          </CustomText>
          <CustomText
            numberOfLines={1}
            light={colors.main}
            dark={colors.main}
            style={[
              stylesText.secondaryTextBold,
              { marginLeft: 7, marginTop: 7, width: dimensions.Width(50) },
            ]}>
            {item.status}
          </CustomText>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: dimensions.ScreenWidth,
    height: dimensions.Height(10),
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
  },
});

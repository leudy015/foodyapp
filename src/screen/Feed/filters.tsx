import React from 'react';
import { View, FlatList } from 'react-native';
import { Query } from 'react-apollo';
import { query } from '../../GraphQL';
import Loadings from '../../Components/PlaceHolder/CategoryList';
import ItemCategory from './ItemCategory';
import TipoCategory from './tipoCategory';

export default function Filters(props) {
  const {
    setTipo,
    setTipoTitle,
    category,
    catTitle,
    setcategory,
    setcatTitle,
    tipo,
    tipotienda,
    setTipotienda,
    setpage,
    setdatos,
  } = props;
  const renderItem = ({ item }) => {
    switch (item.title) {
      case 'Algo Especial':
        return null;
      case 'Envío Express':
        return null;
      default:
        return (
          <ItemCategory
            setTipo={setTipo}
            setTipoTitle={setTipoTitle}
            item={item}
            category={category}
            catTitle={catTitle}
            setcategory={setcategory}
            setcatTitle={setcatTitle}
            setpage={setpage}
            setdatos={setdatos}
          />
        );
    }
  };

  const renderItemTipo = ({ item }) => {
    return (
      <TipoCategory
        setTipo={setTipo}
        item={item}
        tipo={tipo}
        setpage={setpage}
        setdatos={setdatos}
      />
    );
  };

  const renderItemTipotienda = ({ item }) => {
    return (
      <TipoCategory
        setTipo={setTipo}
        item={item}
        tipo={tipo}
        setpage={setpage}
        setdatos={setdatos}
      />
    );
  };

  const rederCategorias = () => {
    switch (catTitle) {
      case 'Comida':
        return (
          <Query query={query.TIPO}>
            {(response) => {
              if (response.loading) {
                return (
                  <View>
                    <Loadings />
                  </View>
                );
              }
              if (response) {
                response.refetch();
                const data =
                  response && response.data && response.data.getTipo
                    ? response.data.getTipo.data
                    : [];
                return (
                  <View style={{ marginBottom: 15 }}>
                    <FlatList
                      data={data}
                      renderItem={(item: any) => renderItemTipo(item)}
                      keyExtractor={(item: any) => item._id}
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                    />
                  </View>
                );
              }
            }}
          </Query>
        );
      case 'Tiendas':
        return (
          <Query query={query.TIPO_TIENDAS}>
            {(response) => {
              if (response.loading) {
                return (
                  <View>
                    <Loadings />
                  </View>
                );
              }
              if (response) {
                response.refetch();
                const data =
                  response && response.data && response.data.getTipoTienda
                    ? response.data.getTipoTienda.data
                    : [];
                return (
                  <View style={{ marginBottom: 15 }}>
                    <FlatList
                      data={data}
                      renderItem={(item: any) => renderItemTipotienda(item)}
                      keyExtractor={(item: any) => item._id}
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                    />
                  </View>
                );
              }
            }}
          </Query>
        );
      default:
        return (
          <Query query={query.CATEGORY}>
            {(response) => {
              if (response.loading) {
                return (
                  <View>
                    <Loadings />
                  </View>
                );
              }
              if (response.error) {
                return (
                  <View>
                    <Loadings />
                  </View>
                );
              }
              if (response) {
                response.refetch();
                const data =
                  response && response.data && response.data.getCategory
                    ? response.data.getCategory.data
                    : [];
                return (
                  <View style={{ marginBottom: 15 }}>
                    <FlatList
                      data={data}
                      renderItem={(item: any) => renderItem(item)}
                      keyExtractor={(item: any) => item._id}
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                    />
                  </View>
                );
              }
            }}
          </Query>
        );
    }
  };

  return rederCategorias();
}

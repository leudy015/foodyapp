export const NETWORK_INTERFACE_URL = 'https://api.v1.wilbby.com';
export const NETWORK_INTERFACE = `${NETWORK_INTERFACE_URL}/graphql`;
export const NETWORK_INTERFACE_LINK = `${NETWORK_INTERFACE_URL}/api/user`;
export const NETWORK_INTERFACE_LINK_AVATAR = `${NETWORK_INTERFACE_URL}/assets/images/`;
export const GOOGLE_DISTANCE_API =
  'https://maps.googleapis.com/maps/api/distancematrix/json';
export const GOOGLE_COODENATE_URL =
  'https://maps.googleapis.com/maps/api/geocode/json';
export const GOOGLE_API_KEY = 'AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA';
export const STRIPE_CLIENT =
  'pk_live_51IK8ZbCf1EdHpU7RC2PVdZ3ZftRihOfazxCC8da5mJJifgDsc6xCbUPwn6cYTdFE011ZeTbIJN4Fp1KhdjyqTmfN00fBl3wKMW';
export const STRIPE_CLIENT_TETS =
  'pk_test_51IK8ZbCf1EdHpU7RnYlZAF14fQyca9bxHqknDhvNotzvNkFUU54BikbcfYYS9MAilcpo1SWmUTG8crWmajZugZKg00PwmTtAK8';

import React, { useState } from 'react';
import {
  View,
  Modal,
  TouchableOpacity,
  TextInput,
  Platform,
  ScrollView,
  Alert,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import { colors, stylesText, dimensions, image } from '../../theme';
import { CustomText } from '../../Components/CustomTetx';
import Icon from 'react-native-dynamic-vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button } from '../../Components/Button';
import { useMutation } from 'react-apollo';
import { mutations } from '../../GraphQL';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-toast-message';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const tipos = [
  {
    id: 1,
    cantidad: 'Casa',
    type: 'AntDesign',
    name: 'home',
  },
  {
    id: 2,
    cantidad: 'Trabajo',
    type: 'MaterialCommunityIcons',
    name: 'briefcase-outline',
  },
  {
    id: 3,
    cantidad: 'Mi churri',
    type: 'AntDesign',
    name: 'hearto',
  },

  {
    id: 3,
    cantidad: 'Otro',
    type: 'AntDesign',
    name: 'pushpino',
  },
];

export default function AdressManual(props: any) {
  const { visible, setModalVisible, city, lat, lgn, user } = props;
  const styles = useDynamicValue(dynamicStyles);

  const [calle, setcalle] = useState('');
  const [numero, setnumero] = useState('');
  const [puerta, setpuerta] = useState('');
  const [ciudad, setciudad] = useState(city);
  const [cp, setcp] = useState('');

  const [Loading, setLoading] = useState(false);

  const [createAdress] = useMutation(mutations.CREATE_ADRESS);

  const [tipo, settipo] = useState('Casa');

  const BorderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const borderColor = BorderColor[mode];

  const showAler = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: 'Vale',
          onPress: () => {},
        },
      ],

      { cancelable: false },
    );
  };

  const saveAdrees = () => {
    setLoading(true);

    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);

    const input = {
      formatted_address: `${calle} ${numero}`,
      puertaPiso: puerta,
      city: ciudad,
      postalcode: cp,
      type: tipo,
      usuario: user,
      lat: String(lat),
      lgn: String(lgn),
    };

    if (!calle) {
      showAler(
        'Debes añadir una calle',
        'Para continuar debes añadir un nombre de la calle',
      );
      setLoading(false);
    } else if (!numero) {
      showAler(
        'Debes añadir una número',
        'Para continuar debes añadir un número',
      );
      setLoading(false);
    } else if (!puerta) {
      showAler(
        'Debes indicar la puerta o piso',
        'Para continuar debes indicar la puerta o piso',
      );
      setLoading(false);
    } else if (!ciudad) {
      showAler(
        'Debes añadir una ciudad',
        'Para continuar debes añadir una ciudad',
      );
      setLoading(false);
    } else if (!cp) {
      showAler(
        'Debes añadir una código postal',
        'Para continuar debes añadir un código postal',
      );
      setLoading(false);
    } else {
      createAdress({ variables: { input: input } })
        .then((res) => {
          if (res.data.createAdress.success) {
            setLoading(false);
            const dat = res.data.createAdress.data;
            AsyncStorage.setItem('adressName', dat.formatted_address);
            AsyncStorage.setItem('adressId', dat.id);
            Toast.show({
              text1: 'Dirección añadida',
              text2: 'Dirección añadida con éxito',
              position: 'top',
              type: 'success',
              topOffset: 50,
              visibilityTime: 4000,
            });
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            setModalVisible(false);
          }
        })
        .catch(() => {
          setLoading(false);
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          Toast.show({
            text1: 'Algo salio mal',
            text2: 'Algo ha salido mal intentalo de nuevo',
            position: 'top',
            type: 'error',
            topOffset: 50,
            visibilityTime: 4000,
          });
        });
    }
  };

  return (
    <Modal
      animationType="slide"
      transparent={false}
      presentationStyle="formSheet"
      visible={visible}
      statusBarTranslucent={true}>
      <View style={styles.container}>
        <View style={styles.HeaderModal}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryText]}>
            Añadir dirección de entrega
          </CustomText>
          <TouchableOpacity
            onPress={() => {
              setModalVisible(false);
            }}
            style={{ marginLeft: 'auto' }}>
            <Icon
              name="closecircleo"
              type="AntDesign"
              size={30}
              color={colors.rgb_153}
            />
          </TouchableOpacity>
        </View>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}>
          <View style={styles.formView}>
            <TextInput
              style={styles.textInput}
              selectionColor={colors.main}
              placeholderTextColor={colors.rgb_153}
              placeholder="Nombre de la calle"
              clearButtonMode="always"
              autoCompleteType="name"
              autoCorrect={false}
              onChangeText={(clave: any) => setcalle(clave)}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <TextInput
                style={[styles.textInput, { width: dimensions.Width(45) }]}
                selectionColor={colors.main}
                placeholderTextColor={colors.rgb_153}
                placeholder="Número"
                clearButtonMode="always"
                autoCompleteType="name"
                autoCorrect={false}
                onChangeText={(clave: any) => setnumero(clave)}
              />
              <TextInput
                style={[styles.textInput, { width: dimensions.Width(45) }]}
                selectionColor={colors.main}
                placeholderTextColor={colors.rgb_153}
                placeholder="Puerta / Piso"
                autoCompleteType="name"
                clearButtonMode="always"
                autoCorrect={false}
                onChangeText={(clave: any) => setpuerta(clave)}
              />
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <TextInput
                style={[styles.textInput, { width: dimensions.Width(55) }]}
                selectionColor={colors.main}
                placeholderTextColor={colors.rgb_153}
                defaultValue={ciudad}
                placeholder="Ciudad"
                autoCompleteType="name"
                autoCorrect={false}
                clearButtonMode="always"
                onChangeText={(clave: any) => setciudad(clave)}
              />
              <TextInput
                style={[styles.textInput, { width: dimensions.Width(35) }]}
                selectionColor={colors.main}
                placeholderTextColor={colors.rgb_153}
                placeholder="Código postal"
                keyboardType="number-pad"
                clearButtonMode="always"
                autoCorrect={false}
                onChangeText={(clave: any) => setcp(clave)}
              />
            </View>
            <View style={{ marginTop: 40, marginLeft: -10 }}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {tipos.map((t, i) => {
                  return (
                    <TouchableOpacity
                      onPress={() => settipo(t.cantidad)}
                      style={[
                        styles.tipos,
                        {
                          borderColor: borderColor,
                          backgroundColor:
                            tipo === t.cantidad ? colors.main : 'transparent',
                        },
                      ]}
                      key={i}>
                      <Icon
                        name={t.name}
                        //@ts-ignore
                        type={t.type}
                        size={20}
                        color={
                          tipo === t.cantidad ? colors.white : colors.rgb_153
                        }
                        style={{ marginRight: 5 }}
                      />
                      <CustomText
                        light={
                          tipo === t.cantidad ? colors.white : colors.rgb_153
                        }
                        dark={
                          tipo === t.cantidad ? colors.white : colors.rgb_153
                        }
                        style={[stylesText.secondaryText]}>
                        {t.cantidad}
                      </CustomText>
                    </TouchableOpacity>
                  );
                })}
              </ScrollView>
            </View>

            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                loading={Loading}
                containerStyle={styles.buttonView}
                onPress={() => saveAdrees()}
                title="Guadar dirección"
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    minHeight: dimensions.Height(100),
  },
  HeaderModal: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 90,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  formView: {
    marginTop: dimensions.Height(2),
    marginHorizontal: dimensions.Width(4),
  },

  textInput: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    padding: Platform.select({
      android: 10,
      ios: dimensions.IsIphoneX() ? 20 : 10,
    }),
    marginTop: 20,
    borderRadius: 5,
    color: colors.rgb_153,
    borderColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    borderWidth: 0.3,
  },

  tipos: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderWidth: 2,
    marginHorizontal: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(6),
    alignSelf: 'center',
    width: dimensions.Width(90),
    paddingBottom: dimensions.IsIphoneX() ? 20 : 0,
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(16),
    textAlign: 'center',
  },
});
